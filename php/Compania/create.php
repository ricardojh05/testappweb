<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return $mysqli;
    $sql_create_empresa = "INSERT INTO $DB_NAME.compania "
            . "(idPais, compania, callePrincipal, calleSecundaria, contacto, color, comentario, latitud, longitud, idAdministradorRegistro, ruc) "
            . "VALUES "
            . "(" . $data->pais . ", ('" . $mysqli->real_escape_string($data->compania) . "')"
            . ", '" . $mysqli->real_escape_string($data->callePrin) . "'" . ", '" . $mysqli->real_escape_string($data->calleSec) . "'"
            . ", '" . $data->contacto . "'" . ", '" . $data->color . "'"
            . ", '" . $mysqli->real_escape_string($data->comentario) . "'" . ", '" . $data->latitud . "'"
            . ", '" . $data->longitud . "'" . ", '" . $_SESSION["ID_ADMINISTRADOR"] . "'"
            . ", '" . $data->ruc . "'"
            . ");";
    $compania = EJECUTAR_SQL($mysqli, $sql_create_empresa);
    if (isset($compania['id'])) {
        if ($compania['id'] > 0) {
            $administradores =json_decode($administradores);
            if (count($administradores) > 0) {
                    $sql_insert_admin_compania = "INSERT INTO $DB_NAME.administrador_compania "
                            . "(idAdministrador, idCompania, hablilitado, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito) "
                            . "VALUES ";
                    foreach ($administradores as $v) {
                        $sql_insert_admin_compania .= "(" . $v->id . ", " . $compania['id'] . "". ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()),";
                    }
                    $sql_insert_admin_compania = substr($sql_insert_admin_compania, 0, -1);
                    echo json_encode(EJECUTAR_SQL($mysqli, $sql_insert_admin_compania));
                } else {
                    echo json_encode($compania);
                }
        }
    }
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}