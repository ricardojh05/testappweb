<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql_total = "SELECT COUNT(c.idCompania) AS total FROM $DB_NAME.compania c ORDER BY c.idCompania DESC";
$result = $mysqli->query($sql_total);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", "sql" => $sql));
    return $mysqli->close();
}
$myrow_read = $result->fetch_assoc();
$total = intval($myrow_read['total']);
if ($total > 0) {
    $sql = "SELECT idPais, idCompania, compania, ruc, callePrincipal, calleSecundaria, contacto, color, comentario, latitud, longitud, "
            . " idAdministradorRegistro, fecha_registro, idAdministradorActualizo, fecha_actualizo "
            . "FROM $DB_NAME.compania WHERE TRUE ";
    if (isset($param)) {
        $sql .= "AND ((compania) LIKE ('$param%')"
                . "OR (callePrincipal) LIKE ('$param%') "
                . "OR (calleSecundaria) LIKE ('$param%') "
                . "OR contacto LIKE '$param%') ";
    }
    if (isset($paises) && $paises !== '' && $paises != 0) {
        $sql .= " AND idPais IN ($paises) ";
    }

//if (isset($param)) {
//    $sql .= "WHERE ((compania) LIKE ('$param%')"
//            . "OR (callePrincipal) LIKE ('$param%') "
//            . "OR (calleSecundaria) LIKE ('$param%') "
//            . "OR contacto LIKE '$param%') ";
//}
//if (isset($paises)) {
//    if ($paises !== '' && $paises != 0) {
//        if (isset($param)) {
//            $sql .= " AND ";
//        } else {
//            $sql .= " WHERE ";
//        }
//        $sql .= " idPais IN ($paises) ";
//    }
//}

    $sql .= " ORDER BY idCompania DESC ";

    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }

    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read_compania = $result->fetch_assoc()) {
        $arreglo[] = array(
            'pais' => intval($myrow_read_compania["idPais"]),
            'id' => intval($myrow_read_compania["idCompania"]),
            'compania' => $myrow_read_compania["compania"],
            'ruc' => $myrow_read_compania["ruc"],
            'callePrin' => $myrow_read_compania["callePrincipal"],
            'calleSec' => $myrow_read_compania["calleSecundaria"],
            'contacto' => $myrow_read_compania["contacto"],
            'color' => $myrow_read_compania["color"],
            'comentario' => $myrow_read_compania["comentario"],
            'latitud' => $myrow_read_compania["latitud"],
            'longitud' => $myrow_read_compania["longitud"],
            'fRegistro' => $myrow_read_compania["fecha_registro"],
            'idUserCreate' => $myrow_read_compania["idAdministradorRegistro"],
            'dateCreate' => $myrow_read_compania["fecha_registro"],
            'idUserUpdate' => $myrow_read_compania["idAdministradorActualizo"],
            'dateUpdate' => $myrow_read_compania["fecha_actualizo"],
        );
    }
    echo json_encode(array('success' => true, 'compania' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => true, 'compania' => [], 'total' => 0));
}
$mysqli->close();

