<?php
extract($_GET);
extract($_POST);
include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return $mysqli;
    $sql_update_empresa = "UPDATE $DB_NAME.compania SET ";
    $sql_update_empresa .= (isset($data->pais)) ? "idPais = " . $data->pais . ", " : "";
    $sql_update_empresa .= (isset($data->compania)) ? "compania = '" . $mysqli->real_escape_string($data->compania) . "', " : "";
    $sql_update_empresa .= (isset($data->ruc)) ? "ruc = '" . $data->ruc . "', " : "";
    $sql_update_empresa .= (isset($data->callePrin)) ? "callePrincipal = '" . $mysqli->real_escape_string($data->callePrin) . "', " : "";
    $sql_update_empresa .= (isset($data->calleSec)) ? "calleSecundaria = '" . $mysqli->real_escape_string($data->calleSec) . "', " : "";
    $sql_update_empresa .= (isset($data->contacto)) ? "contacto = '" . $data->contacto . "', " : "";
    $sql_update_empresa .= (isset($data->color)) ? "color = '" . $data->color . "', " : "";
    $sql_update_empresa .= (isset($data->comentario)) ? "comentario = '" . $mysqli->real_escape_string($data->comentario) . "', " : "";
    $sql_update_empresa .= (isset($data->latitud)) ? "latitud = " . $data->latitud . ", " : "";
    $sql_update_empresa .= (isset($data->longitud)) ? "longitud = " . $data->longitud . ", " : "";
    $sql_update_empresa .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update_empresa .= 'fecha_actualizo = NOW() ';
    $sql_update_empresa .= 'WHERE idCompania = ' . $data->id;
    $administradores = json_decode($administradores);
    if (count($administradores) > 0) {
        $sql_update_admin_compania = "INSERT INTO $DB_NAME.administrador_compania "
                . "(idAdministrador, idCompania, hablilitado, idAdministradorRegistro, fecha_registro, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito,fecha_deshabilito) "
                . "VALUES ";
        foreach ($administradores as $v) {
            if ($v->habilitado) {
                $sql_update_admin_compania .= "('" . $v->id . "', '" . $data->id . "'"
                        . ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . ", NULL, NULL"
                        . "),";
            } else {
                $sql_update_admin_compania .= "('" . $v->id . "', '" . $data->id . "'"
                        . ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . ", NULL, NULL"
                        . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . "),";
            }
        }
        $sql_update_admin_compania = substr($sql_update_admin_compania, 0, -1);
        $sql_update_admin_compania .= " ON DUPLICATE KEY UPDATE idAdministrador=VALUES(idAdministrador),"
                . "idCompania=VALUES(idCompania),"
                . "hablilitado=VALUES(hablilitado),"
                . "idAdministradorHabilito=VALUES(idAdministradorHabilito),"
                . "fecha_habilito=VALUES(fecha_habilito),"
                . "idAdministradorDeshabilito=VALUES(idAdministradorDeshabilito),"
                . "fecha_deshabilito=VALUES(fecha_deshabilito);";
        EJECUTAR_SQL($mysqli, $sql_update_admin_compania);
    }
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update_empresa));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();
