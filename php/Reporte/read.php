<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql = "SELECT COUNT(a.idAlumno) AS total"
        . " FROM $DB_NAME.alumno a "
        . " INNER JOIN $DB_NAME.clase_alumno ca ON ca.idAlumno=a.idAlumno"
        . " INNER JOIN $DB_NAME.clase c ON ca.idClase=ca.idClase"
        . " WHERE TRUE ";

/* if (isset($idUsuario)) {
    if (isset($id)) {
        $sql .= " AND ";
    } else {
        $sql .= " WHERE ";
    }
    $sql .= " uaa.idUsuarioAccesoApi = '$idUsuario' ";
}
if (isset($idEstado)) {
    if ($idEstado == 1) {
        if (isset($id)|| isset($idUsuario)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.confirmada = 1 ";
    }
    if ($idEstado == 2) {
        if (isset($id)|| isset($idUsuario)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.validada = 1 ";
    }
} */
if (isset($param)) {
    if ($param !== '') {
        
        $sql .= " AND ( LOWER(a.nombre) LIKE LOWER('%$param%') OR LOWER(a.apellido) LIKE LOWER('%$param%')) ";
    }
}
$sql .= " ORDER BY a.idAlumno ASC ";
//echo $sql;
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
//echo $total;
if ($total > 0) {
    $sql = "SELECT a.idAlumno,a.nombre,a.apellido,a.imagen,a.correo,c.nombre AS nombreClase,c.idClase"
            . " FROM $DB_NAME.alumno a"
            . " INNER JOIN $DB_NAME.clase_alumno ca ON ca.idAlumno=a.idAlumno"
            . " INNER JOIN $DB_NAME.clase c ON ca.idClase=ca.idClase"
            . " WHERE TRUE ";

/* 
    if (isset($idUsuario)) {
        if (isset($id)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.idUsuarioAccesoApi = '$idUsuario' ";
    }
    if (isset($idEstado)) {
        if ($idEstado == 1) {
            if (isset($id) || isset($idUsuario)) {
                $sql .= " AND ";
            } else {
                $sql .= " WHERE ";
            }
            $sql .= " uaa.confirmada = 1 ";
        }
        if ($idEstado == 2) {
            if (isset($id) || isset($idUsuario)) {
                $sql .= " AND ";
            } else {
                $sql .= " WHERE ";
            }
            $sql .= " uaa.validada = 1 ";
        }
    } */

    if (isset($param)) {
        
        if (isset($param)) {
            $sql .= " AND ( LOWER(uaa.correoRemitente) LIKE LOWER('%$param%') OR LOWER(uaa.mensaje) LIKE LOWER('%$param%') OR LOWER(uaa.saldoCorreo) LIKE LOWER('%$param%') OR LOWER(ua.usuario) LIKE LOWER('%$param%') ) ";
        }
    }
    $sql .= " ORDER BY a.idAlumno ASC ";
//echo $sql;
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS ";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
        return $mysqli->close();
    }
    $arreglo = array();
    while ($myrow = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow["idAlumno"]),
            'idAlumno' => intval($myrow["idAlumno"]),
            'idClase' => intval($myrow["idClase"]),
            'correo' => $myrow["correo"],
            'nombre' => $myrow["nombre"],
            'nombreClase' => $myrow["nombreClase"],
            'apellido' => $myrow["apellido"],
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => TRUE, 'data' => [], 'total' => 0));
}
$mysqli->close();
