<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
extract($_POST);

if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idAdministrador=$_SESSION["ID_ADMINISTRADOR"];

    $sql="SELECT DISTINCT ad.idAdministrador,ad.idDepartamento,ads.idSucursal,adc.idCompania 
    FROM $DB_NAME.administrador_departamento ad
    INNER JOIN $DB_NAME.administrador_sucursal ads ON ads.idAdministrador=ad.idAdministrador
    INNER JOIN $DB_NAME.administrador_compania adc ON adc.idAdministrador=ad.idAdministrador
    WHERE ad.idAdministrador=$idAdministrador and ad.idDepartamento=$idDepartamento and ads.idSucursal=$idSucursal and adc.idCompania=$idCompania";

    //echo $sql;
     $result = $mysqli->query($sql);
     $myrow = $result->fetch_assoc();
     
     $idDepartamento = intval($myrow['idDepartamento']);
     $idCompania = intval($myrow['idCompania']);
     $idSucursal = intval($myrow['idSucursal']);

    echo json_encode(array('success' => true,'idDepartamento'=>$idDepartamento,'idAplicativo'=>$idAplicativo,'idSucursal'=>$idSucursal,'idCompania'=>$idCompania));
    $mysqli->close();
}    