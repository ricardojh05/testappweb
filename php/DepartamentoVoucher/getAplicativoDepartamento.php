<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idAdministrador=$_SESSION["ID_ADMINISTRADOR"];
    
    //consultar compania
    $sqlConsultaCompania="SELECT idCompania FROM $DB_NAME.administrador_compania WHERE idAdministrador=$idAdministrador AND hablilitado=1 LIMIT 1";
      //  echo $sqlConsultaCompania;
        $resultComp = $mysqli->query($sqlConsultaCompania);
        $myrowComp = $resultComp->fetch_assoc();
        $idCompania = intval($myrowComp['idCompania']);

//consulara sucursal
    $sqlConsultaSucursal="SELECT idSucursal FROM $DB_NAME.administrador_sucursal WHERE idAdministrador=$idAdministrador AND hablilitado=1 LIMIT 1";
      //  echo $sqlConsultaSucursal;
        $resultSucu = $mysqli->query($sqlConsultaSucursal);
        $myrowSucu = $resultSucu->fetch_assoc();
        $idSucursal = intval($myrowSucu['idSucursal']);

//consulta aplicativo
    $sqlConsultaAplicativo="SELECT idAplicativo FROM $DB_NAME.administrador_aplicativo WHERE idAdministrador=$idAdministrador AND hablilitado=1 LIMIT 1";
   // echo $sqlConsultaAplicativo;
    $result = $mysqli->query($sqlConsultaAplicativo);
    $myrow = $result->fetch_assoc();
    $idAplicativo = intval($myrow['idAplicativo']);
    
    //consulta departamento 
    $sqlConsultaDepartamento="SELECT idDepartamento FROM $DB_NAME.administrador_departamento WHERE idAdministrador=$idAdministrador AND hablilitado=1 LIMIT 1";
    //echo $sqlConsultaDepartamento;
    $resultDepar = $mysqli->query($sqlConsultaDepartamento);
    $myrowDepar = $resultDepar->fetch_assoc();
    $idDepartamento = intval($myrowDepar['idDepartamento']);
    
    $sql="SELECT DISTINCT ad.idAdministrador,ad.idDepartamento,ads.idSucursal,adc.idCompania 
    FROM $DB_NAME.administrador_departamento ad
    INNER JOIN $DB_NAME.administrador_sucursal ads ON ads.idAdministrador=ad.idAdministrador
    INNER JOIN $DB_NAME.administrador_compania adc ON adc.idAdministrador=ad.idAdministrador
    WHERE ad.idAdministrador=$idAdministrador and ad.idDepartamento=$idDepartamento and ads.idSucursal=$idSucursal and adc.idCompania=$idCompania";

     $result = $mysqli->query($sql);
     $myrow = $result->fetch_assoc();

     $idDepartamento = intval($myrow['idDepartamento']);
     $idCompania = intval($myrow['idCompania']);
     $idSucursal = intval($myrow['idSucursal']);

    echo json_encode(array('success' => true,'idDepartamento'=>$idDepartamento,'idAplicativo'=>$idAplicativo,'idSucursal'=>$idSucursal,'idCompania'=>$idCompania));
    $mysqli->close();
}    