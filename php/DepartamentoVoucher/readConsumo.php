<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {

    $sql_total = "SELECT COUNT(c.id_cliente) AS total "
            . " FROM $DB_NAME.clientes c"
            . " INNER JOIN $DB_NAME.departamentoCliente dc ON c.id_cliente = dc.idCliente"
            . " INNER JOIN $DB_NAME.departamentoClienteCredito cr ON dc.idDepartamentoCliente = cr.idDepartamentoCliente"
            . " LEFT JOIN $DB_NAME.departamentoClienteConsumo co ON co.idDepartamentoCliente = dc.idDepartamentoCliente AND co.idDepartamentoClienteCredito = cr.idDepartamentoClienteCredito"
            . " WHERE dc.idDepartamento = $idDepartamento AND dc.idDepartamentoCliente = $idDeparCli";

            if (isset($mes) && isset($anio)) {
                $sql_total .= " AND cr.anio= $anio AND cr.mes = $mes";
            }
//echo $sql_total;
$result = $mysqli->query($sql_total);
    $myrow = $result->fetch_assoc();
$total = intval($myrow['total']);
    
    $sqlConsulta = "SELECT c.id_cliente AS idCliente, dc.idDepartamento, dc.idDepartamentoCliente AS idDeparCli,co.id_solicitud AS idSolicitud,"
            . " cr.idDepartamentoClienteCredito AS idDeparCliCre, CONCAT(c.nombres, ' ', c.apellidos) AS cliente,"
            . " c.cedula, dc.codigo, co.consumo, co.lt_cliente AS latC, co.lg_cliente AS lngC,"
            . " co.lt_conductor AS latCo, co.lg_conductor AS lngCo, co.tipo,"
            . " co.lt_solicitud AS latSo, co.lg_solicitud AS lngSo,"
            . " co.fecha_hora_comsumo AS fhc, cr.desde, cr.hasta, cr.razonCredito,co.detalle,co.nota,"
            . " cr.credito, cr.desde AS fha"
            . " FROM $DB_NAME.clientes c"
            . " INNER JOIN $DB_NAME.departamentoCliente dc ON c.id_cliente = dc.idCliente"
            . " INNER JOIN $DB_NAME.departamentoClienteCredito cr ON dc.idDepartamentoCliente = cr.idDepartamentoCliente"
            . " LEFT JOIN $DB_NAME.departamentoClienteConsumo co ON co.idDepartamentoCliente = dc.idDepartamentoCliente AND co.idDepartamentoClienteCredito = cr.idDepartamentoClienteCredito"
            . " WHERE dc.idDepartamento = $idDepartamento AND dc.idDepartamentoCliente = $idDeparCli";
    if (isset($mes) && isset($anio)) {
        $sqlConsulta .= " AND cr.anio= $anio AND cr.mes = $mes";
    }
    $consumo = 0;
  // echo $sqlConsulta;
    $arreglo = [];
    $result2 = $mysqli->query($sqlConsulta);
    while ($myrow = $result2->fetch_assoc()) {
        $latC = 0;
        $lngC = 0;
        $latCo = 0;
        $latSo = 0;
        $lngCo = 0;
        $lngSo = 0;
        if ($myrow['latC'] != '') {
            $latC = $myrow['latC'];
        }
        if ($myrow['lngC'] != '') {
            $lngC = $myrow['lngC'];
        }
        if ($myrow['latCo'] != '') {
            $latCo = $myrow['latCo'];
        }
        if ($myrow['latSo'] != '') {
            $latSo = $myrow['latSo'];
        }
        if ($myrow['lngCo'] != '') {
            $lngCo = $myrow['lngCo'];
        }
        if ($myrow['lngSo'] != '') {
            $lngSo = $myrow['lngSo'];
        }
        $fha = $myrow['fha'];
        $idCredito = $myrow['idDeparCliCre'];
        $consumo = 0;
        $result = $mysqli->query($sqlConsulta);
        while ($myrowFA = $result->fetch_assoc()) {
            if ($idCredito == $myrowFA['idDeparCliCre']) {
                $consumo = $consumo + doubleval($myrowFA['consumo']);
            }
        }
        $raz_cred = preg_replace("[\n|\r|\n\r\t|']", "\\n", utf8_encode($myrow['razonCredito']));
        $text = $raz_cred . " | Fecha asignada: " . $fha . " | Monto: " . doubleval($myrow['credito']) . " | Consumo: " . $consumo . ' | Desde: ' . $myrow['desde'] . ' | Hasta: ' . $myrow['hasta'];
        $arreglo[] = array(
            'idCliente' => intval($myrow["idCliente"]),
            'idDeparCli' => intval($myrow["idDeparCli"]),
            'idDeparCliCre' => intval($myrow["idDeparCliCre"]),
            'idDepartamento' => intval($idDepartamento),
            'idSolicitud' => intval($myrow["idSolicitud"]),
            'cliente' => $myrow["cliente"],
            'codigo' => $myrow["codigo"],
            'latC' => $latC,
            'lngC' => $lngC,
            'latCo' => $latCo,
            'latSo' => $latSo,
            'lngCo' => $lngCo,
            'lngSo' => $lngSo,
            'tipo' => intval($myrow['tipo']),
            'fhc' => $myrow["fhc"],
            'fhd' => $myrow["desde"],
            'fha' => $myrow["hasta"],
            'rzn_cre' => $myrow["fha"],
            'credito' => doubleval($myrow['credito']),
            'consumo' => doubleval($myrow['consumo']),
            'Detalle' => $text,
            'detalle' => $myrow["detalle"],
            'nota' => $myrow["nota"],

        );
    }
    echo json_encode(array('success' => true, 'data' => $arreglo,'total' => $total));
    $mysqli->close();
}    