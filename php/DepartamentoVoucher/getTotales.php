<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idEmpresa = $_SESSION["IDCOMPANYSIS"];
    $sqlNumClientes = "SELECT COUNT(*) AS num"
            . " FROM $DB_NAME.departamentoCliente cc"
            . " WHERE cc.idDepartamento = '$idEmpresa'";
    $resultNC = $mysqli->query($sqlNumClientes);
    $num = 0;
    while ($myrow = $resultNC->fetch_assoc()) {
        $num = intval($myrow['num']);
    }
    $sqlConsultaCr = "SELECT SUM(IF(credito=NULL,0,credito)) AS credito"
            . " FROM $DB_NAME.departamentoCliente cc"
            . " INNER JOIN $DB_NAME.departamentoClienteCredito crc ON cc.idDepartamentoCliente = crc.idDepartamentoCliente"
            . " WHERE cc.idDepartamento = $idEmpresa"
            . " AND MONTH(crc.fecha_hora_registro) = $mes AND YEAR(crc.fecha_hora_registro)= $anio";
    $resultCr = $mysqli->query($sqlConsultaCr);
//    echo $sqlConsultaCr;
    $credito = 0;
    while ($myrow = $resultCr->fetch_assoc()) {
        $credito = doubleval($myrow['credito']);
    }
    $sqlConsultaCo = "SELECT SUM(IF(consumo=NULL,0,consumo)) AS consumo"
            . " FROM $DB_NAME.departamentoCliente cc"
            . " INNER JOIN $DB_NAME.departamentoClienteConsumo coc ON cc.idDepartamentoCliente = coc.idDepartamentoCliente"
            . " WHERE cc.idDepartamento = $idEmpresa"
            . " AND MONTH(coc.fecha_hora_comsumo) = $mes AND YEAR(coc.fecha_hora_comsumo)= $anio";
    $resultCo = $mysqli->query($sqlConsultaCo);
    $consumo = 0;
    while ($myrow = $resultCo->fetch_assoc()) {
        $consumo = doubleval($myrow['consumo']);
    }
//    echo $sqlConsulta;
    $objJson = "{data: [{"
            . "num:'" . $num . "',"
            . "credito:'" . $credito . "',"
            . "consumo:'" . $consumo . "',"
            . "}]}";
    echo $objJson;
    $mysqli->close();
}

