<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idEmpresa = $_SESSION["IDCOMPANYSIS"];
    $sqlIsVoucher = "SELECT em.empresa,em.id_aplicativo FROM $DB_NAME.empresas em "
            . "WHERE em.voucher = 1 AND em.id_empresa = '$idEmpresa' LIMIT 1";
//    echo $sqlIsVoucher;
    $result = $mysqli->query($sqlIsVoucher);
    $objJson = "{data: [";
    if ($result->num_rows > 0) {
        $myrow = $result->fetch_assoc();
        $objJson.= "{"
                . "empresa:'" . preg_replace("[\n|\r|\n\r\t|']", "\\n", utf8_encode($myrow['empresa'])) . "',"
                . "isValid: 1,"
                . "idAplicativo:" . intval($myrow['id_aplicativo']) . ","
                . "},";
    } else {
        $objJson.= "{"
                . "empresa:'La empresa que tiene asignada no es de tipo voucher, por favor solicite  al administrador del sistema que cambie al tipo requerido.',"
                . "isValid: 0,"
                . "},";
    }
//    echo $sqlConsulta;
    $objJson.= "]}";
    echo $objJson;
    $mysqli->close();
}

