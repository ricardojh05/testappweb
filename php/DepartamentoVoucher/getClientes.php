<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {

    $sql_total = "SELECT COUNT(idDepartamento) AS total "
            . "FROM $DB_NAME.departamentoCliente dc "
            . "INNER JOIN $DB_NAME.clientes c ON dc.idCliente = c.id_cliente "
            . "WHERE dc.idDepartamento = $idDepartamento";
    if (isset($params) && ($params !== '')) {
        $sql_total .= " AND ( LOWER(c.nombres) LIKE LOWER('$params%')) OR (LOWER(c.apellidos) LIKE LOWER('$params%'))";
    }
//    echo $sql_total;
    $result = $mysqli->query($sql_total);
    $myrow = $result->fetch_assoc();
    $total = $myrow['total'];
//CONSULTA SQL
    $sqlConsulta = "SELECT IFNULL(dcm.credito, 0) AS creditoMes, IFNULL(dcm.consumo, 0) AS consumoMes, c.nombres, c.apellidos,"
            . " a.nombre AS aplicativo,a.id_aplicativo as idAplicativo,dc.idCliente,dc.codigo,dc.idDepartamentoCliente,"
            . " c.cedula,c.celular,dc.idDepartamento,CONCAT(c.nombres, ' ', c.apellidos) AS cliente,dcc.idDepartamentoClienteCredito,"
            . " IF(dc.habilitado = 1, 1, 0) AS habilitado,"
            . " dc.idAdministradorRegistro, IF(MONTH(dc.fecha_registro) < 10, DATE_FORMAT(dc.fecha_registro, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(dc.fecha_registro, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,"
            . " dc.idAdministradorActualizo, IF(MONTH(dc.fecha_actualizo) < 10, DATE_FORMAT(dc.fecha_actualizo, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(dc.fecha_actualizo, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo,"
            . " dc.idAdministradorHabilito, IF(MONTH(dc.fecha_habilito) < 10, DATE_FORMAT(dc.fecha_habilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(dc.fecha_habilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,"
            . " dc.idAdministradorDeshabilito, IF(MONTH(dc.fecha_deshabilito) < 10, DATE_FORMAT(dc.fecha_deshabilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(dc.fecha_deshabilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito"
            . " FROM $DB_NAME.departamentoCliente dc"
            . " INNER JOIN $DB_NAME.clientes c ON dc.idCliente = c.id_cliente"
            . " INNER JOIN $DB_NAME.aplicativos a ON c.id_aplicativo = a.id_aplicativo"
            . " LEFT JOIN $DB_NAME.departamentoClienteCredito dcc ON dcc.idDepartamentoCliente = dc.idDepartamentoCliente"
            . " LEFT JOIN $DB_NAME.departamentoClienteConsumoMes dcm ON dc.idDepartamentoCliente = dcm.idDepartamentoCliente AND dcm.anio = $anio AND dcm.mes = $mes"
            . " WHERE dc.idDepartamento = $idDepartamento";

    if (isset($params) && ($params !== '')) {
        $sqlConsulta .= " AND ( LOWER(c.nombres) LIKE LOWER('$params%')) OR (LOWER(c.apellidos) LIKE LOWER('$params%'))";
    }
    $sqlConsulta .= " GROUP BY dc.idDepartamentoCliente";
//    echo $sqlConsulta;
    $result = $mysqli->query($sqlConsulta);
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {
        $fechaChange = ($myrow["habilitado"]) ? $myrow["fecha_habilito"] : $myrow["fecha_deshabilito"];
        $idUserChange = ($myrow["habilitado"]) ? $myrow["idAdministradorHabilito"] : $myrow["idAdministradorDeshabilito"];
        $arreglo[] = array(
            'idCliente' => intval($myrow["idCliente"]), //idC
            'idDeparCli' => intval($myrow["idDepartamentoCliente"]), //idCC
            'id' => intval($myrow["idDepartamentoCliente"]),
            'idDeparCliCre' => intval($myrow["idDepartamentoClienteCredito"]), //idCCC
            'idDepartamento' => intval($myrow["idDepartamento"]), //idE
            'idDeparCli' => intval($myrow["idDepartamentoCliente"]),
            'idAplicativo' => intval($myrow["idAplicativo"]),
            'app' => $myrow["aplicativo"],
            'cedula' => $myrow["cedula"],
            'celular' => $myrow["celular"],
            'nombres' => $myrow["nombres"],
            'cliente' => $myrow["cliente"],
            'apellidos' => $myrow["apellidos"],
            'codigo' => $myrow["codigo"],
            'creditoMes' => doubleval($myrow['creditoMes']),
            'consumoMes' => doubleval($myrow['consumoMes']),
            'idUserCreate' => $myrow["idAdministradorRegistro"],
            'dateCreate' => $myrow["fecha_registro"],
            'idUserChange' => $idUserChange,
            'dateChange' => $fechaChange,
            'idUserUpdate' => $myrow["idAdministradorActualizo"],
            "dateUpdate" => $myrow["fecha_actualizo"]
        );
    }

    echo json_encode(array('success' => true, 'data' => $arreglo, 'total' => $total));
    $mysqli->close();
}

