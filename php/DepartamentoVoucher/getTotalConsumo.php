<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    if (isset($idE) && intval($idE) == 0 && $idE != '') {
        $idEmpresa = $idE;
    } else {
        $idEmpresa = $_SESSION["IDCOMPANYSIS"];
    }
    $sqlConsultaCo = "SELECT SUM(co.consumo) AS consumo"
            . " FROM $DB_NAME.departamentoCliente cc"
            . " INNER JOIN $DB_NAME.departamentoClienteCredito cr ON cc.idDepartamentoCliente = cr.idDepartamentoCliente"
            . " INNER JOIN $DB_NAME.departamentoClienteConsumo co ON co.idDepartamentoCliente = cc.idDepartamentoCliente"
            . " AND co.idDepartamentoClienteCredito = cr.idDepartamentoClienteCredito"
            . " WHERE cc.idDepartamento = $idEmpresa ";
    $sqlConsultaCr = "SELECT SUM(cr.credito) AS credito"
            . " FROM $DB_NAME.departamentoCliente cc"
            . " INNER JOIN $DB_NAME.departamentoClienteCredito cr ON cc.idDepartamentoCliente = cr.idDepartamentoCliente"
            . " WHERE cc.idDepartamento = $idEmpresa";
    if (isset($idC)) {
        $sqlConsultaCo.= " AND cc.idCliente = $idC";
        $sqlConsultaCr.= " AND cc.idCliente = $idC";
    }
    if (isset($mes) && isset($anio)) {
        $sqlConsultaCo .= " AND YEAR(cr.anio)= $anio AND MONTH(cr.mes) = $mes";
        $sqlConsultaCr .= " AND YEAR(cr.anio)= $anio AND MONTH(cr.mes) = $mes";
    }
    if (isset($dia) && isset($mes) && isset($anio)) {
        $sqlConsultaCo .= " AND cr.fecha_hora_registro BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio-$mes-$dia 23:59:59'";
        $sqlConsultaCr .= " AND cr.fecha_hora_registro BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio-$mes-$dia 23:59:59'";
    }
//    echo $sqlConsultaCr;
    $resultCr = $mysqli->query($sqlConsultaCr);
    $myrowCr = $resultCr->fetch_assoc();
    $resultCo = $mysqli->query($sqlConsultaCo);
    $myrowCo = $resultCo->fetch_assoc();
    $objJson = "{data: [{"
            . "totalCo:'" . doubleval($myrowCo['consumo']) . "',"
            . "totalCr:'" . doubleval($myrowCr['credito']) . "',"
            . "},";

    $objJson .= "]}";
    echo $objJson;
    $mysqli->close();
}