<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $sqlCreditoCom = "SELECT cCr.idDepartamento, "
            . " cCr.anio, cCr.mes, cCr.credito,cCr.asignado,cCr.consumido"
            . " FROM $DB_NAME.departamentoCredito cCr";
    $sqlCreditoCom .= " WHERE cCr.idDepartamento = '$idDepartamento'";
    
    if (isset($mes)) {
        $sqlCreditoCom .= " AND cCr.mes = $mes";
        $consumo = 0;
    }
    if (isset($anio)) {
        $sqlCreditoCom .= " AND cCr.anio = $anio";
        $consumo = 0;
    }
//    echo $sqlCreditoCom;
    $result = $mysqli->query($sqlCreditoCom);
    if ($consumo == 0) {
        $sqlConsumo = "SELECT SUM(ccc.consumo) AS consumo "
                . " FROM $DB_NAME.departamentoClienteConsumo ccc"
                . " INNER JOIN $DB_NAME.departamentoCliente cc ON cc.idDepartamentoCliente = ccc.idDepartamentoCliente"
                . " WHERE cc.idDepartamento = $idDepartamento AND YEAR(ccc.fecha_hora_comsumo) = $anio AND MONTH(ccc.fecha_hora_comsumo) = $mes";
        $resultCo = $mysqli->query($sqlConsumo);
        if ($resultCo->num_rows > 0) {
            $myrowCo = $resultCo->fetch_assoc();
            $consumo = doubleval($myrowCo['consumo']);
        }
    } else {
        $consumo = 0;
    }
//    $objJson = "{data: [";
//    echo $sqlCreditoCom;
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {
        
            $arreglo[] = array(
        'idEmpresa' => intval($myrow["idDepartamento"]),
        'mes' => $myrow["mes"],
        'anio' => $myrow["anio"],
        'credito' => doubleval($myrow['credito']),
        'consumo' => doubleval($myrow['consumido']),
        'asignado' => doubleval($myrow['asignado']),
        'saldo'=>  number_format(doubleval($myrow['credito']) - $consumo, 2)      
    );
            
//        $objJson.= "{"
//                . "idCCr:'" . intval($myrow['idCCr']) . "',"
//                . "idE:'" . intval($myrow['idE']) . "',"
//                . "idCCrE:'" . intval($myrow['idCCrE']) . "',"
//                . "mes:'" . $myrow['mes'] . "',"
//                . "anio:'" . $myrow['anio'] . "',"
//                . "tipo:'" . $myrow['tipo'] . "',"
//                . "empresa:'" . preg_replace("[\n|\r|\n\r\t|']", "\\n", utf8_encode($myrow['empresa'])) . "',"
//                . "estado:'" . $myrow['estado'] . "',"
//                . "moneda:'" . $myrow['moneda'] . "',"
//                . "credito:'" . doubleval($myrow['credito']) . "',"
//                . "consumo:'" . $consumo . "',"
//                . "saldo:'" . number_format(doubleval($myrow['credito']) - $consumo, 2) . "',"
//                . "},";
    }
//    echo $sqlConsulta;
//    $objJson.= "]}";
//    echo $objJson;
    echo json_encode(array('success' => true, 'data' => $arreglo));
    $mysqli->close();
}

