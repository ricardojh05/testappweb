<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT 
            cc.id_config_ktaxi_ciudad,
            cc.id_aplicativo,
            ap.nombre as aplicativo,
            cc.id_ciudad,
            c.ciudad,
            cc.id_label,
            lc.nombre as label,
            cc.nombre_label,
            cc.valor_por_defecto_label,
            IF(cc.habilitado = 0, 0, 1) AS habilitado,
            cc.idAdministradorRegistro,
            IF(MONTH(cc.fecha_registro) < 10 , DATE_FORMAT(cc.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            cc.idAdministradorHabilito,
            cc.idAdministradorDeshabilito,
            IF(MONTH(cc.fecha_habilito) < 10 , DATE_FORMAT(cc.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(cc.fecha_deshabilito) < 10 , DATE_FORMAT(cc.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            cc.idAdministradorActualizo,
            IF(MONTH(cc.fecha_actualizo) < 10 , DATE_FORMAT(cc.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.config_ktaxi_ciudad cc
                INNER JOIN
            $DB_NAME.aplicativos ap ON ap.id_aplicativo = cc.id_aplicativo
                INNER JOIN
            $DB_NAME.ciudades c ON c.id_ciudad = cc.id_ciudad
                INNER JOIN
            $DB_NAME.label_config_ktaxi lc ON lc.id_label_config_ktaxi = cc.id_label
                WHERE TRUE ";

if (isset($param)) {
    $sql .= " AND (LOWER(cc.nombre_label) LIKE LOWER('%$param%') "
            . " OR cc.valor_por_defecto_label LIKE '%$param%') ";
}
if (isset($aplicativos)) {
    if ($aplicativos !== '') {
        $sql .= " AND cc.id_aplicativo IN ($aplicativos) ";
    }
}
if (isset($ciudades)) {
    if ($ciudades !== '') {
        $sql .= " AND cc.id_ciudad IN ($ciudades) ";
    }
}
if (isset($labels)) {
    if ($labels !== '') {
        $sql .= " AND cc.id_label IN ($labels) ";
    }
}
$sql .= " ORDER BY cc.id_config_ktaxi_ciudad DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_config_ciudad = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read_config_ciudad["habilitado"]) ? $myrow_read_config_ciudad["fecha_habilito"] : $myrow_read_config_ciudad["fecha_deshabilito"];
    $idUserChange = ($myrow_read_config_ciudad["habilitado"]) ? $myrow_read_config_ciudad["idAdministradorHabilito"] : $myrow_read_config_ciudad["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'idAplicativo' => intval($myrow_read_config_ciudad["id_aplicativo"]),
        'aplicativo' => ($myrow_read_config_ciudad["aplicativo"]),
        'idCiudad' => intval($myrow_read_config_ciudad["id_ciudad"]),
        'ciudad' => ($myrow_read_config_ciudad["ciudad"]),
        'idLabel' => intval($myrow_read_config_ciudad["id_label"]),
        'label' => ($myrow_read_config_ciudad["label"]),
        'id' => intval($myrow_read_config_ciudad["id_config_ktaxi_ciudad"]),
        'nombre' => $myrow_read_config_ciudad["nombre_label"],
        'valor' => $myrow_read_config_ciudad["valor_por_defecto_label"],
        'habilitado' => intval($myrow_read_config_ciudad["habilitado"]),
        'idUserCreate' => $myrow_read_config_ciudad["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_config_ciudad["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read_config_ciudad["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read_config_ciudad["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'configuraciones' => $arreglo));
