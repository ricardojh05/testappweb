<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT m.idModulo AS id, m.modulo, IF(ma.leer IS NULL OR ma.leer = 0, 0, 1) AS leer,"
        . " IF(ma.crear IS NULL OR ma.crear = 0, 0, 1) AS crear,"
        . " IF(ma.editar IS NULL OR ma.editar = 0, 0, 1) AS editar,"
        . " IF(ma.eliminar IS NULL OR ma.eliminar = 0, 0, 1) AS eliminar,"
        . " IF(ma.habilitar IS NULL OR ma.habilitar = 0, 0, 1) AS habilitar"
        . " FROM $DB_NAME.modulo m "
        . " LEFT JOIN $DB_NAME.moduloadministrador ma ON m.idModulo = ma.idModulo AND ma.idAdministrador = $idAdmin";

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_administrador = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_administrador["id"]),
        'modulo' => ($myrow_read_administrador["modulo"]),
        'leer' => (bool) ($myrow_read_administrador["leer"]),
        'crear' => (bool) ($myrow_read_administrador["crear"]),
        'editar' => (bool) ($myrow_read_administrador["editar"]),
        'eliminar' => (bool) ($myrow_read_administrador["eliminar"]),
        'habilitar' => (bool) ($myrow_read_administrador["habilitar"])
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'modulos' => $arreglo));
