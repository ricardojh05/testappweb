<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT a.idAdministrador AS id, a.usuario, a.contrasenia, a.nombres, a.apellidos, a.celular, a.cedula,"
        . " a.correo, a.fecha_nacimiento AS fn, a.direccion, a.imagen,"
        . " a.idAdministradorRegistro, "
        . " a.idAdministradorEdito, "
        . " IF(a.bloqueado=0,0,1) AS bloqueado, a.bloqueadoMensaje AS msgB,"
        . " IF(MONTH(a.fecha_registro) < 10 , DATE_FORMAT(a.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(a.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,"
        . " IF(MONTH(a.fecha_edicion) < 10 , DATE_FORMAT(a.fecha_edicion,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(a.fecha_edicion,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_edicion,"
        . " a.fecha_bloqueo AS fhb, "
        . " a.fecha_reactivacion AS fhre, "
        . " a.mensajeReactivacion AS msgR"
        . " FROM $DB_NAME.administrador a";
if (isset($param)) {
    $sql .= " WHERE a.cedula LIKE '%$param%' OR a.usuario LIKE '%$param%' OR a.nombres LIKE '%$param%' OR a.apellidos LIKE '%$param%'"
            . " OR CONCAT(a.nombres,' ',a.apellidos) LIKE '%$param%'";
}
$sql .= " ORDER BY a.idAdministrador DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_administrador = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_administrador["id"]),
        'usuario' => ($myrow_read_administrador["usuario"]),
        'contrasenia' => ($myrow_read_administrador["contrasenia"]),
        'persona' => $myrow_read_administrador["nombres"] . ' ' . $myrow_read_administrador["apellidos"],
        'nombres' => $myrow_read_administrador["nombres"],
        'apellidos' => $myrow_read_administrador["apellidos"],
        'celular' => ($myrow_read_administrador["celular"]),
        'cedula' => ($myrow_read_administrador["cedula"]),
        'correo' => ($myrow_read_administrador["correo"]),
        'cedula' => ($myrow_read_administrador["cedula"]),
        'fNacimiento' => ($myrow_read_administrador["fn"]), //fecha de nacimiento
        'direccion' => ($myrow_read_administrador["direccion"]),
        'imagen' => ($myrow_read_administrador["imagen"]),
        'bloqueado' => (bool) ($myrow_read_administrador["bloqueado"]),
        'msgB' => preg_replace("[\n|\r|\n\r\t|']", "\\n", $myrow_read_administrador["msgB"]),
        'fhre' => ($myrow_read_administrador["fhre"]),
        'msgR' => preg_replace("[\n|\r|\n\r\t|']", "\\n", $myrow_read_administrador["msgR"]),
        'idUserCreate' => $myrow_read_administrador["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_administrador["fecha_registro"],
        'idUserUpdate' => $myrow_read_administrador["idAdministradorEdito"],
        'dateUpdate' => $myrow_read_administrador["fecha_edicion"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'administrador' => $arreglo));
