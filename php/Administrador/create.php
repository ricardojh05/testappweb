<?php
//editado
include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
extract($_GET);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return;

    if (!isset($data->image)) {
        $img = "usuario.png";
    } else {
        $img = $data->image;
    }
    $sql_create_Administrador = "INSERT INTO $DB_NAME.administrador "
            . "(usuario, contrasenia, nombres, apellidos, celular, cedula,"
            . " correo, fecha_nacimiento, direccion, imagen,"
            . " idAdministradorRegistro, bloqueado, fecha_registro)"
            . " VALUES "
            . "('" . $data->usuario . "', MD5(CONCAT('" . $data->contrasenia . "' , '$TOKEN_LOGIN'))"
            . ", '" . $data->nombres . "'" . ", '" . $data->apellidos . "'"
            . ", '" . $data->celular . "'" . ", '" . $data->cedula . "'"
            . ", '" . $data->correo . "'" . ", '" . $data->fNacimiento . "'"
            . ", '" . $data->direccion . "'"
            . ", '" . $img . "'"
            . ", '" . $_SESSION["ID_ADMINISTRADOR"] . "'" . ", b'$data->bloqueado'"
            . ", NOW()"
            . ")";
//    echo $sql_create_Administrador;
    $res = EJECUTAR_SQL($mysqli, $sql_create_Administrador);
    if (isset($res['id']) && intval($res['id']) > 0) {
        $permisos = json_decode($permisos);
        if (count($permisos) > 0) {
            $sql_create_admin_permisos = "INSERT INTO $DB_NAME.moduloadministrador "
                    . "(idModulo, idAdministrador, leer, crear, editar, eliminar, idAdministradorRegistro,fecha_registro) "
                    . "VALUES ";
            foreach ($permisos as $p) {
                $sql_create_admin_permisos .= "('" . $p->id . "', '" . $res['id'] . "'"
                        . ", " . (int) $p->leer . "" . ", " . (int) $p->crear . ""
                        . ", " . (int) $p->editar . "" . ", " . (int) $p->eliminar . ""
                        . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . "),";
            }
            $sql_create_admin_permisos = substr($sql_create_admin_permisos, 0, -1);
            EJECUTAR_SQL($mysqli, $sql_create_admin_permisos);
        }

        $perfiles = json_decode($perfiles);
        if (count($perfiles) > 0) {
            $sql_create_admin_perfil = "INSERT INTO $DB_NAME.perfil_administrador "
                    . "(idPerfil, idAdministrador, habilitado, idAdministradorRegistro, idAdministradorActualizo, fecha_actualizo, idAdministradorHabilito, fecha_habilito) "
                    . "VALUES ";
            foreach ($perfiles as $p) {
                if ((bool) $p->nuevo) {
                    $sql_create_admin_perfil .= "(" . $p->id . ", " . $res['id'] . ""
                            . ", " . $p->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . "),";
                }
            }
            $sql_create_admin_perfil = substr($sql_create_admin_perfil, 0, -1);
            EJECUTAR_SQL($mysqli, $sql_create_admin_perfil);
        }
        $listaAplicativo = json_decode($listaAplicativo);
        if (count($listaAplicativo) > 0) {
            $sql_update_admin_aplicativo = "INSERT INTO $DB_NAME.administrador_aplicativo "
                    . "( idAplicativo, idAdministrador, hablilitado, "
                    . "idAdministradorRegistro, fecha_registro, "
                    . "idAdministradorHabilito, fecha_habilito, "
                    . "idAdministradorDeshabilito, fecha_deshabilito) "
                    . "VALUES ";
            foreach ($listaAplicativo as $p) {
                if ((bool) $p->nuevo) {
                    $sql_update_admin_aplicativo .= "(" . $p->id . ", " . $res['id'] . ""
                            . ", " . $p->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", NULL, NULL"
                            . "),";
                }
            }
            $sql_update_admin_aplicativo = substr($sql_update_admin_aplicativo, 0, -1);
            EJECUTAR_SQL($mysqli, $sql_update_admin_aplicativo);
        }
    }
    echo json_encode($res);
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
