<?php

//Departamento
include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql_total = "SELECT COUNT(d.idDepartamento) AS total FROM $DB_NAME.departamento d "
        . " INNER JOIN $DB_NAME.sucursal s on s.idSucursal=d.idSucursal "
        . " INNER JOIN $DB_NAME.administrador_departamento da on da.idDepartamento=d.idDepartamento"
        . " WHERE da.idAdministrador=" . $_SESSION["ID_ADMINISTRADOR"] . " AND da.hablilitado= TRUE "
        . " ORDER BY d.idDepartamento DESC";
$result = $mysqli->query($sql_total);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", "sql" => $sql));
    return $mysqli->close();
}
$myrow_read = $result->fetch_assoc();
$total = intval($myrow_read['total']);
if ($total > 0) {
    $sql = "SELECT d.idDepartamento, d.idSucursal, s.sucursal, d.departamento, d.contacto, d.color, d.correo, d.comentario,d.isNota,"
            . " d.idAdministradorRegistro, d.fecha_registro, d.idAdministradorActualizo, d.fecha_actualizo "
            . "FROM $DB_NAME.departamento d "
            . "INNER JOIN $DB_NAME.sucursal s on s.idSucursal=d.idSucursal "
            . "INNER JOIN $DB_NAME.administrador_departamento da on da.idDepartamento=d.idDepartamento "
            . "WHERE da.idAdministrador=" . $_SESSION["ID_ADMINISTRADOR"];
    if (isset($param)) {
        $sql .= " and d.departamento LIKE ('$param%')";
    }

    if (isset($sucursal) && $sucursal !== '' && $sucursal != 0) {
        $sql .= " AND d.idSucursal IN ($sucursal) ";
    }

    $sql .= " ORDER BY d.idDepartamento DESC ";

    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
//    echo $sql;
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read_departamento = $result->fetch_assoc()) {
        $insert = "SELECT count(idSucursalDepartamento) as total_insert FROM $DB_NAME.sucursalDepartamento where idDepartamento=" . $myrow_read_departamento["idDepartamento"] . ";";
        $result_insert = $mysqli->query($insert);
        $myrow_insert = $result_insert->fetch_assoc();
        $total_insert = intval($myrow_insert['total_insert']);
        $arreglo[] = array(
            'id' => intval($myrow_read_departamento["idDepartamento"]),
            'idSucursal' => intval($myrow_read_departamento["idSucursal"]),
            'isNota' => intval($myrow_read_departamento["isNota"]),
            'departamento' => $myrow_read_departamento["departamento"],
            'sucursal' => $myrow_read_departamento["sucursal"],
            'contacto' => $myrow_read_departamento["contacto"],
            'color' => $myrow_read_departamento["color"],
            'correo' => $myrow_read_departamento["correo"],
            'comentario' => $myrow_read_departamento["comentario"],
            'fRegistro' => $myrow_read_departamento["fecha_registro"],
            'idUserCreate' => $myrow_read_departamento["idAdministradorRegistro"],
            'dateCreate' => $myrow_read_departamento["fecha_registro"],
            'idUserUpdate' => $myrow_read_departamento["idAdministradorActualizo"],
            'dateUpdate' => $myrow_read_departamento["fecha_actualizo"],
            'insertado' => $total_insert
        );
    }
    echo json_encode(array('success' => true, 'departamentoes' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => true, 'departamentoes' => [], 'total' => 0));
}
$mysqli->close();

