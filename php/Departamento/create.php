<?php
// Departamento
include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {

    if (!$mysqli = getConectionDb())
        return $mysqli;
    $sql_create_empresa = "INSERT INTO $DB_NAME.departamento "
            . "(idSucursal, departamento, contacto, correo, color, isNota,comentario, idAdministradorRegistro) "
            . "VALUES "
            . "(" . $data->sucursal . ", ('" . $mysqli->real_escape_string($data->departamento) . "')"
            . ", '" . $data->contacto . "'" . ", '" . $data->correo . "'". ", '" . $data->color . "'".", '" . $data->isNota . "'"
            . ", '" . $mysqli->real_escape_string($data->comentario) . "'" 
            . ", '" . $_SESSION["ID_ADMINISTRADOR"] . "'"
            . ");";
            //echo $sql_create_empresa;
    $departamento = EJECUTAR_SQL($mysqli, $sql_create_empresa);
    if (isset($departamento['id'])) {
        if ($departamento['id'] > 0) {
            $sql_create_sucursal_departamento="INSERT INTO $DB_NAME.sucursalDepartamento "
                    . "( idSucursal, idDepartamento, credito, consumo, habilitado,idAdministradorRegistro, fecha_registro)"
                    . "VALUES "
                    . "(".$data->sucursal.", ". $departamento['id'] .", 0, 0, 1, ". $_SESSION["ID_ADMINISTRADOR"] . ", NOW())";
            EJECUTAR_SQL($mysqli, $sql_create_sucursal_departamento);
            $administradores =json_decode($administradores);
            if (count($administradores) > 0) {
                    $sql_insert_admin_departamento = "INSERT INTO $DB_NAME.administrador_departamento "
                            . "(idAdministrador, idDepartamento, hablilitado, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito) "
                            . "VALUES ";
                    foreach ($administradores as $v) {
                        $sql_insert_admin_departamento .= "(" . $v->id . ", " . $departamento['id'] . "". ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()),";
                    }
                    $sql_insert_admin_departamento = substr($sql_insert_admin_departamento, 0, -1);
                    echo json_encode(EJECUTAR_SQL($mysqli, $sql_insert_admin_departamento));
                } else {
                    echo json_encode($departamento);
                }
        }
    }
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}

//include '../../dll/config.php';
//include '../../dll/funciones.php';
//$arrayData = array();
//$data = json_decode(file_get_contents('php://input'));
//if (isset($data)) {
//    if (!$mysqli = getConectionDb())
//        return $mysqli;
//    $sql_create_departamento = "INSERT INTO $DB_NAME.departamento "
//            . "(idCiudad, departamento, callePrincipal, calleSecundaria, contacto, color, latitud, longitud, idAdministradorRegistro, idDepartamento, correo) "
//            . "VALUES "
//            . "(" . $data->ciudad . ", '" . $mysqli->real_escape_string($data->departamento) . "'"
//            . ", '" . $mysqli->real_escape_string($data->callePrin) . "'" . ", '" . $mysqli->real_escape_string($data->calleSec) . "'"
//            . ", '" . $data->contacto . "'" . ", '" . $data->color . "'"
//            . ", '" . $data->latitud . "'"
//            . ", '" . $data->longitud . "'" . ", '" . $_SESSION["ID_ADMINISTRADOR"] . "'"
//            . ", " . $data->company . "" . ", '" . $mysqli->real_escape_string($data->correo) . "'"
//            . ");";
//    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_departamento));
//    $mysqli->close();
//} else {
//    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
//}