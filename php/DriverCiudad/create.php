<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
extract($_GET);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return;
    (!isset($data->habilitado)) ? $data->habilitado = 0 : '';
    if ((bool) $data->habilitado) {
        $idHabilitado = 'idAdministradorHabilito';
        $fHabilitado = 'fecha_habilito';
    } else {
        $idHabilitado = 'idAdministradorDeshabilito';
        $fHabilitado = 'fecha_deshabilito';
    }
    $sql_create_config_ciudad = "INSERT INTO $DB_NAME.config_driver_ciudad "
            . "(id_aplicativo, id_ciudad, "
            . "id_label, nombre_label,"
            . "valor_por_defecto_label,habilitado, "
            . "idAdministradorRegistro, "
            . "$idHabilitado, $fHabilitado)"
            . " VALUES "
            . "(" . $data->idAplicativo . ", " . $data->idCiudad . ""
            . ", " . $data->idLabel . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->nombre)) . "'"
            . ", '" . $data->valor . "'" . ", " . $data->habilitado . ""
            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
            . ")";
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_config_ciudad));
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
