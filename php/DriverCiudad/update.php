<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    extract($_GET);
    if (!$mysqli = getConectionDb())
        return;
    $sql_update_config_ciudad = "UPDATE $DB_NAME.config_driver_ciudad SET ";
    $sql_update_config_ciudad .= (isset($data->idAplicativo)) ? "id_aplicativo = $data->idAplicativo, " : "";
    $sql_update_config_ciudad .= (isset($data->idCiudad)) ? "id_ciudad = " . $data->idCiudad . ", " : "";
    $sql_update_config_ciudad .= (isset($data->idLabel)) ? "id_label = " . $data->idLabel . ", " : "";
    $sql_update_config_ciudad .= (isset($data->nombre)) ? "nombre_label = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->nombre)) . "', " : "";
    $sql_update_config_ciudad .= (isset($data->valor)) ? "valor_por_defecto_label = '$data->valor', " : "";
    $sql_update_config_ciudad .= (isset($data->habilitado)) ? "habilitado = $data->habilitado, " : "";
    if (isset($data->habilitado)) {
        if ($data->habilitado == 1) {
            $sql_update_config_ciudad .= 'idAdministradorHabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update_config_ciudad .= 'fecha_habilito = NOW(), ';
        } else {
            $sql_update_config_ciudad .= 'idAdministradorDeshabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update_config_ciudad .= 'fecha_deshabilito = NOW(), ';
        }
    }
    $sql_update_config_ciudad .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update_config_ciudad .= 'fecha_actualizo = NOW() ';
    $sql_update_config_ciudad .= 'WHERE id_config_driver_ciudad = ' . $data->id;
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update_config_ciudad));
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}