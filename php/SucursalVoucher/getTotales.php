<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idSucursal = $_SESSION["IDCOMPANYSIS"];
    $sqlNumDepartamentos = "SELECT COUNT(*) AS num"
            . " FROM $DB_NAME.sucursalDepartamento cc"
            . " WHERE cc.idSucursal = '$idSucursal'";
    $resultNC = $mysqli->query($sqlNumDepartamentos);
    $num = 0;
    while ($myrow = $resultNC->fetch_assoc()) {
        $num = intval($myrow['num']);
    }
    $sqlConsultaCr = "SELECT SUM(IF(credito=NULL,0,credito)) AS credito"
            . " FROM $DB_NAME.sucursalDepartamento cc"
            . " INNER JOIN $DB_NAME.sucursalDepartamentoCredito crc ON cc.idSucursalDepartamento = crc.idSucursalDepartamento"
            . " WHERE cc.idSucursal = $idSucursal"
            . " AND MONTH(crc.fecha_hora_registro) = $mes AND YEAR(crc.fecha_hora_registro)= $anio";
    $resultCr = $mysqli->query($sqlConsultaCr);
//    echo $sqlConsultaCr;
    $credito = 0;
    while ($myrow = $resultCr->fetch_assoc()) {
        $credito = doubleval($myrow['credito']);
    }
    $sqlConsultaCo = "SELECT SUM(IF(consumo=NULL,0,consumo)) AS consumo"
            . " FROM $DB_NAME.sucursalDepartamento cc"
            . " INNER JOIN $DB_NAME.sucursalDepartamentoConsumo coc ON cc.idSucursalDepartamento = coc.idSucursalDepartamento"
            . " WHERE cc.idSucursal = $idSucursal"
            . " AND MONTH(coc.fecha_hora_comsumo) = $mes AND YEAR(coc.fecha_hora_comsumo)= $anio";
    $resultCo = $mysqli->query($sqlConsultaCo);
    $consumo = 0;
    while ($myrow = $resultCo->fetch_assoc()) {
        $consumo = doubleval($myrow['consumo']);
    }
//    echo $sqlConsulta;
    $objJson = "{data: [{"
            . "num:'" . $num . "',"
            . "credito:'" . $credito . "',"
            . "consumo:'" . $consumo . "',"
            . "}]}";
    echo $objJson;
    $mysqli->close();
}

