<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {

$sql_total = "SELECT COUNT(d.idDepartamento) AS total "
            . "FROM $DB_NAME.sucursalDepartamento sd "
            . "LEFT JOIN $DB_NAME.departamento d ON d.idDepartamento = sd.idDepartamento "
            . "WHERE sd.idSucursal = $idSucursal";
    if (isset($params) && ($params !== '')) {
        $sql_total .= " AND ( LOWER(c.nombres) LIKE LOWER('$params%')) OR (LOWER(c.apellidos) LIKE LOWER('$params%'))";
    }
//    echo $sql_total;
    $result = $mysqli->query($sql_total);
    $myrow = $result->fetch_assoc();
    $total = $myrow['total'];
    
    $bandera = FALSE;
    $idDepartamento = "";
    if (isset($id)) {//id Departamento
        $bandera = TRUE;
        $agregaDepartamento = "AND cc.idDepartamento = '$id'";
    }
    $idioma = "SET lc_time_names = 'es_MX'; ";
    $resulIdo = $mysqli->query($idioma);


    $sqlConsulta="SELECT cc.idDepartamento AS idDepar,cc.idSucursalDepartamento AS idSucuDepar, cc.idSucursal AS idSucursal,c.departamento,"
            . "IFNULL(ccc.credito, 0) AS creditoMes,IFNULL(ccc.consumo, 0)AS consumoMes,"
            . "IF(cc.habilitado = 1, 1, 0) AS habilitado, cc.idAdministradorRegistro, "
            . "IF(MONTH(cc.fecha_registro) < 10, DATE_FORMAT(cc.fecha_registro, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_registro, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro, cc.idAdministradorActualizo, "
            . "IF(MONTH(cc.fecha_actualizo) < 10, DATE_FORMAT(cc.fecha_actualizo, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_actualizo, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo, cc.idAdministradorHabilito, "
            . "IF(MONTH(cc.fecha_habilito) < 10, DATE_FORMAT(cc.fecha_habilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_habilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito, cc.idAdministradorDeshabilito, "
            . "IF(MONTH(cc.fecha_deshabilito) < 10, DATE_FORMAT(cc.fecha_deshabilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(cc.fecha_deshabilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito "
            . "FROM $DB_NAME.sucursalDepartamento cc "
            . "INNER JOIN $DB_NAME.departamento c ON cc.idDepartamento = c.idDepartamento "
            . "LEFT JOIN $DB_NAME.sucursalDepartamentoConsumoMes ccc ON ccc.idSucursalDepartamento = cc.idSucursalDepartamento AND ccc.anio = $anio AND ccc.mes = $mes WHERE cc.idSucursal = $idSucursal";
            
//    echo $sqlConsulta;
    $result = $mysqli->query($sqlConsulta);
    $objJson = "{data: [";
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {
        $fechaChange = ($myrow["habilitado"]) ? $myrow["fecha_habilito"] : $myrow["fecha_deshabilito"];
        $idUserChange = ($myrow["habilitado"]) ? $myrow["idAdministradorHabilito"] : $myrow["idAdministradorDeshabilito"];
        $arreglo[] = array(
            'idDepar' => intval($myrow["idDepar"]),
            'idSucuDepar' => intval($myrow["idSucuDepar"]),
            'id' => intval($myrow["idDepar"]),
            'idSucursal' => intval($myrow["idSucursal"]),
            'departamento' => $myrow["departamento"],
            'creditoMes' => doubleval($myrow['creditoMes']),
            'consumoMes' => doubleval($myrow['consumoMes']),
            'idUserCreate' => $myrow["idAdministradorRegistro"],
            'dateCreate' => $myrow["fecha_registro"],
            'idUserChange' => $idUserChange,
            'dateChange' => $fechaChange,
            'idUserUpdate' => $myrow["idAdministradorActualizo"],
            "dateUpdate" => $myrow["fecha_actualizo"]
        );
    }

    echo json_encode(array('success' => true, 'data' => $arreglo,'total' => $total));
    $mysqli->close();
}

