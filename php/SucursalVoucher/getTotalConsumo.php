<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    if (isset($idE) && intval($idE) == 0 && $idE != '') {
        $idSucursal = $idE;
    } else {
        $idSucursal = $_SESSION["IDCOMPANYSIS"];
    }
    $sqlConsultaCo = "SELECT SUM(co.consumo) AS consumo"
            . " FROM $DB_NAME.sucursalDepartamento cc"
            . " INNER JOIN $DB_NAME.sucursalDepartamentoCredito cr ON cc.idSucursalDepartamento = cr.idSucursalDepartamento"
            . " INNER JOIN $DB_NAME.sucursalDepartamentoConsumo co ON co.idSucursalDepartamento = cc.idSucursalDepartamento"
            . " AND co.idSucursalDepartamentoCredito = cr.idSucursalDepartamentoCredito"
            . " WHERE cc.idSucursal = $idSucursal ";
    $sqlConsultaCr = "SELECT SUM(cr.credito) AS credito"
            . " FROM $DB_NAME.sucursalDepartamento cc"
            . " INNER JOIN $DB_NAME.sucursalDepartamentoCredito cr ON cc.idSucursalDepartamento = cr.idSucursalDepartamento"
            . " WHERE cc.idSucursal = $idSucursal";
    if (isset($idC)) {
        $sqlConsultaCo.= " AND cc.idDepartamento = $idC";
        $sqlConsultaCr.= " AND cc.idDepartamento = $idC";
    }
    if (isset($mes) && isset($anio)) {
        $sqlConsultaCo .= " AND YEAR(cr.anio)= $anio AND MONTH(cr.mes) = $mes";
        $sqlConsultaCr .= " AND YEAR(cr.anio)= $anio AND MONTH(cr.mes) = $mes";
    }
    if (isset($dia) && isset($mes) && isset($anio)) {
        $sqlConsultaCo .= " AND cr.fecha_hora_registro BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio-$mes-$dia 23:59:59'";
        $sqlConsultaCr .= " AND cr.fecha_hora_registro BETWEEN '$anio-$mes-$dia 00:00:00' AND '$anio-$mes-$dia 23:59:59'";
    }
//    echo $sqlConsultaCr;
    $resultCr = $mysqli->query($sqlConsultaCr);
    $myrowCr = $resultCr->fetch_assoc();
    $resultCo = $mysqli->query($sqlConsultaCo);
    $myrowCo = $resultCo->fetch_assoc();
    $objJson = "{data: [{"
            . "totalCo:'" . doubleval($myrowCo['consumo']) . "',"
            . "totalCr:'" . doubleval($myrowCr['credito']) . "',"
            . "},";

    $objJson .= "]}";
    echo $objJson;
    $mysqli->close();
}