<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
//    $idSucursal = 0;
//    $idSucursal = $_SESSION["IDCOMPANYSIS"];
//    if (isset($idE) && intval($idE) == 0 && $idE != '') {
//        $idSucursal = $idE;
//    } else {
//        $idSucursal = $_SESSION["IDCOMPANYSIS"];
//    }
    $sqlCreditoCom = "SELECT cCr.idSucursal, "
            . " cCr.anio, cCr.mes, cCr.credito,cCr.asignado,cCr.consumido"
            . " FROM $DB_NAME.sucursalCredito cCr";
    $sqlCreditoCom .= " WHERE cCr.idSucursal = '$idSucursal'";
    
//    $sqlCreditoCom = "SELECT cCr.id_compania_credito AS idCCr, cCr.idEmpresa AS idE, em.empresa, "
//            . " cCr.id_compania_credito_estado AS idCCrE, cCr.anio, cCr.mes, cCr.credito,cCrE.estado, IF(cCrE.tipo = 1,1,2) AS tipo,"
//            . " p.moneda"
//            . " FROM compania_credito cCr"
//            . " INNER JOIN compania_credito_estado cCrE ON cCrE.id_compania_credito_estado = cCr.id_compania_credito_estado"
//            . " INNER JOIN empresas em ON em.idEmpresa = cCr.idEmpresa "
//            . " INNER JOIN ciudades ci ON ci.id_ciudad = em.id_ciudad "
//            . " INNER JOIN paises p ON p.id_pais = ci.id_pais";
//    $sqlCreditoCom .= " WHERE em.voucher = 1 AND cCr.idEmpresa = '$idSucursal'";
//    $consumo = -1;
    if (isset($mes)) {
        $sqlCreditoCom .= " AND cCr.mes = $mes";
        $consumo = 0;
    }
    if (isset($anio)) {
        $sqlCreditoCom .= " AND cCr.anio = $anio";
        $consumo = 0;
    }
//    echo $sqlCreditoCom;
    $result = $mysqli->query($sqlCreditoCom);
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {   
            $arreglo[] = array(
        'idEmpresa' => intval($myrow["idSucursal"]),
        'mes' => $myrow["mes"],
        'anio' => $myrow["anio"],
        'credito' => doubleval($myrow['credito']),
        'consumo' => doubleval($myrow['consumido']),
        'asignado' => doubleval($myrow['asignado']),
        'saldo'=>  number_format(doubleval($myrow['credito']) - $consumo, 2)      
    );
    }
    echo json_encode(array('success' => true, 'data' => $arreglo));
    $mysqli->close();
}

