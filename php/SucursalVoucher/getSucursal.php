<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idAdministrador=$_SESSION["ID_ADMINISTRADOR"];
    $sqlConsultaSucursal="SELECT idSucursal FROM $DB_NAME.administrador_sucursal WHERE idAdministrador=$idAdministrador AND hablilitado=1 LIMIT 1";
//    echo $sqlConsultaSucursal;
    $result = $mysqli->query($sqlConsultaSucursal);
    $myrow = $result->fetch_assoc();
    $idSucursal = intval($myrow['idSucursal']);

    echo json_encode(array('success' => true,'idSucursal'=>$idSucursal));
    $mysqli->close();
}    