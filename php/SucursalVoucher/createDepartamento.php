<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
extract($_POST);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $cliente = json_decode(stripslashes($id));
    $codigo = json_decode(stripslashes($codigo));
    if (isset($idE) && intval($idE) == 0 && $idE != '') {
        $idSucursal = json_decode(stripslashes($idE));
    } else {
//        $idSucursal = intval($_SESSION["IDCOMPANYSIS"]);
    }
    $idUser = intval($_SESSION["ID_ADMINISTRADOR"]);
    $idApp = intval($idApp);
    $insertcompania = false;
    $insertFpc = false;
    $existeSql = "SELECT idSucursalDepartamento FROM $DB_NAME.departamentoDepartamento WHERE (idDepartamento='" . $cliente . "' and idSucursal='" . $idSucursal . "')";
//    echo $existeSql;
    $result = $mysqli->query($existeSql);
    if ($result->num_rows > 0) {
        echo "{success: false, message: 'El cliente ya se encuentra registrado.'}";
    } else {
        $insertSql = "INSERT INTO $DB_NAME.departamentoDepartamento (idSucursal,idDepartamento,codigo,idAdministradorRegistro,fecha_registro) "
                . "VALUES($idSucursal,?,$codigo,$idUser,NOW())";
        $stmt = $mysqli->prepare($insertSql);
//        echo $insertSql;
        if ($stmt) {
            $stmt->bind_param("i", $cliente);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $insertcompania = true;
                $consultaFormaPago = "SELECT id_forma_pago FROM formas_pago where id_aplicativo=$idApp and tipo=1 limit 1";
                $resultFormaPago = $mysqli->query($consultaFormaPago);
                $myrowFormaPago = $resultFormaPago->fetch_assoc();
                $formaPago = $myrowFormaPago ["id_forma_pago"];
                if ($formaPago != '') {
                    $insertSql = "INSERT INTO $DB_NAME.formas_pago_clientes (id_cliente,id_forma_pago,estado) "
                            . "VALUES(?,$formaPago,0)";
                    $stmt = $mysqli->prepare($insertSql);
                    if ($stmt) {
                        $stmt->bind_param("i", $cliente);
                        $stmt->execute();
                        if ($stmt->affected_rows > 0) {
                            $insertFpc = true;
                        } else {
                            $insertFpc = false;
                        }
                    } else {
                        $insertFpc = false;
                    }
                }
            } else {
                $insertcompania = false;
            }
        } else {
            $insertcompania = false;
        }
        if ($insertcompania && $insertFpc) {
            echo "{success: true, message: 'Departamento registrado correctamente'}";
        } else {
            if ($insertcompania && !$insertFpc) {
                echo "{success: true, message: 'Departamento registrado correctamente en cliente compania pero no en formas de pago cliente.'}";
            } else {
                if (!$insertcompania && $insertFpc) {
                    echo "{success: true, message: 'Departamento registrado correctamente en formas de pago cliente pero no en cliente compania.'}";
                } else {
                    echo "{success: false, message: 'Ya se encuentra registrado.'}";
                }
            }
        }
    }
    $mysqli->close();
}
