<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
(!isset($param)) ? $param = '' : '';
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql = "SELECT 
            CONCAT(adR.nombres, ' ', adR.apellidos) AS nombreReg,
            adR.cedula AS cedulaReg, adR.celular AS celularReg, adR.imagen AS imagenReg,
            CONCAT(adM.nombres, ' ', adM.apellidos) AS nombreAct,
            adM.cedula AS cedulaAct, adM.celular AS celularAct, adM.imagen AS imagenAct,
            CONCAT(adC.nombres, ' ', adC.apellidos) AS nombreCamb,
            adC.cedula AS cedulaCamb, adC.celular AS celularCamb, adC.imagen AS imagenCamb
        FROM
            $DB_NAME.administrador adR 
                LEFT JOIN
            $DB_NAME.administrador adM ON adM.idAdministrador = $idActualizo
                LEFT JOIN
            $DB_NAME.administrador adC ON adC.idAdministrador = $idCambio
            where adR.idAdministrador = $idRegistro ";

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'error' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo = array(
        'nombreReg' => $myrow_read_users["nombreReg"],
        'cedulaReg' => $myrow_read_users["cedulaReg"],
        'celularReg' => $myrow_read_users["celularReg"],
        'imagenReg' => $myrow_read_users["imagenReg"],
        'nombreAct' => $myrow_read_users["nombreAct"],
        'cedulaAct' => $myrow_read_users["cedulaAct"],
        'celularAct' => $myrow_read_users["celularAct"],
        'imagenAct' => $myrow_read_users["imagenAct"],
        'nombreCamb' => $myrow_read_users["nombreCamb"],
        'cedulaCamb' => $myrow_read_users["cedulaCamb"],
        'celularCamb' => $myrow_read_users["celularCamb"],
        'imagenCamb' => $myrow_read_users["imagenCamb"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
