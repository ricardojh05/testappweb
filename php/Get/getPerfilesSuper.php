<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!isset($param)) {
    $param = 0;
}
if (!$mysqli = getConectionDb())
    return $mysqli;

$perfil3 = $_SESSION['PERFILES_ASIGNADOS'][3];

$sql = "SELECT 
            p.perfil,
            pa.idAdministrador,
            pa.idPerfil,
            IF(pa.habilitado = 0, 0, 1) AS habilitado,
            pa.idAdministradorRegistro,
            IF(MONTH(pa.fecha_registro) < 10 , DATE_FORMAT(pa.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(pa.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            pa.idAdministradorHabilito,
            pa.idAdministradorDeshabilito,
            IF(MONTH(pa.fecha_habilito) < 10 , DATE_FORMAT(pa.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(pa.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(pa.fecha_deshabilito) < 10 , DATE_FORMAT(pa.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(pa.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            pa.idAdministradorActualizo,
            IF(MONTH(pa.fecha_actualizo) < 10 , DATE_FORMAT(pa.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(pa.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.perfil_administrador pa
                INNER JOIN
            $DB_NAME.perfil p ON p.idPerfil = pa.idPerfil
        WHERE
            pa.idAdministrador = $param ";
if (!$perfil3) {
    $sql .= " AND pa.idPerfil <> 3 ";
}
$resultPerfiles = $mysqli->query($sql);
$array = [];
while ($myrow_read_perfiles_user = $resultPerfiles->fetch_assoc()) {
    $fechaChange = ($myrow_read_perfiles_user["habilitado"]) ? $myrow_read_perfiles_user["fecha_habilito"] : $myrow_read_perfiles_user["fecha_deshabilito"];
    $idUserChange = ($myrow_read_perfiles_user["habilitado"]) ? $myrow_read_perfiles_user["idAdministradorHabilito"] : $myrow_read_perfiles_user["idAdministradorDeshabilito"];
    $array[] = array(
        'id' => intval($myrow_read_perfiles_user["idPerfil"]),
        'idAdmin' => intval($myrow_read_perfiles_user["idAdministrador"]),
        'perfil' => $myrow_read_perfiles_user["perfil"],
        'habilitado' => intval($myrow_read_perfiles_user["habilitado"]),
        'idUserCreate' => $myrow_read_perfiles_user["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_perfiles_user["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read_perfiles_user["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read_perfiles_user["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'perfiles' => $array));
