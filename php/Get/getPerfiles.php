<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
(!isset($param)) ? $param = '' : '';
if (!$mysqli = getConectionDb())
    return $mysqli;

$perfil3 = $_SESSION['PERFILES_ASIGNADOS'][3];

$sql = "SELECT idPerfil, perfil, descripcion "
        . "FROM $DB_NAME.perfil "
        . "WHERE ((perfil) LIKE ('$param%') "
        . "OR (descripcion) LIKE ('$param%') "
        . "OR idPerfil = '$param') ";
if (!$perfil3) {
    $sql .= " AND idPerfil <> 3 ";
}
if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_perfiles = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_perfiles["idPerfil"]),
        'text' => $myrow_read_perfiles["perfil"],
        'descripcion' => $myrow_read_perfiles["descripcion"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
