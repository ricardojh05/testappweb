<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;

$sql = "SELECT id_label_config_ktaxi, nombre"
        . " FROM $DB_NAME.label_config_ktaxi "
        . " WHERE"
        . " LOWER(nombre) LIKE LOWER('%$param%')"
        . " OR id_label_config_ktaxi = '$param'";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS;";
}
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_label = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_get_label["id_label_config_ktaxi"]),
        'text' => $myrow_get_label["nombre"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
