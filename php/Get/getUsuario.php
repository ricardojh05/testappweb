<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$filtroAux = str_replace(' ', '', $filtro);
$sql = "SELECT u.idUsuario, CONCAT(u.nombres, ' ', u.apellidos) AS nombres, u.cedula, u.celular"
        . " FROM $DB_NAME.usuario u"
        . " WHERE"
        . " u.celular LIKE '%$filtro%' OR"
        . " u.correo LIKE '%$filtro%' OR"
        . " u.nombres LIKE '%$filtro%' OR"
        . " u.apellidos LIKE '%$filtro%' OR"
        . " u.cedula LIKE '%$filtro%' OR"
        . " CONCAT(u.nombres, u.apellidos) LIKE '%$filtroAux%' OR "
        . " u.celular LIKE '%$filtroAux%'";
if (isset($limite)) {
    $sql.=" LIMIT $limite";
} else {
    $sql.=" LIMIT 3;";
}
$i = 0;
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_Usuario = $result->fetch_assoc()) {
    $arreglo[$i] = array(
        'id' => intval($myrow_get_Usuario["idUsuario"]),
        'nombres' => ($myrow_get_Usuario["nombres"]),
        'cedula' => ($myrow_get_Usuario["cedula"]),
        'celular' => ($myrow_get_Usuario["celular"]),
        'texto' => preg_replace("[\n|\r|\n\r\t|']", "\\n", ($myrow_get_Usuario["nombres"] . '-' . $myrow_get_Usuario["cedula"]))
    );
    $i++;
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
