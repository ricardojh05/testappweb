<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;

$sql = "SELECT s.idSucursal, s.sucursal, s.latitud, s.longitud"
        . " FROM $DB_NAME.sucursal s"
        . " INNER JOIN $DB_NAME.administrador_sucursal adms on adms.idSucursal=s.idSucursal "
        . " WHERE  adms.idAdministrador=" . $_SESSION["ID_ADMINISTRADOR"]
        . " AND adms.hablilitado=TRUE";

if (isset($param) && $param !== '') {
    $sql .= " AND (LOWER(s.sucursal) LIKE LOWER('%$param%')"
            . " OR s.idSucursal = '$param')";
}
//if ($COMPANIAS_ADMIN !== 'all') {
//    $sql .= " AND id_compania IN ($COMPANIAS_ADMIN) ";
//}
if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS;";
}
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_sucursal = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_get_sucursal["idSucursal"]),
        'text' => $myrow_get_sucursal["sucursal"],
        'latitud' => $myrow_get_sucursal["latitud"],
        'longitud' => $myrow_get_sucursal["longitud"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
