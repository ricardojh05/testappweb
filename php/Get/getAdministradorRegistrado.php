<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
if (isset($param) || isset($cedula)) {
    if (!$mysqli = getConectionDb())
        return $mysqli;

    $sql = "SELECT a.idAdministrador, a.usuario, a.contrasenia, a.nombres, a.apellidos, 
a.celular, a.correo, a.fecha_nacimiento, a.direccion, a.imagen "
            . "FROM $DB_NAME.administrador a ";

    $sql .= "WHERE ";
    if (isset($cedula)) {
        $sql .= " a.cedula = $cedula ";
    }
    $result = $mysqli->query($sql);

    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read_users = $result->fetch_assoc()) {
        $listaCompania = "SELECT ac.idCompania, IF(ac.hablilitado = 1, 1,0) AS habilitado, c.compania "
                . " FROM $DB_NAME.administrador_compania ac "
                . " INNER JOIN $DB_NAME.compania c on c.idCompania=ac.idCompania "
                . " WHERE ac.idAdministrador = " . intval($myrow_read_users["idAdministrador"])
                . " ORDER BY ac.idCompania DESC ";
        $resultCompania = $mysqli->query($listaCompania);
        while ($myrow_read_compania = $resultCompania->fetch_assoc()) {
            $arregloCompanias[] = array(
                'id' => intval($myrow_read_compania["idCompania"]),
                'habilitado' => $myrow_read_compania["habilitado"],
                'compania' => $myrow_read_compania["compania"]
            );
        }

        $arreglo[] = array(
            'id' => intval($myrow_read_users["idAdministrador"]),
            'usuario' => $myrow_read_users["usuario"],
            'contrasenia' => $myrow_read_users["contrasenia"],
            'nombres' => $myrow_read_users["nombres"],
            'apellidos' => $myrow_read_users["apellidos"],
            'celular' => $myrow_read_users["celular"],
            'correo' => $myrow_read_users["correo"],
            'fecha_nacimiento' => $myrow_read_users["fecha_nacimiento"],
            'direccion' => $myrow_read_users["direccion"],
            'imagen' => $myrow_read_users["imagen"],
            'lista_compania' => $arregloCompanias,
        );
    }
    $mysqli->close();
    echo json_encode(array('success' => true, 'administradores' => $arreglo));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
