<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;

$sql = "SELECT id_ciudad, ciudad,latitud,longitud"
        . " FROM $DB_NAME.ciudades "
        . " WHERE"
        . " LOWER(ciudad) LIKE LOWER('%$param%')"
        . " OR id_ciudad = '$param'";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS;";
}
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_ciudad = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_get_ciudad["id_ciudad"]),
        'text' => $myrow_get_ciudad["ciudad"],
        'latitud' => $myrow_get_ciudad["latitud"],
        'longitud' => $myrow_get_ciudad["longitud"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
