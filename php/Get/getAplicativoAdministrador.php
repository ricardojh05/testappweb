<?php
include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);

if (!$mysqli = getConectionDb()) {
    return $mysqli;
}
if (!isset($idAdministrador)) {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
    return '';
}

$sql = "SELECT ac.idAdministrador, ac.idAplicativo, IF(ac.hablilitado = 1, 1,0) AS habilitado,  a.nombre, "
        . "ac.idAdministradorRegistro, "
        . " IF(MONTH(ac.fecha_registro) < 10 , DATE_FORMAT(ac.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ac.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro, "
        . "ac.idAdministradorHabilito, "
        . " IF(MONTH(ac.fecha_habilito) < 10 , DATE_FORMAT(ac.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ac.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito, "
        . "ac.idAdministradorDeshabilito, "
        . " IF(MONTH(ac.fecha_deshabilito) < 10 , DATE_FORMAT(ac.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ac.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito "
        . "FROM $DB_NAME.administrador_aplicativo ac "
        . "INNER JOIN $DB_NAME.aplicativos a on a.id_aplicativo=ac.idAplicativo "
        . "WHERE ac.idAdministrador = $idAdministrador "
        . "ORDER BY ac.idAdministrador DESC ";
$result = $mysqli->query($sql);

if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}

$arreglo = [];
while ($myrow_read_equipos = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read_equipos["habilitado"]) ? $myrow_read_equipos["fecha_habilito"] : $myrow_read_equipos["fecha_deshabilito"];
    $idUserChange = ($myrow_read_equipos["habilitado"]) ? $myrow_read_equipos["idAdministradorHabilito"] : $myrow_read_equipos["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'id' => intval($myrow_read_equipos["idAplicativo"]),
        'idAdministrador' => intval($myrow_read_equipos["idAdministrador"]),
        'habilitado' => (bool) ($myrow_read_equipos["habilitado"]),
        'nombre' => $myrow_read_equipos["nombre"],
        'idUserCreate' => $myrow_read_equipos["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_equipos["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange
    );
}

echo json_encode(array('success' => true, 'data' => $arreglo));
$mysqli->close();
