<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;

$sql = "SELECT id_aplicativo, nombre"
        . " FROM $DB_NAME.aplicativos "
        . " WHERE"
        . " LOWER(nombre) LIKE LOWER('$param%')"
        . " OR id_aplicativo = '$param'";

/* $sql = "SELECT DISTINCT a.id_aplicativo, a.nombre"
        . " FROM $DB_NAME.aplicativos a"
//        . " INNER JOIN $DB_NAME.administrador_aplicativo aa ON a.id_aplicativo=aa.idAplicativo "
        . " WHERE TRUE ";

if (isset($idAplicativo) && $idAplicativo !== '') {
        $sql .= " AND a.id_aplicativo = $idAplicativo ";
}
if (isset($param) && $param !== '') {
        $sql .= " AND ( LOWER(a.nombre) LIKE LOWER('$param%'))";
} */


if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS;";
}
//echo $sql;
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_aplicativo = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_get_aplicativo["id_aplicativo"]),
        'text' => $myrow_get_aplicativo["nombre"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
