<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;

$idAdministrador=$_SESSION["ID_ADMINISTRADOR"];
$sql = "SELECT aa.idAplicativo, a.nombre"
        . " FROM $DB_NAME.administrador_aplicativo aa"
        . " INNER JOIN $DB_NAME.aplicativos a ON a.id_aplicativo=aa.idAplicativo "
        . "WHERE TRUE AND aa.idAdministrador=$idAdministrador AND hablilitado=1";


if (isset($idAplicativo) && $idAplicativo !== '') {
        $sql .= " AND $idAplicativo= $idAplicativo ";
}
if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS;";
}
//echo $sql;
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_aplicativo = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_get_aplicativo["idAplicativo"]),
        'text' => $myrow_get_aplicativo["nombre"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
