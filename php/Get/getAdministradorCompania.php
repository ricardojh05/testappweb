<?php
include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);

if (!$mysqli = getConectionDb()) {
    return $mysqli;
}

if (!isset($idCompania)) {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
    return '';
}

$sql = "SELECT ac.idAdministrador, ac.idCompania, IF(ac.hablilitado = 1, 1,0) AS habilitado, ac.observacion, CONCAT(a.nombres,' ', a.apellidos) as nombres, "
        . "ac.idAdministradorRegistro, "
        . " IF(MONTH(ac.fecha_registro) < 10 , DATE_FORMAT(ac.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ac.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro, "
        . "ac.idAdministradorHabilito, "
        . " IF(MONTH(ac.fecha_habilito) < 10 , DATE_FORMAT(ac.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ac.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito, "
        . "ac.idAdministradorDeshabilito, "
        . " IF(MONTH(ac.fecha_deshabilito) < 10 , DATE_FORMAT(ac.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ac.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito "
        . "FROM $DB_NAME.administrador_compania ac "
        . "INNER JOIN $DB_NAME.administrador a on a.idAdministrador=ac.idAdministrador "
        . "WHERE ac.idCompania = $idCompania "
        . "ORDER BY ac.idAdministrador DESC ";
//echo $sql;
$result = $mysqli->query($sql);

if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}

$arreglo = [];
while ($myrow_read_equipos = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read_equipos["habilitado"]) ? $myrow_read_equipos["fecha_habilito"] : $myrow_read_equipos["fecha_deshabilito"];
    $idUserChange = ($myrow_read_equipos["habilitado"]) ? $myrow_read_equipos["idAdministradorHabilito"] : $myrow_read_equipos["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'id' => intval($myrow_read_equipos["idAdministrador"]),
        'habilitado' => (bool) ($myrow_read_equipos["habilitado"]),
        'nombre' => $myrow_read_equipos["nombres"],
        'idUserCreate' => $myrow_read_equipos["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_equipos["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange
    );
}

$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
