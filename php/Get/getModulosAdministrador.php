<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;
if (isset($perfiles) && isset($perfiles)) {
    $sql = "SELECT 
            m.idModulo,
            m.modulo,
            IF(ma.leer = 1, 1, 0) AS leer,
            IF(ma.crear = 1, 1, 0) AS crear,
            IF(ma.editar = 1, 1, 0) AS editar,
            IF(ma.eliminar = 1, 1, 0) AS eliminar
        FROM
            $DB_NAME.modulo m
                INNER JOIN
            $DB_NAME.modulo_perfil mp ON mp.idModulo = m.idModulo
                AND mp.idPerfil IN ($perfiles)
                LEFT JOIN
            $DB_NAME.moduloadministrador ma ON ma.idModulo = m.idModulo
                AND ma.idAdministrador = $idAdmin
        GROUP BY m.idModulo
        ORDER BY m.modulo;";
//    echo $sql;
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read_modulos = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow_read_modulos["idModulo"]),
            'modulo' => $myrow_read_modulos["modulo"],
            'leer' => intval($myrow_read_modulos["leer"]),
            'crear' => intval($myrow_read_modulos["crear"]),
            'editar' => intval($myrow_read_modulos["editar"]),
            'eliminar' => intval($myrow_read_modulos["eliminar"])
        );
    }
    $mysqli->close();
    echo json_encode(array('success' => true, 'modulos' => $arreglo));
} else {
    echo json_encode(array('success' => false, 'error' => "FALTA PARAMETROS", 'mesagge' => "FALTA PARAMETROS"));
}