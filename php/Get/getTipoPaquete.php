<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return;

$sql = "SELECT idPaqueteTipo, paqueteTipo"
        . " FROM $DB_NAME.paqueteTipo "
        . " WHERE"
        . " LOWER(paqueteTipo) LIKE LOWER('%$param%')"
        . " OR LOWER(descripcion) LIKE LOWER('%$param%')"
        . " OR idPaqueteTipo = '$param'";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_COMBOS;";
}
$result = EJECUTAR_SELECT($mysqli, $sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_get_tipo_paquete = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_get_tipo_paquete["idPaqueteTipo"]),
        'text' => $myrow_get_tipo_paquete["paqueteTipo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
