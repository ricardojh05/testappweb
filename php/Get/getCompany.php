<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;
$sql = "SELECT DISTINCT e.idCompania, e.compania, e.latitud, e.longitud "
        . "FROM $DB_NAME.administrador_compania ae "
        . "INNER JOIN $DB_NAME.compania e ON e.idCompania = ae.idCompania "
        . "WHERE ae.idAdministrador=  " . $_SESSION["ID_ADMINISTRADOR"] . " AND hablilitado=TRUE";

if (isset($idCompania) && $idCompania !== '') {
    $sql .= " AND e.idCompania= $idCompania ";
}

if (isset($param) && $param !== '') {
    $sql .= " AND e.compania LIKE '$param%' ";
}

if (isset($limit)) {
    $sql .= " LIMIT $limit";
} else {
    $sql .= " LIMIT 12";
}
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_users = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_users["idCompania"]),
        'company' => $myrow_read_users["compania"],
        'text' => $myrow_read_users["compania"],
        'latitud' => $myrow_read_users["latitud"],
        'longitud' => $myrow_read_users["longitud"]
    );
}
echo json_encode(array('success' => true, 'data' => $arreglo));

$mysqli->close();
