<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (isset($param) || isset($cedula)) {
    if (!$mysqli = getConectionDb())
        return $mysqli;

    $sql = "SELECT DISTINCT a.idAdministrador, CONCAT(a.nombres,' ', a.apellidos) as nombres "
            . "FROM $DB_NAME.administrador a ";

    if ($_SESSION['PERFILES_ASIGNADOS'][1] !== false && !$_SESSION['PERFILES_ASIGNADOS'][2] && !$_SESSION['PERFILES_ASIGNADOS'][3]) {
        $sql .= " INNER JOIN $DB_NAME.vehiculo_administrador va ON va.idAdministrador = a.idAdministrador AND va.idVehiculo IN ($idsVehiculos) ";
    }
    $sql .= "WHERE ";
    if (isset($cedula)) {
        $sql .= " a.cedula = $cedula ";
    } else {
        $sql .= " (a.nombres LIKE '$param%' "
                . " OR a.apellidos LIKE '$param%' "
                . " OR a.cedula LIKE '$param%' "
                . " OR a.usuario LIKE '$param%' "
                . " OR a.idAdministrador = '$param') ";
    }

    if (isset($asignados)) {
        if ($asignados !== 'false') {
            $sql .= " AND a.idAdministrador NOT IN ($asignados) ";
        }
    }
    
    if (isset($limite)) {
        $sql .= " LIMIT $limite";
    } else {
        $sql .= " LIMIT $LIMITE_COMBOS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read_users = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow_read_users["idAdministrador"]),
            'nombres' => $myrow_read_users["nombres"]
        );
    }
    $mysqli->close();
    echo json_encode(array('success' => true, 'administradores' => $arreglo));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
