<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return;
    $data->imagen = (isset($data->imagen)) ? $data->imagen : '';
    $sql_create = "INSERT INTO $DB_NAME.lugarPunto "
            . "(idLugar, barrio, callePrincipal, calleSecundaria, referencia, latitud, longitud, imagen, habilitado, idAdministradorRegistro) "
            . "VALUES "
            . "(" . $data->idLugar . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->barrio)) . "'"
            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->principal)) . "'" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->secundaria)) . "'"
            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->referencia)) . "'" . ", '" . $data->latitud . "'"
            . ", '" . $data->longitud . "'" . ", '" . $data->imagen . "'"
            . ", " . (int) $data->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ");";
    $res = EJECUTAR_SQL($mysqli, $sql_create);
    if (isset($res['id'])) {
        if (intval($res['id']) > 0) {
            if ($data->imagen !== '') {
                $sql_update = "UPDATE $DB_NAME.lugarPunto SET imagen = '" . $res['id'] . ".png' WHERE idLugarPunto = " . $res['id'];
                EJECUTAR_SQL($mysqli, $sql_update);
            }
            echo json_encode($res);
        }
    }
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
