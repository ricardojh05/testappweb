<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return;
    if (isset($data->limite)) {
        $newCoord = str_replace(";", ",", str_replace(",", " ", $data->limite));
        $newCoord = "geomfromtext('POLYGON((" . $newCoord . "))')";
    }
    $sql_update = "UPDATE $DB_NAME.lugar SET ";
    if (isset($data->habilitado)) {
        if ((bool) $data->habilitado) {
            $sql_update .= 'idAdministradorHabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update .= 'fecha_habilito = NOW(), ';
        } else {
            $sql_update .= 'idAdministradorDeshabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update .= 'fecha_deshabilito = NOW(), ';
        }
    }
    $sql_update .= (isset($data->idAplicativo)) ? "idAplicativo = $data->idAplicativo, " : "";
    $sql_update .= (isset($data->idCiudad)) ? "idCiudad = $data->idCiudad, " : "";
    $sql_update .= (isset($data->detalle)) ? "detalle = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->detalle)) . "', " : "";
    $sql_update .= (isset($data->color)) ? "color = '$data->color', " : "";
    $sql_update .= (isset($data->descripcion)) ? "descripcion = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->descripcion)) . "', " : "";
    $sql_update .= (isset($data->limite)) ? "limite = $newCoord, " : "";
    $sql_update .= (isset($data->habilitado)) ? "habilitado = " . $data->habilitado . ", " : "";
    $sql_update .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update .= 'fecha_actualizo = NOW() ';
    $sql_update .= 'WHERE idLugar = ' . $data->id;
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update));
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}