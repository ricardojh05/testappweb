<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT 
            l.idLugar,
            l.idAplicativo,
            ap.nombre as aplicativo,
            l.idCiudad,
            c.ciudad,
            l.detalle,
            l.color,
            l.descripcion,
            REPLACE(REPLACE(REPLACE(SUBSTRING(asText(l.limite),10),'))',''),',',';'),' ',',') as limite,
            IF(l.habilitado = 0, 0, 1) AS habilitado,
            l.idAdministradorRegistro,
            IF(MONTH(l.fecha_registro) < 10 , DATE_FORMAT(l.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(l.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            l.idAdministradorHabilito,
            l.idAdministradorDeshabilito,
            IF(MONTH(l.fecha_habilito) < 10 , DATE_FORMAT(l.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(l.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(l.fecha_deshabilito) < 10 , DATE_FORMAT(l.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(l.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            l.idAdministradorActualizo,
            IF(MONTH(l.fecha_actualizo) < 10 , DATE_FORMAT(l.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(l.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.lugar l
                INNER JOIN
            $DB_NAME.aplicativos ap ON ap.id_aplicativo = l.idAplicativo";

if (isset($aplicativos)) {
    if ($aplicativos !== '') {
        $sql .= " AND l.id_aplicativo IN ($aplicativos) ";
    }
}

$sql .= "       INNER JOIN
            $DB_NAME.ciudades c ON c.id_ciudad = l.idCiudad ";

if (isset($ciudades)) {
    if ($ciudades !== '') {
        $sql .= " AND l.id_ciudad IN ($ciudades) ";
    }
}

if (isset($param)) {
    $sql .= " WHERE (LOWER(l.detalle) LIKE LOWER('%$param%') "
            . " OR (LOWER(l.descripcion) LIKE LOWER('%$param%')) ";
}


$sql .= " ORDER BY l.idLugar DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read["habilitado"]) ? $myrow_read["fecha_habilito"] : $myrow_read["fecha_deshabilito"];
    $idUserChange = ($myrow_read["habilitado"]) ? $myrow_read["idAdministradorHabilito"] : $myrow_read["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'idAplicativo' => intval($myrow_read["idAplicativo"]),
        'aplicativo' => ($myrow_read["aplicativo"]),
        'idCiudad' => intval($myrow_read["idCiudad"]),
        'ciudad' => ($myrow_read["ciudad"]),
        'id' => intval($myrow_read["idLugar"]),
        'detalle' => $myrow_read["detalle"],
        'color' => $myrow_read["color"],
        'descripcion' => $myrow_read["descripcion"],
        'limite' => $myrow_read["limite"],
        'habilitado' => intval($myrow_read["habilitado"]),
        'idUserCreate' => $myrow_read["idAdministradorRegistro"],
        'dateCreate' => $myrow_read["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
