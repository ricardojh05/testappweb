<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return;

    $sql_update = "UPDATE $DB_NAME.lugarPunto SET ";
    if (isset($data->habilitado)) {
        if ((bool) $data->habilitado) {
            $sql_update .= 'idAdministradorHabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update .= 'fecha_habilito = NOW(), ';
        } else {
            $sql_update .= 'idAdministradorDeshabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update .= 'fecha_deshabilito = NOW(), ';
        }
    }
    $sql_update .= (isset($data->barrio)) ? "barrio = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->barrio)) . "', " : "";
    $sql_update .= (isset($data->principal)) ? "callePrincipal = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->principal)) . "', " : "";
    $sql_update .= (isset($data->secundaria)) ? "calleSecundaria = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->secundaria)) . "', " : "";
    $sql_update .= (isset($data->referencia)) ? "referencia = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->referencia)) . "', " : "";
    $sql_update .= (isset($data->latitud)) ? "latitud = '$data->latitud', " : "";
    $sql_update .= (isset($data->longitud)) ? "longitud = '$data->longitud', " : "";
    $sql_update .= (isset($data->imagen)) ? "imagen = '$data->imagen', " : "";
    $sql_update .= (isset($data->habilitado)) ? "habilitado = " . $data->habilitado . ", " : "";
    $sql_update .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update .= 'fecha_actualizo = NOW() ';
    $sql_update .= 'WHERE idLugarPunto = ' . $data->id;
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update));
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}