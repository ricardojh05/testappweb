<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT 
            lp.idLugarPunto,
            lp.idLugar,
            lp.barrio,
            lp.callePrincipal,
            lp.calleSecundaria,
            lp.referencia,
            lp.latitud,
            lp.longitud,
            lp.imagen,
            IF(lp.habilitado = 0, 0, 1) AS habilitado,
            lp.idAdministradorRegistro,
            IF(MONTH(lp.fecha_registro) < 10 , DATE_FORMAT(lp.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(lp.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            lp.idAdministradorHabilito,
            lp.idAdministradorDeshabilito,
            IF(MONTH(lp.fecha_habilito) < 10 , DATE_FORMAT(lp.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(lp.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(lp.fecha_deshabilito) < 10 , DATE_FORMAT(lp.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(lp.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            lp.idAdministradorActualizo,
            IF(MONTH(lp.fecha_actualizo) < 10 , DATE_FORMAT(lp.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(lp.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.lugarPunto lp ";

if (isset($param)) {
    if ($param !== '') {
        $sql .= " WHERE (LOWER(lp.barrio) LIKE LOWER('%$param%') "
                . " OR (LOWER(lp.callePrincipal) LIKE LOWER('%$param%') "
                . " OR (LOWER(lp.calleSecundaria) LIKE LOWER('%$param%') "
                . " OR (LOWER(lp.referencia) LIKE LOWER('%$param%')) ";
    }
}

if (isset($idLugar)) {
    if ($idLugar !== '') {
        $sql .= " WHERE lp.idLugar = $idLugar ";
    }
}


$sql .= " ORDER BY lp.idLugarPunto DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read["habilitado"]) ? $myrow_read["fecha_habilito"] : $myrow_read["fecha_deshabilito"];
    $idUserChange = ($myrow_read["habilitado"]) ? $myrow_read["idAdministradorHabilito"] : $myrow_read["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'id' => intval($myrow_read["idLugarPunto"]),
        'barrio' => $myrow_read["barrio"],
        'principal' => $myrow_read["callePrincipal"],
        'secundaria' => $myrow_read["calleSecundaria"],
        'referencia' => $myrow_read["referencia"],
        'latitud' => $myrow_read["latitud"],
        'longitud' => $myrow_read["longitud"],
        'imagen' => $myrow_read["imagen"],
        'habilitado' => intval($myrow_read["habilitado"]),
        'idUserCreate' => $myrow_read["idAdministradorRegistro"],
        'dateCreate' => $myrow_read["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'data' => $arreglo));
