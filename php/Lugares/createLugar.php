<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return;
    $newCoord = str_replace(";", ",", str_replace(",", " ", $data->limite));
    $newCoord = "geomfromtext('POLYGON((" . $newCoord . "))')";
    $sql_create = "INSERT INTO $DB_NAME.lugar "
            . "(idAplicativo, idCiudad, detalle, color, descripcion, limite, habilitado, idAdministradorRegistro) "
            . "VALUES "
            . "(" . $data->idAplicativo . ", " . $data->idCiudad . ""
            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->detalle)) . "'" . ", '" . $data->color . "'"
            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->descripcion)) . "'" . ", " . $newCoord . ""
            . ", " . (int) $data->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ");";
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create));
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
