<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql = "SELECT COUNT(a.idEvaluacion) AS total"
        . " FROM $DB_NAME.evaluacion a WHERE TRUE ";

/* if (isset($idUsuario)) {
    if (isset($id)) {
        $sql .= " AND ";
    } else {
        $sql .= " WHERE ";
    }
    $sql .= " uaa.idUsuarioAccesoApi = '$idUsuario' ";
}
if (isset($idEstado)) {
    if ($idEstado == 1) {
        if (isset($id)|| isset($idUsuario)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.confirmada = 1 ";
    }
    if ($idEstado == 2) {
        if (isset($id)|| isset($idUsuario)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.validada = 1 ";
    }
} */
if (isset($param)) {
    if ($param !== '') {
        
        $sql .= " AND ( LOWER(a.nombre) LIKE LOWER('%$param%') ) ";
    }
}
$sql .= " ORDER BY a.idEvaluacion ASC ";
//echo $sql;
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
//echo $total;
if ($total > 0) {
    $sql = "SELECT a.idEvaluacion,a.nombre,a.fecha,a.opcion,a.numero,a.respuesta"
            . " FROM $DB_NAME.evaluacion a WHERE TRUE ";

/* 
    if (isset($idUsuario)) {
        if (isset($id)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.idUsuarioAccesoApi = '$idUsuario' ";
    }
    if (isset($idEstado)) {
        if ($idEstado == 1) {
            if (isset($id) || isset($idUsuario)) {
                $sql .= " AND ";
            } else {
                $sql .= " WHERE ";
            }
            $sql .= " uaa.confirmada = 1 ";
        }
        if ($idEstado == 2) {
            if (isset($id) || isset($idUsuario)) {
                $sql .= " AND ";
            } else {
                $sql .= " WHERE ";
            }
            $sql .= " uaa.validada = 1 ";
        }
    } */

    if (isset($param)) {
        if ($param !== '') {
            
            $sql .= " AND ( LOWER(a.nombre) LIKE LOWER('%$param%') ) ";
        }
    }
    $sql .= " ORDER BY a.idEvaluacion ASC ";
//echo $sql;
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS ";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
        return $mysqli->close();
    }
    $arreglo = array();
    while ($myrow = $result->fetch_assoc()) {
        $sql_plazas = "SELECT ca.idClase, ca.idEvaluacion,c.nombre "
        /* . " ca.idAdministradorRegistro, IF(MONTH(ca.fecha_registro) < 10, DATE_FORMAT(ca.fecha_registro, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ca.fecha_registro, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,"
        . " ca.idAdministradorHabilito, IF(MONTH(ca.fecha_habilito) < 10, DATE_FORMAT(ca.fecha_habilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ca.fecha_habilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,"
        . " ca.idAdministradorDeshabilito, IF(MONTH(ca.fecha_deshabilito) < 10, DATE_FORMAT(ca.fecha_deshabilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ca.fecha_deshabilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito"
 */        
        . " FROM $DB_NAME.clase_evaluacion ca"
        . " INNER JOIN $DB_NAME.clase c on c.idClase=ca.idClase "
        //. " INNER JOIN $DB_NAME.plaza c ON ca.idPlaza = c.idPlaza"
        . " WHERE ca.idEvaluacion = " . intval($myrow["idEvaluacion"])
        . " ORDER BY ca.idClase DESC";
        $resultAlumnos = $mysqli->query($sql_plazas);
        $clases = [];
        while ($myrow_read_clases = $resultAlumnos->fetch_assoc()) {
            //$fechaChange = ($myrow_read_clases["habilitado"]) ? $myrow_read_clases["fecha_habilito"] : $myrow_read_clases["fecha_deshabilito"];
            //$idUserChange = ($myrow_read_clases["habilitado"]) ? $myrow_read_clases["idAdministradorHabilito"] : $myrow_read_clases["idAdministradorDeshabilito"];
            $clases[] = array(
                'idClase' => intval($myrow_read_clases["idClase"]),
                'idEvaluacion' => $myrow_read_clases["idEvaluacion"],
                'nombre' => $myrow_read_clases["nombre"],
            );
        }
        $arreglo[] = array(
            'id' => intval($myrow["idEvaluacion"]),
            'idEvaluacion' => intval($myrow["idEvaluacion"]),
            'fecha' => $myrow["fecha"],
            'nombre' => $myrow["nombre"],
            'numero' => $myrow["numero"],
            'opcion' => $myrow["opcion"],
            'respuesta' => $myrow["respuesta"],
            'clases' => $clases
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => TRUE, 'data' => [], 'total' => 0));
}
$mysqli->close();
