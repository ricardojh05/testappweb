<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
if (!$mysqli = getConectionDb())
    return errorLogin($MSJ_ERROR_CONEXION);
extract($_POST);
$sqlU = "SELECT a.idAdministrador FROM $DB_NAME.administrador a WHERE a.usuario = '$usuario' LIMIT 1;";
//echo $sqlU;
$resultU = $mysqli->query($sqlU);
if ($resultU->num_rows > 0) {
    $myrowU = $resultU->fetch_assoc();
    $sqlC = "SELECT a.idAdministrador,a.usuario, a.contrasenia FROM $DB_NAME.administrador a WHERE a.contrasenia = MD5(CONCAT('$contrasenia' , '$TOKEN_LOGIN')) AND a.idAdministrador = '" . $myrowU['idAdministrador'] . "' LIMIT 1;";
  //  echo $sqlC;
    $result = $mysqli->query($sqlC);
    if ($result->num_rows > 0) {
        $myrow = $result->fetch_assoc();
        $sqlModulos = "SELECT IF(ma.inicio=1,1,0) AS inicio, m.modulo,m.path, m.vista FROM $DB_NAME.moduloadministrador ma INNER JOIN $DB_NAME.modulo m ON ma.idModulo = m.idModulo WHERE ma.leer = 1 AND ma.idAdministrador=" . intval($myrow["idAdministrador"]) . " ORDER BY ma.inicio DESC LIMIT 1;";
        //echo $sqlModulos;
        $resultModulos = $mysqli->query($sqlModulos);
        if ($resultModulos->num_rows <= 0) {
            echo json_encode(array('success' => false, 'message' => "SU USUARIO NO TIENE ASIGNADO MÓDULOS POR FAVOR LLAMAR A SOPORTE TÉCNICO PARA QUE SOLVENTE EL PROBLEMA"));
        } else {
            $myrowModulos = $resultModulos->fetch_assoc();
            $_SESSION["IS_SESSION"] = 1;
            $_SESSION["URL_SISTEMA"] = $myrowModulos['path'];
            $i = 0;
            echo json_encode(array('success' => true, 'MODULO' => array('INICIO' => intval($myrowModulos['inicio']), 'MODULO' => $myrowModulos['modulo'], 'PATH' => $myrowModulos['path'], 'VISTA' => $myrowModulos['vista']), 'sesion' => array('ID_ADMINISTRADOR' => intval($myrow["idAdministrador"]), 'USUARIO' => $myrow["usuario"], 'LAT_U' => $latitud, 'LNG_U' => $longitud)));
        }
    } else
        echo json_encode(array('success' => false, 'message' => "LA CONTRASEÑA QUE HA INGRESADO ES INCORRECTA."));
} else
    echo json_encode(array('success' => false, 'message' => "EL USUARIO QUE HA INGRESADO ES INCORRECTO."));
$mysqli->close();
