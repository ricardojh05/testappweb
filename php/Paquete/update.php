<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    extract($_GET);
    if (!$mysqli = getConectionDb())
        return;
    $sql_update_paquete = "UPDATE $DB_NAME.paquete SET ";
    $sql_update_paquete .= (isset($data->idAplicativo)) ? "idAplicativo = $data->idAplicativo, " : "";
    $sql_update_paquete .= (isset($data->idCiudad)) ? "idCiudad = " . $data->idCiudad . ", " : "";
    $sql_update_paquete .= (isset($data->idTipo)) ? "idPaqueteTipo = " . $data->idTipo . ", " : "";
    $sql_update_paquete .= (isset($data->nombre)) ? "nombre = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->nombre)) . "', " : "";
    $sql_update_paquete .= (isset($data->desCorta)) ? "descripcionCorta = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->desCorta)) . "', " : "";
    $sql_update_paquete .= (isset($data->desLarga)) ? "descripcionLarga = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->desLarga)) . "', " : "";
    $sql_update_paquete .= (isset($data->costo)) ? "costo = $data->costo, " : "";
    $sql_update_paquete .= (isset($data->relacion)) ? "relacion = $data->relacion, " : "";
    $sql_update_paquete .= (isset($data->relacionPorcentaje)) ? "relacionPorcentaje = $data->relacionPorcentaje, " : "";
    $sql_update_paquete .= (isset($data->habilitado)) ? "habilitado = $data->habilitado, " : "";
    if (isset($data->habilitado)) {
        if ($data->habilitado == 1) {
            $sql_update_paquete .= 'idAdministradorHabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update_paquete .= 'fecha_habilito = NOW(), ';
        } else {
            $sql_update_paquete .= 'idAdministradorDeshabilito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
            $sql_update_paquete .= 'fecha_deshabilito = NOW(), ';
        }
    }
    $sql_update_paquete .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update_paquete .= 'fecha_actualizo = NOW() ';
    $sql_update_paquete .= 'WHERE idPaquete = ' . $data->id;
    $res = json_encode(EJECUTAR_SQL($mysqli, $sql_update_paquete));
    $error = false;
    if (isset($promociones)) {
        $promociones = json_decode($promociones);
        if (count($promociones) > 0) {
            $nuevos = false;
            $actualizar = false;
            $sql_create_promocion = "INSERT INTO $DB_NAME.paquetePromocion "
                    . "(idPaquete, promocion, promocionPorcentaje, desde, hasta, habilitado, caduca, descripcion, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito) "
                    . "VALUES ";
            $sql_update_promocion = "INSERT INTO $DB_NAME.paquetePromocion  "
                    . "(idPaquetePromocion, idPaquete, promocion, promocionPorcentaje, desde, hasta, habilitado, caduca, descripcion, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito, fecha_deshabilito, idAdministradorActualizo, fecha_actualizo, idAdministradorRegistro) "
                    . "VALUES ";
            $sql_validar_fechas = "";
            foreach ($promociones as $p) {
                $sql_validar_fechas .= "
                        SELECT 
                            idPaquetePromocion
                        FROM
                            paquetePromocion
                        WHERE
                            idPaquetePromocion <> '$p->id'
                            AND idPaquete = '$data->id'
                            AND habilitado = 1
                            AND
                            (
                                    (desde >= DATE('$p->desde') AND desde <= DATE('$p->hasta'))
                                    OR 
                                    (hasta >= DATE('$p->desde') AND hasta <= DATE('$p->hasta'))
                            )
                        UNION ";
                if ((bool) $p->nuevo) {
                    $nuevos = true;
                    $sql_create_promocion .= "(" . $data->id . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->promocion)) . "'"
                            . ", " . (int) $p->promPorcentaje . "" . ", '" . $p->desde . "'"
                            . ", '" . $p->hasta . "'" . ", " . $p->habilitado . ""
                            . ", " . $p->caduca . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->descripcion)) . "'"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                            . ", NOW()"
                            . "),";
                } else {
                    $actualizar = true;
                    if ($p->habilitado) {
                        $sql_update_promocion .= "(" . $p->id . ", " . $data->id . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->promocion)) . "'"
                                . ", " . (int) $p->promPorcentaje . "" . ", '" . $p->desde . "'"
                                . ", '" . $p->hasta . "'" . ", " . $p->habilitado . ""
                                . ", " . $p->caduca . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->descripcion)) . "'"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", NULL, NULL"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                . "),";
                    } else {
                        $sql_update_promocion .= "(" . $p->id . ", " . $data->id . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->promocion)) . "'"
                                . ", " . (int) $p->promPorcentaje . "" . ", '" . $p->desde . "'"
                                . ", '" . $p->hasta . "'" . ", " . $p->habilitado . ""
                                . ", " . $p->caduca . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->descripcion)) . "'"
                                . ", NULL, NULL"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                . "),";
                    }
                }
            }

            $sql_validar_fechas = substr($sql_validar_fechas, 0, -6);
            $resultValidar = $mysqli->query($sql_validar_fechas);
            if (!isset($resultValidar->num_rows)) {
                echo json_encode(array('success' => false, 'error' => "UPS! HA OCURRIDO ALGO, INTENTELO NUEVAMENTE."));
                return $mysqli->close();
            } else {
                if ($resultValidar->num_rows === 0) {
                    if ($actualizar) {
                        $sql_update_promocion = substr($sql_update_promocion, 0, -1);
                        $sql_update_promocion .= " ON DUPLICATE KEY UPDATE idPaquetePromocion=VALUES(idPaquetePromocion),"
                                . "idPaquete=VALUES(idPaquete),"
                                . "promocion=VALUES(promocion),"
                                . "promocionPorcentaje=VALUES(promocionPorcentaje),"
                                . "desde=VALUES(desde),"
                                . "hasta=VALUES(hasta),"
                                . "caduca=VALUES(caduca),"
                                . "descripcion=VALUES(descripcion),"
                                . "habilitado=VALUES(habilitado),"
                                . "idAdministradorHabilito=VALUES(idAdministradorHabilito),"
                                . "fecha_habilito=VALUES(fecha_habilito),"
                                . "idAdministradorDeshabilito=VALUES(idAdministradorDeshabilito),"
                                . "fecha_deshabilito=VALUES(fecha_deshabilito),"
                                . "idAdministradorActualizo=VALUES(idAdministradorActualizo),"
                                . "fecha_actualizo=VALUES(fecha_actualizo);";
                        EJECUTAR_SQL($mysqli, $sql_update_promocion);
                    }
                    if ($nuevos) {
                        $sql_create_promocion = substr($sql_create_promocion, 0, -1);
                        EJECUTAR_SQL($mysqli, $sql_create_promocion);
                    }
                } else {
                    $error = true;
                    $arrayValidar = [];
                    while ($myrow_validar = $resultValidar->fetch_assoc()) {
                        $arrayValidar[] = $myrow_validar['idPaquetePromocion'];
                    }
                    echo json_encode(array('success' => false, 'error' => "EXISTE UN COMFLICTO DE FECHAS EN LA O LAS PROMOCIONES CON IDENTIFICADOR: " . implode(", ", $arrayValidar)));
                    return $mysqli->close();
                }
            }
        }
    }

    if (isset($descuentos)) {
        $descuentos = json_decode($descuentos);
        if (count($descuentos) > 0) {
            $nuevos = false;
            $actualizar = false;
            $sql_create_descuento = "INSERT INTO $DB_NAME.paqueteDescuento "
                    . "(idPaquete, relacion, relacionPorcentaje, desde, hasta, habilitado, descripcion, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito) "
                    . "VALUES ";
            $sql_update_descuento = "INSERT INTO $DB_NAME.paqueteDescuento  "
                    . "(idpaqueteDescuento, idPaquete, relacion, relacionPorcentaje, desde, hasta, habilitado, descripcion, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito, fecha_deshabilito, idAdministradorActualizo, fecha_actualizo, idAdministradorRegistro) "
                    . "VALUES ";
            $sql_validar_fechas = "";
            foreach ($descuentos as $d) {
                $sql_validar_fechas .= "
                        SELECT 
                            idpaqueteDescuento
                        FROM
                            paqueteDescuento
                        WHERE
                            idpaqueteDescuento <> '$d->id'
                            AND idPaquete = '$data->id'
                            AND habilitado = 1
                            AND
                            (
                                (desde >= DATE('$d->desde') AND desde <= DATE('$d->hasta'))
                                OR 
                                (hasta >= DATE('$d->desde') AND hasta <= DATE('$d->hasta'))
                            )
                                UNION ";
                if ((bool) $d->nuevo) {
                    $nuevos = true;
                    $sql_create_descuento .= "(" . $data->id . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->relacion)) . "'"
                            . ", " . (int) $d->relacionPorcentaje . "" . ", '" . $d->desde . "'"
                            . ", '" . $d->hasta . "'" . ", " . $d->habilitado . ""
                            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->descripcion)) . "'"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                            . ", NOW()"
                            . "),";
                } else {
                    $actualizar = true;
                    if ($d->habilitado) {
                        $sql_update_descuento .= "(" . $d->id . ", " . $data->id . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->relacion)) . "'"
                                . ", " . (int) $d->relacionPorcentaje . "" . ", '" . $d->desde . "'"
                                . ", '" . $d->hasta . "'" . ", " . $d->habilitado . ""
                                . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->descripcion)) . "'"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", NULL, NULL"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                . "),";
                    } else {
                        $sql_update_descuento .= "(" . $d->id . ", " . $data->id . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->relacion)) . "'"
                                . ", " . (int) $d->relacionPorcentaje . "" . ", '" . $d->desde . "'"
                                . ", '" . $d->hasta . "'" . ", " . $d->habilitado . ""
                                . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->descripcion)) . "'"
                                . ", NULL, NULL"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                                . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                . "),";
                    }
                }
            }
            $sql_validar_fechas = substr($sql_validar_fechas, 0, -6);
            $resultValidar = $mysqli->query($sql_validar_fechas);
            if (!isset($resultValidar->num_rows)) {
                echo json_encode(array('success' => false, 'error' => "UPS! HA OCURRIDO ALGO, INTENTELO NUEVAMENTE."));
                return $mysqli->close();
            } else {
                if ($resultValidar->num_rows === 0) {
                    if ($actualizar) {
                        $sql_update_descuento = substr($sql_update_descuento, 0, -1);
                        $sql_update_descuento .= " ON DUPLICATE KEY UPDATE idpaqueteDescuento=VALUES(idpaqueteDescuento),"
                                . "idPaquete=VALUES(idPaquete),"
                                . "relacion=VALUES(relacion),"
                                . "relacionPorcentaje=VALUES(relacionPorcentaje),"
                                . "desde=VALUES(desde),"
                                . "hasta=VALUES(hasta),"
                                . "descripcion=VALUES(descripcion),"
                                . "habilitado=VALUES(habilitado),"
                                . "idAdministradorHabilito=VALUES(idAdministradorHabilito),"
                                . "fecha_habilito=VALUES(fecha_habilito),"
                                . "idAdministradorDeshabilito=VALUES(idAdministradorDeshabilito),"
                                . "fecha_deshabilito=VALUES(fecha_deshabilito),"
                                . "idAdministradorActualizo=VALUES(idAdministradorActualizo),"
                                . "fecha_actualizo=VALUES(fecha_actualizo);";
                        EJECUTAR_SQL($mysqli, $sql_update_descuento);
                    }
                    if ($nuevos) {
                        $sql_create_descuento = substr($sql_create_descuento, 0, -1);
                        EJECUTAR_SQL($mysqli, $sql_create_descuento);
                    }
                } else {
                    $error = true;
                    $arrayValidar = [];
                    while ($myrow_validar = $resultValidar->fetch_assoc()) {
                        $arrayValidar[] = $myrow_validar['idpaqueteDescuento'];
                    }
                    echo json_encode(array('success' => false, 'error' => "EXISTE UN COMFLICTO DE FECHAS EN EL O LOS DESCUENTOS CON IDENTIFICADOR: " . implode(", ", $arrayValidar)));
                    return $mysqli->close();
                }
            }
        }
    }
    if (!isset($promociones) && !isset($descuentos) && !error) {
        echo $res;
    }
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'error' => "FALTAN PARÁMETROS"));
}