<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
extract($_GET);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return;

    (isset($data->habilitado)) ? '' : $data->habilitado = 0;
    (isset($data->relacionPorcentaje)) ? '' : $data->relacionPorcentaje = 0;

    if ((bool) $data->habilitado) {
        $idHabilitado = 'idAdministradorHabilito';
        $fHabilitado = 'fecha_habilito';
    } else {
        $idHabilitado = 'idAdministradorDeshabilito';
        $fHabilitado = 'fecha_deshabilito';
    }

    $sql_create_paquete = "INSERT INTO $DB_NAME.paquete "
            . "(idAplicativo, idCiudad, "
            . "idPaqueteTipo, nombre,"
            . "descripcionCorta,descripcionLarga, "
            . "costo,relacion, "
            . "relacionPorcentaje,habilitado, "
            . "idAdministradorRegistro, "
            . "$idHabilitado, $fHabilitado)"
            . " VALUES "
            . "(" . $data->idAplicativo . ", " . $data->idCiudad . ""
            . ", " . $data->idTipo . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->nombre)) . "'"
            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->desCorta)) . "'" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->desLarga)) . "'"
            . ", " . $data->costo . "" . ", " . $data->relacion . ""
            . ", " . $data->relacionPorcentaje . "" . ", " . $data->habilitado . ""
            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
            . ")";
    $res =  EJECUTAR_SQL($mysqli, $sql_create_paquete);
    if (isset($res['id'])) {
        if ($res['id'] > 0) {
            if (isset($promociones)) {
                $promociones = json_decode($promociones);
                if (count($promociones) > 0) {
                    $sql_create_promocion = "INSERT INTO $DB_NAME.paquetePromocion "
                            . "(idPaquete, promocion, promocionPorcentaje, desde, hasta, habilitado, caduca, descripcion, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito, fecha_deshabilito) "
                            . "VALUES ";
                    foreach ($promociones as $p) {
                        if ((bool) $p->habilitado) {
                            $nuevos = true;
                            $sql_create_promocion .= "(" . $res['id'] . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->promocion)) . "'"
                                    . ", " . $p->promPorcentaje . "" . ", '" . $p->desde . "'"
                                    . ", '" . $p->hasta . "'" . ", " . $p->habilitado . ""
                                    . ", " . $p->caduca . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->descripcion)) . "'"
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"] . "" . ", NOW()"
                                    . ", NULL, NULL"
                                    . "),";
                        } else {
                            $sql_create_promocion .= "(" . $res['id'] . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->promocion)) . "'"
                                    . ", " . $p->promPorcentaje . "" . ", '" . $p->desde . "'"
                                    . ", '" . $p->hasta . "'" . ", " . $p->habilitado . ""
                                    . ", " . $p->caduca . "" . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($p->descripcion)) . "'"
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                    . ", NULL, NULL"
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"] . "" . ", NOW()"
                                    . "),";
                        }
                    }
                    if ($nuevos) {
                        $sql_create_promocion = substr($sql_create_promocion, 0, -1);
                        EJECUTAR_SQL($mysqli, $sql_create_promocion);
                    }
                }
            }

            if (isset($descuentos)) {
                $descuentos = json_decode($descuentos);
                if (count($descuentos) > 0) {
                    $sql_create_descuento = "INSERT INTO $DB_NAME.paqueteDescuento "
                            . "(idPaquete, relacion, relacionPorcentaje, desde, hasta, habilitado, descripcion, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito, fecha_deshabilito) "
                            . "VALUES ";
                    foreach ($descuentos as $d) {
                        if ((bool) $d->habilitado) {
                            $sql_create_descuento .= "(" . $res['id'] . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->relacion)) . "'"
                                    . ", " . $d->relacionPorcentaje . "" . ", '" . $d->desde . "'"
                                    . ", '" . $d->hasta . "'" . ", " . $d->habilitado . ""
                                    . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->descripcion)) . "'"
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"] . "" . ", NOW()"
                                    . ", NULL, NULL"
                                    . "),";
                        } else {
                            $sql_create_descuento .= "(" . $res['id'] . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->relacion)) . "'"
                                    . ", " . $d->relacionPorcentaje . "" . ", '" . $d->desde . "'"
                                    . ", '" . $d->hasta . "'" . ", " . $d->habilitado . ""
                                    . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($d->descripcion)) . "'"
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"]
                                    . ", NULL, NULL"
                                    . ", " . $_SESSION["ID_ADMINISTRADOR"] . "" . ", NOW()"
                                    . "),";
                        }
                    }
                    $sql_create_descuento = substr($sql_create_descuento, 0, -1);
                    EJECUTAR_SQL($mysqli, $sql_create_descuento);
                }
            }
            echo json_encode($res);
        } else {
            echo json_encode($res);
        }
    } else {
        echo json_encode($res);
    }
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'error' => "FALTAN PARÁMETROS"));
}
