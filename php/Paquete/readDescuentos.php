<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT 
            d.idPaquete,
            d.idpaqueteDescuento,
            d.relacion,
            IF(d.relacionPorcentaje = 0, 0, 1) AS relacionPorcentaje,
            d.desde,
            d.hasta,
            d.descripcion,
            IF(d.habilitado = 0, 0, 1) AS habilitado,
            d.idAdministradorRegistro,
            IF(MONTH(d.fecha_registro) < 10 , DATE_FORMAT(d.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(d.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            d.idAdministradorHabilito,
            d.idAdministradorDeshabilito,
            IF(MONTH(d.fecha_habilito) < 10 , DATE_FORMAT(d.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(d.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(d.fecha_deshabilito) < 10 , DATE_FORMAT(d.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(d.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            d.idAdministradorActualizo,
            IF(MONTH(d.fecha_actualizo) < 10 , DATE_FORMAT(d.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(d.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.paqueteDescuento d
                WHERE TRUE ";

if (isset($param)) {
    $sql .= " AND idPaquete = $param ";
}
$sql .= " ORDER BY d.idpaqueteDescuento DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_descuentos = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read_descuentos["habilitado"]) ? $myrow_read_descuentos["fecha_habilito"] : $myrow_read_descuentos["fecha_deshabilito"];
    $idUserChange = ($myrow_read_descuentos["habilitado"]) ? $myrow_read_descuentos["idAdministradorHabilito"] : $myrow_read_descuentos["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'idPaquete' => intval($myrow_read_descuentos["idPaquete"]),
        'id' => intval($myrow_read_descuentos["idpaqueteDescuento"]),
        'relacion' => $myrow_read_descuentos["relacion"],
        'relacionPorcentaje' => intval($myrow_read_descuentos["relacionPorcentaje"]),
        'desde' => $myrow_read_descuentos["desde"],
        'hasta' => $myrow_read_descuentos["hasta"],
        'fInicio' => $myrow_read_descuentos["desde"],
        'hInicio' => $myrow_read_descuentos["desde"],
        'fFin' => $myrow_read_descuentos["hasta"],
        'hFin' => $myrow_read_descuentos["hasta"],
        'descripcion' => $myrow_read_descuentos["descripcion"],
        'habilitado' => intval($myrow_read_descuentos["habilitado"]),
        'idUserCreate' => $myrow_read_descuentos["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_descuentos["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read_descuentos["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read_descuentos["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'descuentos' => $arreglo));
