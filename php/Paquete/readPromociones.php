<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT 
            p.idPaquete,
            p.idPaquetePromocion,
            p.promocion,
            IF(p.promocionPorcentaje = 0, 0, 1) AS promocionPorcentaje,
            p.desde,
            p.hasta,
            p.caduca,
            p.descripcion,
            IF(p.habilitado = 0, 0, 1) AS habilitado,
            p.idAdministradorRegistro,
            IF(MONTH(p.fecha_registro) < 10 , DATE_FORMAT(p.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            p.idAdministradorHabilito,
            p.idAdministradorDeshabilito,
            IF(MONTH(p.fecha_habilito) < 10 , DATE_FORMAT(p.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(p.fecha_deshabilito) < 10 , DATE_FORMAT(p.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            p.idAdministradorActualizo,
            IF(MONTH(p.fecha_actualizo) < 10 , DATE_FORMAT(p.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.paquetePromocion p
                WHERE TRUE ";

if (isset($param)) {
    $sql .= " AND idPaquete = $param ";
}
$sql .= " ORDER BY p.idPaquetePromocion DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}

$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_promociones = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read_promociones["habilitado"]) ? $myrow_read_promociones["fecha_habilito"] : $myrow_read_promociones["fecha_deshabilito"];
    $idUserChange = ($myrow_read_promociones["habilitado"]) ? $myrow_read_promociones["idAdministradorHabilito"] : $myrow_read_promociones["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'idPaquete' => intval($myrow_read_promociones["idPaquete"]),
        'id' => intval($myrow_read_promociones["idPaquetePromocion"]),
        'promocion' => $myrow_read_promociones["promocion"],
        'promPorcentaje' => intval($myrow_read_promociones["promocionPorcentaje"]),
        'desde' => $myrow_read_promociones["desde"],
        'hasta' => $myrow_read_promociones["hasta"],
        'fInicio' => $myrow_read_promociones["desde"],
        'hInicio' => $myrow_read_promociones["desde"],
        'fFin' => $myrow_read_promociones["hasta"],
        'hFin' => $myrow_read_promociones["hasta"],
        'caduca' => $myrow_read_promociones["caduca"],
        'descripcion' => $myrow_read_promociones["descripcion"],
        'habilitado' => intval($myrow_read_promociones["habilitado"]),
        'idUserCreate' => $myrow_read_promociones["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_promociones["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read_promociones["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read_promociones["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'promociones' => $arreglo));
