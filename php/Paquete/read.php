<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT 
            p.idPaquete,
            p.idAplicativo,
            ap.nombre as aplicativo,
            p.idCiudad,
            c.ciudad,
            p.idPaqueteTipo,
            pt.paqueteTipo,
            p.nombre,
            p.descripcionCorta,
            p.descripcionLarga,
            p.costo,
            p.relacion,
            IF(p.relacionPorcentaje = 0, 0, 1) AS relacionPorcentaje,
            IF(p.habilitado = 0, 0, 1) AS habilitado,
            p.idAdministradorRegistro,
            IF(MONTH(p.fecha_registro) < 10 , DATE_FORMAT(p.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,
            p.idAdministradorHabilito,
            p.idAdministradorDeshabilito,
            IF(MONTH(p.fecha_habilito) < 10 , DATE_FORMAT(p.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,
            IF(MONTH(p.fecha_deshabilito) < 10 , DATE_FORMAT(p.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito,
            p.idAdministradorActualizo,
            IF(MONTH(p.fecha_actualizo) < 10 , DATE_FORMAT(p.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(p.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo
        FROM
            $DB_NAME.paquete p
                INNER JOIN
            $DB_NAME.aplicativos ap ON ap.id_aplicativo = p.idAplicativo
                INNER JOIN
            $DB_NAME.ciudades c ON c.id_ciudad = p.idCiudad
                INNER JOIN
            $DB_NAME.paqueteTipo pt ON pt.idPaqueteTipo = p.idPaqueteTipo
                WHERE TRUE ";

if (isset($param)) {
    $sql .= " AND (LOWER(p.nombre) LIKE LOWER('%$param%') "
            . " OR LOWER(p.descripcionCorta) LIKE LOWER('%$param%')"
            . " OR LOWER(p.descripcionLarga) LIKE LOWER('%$param%')"
            . ") ";
}
if (isset($aplicativos)) {
    if ($aplicativos !== '') {
        $sql .= " AND p.idAplicativo IN ($aplicativos) ";
    }
}
if (isset($ciudades)) {
    if ($ciudades !== '') {
        $sql .= " AND p.idCiudad IN ($ciudades) ";
    }
}
if (isset($tipos)) {
    if ($tipos !== '') {
        $sql .= " AND p.idPaqueteTipo IN ($tipos) ";
    }
}
$sql .= " ORDER BY p.idPaquete DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
//echo $sql;
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_paquete = $result->fetch_assoc()) {
    $fechaChange = ($myrow_read_paquete["habilitado"]) ? $myrow_read_paquete["fecha_habilito"] : $myrow_read_paquete["fecha_deshabilito"];
    $idUserChange = ($myrow_read_paquete["habilitado"]) ? $myrow_read_paquete["idAdministradorHabilito"] : $myrow_read_paquete["idAdministradorDeshabilito"];
    $arreglo[] = array(
        'idAplicativo' => intval($myrow_read_paquete["idAplicativo"]),
        'aplicativo' => ($myrow_read_paquete["aplicativo"]),
        'idCiudad' => intval($myrow_read_paquete["idCiudad"]),
        'ciudad' => ($myrow_read_paquete["ciudad"]),
        'idTipo' => intval($myrow_read_paquete["idPaqueteTipo"]),
        'tipo' => ($myrow_read_paquete["paqueteTipo"]),
        'id' => intval($myrow_read_paquete["idPaquete"]),
        'nombre' => $myrow_read_paquete["nombre"],
        'desCorta' => $myrow_read_paquete["descripcionCorta"],
        'desLarga' => $myrow_read_paquete["descripcionLarga"],
        'costo' => floatval($myrow_read_paquete["costo"]),
        'relacion' => floatval($myrow_read_paquete["relacion"]),
        'relacionPorcentaje' => intval($myrow_read_paquete["relacionPorcentaje"]),
        'habilitado' => intval($myrow_read_paquete["habilitado"]),
        'idUserCreate' => $myrow_read_paquete["idAdministradorRegistro"],
        'dateCreate' => $myrow_read_paquete["fecha_registro"],
        'idUserChange' => $idUserChange,
        "dateChange" => $fechaChange,
        'idUserUpdate' => $myrow_read_paquete["idAdministradorActualizo"],
        'dateUpdate' => $myrow_read_paquete["fecha_actualizo"]
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'paquetes' => $arreglo));
