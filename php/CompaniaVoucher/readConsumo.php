<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    if (isset($idSucursal)) {
        $sql_total = "SELECT COUNT(d.idSucursal) AS total "
        . " FROM $DB_NAME.departamento d"
        . " INNER JOIN $DB_NAME.sucursalDepartamento sd ON sd.idDepartamento = d.idDepartamento"
        . " LEFT JOIN $DB_NAME.departamentoCredito dc ON dc.idDepartamento = d.idDepartamento AND dc.anio= $anio AND dc.mes = $mes"
        . " WHERE sd.idSucursal = $idSucursal ";

  //echo $sql_total;
    $result = $mysqli->query($sql_total);
        $myrow = $result->fetch_assoc();
    $total = intval($myrow['total']);

        $sql = "SELECT d.departamento, dc.consumido, dc.asignado, dc.credito"
                . " FROM $DB_NAME.departamento d"
                . " INNER JOIN $DB_NAME.sucursalDepartamento sd ON sd.idDepartamento = d.idDepartamento"
                . " LEFT JOIN $DB_NAME.departamentoCredito dc ON dc.idDepartamento = d.idDepartamento AND dc.anio= $anio AND dc.mes = $mes"
                . " WHERE sd.idSucursal = $idSucursal ";
        if (isset($limit)) {
                $inicio = intval($limit) * (intval($page) - 1);
                $sql .= " LIMIT $inicio, $limit ";
            } else {
                $sql .= " LIMIT $LIMITE_REGISTROS ";
            }
                
        $arreglo = [];
        $result = $mysqli->query($sql);
        while ($myrow = $result->fetch_assoc()) {
            $arreglo[] = array(
                'departamento' => $myrow["departamento"],
                'credito' => doubleval($myrow['credito']),
                'asignado' => doubleval($myrow['asignado']),
                'consumido' => doubleval($myrow['consumido'])
            );
        }
        echo json_encode(array('success' => true, 'data' => $arreglo,'total' => $total));
        $mysqli->close();
    }
}    