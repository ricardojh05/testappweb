<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $idAdministrador=$_SESSION["ID_ADMINISTRADOR"];
    $sqlConsultaCompania="SELECT idCompania FROM $DB_NAME.administrador_compania WHERE idAdministrador=$idAdministrador AND hablilitado=1 LIMIT 1";
//    echo $sqlConsultaCompania;
    $result = $mysqli->query($sqlConsultaCompania);
    $myrow = $result->fetch_assoc();
    $idCompaniaVoucher = intval($myrow['idCompania']);

    echo json_encode(array('success' => true,'idCompaniaVoucher'=>$idCompaniaVoucher));
    $mysqli->close();
}   