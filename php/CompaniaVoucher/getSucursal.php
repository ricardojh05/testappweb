<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $sql_total = "SELECT COUNT(s.idSucursal) AS total "
            . "FROM $DB_NAME.companiaSucursal cs "
            . "LEFT JOIN $DB_NAME.sucursal s ON s.idSucursal = cs.idSucursal "
            . "WHERE cs.idCompania = $idCompaniaVoucher";
    if (isset($params) && ($params !== '')) {
        $sql_total .= " AND ( LOWER(s.sucursal) LIKE LOWER('$params%'))";
    }
         // echo $sql_total;
    $result = $mysqli->query($sql_total);
        $myrow = $result->fetch_assoc();
    $total = intval($myrow['total']);

    $sql = "SELECT cc.idSucursal,cc.idCompaniaSucursal,cc.idCompania ,c.sucursal,"
            . "IFNULL(ccc.credito,0) AS creditoMes,IFNULL(ccc.consumo,0) AS consumoMes,ccc.anio,ccc.mes,"
            . "IF(cc.habilitado=1,1,0) AS habilitado,cc.idAdministradorRegistro,"
            . "IF(MONTH(cc.fecha_registro)<10,DATE_FORMAT(cc.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'),DATE_FORMAT(cc.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,cc.idAdministradorActualizo,"
            . "IF(MONTH(cc.fecha_actualizo)<10,DATE_FORMAT(cc.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'),DATE_FORMAT(cc.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo,cc.idAdministradorHabilito,"
            . "IF(MONTH(cc.fecha_habilito)<10,DATE_FORMAT(cc.fecha_habilito,'%Y-0%c-%dT%H:%i:%s.000Z'),DATE_FORMAT(cc.fecha_habilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,cc.idAdministradorDeshabilito,"
            . "IF(MONTH(cc.fecha_deshabilito)<10,DATE_FORMAT(cc.fecha_deshabilito,'%Y-0%c-%dT%H:%i:%s.000Z'),DATE_FORMAT(cc.fecha_deshabilito,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito"
            . " FROM $DB_NAME.companiaSucursal cc"
            . " LEFT JOIN $DB_NAME.sucursal c ON cc.idSucursal=c.idSucursal"
            . " LEFT JOIN $DB_NAME.companiaSucursalConsumoMes ccc ON ccc.idCompaniaSucursal=cc.idCompaniaSucursal AND ccc.anio=$anio AND ccc.mes=$mes"
            . " WHERE cc.idCompania=$idCompaniaVoucher";

            
             if (isset($params) && ($params !== '')) {
                $sql .= " AND ( LOWER(c.sucursal) LIKE LOWER('$params%'))";
            }
            
            if (isset($limit)) {
                $inicio = intval($limit) * (intval($page) - 1);
                $sql .= " LIMIT $inicio, $limit ";
            } else {
                $sql .= " LIMIT $LIMITE_REGISTROS ";
            }
             //echo $sql;
    $result = $mysqli->query($sql);
    $objJson = "{data: [";
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {
        $fechaChange = ($myrow["habilitado"]) ? $myrow["fecha_habilito"] : $myrow["fecha_deshabilito"];
        $idUserChange = ($myrow["habilitado"]) ? $myrow["idAdministradorHabilito"] : $myrow["idAdministradorDeshabilito"];
        $arreglo[] = array(
            'idSucursal' => intval($myrow["idSucursal"]),
            'idCompaniaSucursal' => intval($myrow["idCompaniaSucursal"]),
            'id' => intval($myrow["idSucursal"]),
            'idCompaniaVoucher' => intval($myrow["idCompania"]),
            'anio' => intval($myrow["anio"]),
            'mes' => intval($myrow["mes"]),
            'sucursal' => $myrow["sucursal"],
            'creditoMes' => doubleval($myrow['creditoMes']),
            'consumoMes' => doubleval($myrow['consumoMes']),
            'idUserCreate' => $myrow["idAdministradorRegistro"],
            'dateCreate' => $myrow["fecha_registro"],
            'idUserChange' => $idUserChange,
            'dateChange' => $fechaChange,
            'idUserUpdate' => $myrow["idAdministradorActualizo"],
            "dateUpdate" => $myrow["fecha_actualizo"]
        );
    }

    echo json_encode(array('success' => true, 'data' => $arreglo, 'total' => $total));
    $mysqli->close();
}

