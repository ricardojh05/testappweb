<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $sqlBuscarSucursal = "SELECT c.id_cliente AS id, c.id_aplicativo AS idApp, c.id_ciudad AS idC, app.nombre AS app,"
            . " CONCAT(c.nombres, ' ', c.apellidos) AS cliente,c.nombres,c.apellidos, c.correo, c.cedula, c.celular"
            . " FROM clientes c"
            . " LEFT JOIN ciudades ci ON ci.id_ciudad = c.id_ciudad"
            . " INNER JOIN aplicativos app ON c.id_aplicativo = app.id_aplicativo"
            . " WHERE c.cedula = '$cedula' AND c.id_aplicativo=$idAplicativo "
            . " ORDER BY c.fecha_hora_registro"
            . " DESC LIMIT $limit";
    $result = $mysqli->query($sqlBuscarSucursal);
    $objJson = "{data: [";
    while ($myrow = $result->fetch_assoc()) {
        $objJson .= "{"
                . "id:'" . intval($myrow['id']) . "',"//id cliente
                . "idApp:" . intval($myrow['idApp']) . ","//id app
                . "idC:'" . intval($myrow['idC']) . "',"//id ciudad
                . "app:'" . utf8_encode($myrow['app']) . "',"//aplicativo
                . "cliente:'" . preg_replace("[\n|\r|\n\r\t|']", "\\n", utf8_encode($myrow['cliente'])) . "',"
                . "nombres:'" . $myrow['nombres'] . "',"
                . "apellidos:'" . $myrow['apellidos'] . "',"
                . "correo:'" . $myrow['correo'] . "',"
                . "cedula:'" . $myrow['cedula'] . "',"
                . "celular:'" . $myrow['celular'] . "'"
                . "},";
    }
    $objJson .= "]}";
    echo $objJson;
    $mysqli->close();
}


