<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $sqlConsultaCompania="SELECT idCompania FROM $DB_NAME.administrador_compania WHERE hablilitado=1 LIMIT 1";
//    echo $sqlConsultaCompania;
    $result = $mysqli->query($sqlConsultaCompania);
    $myrow = $result->fetch_assoc();
    $idAdminVoucher = intval($myrow['idCompania']);

    echo json_encode(array('success' => true,'idAdminVoucher'=>$idAdminVoucher));
    $mysqli->close();
}   