<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $sql_total = "SELECT COUNT(c.idCompania) AS total "
            . "FROM $DB_NAME.compania c "
            . "WHERE TRUE";
    if (isset($params) && ($params !== '')) {
        $sql_total .= " AND ( LOWER(c.compania) LIKE LOWER('$params%'))";
    }

         //echo $sql_total;
    $result = $mysqli->query($sql_total);
        $myrow = $result->fetch_assoc();
    $total = intval($myrow['total']);

    $sql="SELECT c.idCompania,c.compania,IFNULL(cc.credito, 0) AS creditoMes,"
            . "IFNULL(cc.consumido, 0) AS consumoMes,cc.anio,cc.mes "
            . "FROM $DB_NAME.compania c "
            . "LEFT JOIN $DB_NAME.companiaCredito cc ON cc.idCompania = c.idCompania AND cc.anio = $anio AND cc.mes = $mes "
            . "WHERE TRUE ";
           
             if (isset($params) && ($params !== '')) {
                $sql .= " AND ( LOWER(c.compania) LIKE LOWER('$params%'))";
            }
            
            if (isset($limit)) {
                $inicio = intval($limit) * (intval($page) - 1);
                $sql .= " LIMIT $inicio, $limit ";
            } else {
                $sql .= " LIMIT $LIMITE_REGISTROS ";
            }
             //echo $sql;
    $result = $mysqli->query($sql);
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {
       /*  $fechaChange = ($myrow["habilitado"]) ? $myrow["fecha_habilito"] : $myrow["fecha_deshabilito"];
        $idUserChange = ($myrow["habilitado"]) ? $myrow["idAdministradorHabilito"] : $myrow["idAdministradorDeshabilito"]; */
        $arreglo[] = array(
            'idCompania' => intval($myrow["idCompania"]),
            'id' => intval($myrow["idCompania"]),
            'idCompaniaVoucher' => intval($myrow["idCompania"]),
            'anio' => intval($myrow["anio"]),
            'mes' => intval($myrow["mes"]),
            'compania' => $myrow["compania"],
            'creditoMes' => doubleval($myrow['creditoMes']),
            'consumoMes' => doubleval($myrow['consumoMes']),
            /* 'idUserCreate' => $myrow["idAdministradorRegistro"],
            'dateCreate' => $myrow["fecha_registro"],
            'idUserChange' => $idUserChange,
            'dateChange' => $fechaChange,
            'idUserUpdate' => $myrow["idAdministradorActualizo"],
            "dateUpdate" => $myrow["fecha_actualizo"] */
        );
    }

    echo json_encode(array('success' => true, 'data' => $arreglo, 'total' => $total));
    $mysqli->close();
}

