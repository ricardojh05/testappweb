<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    if (isset($idCompania)) {
        $sql_total = "SELECT COUNT(d.idSucursal) AS total "
        . " FROM $DB_NAME.sucursal d"
        . " INNER JOIN $DB_NAME.companiaSucursal sd ON sd.idSucursal = d.idSucursal"
        . " LEFT JOIN $DB_NAME.sucursalCredito dc ON dc.idSucursal = d.idSucursal AND dc.anio= $anio AND dc.mes = $mes"
        . " WHERE sd.idCompania = $idCompania ";

  //echo $sql_total;
    $result = $mysqli->query($sql_total);
        $myrow = $result->fetch_assoc();
    $total = intval($myrow['total']);

        $sql = "SELECT d.sucursal, dc.consumido, dc.asignado, dc.credito"
                . " FROM $DB_NAME.sucursal d"
                . " INNER JOIN $DB_NAME.companiaSucursal sd ON sd.idSucursal = d.idSucursal"
                . " LEFT JOIN $DB_NAME.sucursalCredito dc ON dc.idSucursal = d.idSucursal AND dc.anio= $anio AND dc.mes = $mes"
                . " WHERE sd.idCompania = $idCompania ";
        if (isset($limit)) {
                $inicio = intval($limit) * (intval($page) - 1);
                $sql .= " LIMIT $inicio, $limit ";
            } else {
                $sql .= " LIMIT $LIMITE_REGISTROS ";
            }
                
        $arreglo = [];
        $result = $mysqli->query($sql);
        while ($myrow = $result->fetch_assoc()) {
            $arreglo[] = array(
                'sucursal' => $myrow["sucursal"],
                'credito' => doubleval($myrow['credito']),
                'asignado' => doubleval($myrow['asignado']),
                'consumido' => doubleval($myrow['consumido'])
            );
        }
        echo json_encode(array('success' => true, 'data' => $arreglo,'total' => $total));
        $mysqli->close();
    }
}    