<?php

include("../../php/login/isLogin.php");
include ('../../dll/config.php');
include ('../../dll/funciones.php');
extract($_GET);
if (!$mysqli = getConectionDb()) {
    echo "{success: false, message: '$errorConexion'}";
} else {
    $sqlCreditoCom = "SELECT cCr.idCompania, "
            . " cCr.anio, cCr.mes, cCr.credito,cCr.asignado,cCr.consumido"
            . " FROM $DB_NAME.companiaCredito cCr";
    $sqlCreditoCom .= " WHERE cCr.idCompania = '$idCompania'";
    if (isset($mes)) {
        $sqlCreditoCom .= " AND cCr.mes = $mes";
        $consumo = 0;
    }
    if (isset($anio)) {
        $sqlCreditoCom .= " AND cCr.anio = $anio";
        $consumo = 0;
    }
//    echo $sqlCreditoCom;
    $result = $mysqli->query($sqlCreditoCom);
    $arreglo = [];
    while ($myrow = $result->fetch_assoc()) {   
            $arreglo[] = array(
        'idCompania' => intval($myrow["idCompania"]),
        'mes' => $myrow["mes"],
        'anio' => $myrow["anio"],
        'credito' => doubleval($myrow['credito']),
        'consumo' => doubleval($myrow['consumido']),
        'asignado' => doubleval($myrow['asignado']),
        'saldo'=>  number_format(doubleval($myrow['credito']) - $consumo, 2)      
    );
    }
    echo json_encode(array('success' => true, 'data' => $arreglo));
    $mysqli->close();
}

