<?php

include '../../dll/config.php';
include '../../dll/funciones.php';

extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql = "SELECT lck.id_label_config_ktaxi AS id, lck.nombre, lck.descripcion, lck.idAdministradorRegistro,"
        . "IF(MONTH(lck.fecha_registro) < 10 , DATE_FORMAT(lck.fecha_registro,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(lck.fecha_registro,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro, "
        . "lck.idAdministradorActualizo, IF(MONTH(lck.fecha_actualizo) < 10 , DATE_FORMAT(lck.fecha_actualizo,'%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(lck.fecha_actualizo,'%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_actualizo "
        . " FROM $DB_NAME.label_config_ktaxi lck ";

if (isset($param)) {
    $sql .= " WHERE lck.nombre LIKE LOWER('%$param%') "
            . " OR lck.nombre LIKE ('%" . ucwords($param) . "%') ";
}
$sql .= " ORDER BY lck.id_label_config_ktaxi DESC ";

if (isset($limite)) {
    $sql .= " LIMIT $limite";
} else {
    $sql .= " LIMIT $LIMITE_REGISTROS";
}
//echo $sql;
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
    return $mysqli->close();
}
$arreglo = [];
while ($myrow_read_label_config = $result->fetch_assoc()) {
    $arreglo[] = array(
        'id' => intval($myrow_read_label_config["id"]),
        'nombre' => ($myrow_read_label_config["nombre"]),
        'descripcion' => ($myrow_read_label_config["descripcion"]),
        'idUserCreate' => intval($myrow_read_label_config["idAdministradorRegistro"]),
        'dateCreate' => ($myrow_read_label_config["fecha_registro"]),
        'idUserUpdate' => intval($myrow_read_label_config["idAdministradorActualizo"]),
        'dateUpdate' => ($myrow_read_label_config["fecha_actualizo"])
    );
}
$mysqli->close();
echo json_encode(array('success' => true, 'labels' => $arreglo));
