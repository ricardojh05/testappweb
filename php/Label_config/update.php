<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$dataRecords = json_decode(file_get_contents('php://input'));
extract($_GET);
if (!$mysqli = getConectionDb())
    return;
$sql_update_label_config = "";
if (count($dataRecords) > 1) {
    $j = 0;
    for ($i = 0; $i < count($dataRecords); $i++) {
        $sql_update_label_config = " UPDATE $DB_NAME.label_config_ktaxi SET ";
        $sql_update_label_config .= (isset($dataRecords[$i]->nombre)) ? "nombre = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($dataRecords[$i]->nombre)) . "', " : "";
        $sql_update_label_config .= (isset($dataRecords[$i]->descripcion)) ? "descripcion = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($dataRecords[$i]->descripcion)) . "', " : "";
        $sql_update_label_config .= ' idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
        $sql_update_label_config .= ' fecha_actualizo = NOW() ';
        $sql_update_label_config .= ' WHERE id_label_config_ktaxi = ' . $dataRecords[$i]->id . '; ';
        $res = EJECUTAR_SQL($mysqli, $sql_update_label_config);
        if ($res["filas"] > 0)
            $j++;
    }
    if ($j > 0) {
        echo json_encode(array('success' => true, 'message' => "DATOS ACTUALIZADOS CORRECTAMENTE", 'filas' => $j));
    } else {
        echo json_encode(array('success' => false, 'message' => "NO SE ACTUALIZARON LOS REGISTROS", 'res' => $res));
    }
} else {
    $sql_update_label_config = "UPDATE $DB_NAME.label_config_ktaxi SET ";
    $sql_update_label_config .= (isset($dataRecords->nombre)) ? "nombre = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($dataRecords->nombre)) . "', " : "";
    $sql_update_label_config .= (isset($dataRecords->descripcion)) ? "descripcion = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($dataRecords->descripcion)) . "', " : "";
    $sql_update_label_config .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update_label_config .= 'fecha_actualizo = NOW() ';
    $sql_update_label_config .= ' WHERE id_label_config_ktaxi = ' . $dataRecords->id;
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update_label_config));
}
$mysqli->close();
