<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
$dataRecords = json_decode(file_get_contents('php://input'));
if (!$mysqli = getConectionDb())
    return;
$sql_create_label_config = "INSERT INTO $DB_NAME.label_config_ktaxi "
        . "(nombre,descripcion,idAdministradorRegistro,fecha_registro) "
        . "VALUES ";

if (count($dataRecords) > 1) {
    foreach ($dataRecords as $data) {
        $sql_create_label_config .= "('" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->nombre)) . "'"
                . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($data->descripcion))
                . "', " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()),";
    }
    $sql_create_label_config = substr($sql_create_label_config, 0, -1);
} else {
    $sql_create_label_config .= "('" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($dataRecords->nombre)) . "'"
            . ", '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $mysqli->real_escape_string($dataRecords->descripcion))
            . "', " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW());";
}
echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_label_config));
$mysqli->close();
