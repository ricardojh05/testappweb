<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_POST);
extract($_GET);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return;
    $sql_update_administrador = "UPDATE $DB_NAME.administrador SET ";
    $sql_update_administrador .= (isset($data->usuario)) ? "usuario = '$data->usuario', " : "";
    $sql_update_administrador .= (isset($data->contrasenia)) ? "contrasenia =  MD5(CONCAT('$data->contrasenia' , '$TOKEN_LOGIN')), " : "";
    $sql_update_administrador .= (isset($data->nombres)) ? "nombres = '$data->nombres', " : "";
    $sql_update_administrador .= (isset($data->apellidos)) ? "apellidos = '$data->apellidos', " : "";
    $sql_update_administrador .= (isset($data->celular)) ? "celular = '$data->celular', " : "";
    $sql_update_administrador .= (isset($data->cedula)) ? "cedula = '" . $data->cedula . "', " : "";
    $sql_update_administrador .= (isset($data->correo)) ? "correo = '" . $data->correo . "', " : "";
    $sql_update_administrador .= (isset($data->fn)) ? "fecha_nacimiento = '" . $data->fn . "', " : "";
    $sql_update_administrador .= (isset($data->direccion)) ? "direccion = '" . $data->direccion . "', " : "";
    $sql_update_administrador .= (isset($data->imagen)) ? "imagen = '$data->imagen', " : "";
    $sql_update_administrador .= (isset($data->bloqueado)) ? "bloqueado = b'" . $data->bloqueado . "', " : "";
    $sql_update_administrador .= (isset($data->msgB)) ? "bloqueadoMensaje = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $data->msgB) . "', " : "";
    $sql_update_administrador .= (isset($data->msgR)) ? "mensajeReactivacion = '" . preg_replace("[\n|\r|\n\r\t|']", "\\n", $data->msgR) . "', " : "";
    $sql_update_administrador .= 'idAdministradorEdito =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    if (isset($data->bloqueado) && intval($data->bloqueado) == 1) {
        $sql_update_administrador .= 'fecha_bloqueo = NOW(), ';
    } else if (isset($data->bloqueado) && intval($data->bloqueado) == 0) {
        $sql_update_administrador .= 'fecha_reactivacion = NOW(), ';
    }
    $sql_update_administrador .= 'fecha_edicion = NOW() ';
    $sql_update_administrador .= 'WHERE idAdministrador = ' . $data->id;
    if (isset($permisos)) {
        $permisos = json_decode($permisos);
        if (count($permisos) > 0) {
            $sql_update_admin_permisos = "INSERT INTO $DB_NAME.moduloadministrador "
                    . "(idModulo, idAdministrador, leer, crear, editar, eliminar, idAdministradorRegistro) "
                    . "VALUES ";
            foreach ($permisos as $p) {
                $sql_update_admin_permisos .= "('" . $p->id . "', '" . $data->id . "'"
                        . ", " . (int) $p->leer . "" . ", " . (int) $p->crear . ""
                        . ", " . (int) $p->editar . "" . ", " . (int) $p->eliminar . ""
                        . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                        . "),";
            }
            $sql_update_admin_permisos = substr($sql_update_admin_permisos, 0, -1);
            $sql_update_admin_permisos .= " ON DUPLICATE KEY UPDATE idModulo=VALUES(idModulo),idAdministrador=VALUES(idAdministrador),leer=VALUES(leer),crear=VALUES(crear),editar=VALUES(editar),eliminar=VALUES(eliminar);";
            EJECUTAR_SQL($mysqli, $sql_update_admin_permisos);
        }
    }
    $perfiles = json_decode($perfiles);
        if (count($perfiles) > 0) {
            $sql_update_admin_perfil = "INSERT INTO $DB_NAME.perfil_administrador "
                    . "(idPerfil, idAdministrador, habilitado, idAdministradorRegistro, idAdministradorActualizo, fecha_actualizo, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito, fecha_deshabilito) "
                    . "VALUES ";
            foreach ($perfiles as $p) {
                if ((bool) $p->habilitado) {
                    $sql_update_admin_perfil .= "(" . $p->id . ", " . $data->id . ""
                            . ", " . $p->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", NULL, NULL"
                            . "),";
                } else {
                    $sql_update_admin_perfil .= "(" . $p->id . ", " . $data->id . ""
                            . ", " . $p->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", NULL, NULL"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . "),";
                }
            }
            $sql_update_admin_perfil = substr($sql_update_admin_perfil, 0, -1);
            $sql_update_admin_perfil .= " ON DUPLICATE KEY UPDATE idPerfil=VALUES(idPerfil),"
                    . "idAdministrador=VALUES(idAdministrador),"
                    . "habilitado=VALUES(habilitado),"
                    . "idAdministradorActualizo=VALUES(idAdministradorActualizo),"
                    . "fecha_actualizo=VALUES(fecha_actualizo),"
                    . "idAdministradorHabilito=VALUES(idAdministradorHabilito),"
                    . "fecha_habilito=VALUES(fecha_habilito),"
                    . "idAdministradorDeshabilito=VALUES(idAdministradorDeshabilito),"
                    . "fecha_deshabilito=VALUES(fecha_deshabilito);";
            EJECUTAR_SQL($mysqli, $sql_update_admin_perfil);
        }
        $lista_compania = json_decode($lista_compania);
        if (count($lista_compania) > 0) {
            $sql_update_admin_compania = "INSERT INTO $DB_NAME.administrador_compania "
                    . "(idCompania, idAdministrador, hablilitado, idAdministradorRegistro, fecha_registro, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito, fecha_deshabilito) "
                    . "VALUES ";
            foreach ($lista_compania as $p) {
                if ((bool) $p->habilitado) {
                    $sql_update_admin_compania .= "(" . $p->id . ", " . $data->id . ""
                            . ", " . $p->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", NULL, NULL"
                            . "),";
                } else {
                    $sql_update_admin_compania .= "(" . $p->id . ", " . $data->id . ""
                            . ", 0, "  . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . ", NULL, NULL"
                            . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                            . "),";
                }
            }
            $sql_update_admin_compania = substr($sql_update_admin_compania, 0, -1);
            $sql_update_admin_compania .= " ON DUPLICATE KEY UPDATE idCompania=VALUES(idCompania),"
                    . "idAdministrador=VALUES(idAdministrador),"
                    . "hablilitado=VALUES(hablilitado),"
                    . "idAdministradorHabilito=VALUES(idAdministradorHabilito),"
                    . "fecha_habilito=VALUES(fecha_habilito),"
                    . "idAdministradorDeshabilito=VALUES(idAdministradorDeshabilito),"
                    . "fecha_deshabilito=VALUES(fecha_deshabilito);";
//            echo $sql_update_admin_compania;
            EJECUTAR_SQL($mysqli, $sql_update_admin_compania);
        }
        
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update_administrador));
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}