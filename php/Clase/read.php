<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql = "SELECT COUNT(c.idClase) AS total"
        . " FROM $DB_NAME.clase c "
        . " INNER JOIN $DB_NAME.materia m ON m.idMateria=c.idMateria "
        . " WHERE TRUE ";

/* if (isset($idUsuario)) {
    if (isset($id)) {
        $sql .= " AND ";
    } else {
        $sql .= " WHERE ";
    }
    $sql .= " uaa.idUsuarioAccesoApi = '$idUsuario' ";
}
if (isset($idEstado)) {
    if ($idEstado == 1) {
        if (isset($id)|| isset($idUsuario)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.confirmada = 1 ";
    }
    if ($idEstado == 2) {
        if (isset($id)|| isset($idUsuario)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.validada = 1 ";
    }
} */
if (isset($param) && ($param !== '')) {
        $sql .= " AND ( LOWER(c.nombre) LIKE LOWER('%$param%') OR LOWER(m.materia) LIKE LOWER('%$param%')) ";
}
$sql .= " ORDER BY c.idClase ASC ";
//echo $sql;
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
//echo $total;
if ($total > 0) {
    $sql = "SELECT c.idClase,c.nombre,c.idMateria,c.color,m.materia"
            . " FROM $DB_NAME.clase c"
            . " INNER JOIN $DB_NAME.materia m ON m.idMateria=c.idMateria "
            . " WHERE TRUE ";

/* 
    if (isset($idUsuario)) {
        if (isset($id)) {
            $sql .= " AND ";
        } else {
            $sql .= " WHERE ";
        }
        $sql .= " uaa.idUsuarioAccesoApi = '$idUsuario' ";
    }
    if (isset($idEstado)) {
        if ($idEstado == 1) {
            if (isset($id) || isset($idUsuario)) {
                $sql .= " AND ";
            } else {
                $sql .= " WHERE ";
            }
            $sql .= " uaa.confirmada = 1 ";
        }
        if ($idEstado == 2) {
            if (isset($id) || isset($idUsuario)) {
                $sql .= " AND ";
            } else {
                $sql .= " WHERE ";
            }
            $sql .= " uaa.validada = 1 ";
        }
    } */

    if (isset($param) && ($param !== '')) {
        $sql .= " AND ( LOWER(c.nombre) LIKE LOWER('%$param%') OR LOWER(m.materia) LIKE LOWER('%$param%')) ";
}
    $sql .= " ORDER BY c.idClase ASC ";
//echo $sql;
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS ";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
        return $mysqli->close();
    }
    $arreglo = array();
    while ($myrow = $result->fetch_assoc()) {
        $arreglo[] = array(
            'id' => intval($myrow["idClase"]),
            'idClase' => intval($myrow["idClase"]),
            'idMateria' => intval($myrow["idMateria"]),
            'nombre' => $myrow["nombre"],
            'color' => $myrow["color"],
            'materia' => $myrow["materia"],
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => TRUE, 'data' => [], 'total' => 0));
}
$mysqli->close();
