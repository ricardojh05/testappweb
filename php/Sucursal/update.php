<?php
extract($_GET);
extract($_POST);
include '../../dll/config.php';
include '../../dll/funciones.php';
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data->id)) {
    if (!$mysqli = getConectionDb())
        return $mysqli;
    $sql_update_empresa = "UPDATE $DB_NAME.sucursal SET ";
    $sql_update_empresa .= (isset($data->ciudad)) ? "idCiudad = " . $data->ciudad . ", " : "";
    $sql_update_empresa .= (isset($data->sucursal)) ? "sucursal = '" . $mysqli->real_escape_string($data->sucursal) . "', " : "";
    $sql_update_empresa .= (isset($data->callePrin)) ? "callePrincipal = '" . $mysqli->real_escape_string($data->callePrin) . "', " : "";
    $sql_update_empresa .= (isset($data->calleSec)) ? "calleSecundaria = '" . $mysqli->real_escape_string($data->calleSec) . "', " : "";
    $sql_update_empresa .= (isset($data->contacto)) ? "contacto = '" . $data->contacto . "', " : "";
    $sql_update_empresa .= (isset($data->color)) ? "color = '" . $data->color . "', " : "";
    $sql_update_empresa .= (isset($data->comentario)) ? "comentario = '" . $mysqli->real_escape_string($data->comentario) . "', " : "";
    $sql_update_empresa .= (isset($data->latitud)) ? "latitud = " . $data->latitud . ", " : "";
    $sql_update_empresa .= (isset($data->longitud)) ? "longitud = " . $data->longitud . ", " : "";
    $sql_update_empresa .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
    $sql_update_empresa .= 'fecha_actualizo = NOW() ';
    $sql_update_empresa .= 'WHERE idSucursal = ' . $data->id;
    $administradores = json_decode($administradores);
    if (count($administradores) > 0) {
        $sql_update_admin_sucursal = "INSERT INTO $DB_NAME.administrador_sucursal "
                . "(idAdministrador, idSucursal, hablilitado, idAdministradorRegistro, fecha_registro, idAdministradorHabilito, fecha_habilito, idAdministradorDeshabilito,fecha_deshabilito) "
                . "VALUES ";
        foreach ($administradores as $v) {
            if ($v->habilitado) {
                $sql_update_admin_sucursal .= "('" . $v->id . "', '" . $data->id . "'"
                        . ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . ", NULL, NULL"
                        . "),";
            } else {
                $sql_update_admin_sucursal .= "('" . $v->id . "', '" . $data->id . "'"
                        . ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . ", NULL, NULL"
                        . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()"
                        . "),";
            }
        }
        $sql_update_admin_sucursal = substr($sql_update_admin_sucursal, 0, -1);
        $sql_update_admin_sucursal .= " ON DUPLICATE KEY UPDATE idAdministrador=VALUES(idAdministrador),"
                . "idSucursal=VALUES(idSucursal),"
                . "hablilitado=VALUES(hablilitado),"
                . "idAdministradorHabilito=VALUES(idAdministradorHabilito),"
                . "fecha_habilito=VALUES(fecha_habilito),"
                . "idAdministradorDeshabilito=VALUES(idAdministradorDeshabilito),"
                . "fecha_deshabilito=VALUES(fecha_deshabilito);";
        EJECUTAR_SQL($mysqli, $sql_update_admin_sucursal);
    }
    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update_empresa));
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}
$mysqli->close();

//include '../../dll/config.php';
//include '../../dll/funciones.php';
//$arrayData = array();
//$data = json_decode(file_get_contents('php://input'));
//if (isset($data->id)) {
//    if (!$mysqli = getConectionDb())
//        return $mysqli;
//    $sql_update_sucursal = "UPDATE $DB_NAME.sucursal SET ";
//    $sql_update_sucursal .= (isset($data->ciudad)) ? "idCiudad = $data->ciudad, " : "";
//    $sql_update_sucursal .= (isset($data->sucursal)) ? "idSucursal = $data->sucursal, " : "";
//    $sql_update_sucursal .= (isset($data->sucursal)) ? "sucursal = '" . $mysqli->real_escape_string($data->sucursal) . "', " : "";
//    $sql_update_sucursal .= (isset($data->callePrin)) ? "callePrincipal = '" . $mysqli->real_escape_string($data->callePrin) . "', " : "";
//    $sql_update_sucursal .= (isset($data->calleSec)) ? "calleSecundaria = '" . $mysqli->real_escape_string($data->calleSec) . "', " : "";
//    $sql_update_sucursal .= (isset($data->contacto)) ? "contacto = '$data->contacto', " : "";
//    $sql_update_sucursal .= (isset($data->correo)) ? "correo = '$data->correo', " : "";
//    $sql_update_sucursal .= (isset($data->color)) ? "color = '$data->color', " : "";
//    $sql_update_sucursal .= (isset($data->comentario)) ? "comentario = '" . $mysqli->real_escape_string($data->comentario) . "', " : "";
//    $sql_update_sucursal .= (isset($data->latitud)) ? "latitud = $data->latitud, " : "";
//    $sql_update_sucursal .= (isset($data->longitud)) ? "longitud = $data->longitud, " : "";
//    $sql_update_sucursal .= (isset($data->company)) ? "idSucursal = $data->company, " : "";
//    $sql_update_sucursal .= 'idAdministradorActualizo =' . $_SESSION["ID_ADMINISTRADOR"] . ', ';
//    $sql_update_sucursal .= 'fecha_actualizo = NOW() ';
//    $sql_update_sucursal .= 'WHERE idSucursal = ' . $data->id;
//    echo $sql_update_sucursal;
//    echo json_encode(EJECUTAR_SQL($mysqli, $sql_update_sucursal));
//    $mysqli->close();
//} else {
//    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
//}