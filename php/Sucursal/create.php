<?php
include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
extract($_POST);
$arrayData = array();
$data = json_decode(file_get_contents('php://input'));
if (isset($data)) {
    if (!$mysqli = getConectionDb())
        return $mysqli;
    $sql_create_empresa = "INSERT INTO $DB_NAME.sucursal "
            . "(idCiudad, idCompania, sucursal, callePrincipal, calleSecundaria, contacto, correo, color, comentario, latitud, longitud, idAdministradorRegistro) "
            . "VALUES "
            . "(" . $data->ciudad .", ". $data->compania . ", ('" . $mysqli->real_escape_string($data->sucursal) . "')"
            . ", '" . $mysqli->real_escape_string($data->callePrin) . "'" . ", '" . $mysqli->real_escape_string($data->calleSec) . "'"
            . ", '" . $data->contacto . "'" . ", '" . $data->correo . "'". ", '" . $data->color . "'"
            . ", '" . $mysqli->real_escape_string($data->comentario) . "'" . ", '" . $data->latitud . "'"
            . ", '" . $data->longitud . "'" . ", '" . $_SESSION["ID_ADMINISTRADOR"] . "'"
            . ");";
    $sucursal = EJECUTAR_SQL($mysqli, $sql_create_empresa);
    if (isset($sucursal['id'])) {
        if ($sucursal['id'] > 0) {
            $sql_create_compania_sucursal="INSERT INTO $DB_NAME.companiaSucursal "
                    . "( idCompania, idSucursal, credito, consumo, habilitado, idAdministradorRegistro, fecha_registro)"
                    . "VALUES "
                    . "(".$data->compania.", ". $sucursal['id'] .", 0, 0, 1, ". $_SESSION["ID_ADMINISTRADOR"] . ", NOW())";
            EJECUTAR_SQL($mysqli, $sql_create_compania_sucursal);
            $administradores =json_decode($administradores);
            if (count($administradores) > 0) {
                    $sql_insert_admin_sucursal = "INSERT INTO $DB_NAME.administrador_sucursal "
                            . "(idAdministrador, idSucursal, hablilitado, idAdministradorRegistro, idAdministradorHabilito, fecha_habilito) "
                            . "VALUES ";
                    foreach ($administradores as $v) {
                        $sql_insert_admin_sucursal .= "(" . $v->id . ", " . $sucursal['id'] . "". ", " . (int) $v->habilitado . "" . ", " . $_SESSION["ID_ADMINISTRADOR"] . ""
                                . ", " . $_SESSION["ID_ADMINISTRADOR"] . ", NOW()),";
                    }
                    $sql_insert_admin_sucursal = substr($sql_insert_admin_sucursal, 0, -1);
                    echo json_encode(EJECUTAR_SQL($mysqli, $sql_insert_admin_sucursal));
                } else {
                    echo json_encode($sucursal);
                }
        }
    }
    $mysqli->close();
} else {
    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
}

//include '../../dll/config.php';
//include '../../dll/funciones.php';
//$arrayData = array();
//$data = json_decode(file_get_contents('php://input'));
//if (isset($data)) {
//    if (!$mysqli = getConectionDb())
//        return $mysqli;
//    $sql_create_sucursal = "INSERT INTO $DB_NAME.sucursal "
//            . "(idCiudad, sucursal, callePrincipal, calleSecundaria, contacto, color, latitud, longitud, idAdministradorRegistro, idSucursal, correo) "
//            . "VALUES "
//            . "(" . $data->ciudad . ", '" . $mysqli->real_escape_string($data->sucursal) . "'"
//            . ", '" . $mysqli->real_escape_string($data->callePrin) . "'" . ", '" . $mysqli->real_escape_string($data->calleSec) . "'"
//            . ", '" . $data->contacto . "'" . ", '" . $data->color . "'"
//            . ", '" . $data->latitud . "'"
//            . ", '" . $data->longitud . "'" . ", '" . $_SESSION["ID_ADMINISTRADOR"] . "'"
//            . ", " . $data->company . "" . ", '" . $mysqli->real_escape_string($data->correo) . "'"
//            . ");";
//    echo json_encode(EJECUTAR_SQL($mysqli, $sql_create_sucursal));
//    $mysqli->close();
//} else {
//    echo json_encode(array('success' => false, 'message' => "FALTAN PARÁMETROS"));
//}