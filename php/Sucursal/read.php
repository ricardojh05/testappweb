<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql_total = "SELECT COUNT(c.idSucursal) AS total FROM $DB_NAME.sucursal c "
        . " INNER JOIN $DB_NAME.administrador_sucursal adms on adms.idSucursal= c.idSucursal "
        . " WHERE adms.idAdministrador=" . $_SESSION["ID_ADMINISTRADOR"] . " AND adms.hablilitado=TRUE "
        . " ORDER BY c.idSucursal DESC";
$result = $mysqli->query($sql_total);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", "sql" => $sql));
    return $mysqli->close();
}
$myrow_read = $result->fetch_assoc();
$total = intval($myrow_read['total']);
if ($total > 0) {
    $sql = "SELECT c.idCiudad, c.idSucursal, c.idCompania, c.sucursal, c.callePrincipal, c.calleSecundaria, 
c.contacto, c.color, c.correo, c.latitud, c.longitud, c.idAdministradorRegistro, c.fecha_registro, 
c.idAdministradorActualizo, c.fecha_actualizo "
            . "FROM $DB_NAME.sucursal c "
            . " INNER JOIN $DB_NAME.administrador_sucursal adms on adms.idSucursal= c.idSucursal
WHERE adms.idAdministrador=" . $_SESSION["ID_ADMINISTRADOR"] . " AND adms.hablilitado=TRUE ";
    if (isset($param)) {
        $sql .= " AND ((sucursal) LIKE ('$param%')"
                . " OR (callePrincipal) LIKE ('$param%') "
                . " OR (calleSecundaria) LIKE ('$param%') "
                . " OR contacto LIKE '$param%') ";
    }
    if (isset($ciudades) && $ciudades !== '' && $ciudades != 0) {
        $sql .= " AND c.idCiudad IN ($ciudades) ";
    }
//echo $sql;
//if (isset($param)) {
//    $sql .= "WHERE ((sucursal) LIKE ('$param%')"
//            . "OR (callePrincipal) LIKE ('$param%') "
//            . "OR (calleSecundaria) LIKE ('$param%') "
//            . "OR contacto LIKE '$param%') ";
//}
//if (isset($ciudades)) {
//    if ($ciudades !== '' && $ciudades != 0) {
//        if (isset($param)) {
//            $sql .= " AND ";
//        } else {
//            $sql .= " WHERE ";
//        }
//        $sql .= " idCiudad IN ($ciudades) ";
//    }
//}

    $sql .= " ORDER BY c.idSucursal DESC ";

    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS";
    }
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
        return $mysqli->close();
    }
    $arreglo = [];
    while ($myrow_read_sucursal = $result->fetch_assoc()) {
        $insert = "SELECT count(idCompaniaSucursal) as total_insert FROM $DB_NAME.companiaSucursal where idSucursal=" . $myrow_read_sucursal["idSucursal"] . ";";
        $result_insert = $mysqli->query($insert);
        $myrow_insert = $result_insert->fetch_assoc();
        $total_insert = intval($myrow_insert['total_insert']);
        $arreglo[] = array(
            'ciudad' => intval($myrow_read_sucursal["idCiudad"]),
            'id' => intval($myrow_read_sucursal["idSucursal"]),
            'idCompania' => intval($myrow_read_sucursal["idCompania"]),
            'sucursal' => $myrow_read_sucursal["sucursal"],
            'callePrin' => $myrow_read_sucursal["callePrincipal"],
            'calleSec' => $myrow_read_sucursal["calleSecundaria"],
            'contacto' => $myrow_read_sucursal["contacto"],
            'color' => $myrow_read_sucursal["color"],
            'correo' => $myrow_read_sucursal["correo"],
            'latitud' => $myrow_read_sucursal["latitud"],
            'longitud' => $myrow_read_sucursal["longitud"],
            'fRegistro' => $myrow_read_sucursal["fecha_registro"],
            'idUserCreate' => $myrow_read_sucursal["idAdministradorRegistro"],
            'dateCreate' => $myrow_read_sucursal["fecha_registro"],
            'idUserUpdate' => $myrow_read_sucursal["idAdministradorActualizo"],
            'dateUpdate' => $myrow_read_sucursal["fecha_actualizo"],
            'insertado' => $total_insert
        );
    }
    echo json_encode(array('success' => true, 'sucursales' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => true, 'sucursales' => [], 'total' => 0));
}
$mysqli->close();
//include '../../dll/config.php';
//include '../../dll/funciones.php';
//extract($_GET);
//
//if (!$mysqli = getConectionDb())
//    return $mysqli;
//
//include '../Get/getSucursalsAdmin.php';
//
//$sql = "SELECT idCiudad, idSucursal, idSucursal, sucursal, callePrincipal, calleSecundaria, contacto, correo, color, comentario, latitud, longitud, "
//        . " idAdministradorRegistro, fecha_registro, idAdministradorActualizo, fecha_actualizo "
//        . "FROM $DB_NAME.sucursal  "
//        . "WHERE TRUE ";
//
//if ($COMPANIAS_ADMIN !== 'all') {
//    $sql .= " AND idSucursal IN ($COMPANIAS_ADMIN) ";
//}
//
//if (isset($ciudades) && $ciudades !== '') {
//    $sql .= "AND idCiudad IN ($ciudades) ";
//}
//
//if (isset($param) && $param != '') {
//    $sql .="AND sucursal LIKE '$param%' ";
//}
//
//if (isset($limit)) {
//    $sql .= " LIMIT $limit";
//} else {
//    $sql .= " LIMIT $LIMITE_REGISTROS";
//}
//
//$result = $mysqli->query($sql);
//if (!isset($result->num_rows)) {
//    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS"));
//    return $mysqli->close();
//}
//$arreglo = [];
//while ($myrow_read_sucursals = $result->fetch_assoc()) {
//    $arreglo[] = array(
//        'ciudad' => intval($myrow_read_sucursals["idCiudad"]),
//        'idCompany' => intval($myrow_read_sucursals["idSucursal"]),
//        'id' => intval($myrow_read_sucursals["idSucursal"]),
//        'sucursal' => $myrow_read_sucursals["sucursal"],
//        'callePrin' => $myrow_read_sucursals["callePrincipal"],
//        'calleSec' => $myrow_read_sucursals["calleSecundaria"],
//        'contacto' => $myrow_read_sucursals["contacto"],
//        'correo' => $myrow_read_sucursals["correo"],
//        'color' => $myrow_read_sucursals["color"],
//        'comentario' => $myrow_read_sucursals["comentario"],
//        'latitud' => $myrow_read_sucursals["latitud"],
//        'longitud' => $myrow_read_sucursals["longitud"],
//        'idUserCreate' => $myrow_read_sucursals["idAdministradorRegistro"],
//        'dateCreate' => $myrow_read_sucursals["fecha_registro"],
//        'idUserUpdate' => $myrow_read_sucursals["idAdministradorActualizo"],
//        'dateUpdate' => $myrow_read_sucursals["fecha_actualizo"]
//    );
//}
//$mysqli->close();
//echo json_encode(array('success' => true, 'sucursals' => $arreglo));
