<?php

include '../../dll/config.php';
include '../../dll/funciones.php';
extract($_GET);
if (!$mysqli = getConectionDb())
    return $mysqli;

$sql = "SELECT COUNT(a.idAlumno) AS total"
        . " FROM $DB_NAME.alumno a "
        . " WHERE TRUE ";

        if (isset($param) && ($param !== '')) {
            $sql .= "AND ( LOWER(a.nombre) LIKE LOWER('%$param%') OR LOWER(a.apellido) LIKE LOWER('%$param%') ) ";
        }
        $sql .= " ORDER BY a.apellido ASC ";
//echo $sql;
$result = $mysqli->query($sql);
if (!isset($result->num_rows)) {
    echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
    return $mysqli->close();
}
$myrow = $result->fetch_assoc();
$total = $myrow['total'];
//echo $total;
if ($total > 0) {
    $sql = "SELECT a.idAlumno,a.nombre,a.apellido,a.imagen,a.correo"
            . " FROM $DB_NAME.alumno a"
            . " WHERE TRUE ";

    if (isset($param) && ($param !== '')) {
        $sql .= "AND ( LOWER(a.nombre) LIKE LOWER('%$param%') OR LOWER(a.apellido) LIKE LOWER('%$param%') ) ";
    }
    $sql .= " ORDER BY a.apellido ASC ";
//echo $sql;
    if (isset($limit)) {
        $inicio = intval($limit) * (intval($page) - 1);
        $sql .= " LIMIT $inicio, $limit ";
    } else {
        $sql .= " LIMIT $LIMITE_REGISTROS ";
    }
    
    $result = $mysqli->query($sql);
    if (!isset($result->num_rows)) {
        echo json_encode(array('success' => false, 'message' => "NO EXISTEN RESULTADOS", 'sql' => $sql));
        return $mysqli->close();
    }
    $arreglo = array();
    while ($myrow = $result->fetch_assoc()) {

        $sql_plazas = "SELECT ca.idClase, ca.idAlumno,c.nombre "
        /* . " ca.idAdministradorRegistro, IF(MONTH(ca.fecha_registro) < 10, DATE_FORMAT(ca.fecha_registro, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ca.fecha_registro, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_registro,"
        . " ca.idAdministradorHabilito, IF(MONTH(ca.fecha_habilito) < 10, DATE_FORMAT(ca.fecha_habilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ca.fecha_habilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_habilito,"
        . " ca.idAdministradorDeshabilito, IF(MONTH(ca.fecha_deshabilito) < 10, DATE_FORMAT(ca.fecha_deshabilito, '%Y-0%c-%dT%H:%i:%s.000Z'), DATE_FORMAT(ca.fecha_deshabilito, '%Y-%c-%dT%H:%i:%s.000Z')) AS fecha_deshabilito"
 */        
        . " FROM $DB_NAME.clase_alumno ca"
        . " INNER JOIN $DB_NAME.clase c on c.idClase=ca.idClase "
        //. " INNER JOIN $DB_NAME.plaza c ON ca.idPlaza = c.idPlaza"
        . " WHERE ca.idAlumno = " . intval($myrow["idAlumno"])
        . " ORDER BY ca.idClase DESC";
        $resultAlumnos = $mysqli->query($sql_plazas);
        $clases = [];
        while ($myrow_read_clases = $resultAlumnos->fetch_assoc()) {
            //$fechaChange = ($myrow_read_clases["habilitado"]) ? $myrow_read_clases["fecha_habilito"] : $myrow_read_clases["fecha_deshabilito"];
            //$idUserChange = ($myrow_read_clases["habilitado"]) ? $myrow_read_clases["idAdministradorHabilito"] : $myrow_read_clases["idAdministradorDeshabilito"];
            $clases[] = array(
                'idClase' => intval($myrow_read_clases["idClase"]),
                'idAlumno' => $myrow_read_clases["idAlumno"],
                'nombre' => $myrow_read_clases["nombre"],
            );
        }
        $arreglo[] = array(
            'id' => intval($myrow["idAlumno"]),
            'idAlumno' => intval($myrow["idAlumno"]),
            'correo' => $myrow["correo"],
            'nombre' => $myrow["nombre"],
            'apellido' => $myrow["apellido"],
            'clases' => $clases
        );
    }
    echo json_encode(array('success' => TRUE, 'data' => $arreglo, 'total' => $total));
} else {
    echo json_encode(array('success' => TRUE, 'data' => [], 'total' => 0));
}
$mysqli->close();