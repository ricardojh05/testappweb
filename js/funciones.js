/* global Ext, MENSAJE_ERROR_SOLICITUD_CLIENTE, MENSAJE_ERROR, MENSAJE_RESPUESTA_ERROR, MENSAJE_ESPERA, TITULO_ESPERA, google, storeGenero, mapGoogle, RUTA_IMAGEN_INCOGNITO, infoWindowMarcador, listaMarcadoresPersona, storePrivacidad, storeRecorridoPosicion, colores, rutaMapa, marcadorInicio, marcadorFin, sizeImagen, storePersona, configGrid, RUTA_IMAGEN, MENSAJE_POSICION_INVALIDA, storeDecision, storePosicion, MENSAJE_NO_DATOS_IMAGEN, listaRutaMapa, MAPA_MAPBOX, RUTAS_ANIMADAS, TIEMPO_VELOCIDAD, storeDetallePosicion, storeRecorrido, ol, LISTA_MARCADORES_VEHICULOS_OSM, MAPA_OSM, CONTENT_INFO_WINDOW, OVERLAY_INFO_WINDOW, OVERLAY_OSM, TOOLTIP_OSM, ICONO_VEHICULO_ROL, BANDERA_SEGUIR_MARCADOR, storeVehiculo, ICONO_PERSONA_ROL, ICONO_FIN_RUTA_ROL, ICONO_INICIO_RUTA_ROL, alertify, PANELLOAD, ID_MAPA_CIUDAD, LIST_MAPS */
var DIAS = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
        MESES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
function inicializarEntorno() {
    Ext.Date.dayNames = ['Domingo', 'Lunes', 'Martes', 'Miercoles',
        'Jueves', 'Viernes', 'Sabado'];
    Ext.Date.monthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
        'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
        'Diciembre'];
    if (Ext.grid.RowEditor) {
        Ext.apply(Ext.grid.RowEditor.prototype, {
            saveBtnText: "Guardar",
            cancelBtnText: "Cancelar",
            errorsText: "Errores",
            dirtyText: "Debe guardar o cancelar sus cambios"
        });
    }
}
/**
 * Presenta un mensaje tipo informativo con efecto deslizante
 * @param {String} mensaje
 * @param {int} tipo
 * @returns {undefined}
 */
function notificaciones(mensaje, tipo) {
    var posicion = 'bottom-right';
    alertify.set('notifier', 'position', posicion);
    switch (tipo) {
        case 1://succes
            alertify.success(mensaje);
            break;
        case 2://error
            alertify.error(mensaje);
            break;
        case 3://message
            alertify.message(mensaje);
            break;
        case 4://notificacion
            alertify.notify(mensaje);
            break;
        case 5://warning
            alertify.warning(mensaje);
            break;
        default:
            alertify.custom = alertify.extend("custom");
            alertify.custom("<table><tr><td><img class='icono' src='" + ICO + "'/></td><td>   </td><td><center>    " + mensaje + "</center></td></tr></table>");
            break;
    }
}
function alertaSalir() {
    Ext.MessageBox.buttonText = {
        yes: "Sí",
        no: "No"
    };
    Ext.MessageBox.confirm('Salir', '¿Desea salir del sistema?', function (choice) {
        if (choice === 'yes') {
            borrarCookies();
            window.location = 'php/Login/logout.php';
        }
    });
}
//Cookie
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function eraseCookie(name) {
    createCookie(name, "", 0);
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
function comprobarCookie(clave) {
    if (getCookie(clave) === "") {
        return false;
    } else {
        return true;
    }
}
function borrarCookies() {
    eraseCookie("ID_ADMINISTRADOR");
    eraseCookie("ID_MODULO");
    eraseCookie("LONGITUD");
    eraseCookie("LATITUD");
    eraseCookie("USUARIO");
    eraseCookie("INICIO");
    eraseCookie("MODULO");
    eraseCookie("PATH");
}
function mostrarBarraProgreso(mensaje) {
    Ext.MessageBox.show({
        title: "Espere",
        msg: mensaje,
        wait: true,
        waitConfig: {interval: 200}
    });
}

function ocultarBarraProgreso() {
    Ext.MessageBox.hide();
}
function alterna_modo_de_pantalla(val) {
    if (val) {
        salirPantallaCompleta();
    }
    if ((document.fullScreenElement && document.fullScreenElement !== null) || // metodo alternativo
            (!document.mozFullScreen && !document.webkitIsFullScreen)) {               // metodos actuales
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        salirPantallaCompleta();
    }
}
function salirPantallaCompleta() {
    if (document.cancelFullScreen) {
        document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
    }
}
function get_Ajax(url, params, timeout, callback, headers) {
    if (timeout === undefined || timeout === 0)
        timeout = 5 * 1000 * 60;
    Ext.Ajax.request({
        url: url,
        params: params,
        timeout: timeout,
        headers: headers,
        success: function (response, opts) {
            if (response.status === 200) {
                var obj = {};
                if (isJsonString(response.responseText)) {
                    if (response.responseText.length > 2) {
                        obj = JSON.parse(response.responseText);
                        obj.resAjax = 1;
                    } else {
                        obj.resAjax = -1;
                    }
                } else {
                    obj.resAjax = 2;
                    obj.error = "Exite un error al crear el JSON";
                    obj.message = "Lamentamos los inconvenientes por favor comunícate con Kradac.";
                    obj.response = response.responseText;
                }
                return callback(obj);
            } else {
                console.error('Respuesta: ');
                console.error(response);
                console.error('Parámetros');
                console.error(params);
                return callback({resAjax: -1, respuesta: response, metadata: opts});
            }
        }, failure: function (response, opts) {
            console.error('Respuesta: ');
            console.error(response);
            console.error('Parámetros');
            console.error(params);
            return callback({resAjax: -2, respuesta: response, metadata: opts});
        }
    });
}
//CONECTAR SOCKET
function conectarSocket() {
    SOCKET = io.connect(URL_SERVICIO, {'forceNew': false});
    SOCKET.on('connect', function () {
        SOCKET.emit(EMIT_AUTENTICAR, {idAdministrador: USUARIO.ID_ADMINISTRADOR, imei: USUARIO.ID_ADMINISTRADOR}, function (response) {
            if (response.en === 1) {
                console.log("%c MUY BIEN CONECTADO", "background: green; color: white");
            } else {
                desconectarSocket(0);
            }
        });
    });
}
function desconectarSocket(estado) {//0: NO HAY INRERNET | 1: SI HAY INTERNET NO HAY SERVIDOR
    console.log("%c desconectarSocket", "background: red; color: white");
    if (estado === 0) {
        notificaciones(E_R_R, 2);
        $('#cargandoModal').modal('show');
    } else {
        notificaciones(MENSAJE_ERROR, 2);
    }
    if (SOCKET !== undefined)
        SOCKET.disconnect();
}
//FUNCIONES DE SOCKETS
function registrarUnicoRastreo(idEquipo, idVehiculo, info, data) {
    SOCKET.emit(EMIT_A_REG_UNICO_RASTREO, {idAdministrador: USUARIO.ID_ADMINISTRADOR, idEquipo: idEquipo, imei: USUARIO.ID_ADMINISTRADOR, idVehiculo: idVehiculo}, function (response) {
        console.log(response);
        if (response.en === 1) {
            graficarMarcadorRastreo(LIST_MAPS[ID_MAPA_RASTREO], response.lR[0].latitud, response.lR[0].longitud, data, info);
        }
    });
}
//bateria: Estado la bateria 
//estado: Estado del vehiculo
//g1:
//g2:
//gps:
//gsm:
//idEvento:
//ign:
//ign:
//latitud: posicion de la trama
//longitud:
//rumbo:
//sal:
//v1:
//v2:
//velocidad:
function obtenerTramasRastreo(fecha, horaInicio, horaFin, idEquipo) {
    console.log(horaInicio);
    console.log(horaFin);
    get_Ajax(URL_SERVICIO + REST_RASTREO, {anio: fecha.getFullYear(), mes: (fecha.getMonth() + 1), dia: fecha.getDate(), idEquipo: idEquipo, desde: horaInicio, hasta: horaFin}, 600000, function (response) {
        console.log('obtenerTramasRastreo');
        console.log(response);
        if (response < 0) {
            desconectarSocket(1);
        } else
        if (response.en < 0)
            return  notificaciones(MENSAJE_ERROR, 2);
        else if (response.rastreo.length < 0) {
            return  notificaciones(MENSAJE_NO_EXISTEN_TRAMAS, 2);
        } else {
            Ext.getStore('s_Rastreo').setData(response.rastreo);
            var coordenadas = [];
            for (var i = 0; i < response.rastreo.length; i++)
                coordenadas.push([response.rastreo[i].p[0], response.rastreo[i].p[1]]);
            graficarRuta(LIST_MAPS[ID_MAPA_RASTREO], coordenadas);
        }
    });
}


function obtenerCoordenadas() {
    addClickPoint(LIST_MAPS[ID_MAPA_CIUDAD], PANELLOAD, true);
}

function getNombrePais(id) {
    if (Number.isInteger(id)) {
        var pais = Ext.getStore('s_Paises_Combo').findRecord('idPais', id);
        return pais.data.pais;
    }
    return id;
}

///Recibe el item a buscar, el store en que se buscara.. 
///y los parametros que se enviaran en caso que el store este vacio o no contenga el item a buscar.
function checkStore(item, store, param) {
    if (storeEmpty(store)) {
        store.load({
            params: param
        });
    } else {
        if (!isInStore(store, item, 'id')) {
            store.removeAll();
            store.load({
                params: param
            });
        }
    }
}

function storeEmpty(store) {
    var val = store.data.items.length;
    if (val === 0) {
        return true;
    } else {
        return false;
    }
}

function isInStore(store, item, label) {
    var exist = store.findRecord(label, item);
    if (exist !== null) {
        return exist;
    } else {
        return false;
    }
}

function formatHora(val) {
    if (val === null || val === "") {
        return "";
    } else {
        if (Ext.Date.format(val, 'H:i:s')) {
            return Ext.Date.format(val, 'H:i:s');
        } else {
            return '<span style="color:#FF0000;">SIN FORMATO</span>';
        }
    }
}
function runReloj() {
    FECHA_ACTUAL = new Date();
    var hora = FECHA_ACTUAL.getHours();
    var minuto = FECHA_ACTUAL.getMinutes();
    var segundo = FECHA_ACTUAL.getSeconds();
    var str_segundo = new String(segundo);
    if (str_segundo.length == 1)
        segundo = "0" + segundo
    var str_minuto = new String(minuto);
    if (str_minuto.length == 1)
        minuto = "0" + minuto;
    var str_hora = new String(hora)
    if (str_hora.length == 1)
        hora = "0" + hora;
    document.getElementById("reloj").innerHTML = hora + " : " + minuto + " : " + segundo;
    setTimeout("runReloj()", 1000);
}
function asignarDatosMain() {
    var tolbarMain = Ext.getCmp('toolbarMain');
    tolbarMain.down('[name=FOTO_PERFIL]').setSrc(URL_IMG_UPLOADS_ADMINS + USUARIO.IMAGEN);
    tolbarMain.down('[name=NOMBRE_USUARIO]').setText(USUARIO.PERSONA);
}

function validarPermisosGeneral(panel) {
    var moduloActual = Ext.getStore('Navegacion').byIdMap[panel.xtype].getData();
    var permisos = moduloActual.permisos;
    var formsPanel = panel.query('form');
    var gridsPanel = panel.query('grid');
    for (var i in formsPanel) {
        var btnCrear = formsPanel[i].down('[name=btnCrear]');
        var btnEditar = formsPanel[i].down('[name=btnEditar]');
        var checkHabilitar = formsPanel[i].down('[name=habilitado]');
        if (btnCrear && btnEditar) {
            if (!permisos.crear && !permisos.editar) {
                formsPanel[i].getForm().getFields().each(function (field) {
                    field.setReadOnly(true);
                });
            }
        }
        if (btnCrear) {
            if (!permisos.crear) {
                btnCrear.hide();
            } else {
                btnCrear.enable();
            }
        }
        if (btnEditar) {
            if (!permisos.editar) {
                btnEditar.hide();
            }
        }
        if (checkHabilitar) {
            if (!permisos.habilitar) {
                checkHabilitar.setReadOnly(true);
                checkHabilitar.disable();
            }
        }
    }
    if (!permisos.habilitar) {
        for (var i in gridsPanel) {
            var habilitadoColumn = getColumnByDataIndex(gridsPanel[i], 'habilitado');
            if (habilitadoColumn.getEditor) {
                habilitadoColumn.getEditor().setReadOnly(true);
                habilitadoColumn.getEditor().disable();
            }
        }
    }
}

function showTipConten(value, metaData, record, rowIdx, colIdx, store) {
    if (value && value !== '') {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        value = (value !== '') ? value : '<span style="color:red">s/n</span>';
        return value;
    }
    return "<center><span style='color:red;'>s/n</span></center>";
}

function formatMonitores(records, metaData, record, rowIdx, colIdx, store) {
    if (records) {
        if (records.length > 0) {
            var htmlMonitores = '<table>';
            for (var i in records) {
                htmlMonitores += '</tr>';
                htmlMonitores += '<td>' + records[i].nombres + ' (' + records[i].rol + ')</td>';
                htmlMonitores += '</tr>';
            }
            htmlMonitores += '</table>';
            metaData.tdAttr = 'data-qtip="' + htmlMonitores + '"';
            return htmlMonitores;
        } else {
            return '<span style="color:red;">Sin Monitores</span>';
        }
    } else {
        return '<span style="color:red;">Sin Monitores</span>';
    }
}

function formatEstado(validado) {
    var estado = '';
    (validado) ? estado = '<center style=\"color:green;font-size:20px;\" title=\"Validado\">&#8226;</center>' : estado = '<center style=\"color:red;font-size:20px;\" title=\"Sin Validar\">&#8226;</center>';
    return estado;
}

function formatEstadoInverso(validado) {
    var estado = '';
    (!validado) ? estado = '<center style="color:green;font-size:20px;" title="Validado">&#8226;</center>' : estado = '<center style="color:red;font-size:20px;" title="Sin Validar">&#8226;</center>';
    return estado;
}

function formatColor(color, metaData, record) {
    if (record) {
        var title = (record.get('estadoText')) ? record.get('estadoText') : color;
    }
    var htmlColor = '';
    htmlColor = (color !== '') ? '<center><span style="color:' + color + ';" title="' + title + '"><i class="fa fa-certificate" aria-hidden="true"></i><span></center>' : '';
    if (metaData === 'excel') {
        htmlColor = (color !== '') ? '<center><span style="color:' + color + ';">' + title + '<span></center>' : '';
    }
    return htmlColor;
}

function formatoSiNo(val) {
    var dat = Ext.create('Ktaxi.store.combos.s_Si_No').getById(val);
    if (dat !== null) {
        return '<b><span style="color:' + dat.get('color') + ';">' + dat.get('text') + '</span></b>';
    } else {
        console.log('Error revisar id: ' + val);
        return '<b><span style="color:#FF0000;">Unknown</span></b>';
    }
}

function formatEstadoRegistro(estado) {
    var htmlEstado = '';
    if (estado) {
        htmlEstado = '<span style="color:green;" title="Habilitado">Habilitado</span>';
    } else {
        htmlEstado = '<span style="color:red;" title="Habilitado">Deshabilitado</span>';
    }
    return htmlEstado;
}

function formatEstadoFecha(value, metaData, record) {
    var htmlEstado = '';
    if (record.data.habilitado) {
        htmlEstado = '<span style="color:green;">' + value + '</span>';
        metaData.tdAttr = "data-qtip='Habilitado el: " + value + "'";
    } else {
        htmlEstado = '<span style="color:red;">' + value + '</span>';
        metaData.tdAttr = "data-qtip='Deshabilitado el: " + value + "'";
    }
    return htmlEstado;
}

function buscaPersonaRegCivil(formData) {
    var form = Ext.create('Ext.form.Panel');
    form.getForm().submit({
        url: URL_REST_SERVICIOS + '/registrocivil/webresources/servicios/consultarPost',
        params: {
            ced: formData.down('[name=cedula]').getValue()
        },
        success: function (form, action) {
            var data = action.result.data;
            if (data.nombres === '-') {
                notificaciones("Revisar no existen registros de esta persona en registro civil", 2);
            } else if (data.apellidosNombres.split(" ").length === 3) {
                notificaciones("Revisar nombres y apellidos y validar manualmente", 2);
                formData.down('[name=nombres]').focus(true);
            }
            if (data.apellidosNombres !== '-') {
                formData.down('[name=apellidos]').setValue(data.apellidos);
                formData.down('[name=nombres]').setValue(data.nombres);
                formData.down('[name=direccion]').setValue(data.direccion);
//                formData.down('[name=gen]').setValue(1);
//                if (data.genero === "F") {
//                    formData.down('[name=gen]').setValue(2);
//                }
                formData.down('[name=fNacimiento]').setValue(Ext.Date.subtract(new Date(data.fechaNacimiento.substring(0, 10)), Ext.Date.DAY, -1));
                formData.down('[name=celular]').focus(true);
            }
        },
        failure: function (form, action) {
            notificaciones("No se puso consultar datos de esta persona", 2);
        }
    });
}

function formatTimeReporte(value) {
    if (value && value !== '') {
        return value.substr(0, 5);
    }
    return '00:00';
}

function showAuditoria(grid, record, extra) {
    var idColumn = grid.eventPosition.column.dataIndex;
    if (idColumn === 'id' || idColumn === 'idAdmin' || extra === 'gridAux') {
        var registro = (record.data.idUserCreate && record.data.idUserCreate !== '') ? record.data.idUserCreate : 0;
        var actualizo = (record.data.idUserUpdate && record.data.idUserUpdate !== '') ? record.data.idUserUpdate : 0;
        var cambio = (record.data.idUserChange && record.data.idChange !== '') ? record.data.idUserChange : 0;

        var window = new Ext.Window({
            width: 450,
            title: 'Auditoria',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + record.id + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    }
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        window.close();
                    }
                }
            ]
        });
        Ext.Ajax.request({
            async: true,
            url: 'php/Get/getAuditoria.php',
            params: {
                idRegistro: registro,
                idActualizo: actualizo,
                idCambio: cambio
            },
            callback: function (callback, e, response) {
                window.down('form').removeAll();
                var res = JSON.parse(response.request.result.responseText);
                if (res.success) {
                    var panelCreate = {
                        xtype: 'fieldset',
                        title: 'Datos de creación',
                        width: '100%',
                        cls: 'panelesAuditoria',
                        layout: 'hbox',
                        items: [
                            {
                                flex: 2,
                                defaultType: 'displayfield',
                                items: [
                                    {
                                        fieldLabel: '<b>Creado el</b>',
                                        bind: Ext.Date.format(new Date(record.data.dateCreate), 'j \\de F Y, \\a \\l\\a\\s H:m')
                                    }, {
                                        fieldLabel: '<b>Creado por</b>',
                                        bind: res.data.nombreReg
                                    }, {
                                        fieldLabel: '<b>C.I</b>',
                                        bind: res.data.cedulaReg
                                    }, {
                                        fieldLabel: '<b>Celular</b>',
                                        bind: res.data.celularReg
                                    }
                                ]
                            }, {
                                flex: 1,
                                xtype: 'image',
                                src: URL_IMG + 'uploads/admins/' + res.data.imagenReg,
                                style: 'border-radius: 50%; border: solid; border-color: #5ecac2;',
                                listeners: {
                                    render: function (me) {
                                        me.el.on({
                                            error: function (e, t, eOmpts) {
                                                me.setSrc(URL_IMG_SISTEMA + 'usuario.png');
                                            }
                                        });
                                    }
                                }
                            }

                        ]
                    };
                    var panelUpdate = {
                        xtype: 'fieldset',
                        title: 'Datos de edición',
                        width: '100%',
                        cls: 'panelesAuditoria',
                        layout: 'hbox',
                        items: [
                            {
                                flex: 2,
                                defaultType: 'displayfield',
                                items: [
                                    {
                                        fieldLabel: '<b>Editado el</b>',
                                        bind: Ext.Date.format(new Date(record.data.dateUpdate), 'j \\de F Y, \\a \\l\\a\\s H:m')
                                    }, {
                                        fieldLabel: '<b>Editado por</b>',
                                        bind: res.data.nombreAct
                                    }, {
                                        fieldLabel: '<b>C.I</b>',
                                        bind: res.data.cedulaAct
                                    }, {
                                        fieldLabel: '<b>Celular</b>',
                                        bind: res.data.celularAct
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'image',
                                src: URL_IMG + 'uploads/admins/' + res.data.imagenAct,
                                style: 'border-radius: 50%; border: solid; border-color: #5ecac2;',
                                listeners: {
                                    render: function (me) {
                                        me.el.on({
                                            error: function (e, t, eOmpts) {
                                                me.setSrc(URL_IMG_SISTEMA + 'usuario.png');
                                            }
                                        });
                                    }
                                }
                            }
                        ]
                    };
                    var fieldTextDate = '';
                    var fieldTextUser = '';
                    if (record.data.idUserChange && record.data.idChange !== '') {
                        if (record.data.habilitado) {
                            fieldTextDate = '<b>Habilitado el</b>';
                            fieldTextUser = '<b>Habilitado por</b>';
                        } else {
                            fieldTextDate = '<b>Deshabilitado el</b>';
                            fieldTextUser = '<b>Deshabilitado por</b>';
                        }
                    }
                    var panelChange = {
                        xtype: 'fieldset',
                        title: 'Datos de edición',
                        width: '100%',
                        cls: 'panelesAuditoria',
                        layout: 'hbox',
                        items: [
                            {
                                flex: 2,
                                defaultType: 'displayfield',
                                items: [
                                    {
                                        fieldLabel: fieldTextDate,
                                        bind: Ext.Date.format(new Date(record.data.dateChange), 'j \\de F Y, \\a \\l\\a\\s H:m')
                                    }, {
                                        fieldLabel: fieldTextUser,
                                        bind: res.data.nombreCamb
                                    }, {
                                        fieldLabel: '<b>C.I</b>',
                                        bind: res.data.cedulaCamb
                                    }, {
                                        fieldLabel: '<b>Celular</b>',
                                        bind: res.data.celularCamb
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'image',
                                src: URL_IMG + 'uploads/admins/' + res.data.imagenCamb,
                                style: 'border-radius: 50%; border: solid; border-color: #5ecac2;',
                                listeners: {
                                    render: function (me) {
                                        me.el.on({
                                            error: function (e, t, eOmpts) {
                                                me.setSrc(URL_IMG_SISTEMA + 'usuario.png');
                                            }
                                        });
                                    }
                                }
                            }
                        ]
                    };
                    (record.data.idUserCreate && record.data.idUserCreate !== '') ? window.down('form').add(panelCreate) : '';
                    (record.data.idUserUpdate && record.data.idUserUpdate !== '') ? window.down('form').add(panelUpdate) : '';
                    (record.data.idUserChange && record.data.idUserChange !== '') ? window.down('form').add(panelChange) : '';
                    window.show();
                } else {
                    notificaciones(res.error, 2);
                }
            }
        });
    }
}
function validateRowEdit(value, metaData, record, rowIdx, colIdx, store, gridView) {
    var column = gridView.getGridColumns()[colIdx];
    if (value && value !== '') {
        switch (column.getEditor().xtype) {
            case 'datefield':
                value = Ext.Date.format(new Date(value), 'Y/m/d');
                break;
            case 'timefield':
                value = Ext.Date.format(new Date(value), 'H:i');
                break;
            case 'checkbox':
                value = formatEstado(value);
                break;
        }
        metaData.tdAttr = "data-qtip=\'" + value + "\'";
        return value;
    }
    if (column.getEditor().xtype === 'checkbox') {
        value = formatEstado(value);
        return value;
    }
    return "<center><span style='color:gray;'>" + column.text + "...</span></center>";
}

function getColumnByDataIndex(grid, dataIndex) {
    for (var i in grid.getColumns()) {
        if (grid.getColumns()[i].dataIndex === dataIndex) {
            return grid.getColumns()[i];
        }
    }
    return false;
}

function formatFecha(fecha, metaData) {
    if (fecha) {
        fecha = Ext.Date.format(fecha, 'Y-m-d');
        (metaData) ? metaData.tdAttr = "data-qtip=\'" + fecha + "\'": '';
        return fecha;
    } else {
        return 's/n';
    }
}
function isJsonString(texto) {
    texto = texto.toString();
    if (texto[0] === "{" && texto[texto.length - 1] === "}")
        return true;
    if (texto[0] === "[" && texto[texto.length - 1] === "]")
        return true;
    return false;
}
function formatEstadoPunto(estado, metaData) {
    var htmlEstado = '';
    if (estado) {
        htmlEstado = '<span style="color:green;" title="Habilitado">';
    } else {
        htmlEstado = '<span style="color:red;" title="Deshabilitado">';
    }
    if (metaData === 'excel') {
        htmlEstado += '&#8226;</span>';
        return htmlEstado;
    }
    return htmlEstado + '<i class="fa fa-certificate" aria-hidden="true"></i></span>';
}

function setMensajeGridEmptyText(grid, mensaje) {
    grid.getStore().removeAll();
    grid.getView().setEmptyText('<center>' + mensaje + '</center>');
}

function showTipContenID(value, metaData, record, rowIdx, colIdx, store) {
    if (metaData) {
        metaData.tdAttr = 'data-qtip="' + value + '"';
        metaData.style = 'text-align:center !important; cursor: pointer;';
    }
    return value;
}
function mensajesValidacionForms(fields) {
    fields.each(function (field) {
        if (!field.isValid()) {
            switch (field.name) {
                case 'compania':
                    if (field.getErrors()[0] === "ESTE CAMPO NO PUEDE SER VACÍO") {
                        return  notificaciones("Seleccione o ingrese una compañia", 2);
                    } else if (field.getErrors()[0].includes("minimo")) {
                        return  notificaciones("Ingrese los valores minimos para la Compañia. Min. " + field.minLength, 2);
                    } else if (field.getErrors()[0].includes("maximo")) {
                        return  notificaciones("Ingrese los valores maximos para el Compañia. Max. " + field.maxLength, 2);
                    }
                    break;

            }
        }
    });
}


function onProcesarPeticion(response, funcionLimpiar) {
    if (response.operations.length > 0) {
        var obj = {};
        if (isJsonString(response.operations[0]._response.responseText)) {
            obj = JSON.parse(response.operations[0]._response.responseText);
            if (obj.success)
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
            else {
                if (obj.error)
                    notificaciones(obj.error, 2);
                else if (obj.message)
                    notificaciones(obj.message, 2);
                else
                    notificaciones(MENSAJE_ERROR_CRUD, 2);
            }
        } else {
            obj.resAjax = -2;
            obj['error'] = "Exite un error al crear el JSON";
            obj['message'] = "Exite un error al crear el JSON";
            notificaciones(obj.message, 2);
        }
    }
    if (typeof funcionLimpiar === 'function')
        funcionLimpiar();
}
function getRecord(comp, clave, valor) {
    var record = comp.getStore().findRecord(clave, valor);
    if (record) {
	comp.setValue(record);
	return record;
    } else {
	var index = comp.getStore().findExact(clave, valor);
	if (index > 0) {
	    record = comp.getStore().data.items[index];
	    comp.setValue(record);
	    return record;
	} else
	    return false;
    }
}
function procesarStoresForms(comp, records, success, bandera) {
    if (bandera === undefined || bandera === null)
	bandera = true;
    if (!success)
	if (bandera)
	    notificaciones("Por favor selecione manualmente el elemento que le corresponde.", 2);
	else if (records.length === 0 && bandera)
	    notificaciones("Por favor selecione manualmente el elemento que le corresponde", 2);
	else
	    comp.setValue(records);
}