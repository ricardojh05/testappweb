
function run(callback) {
    $.ajax({
        type: "POST",
        url: 'php/Login/getUserLogin.php',
        data: {idAdministrador: getCookie('ID_ADMINISTRADOR'), usuario: getCookie('USUARIO'), latitud: getCookie('LATITUD'), longitud: getCookie('LONGITUD'), path: getCookie('PATH')},
        success: function (response) {
            var data = JSON.parse(response);
            if (data.success === false) {
                borrarCookies();
                alert(data.message);
                window.location = 'php/Login/logout.php';
                return callback(-1);
            } else {
                USUARIO = data.usuario;
                MODULOS = data.children;
                if (USUARIO.BLOQUEADO === 1) {
                    borrarCookies();
                    window.location = 'php/Login/logout.php';
                    return callback(-1);
                }
                notificaciones(data.message, 1);
                inicializarEntorno();
                return callback(1);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            notificaciones(MENSAJE_ERROR, 2);
        }
    });
}