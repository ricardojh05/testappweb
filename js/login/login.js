$(document).ready(function () {
    $('input[type=password]').keyup(function (e) {
        if (e.keyCode === 13) {
            login();
        }
    });
    $('#bnt-login').click(function () {
        login();
    });
});
function mostrarClave(isSelect, id) {
    if (isSelect) {
        document.getElementById(id).type = "text";
    } else {
        document.getElementById(id).type = "password";
    }
}
function login() {
//    $('#cargandoModal').modal('show');
    $.ajax({
        type: "POST",
        url: 'php/Login/login.php',
        data: {usuario: $('#usuario').val(), contrasenia: hex_md5($('#contrasenia').val()), latitud: $('#latitud').val(), longitud: $('#longitud').val()},
        success: function (response) {
            var data = JSON.parse(response);
            if (data.success === true) {
                createCookie("ID_ADMINISTRADOR", data.sesion.ID_ADMINISTRADOR, 1);
                createCookie("LONGITUD", data.sesion.LNG_U, 1);
                createCookie("LATITUD", data.sesion.LAT_U, 1);
                createCookie("USUARIO", data.sesion.USUARIO, 1);
                createCookie("INICIO", data.MODULO.INICIO, 1);
                createCookie("PATH", data.MODULO.PATH, 1);
                createCookie("MODULO", data.MODULO.MODULO, 1);
                if (data.MODULO.PATH.length > 0)
                    location.href = data.MODULO.PATH;
                else
                    notificaciones(MENSAJE_NO_TIENE_ASIGNADO_URL, 2);
            }
            else
                notificaciones(data.message, 2);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            notificaciones(MENSAJE_ERROR, 2);
        }
    });
}