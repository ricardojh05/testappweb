var permisosCompania;
Ext.define('Ktaxi.view.compania.v_Compania', {
    extend: 'Ext.panel.Panel',
    xtype: 'compania',
//    title: TITULO,
    store: 'compania.s_Compania',
    controller: 'c_Compania',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'moduloCompania',
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.compania.c_Compania'
    ],
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onViewCompania',
        show: 'onShowCompania'
    },
    initComponent: function () {
        var STORE_COMPANIA = Ext.create('Ktaxi.store.compania.s_Compania');
        var STORE_PAISES = Ext.create('Ktaxi.store.combos.s_Paises');
        var STORE_ADMINISTRADOR_COMBO = Ext.create('Ktaxi.store.combos.s_Administrador');
        var STORE_ADMINISTRADORES_COMPANIA = Ext.create('Ktaxi.store.compania.s_Administrador_Compania');
        this.items = [
            {
//                id: 'panelLeerCompania',
                name: 'panelLeerCompania',
                region: 'west',
                xtype: 'panel',
                padding: 5,
                width: '38%',
                layout: 'fit',
                header: false,
                headerAsText: false,
                items: [{
//                        id: 'gridLeerCompanias',
                        name: 'gridLeerCompanias',
                        columnLines: true,
                        xtype: 'grid',
                        plugins: [{ptype: 'gridfilters'}],
                        bufferedRenderer: false,
                        store: STORE_COMPANIA,
                        tbar: [
                            {
                                xtype: 'textfield',
                                width: '60%',
                                tooltip: 'Escribir búsqueda',
                                name: 'paramBusquedaCompania',
                                emptyText: 'Compania, calle principal, calle secun..',
                                minChars: 0,
                                typeAhead: true,
                                listeners: {
                                    specialkey: 'onChangeSearchCompania'
                                }
                            },
                            {
                                xtype: 'combobox',
                                width: '20%',
                                tooltip: 'Seleccionar País',
                                name: 'comboBuscarPais',
                                emptyText: 'Todos',
                                reference: 'states',
                                value: 1,
                                displayField: 'pais',
                                valueField: 'idPais',
                                filterPickList: true,
                                queryParam: 'param',
                                queryMode: 'remote',
                                growMax: 20,
                                store: STORE_PAISES,
                                listeners: {
                                    specialkey: 'onChangeSearchCompania'
                                }
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-search',
                                iconAlign: 'right',
                                tooltip: 'Buscar',
                                handler: 'onChangeSearchCompania'
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-eraser',
                                iconAlign: 'right',
                                tooltip: 'Limpiar',
                                handler: 'onLimpiar'
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-refresh',
                                iconAlign: 'right',
                                tooltip: 'Recargar',
                                handler: 'onRecargar'
                            }
                        ],
                        columns: [
                            Ext.create('Ext.grid.RowNumberer', {header: '#',flex:1, align: 'center'}),
                            {filter: true, tooltip: "Compania", text: "Compania", flex:2, dataIndex: 'compania', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Calle Principal", text: "Calle Principal", flex:2, dataIndex: 'callePrin', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Calle Secundaria", text: "Calle Secundaria", flex:2, dataIndex: 'calleSec', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Color", text: "Color", flex:2, dataIndex: 'color', sortable: true, renderer: formatColor},
                            {filter: true, tooltip: "Contacto", text: "Contacto", flex:2, dataIndex: 'contacto', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Comentario", text: "Comentario", flex:2, dataIndex: 'comentario', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "ID", text: "ID", flex:2, dataIndex: 'id', sortable: true, renderer: showTipContenID, hidden: true}
                        ],
                        height: 210,
                        split: true,
                        region: 'north',
                        listeners: {
                            selectionchange: 'onSelectChangeGridCompania',
                            beforeitemclick: 'onBeforeclickGridCompania',
                            rowdblclick: showAuditoria
                        },
                        viewConfig: {enableTextSelection: true,
                            emptyText: '<center>No existen resultados.</center>'
                        },
                        bbar: Ext.create('Ext.PagingToolbar', {
                            store: STORE_COMPANIA,
                            displayInfo: true,
                            emptyMsg: "Sin datos que mostrar.",
                            displayMsg: ' {0} - {1} de {2} registros',
                            beforePageText: 'Página',
                            afterPageText: 'de {0}',
                            firstText: 'Primera página',
                            prevText: 'Página anterior',
                            nextText: 'Siguiente página',
                            lastText: 'Última página',
                            refreshText: 'Actualizar',
                            inputItemWidth: 35,
                            items: [
                                {
                                    xtype: 'label',
                                    name: 'numRegistrosGrid',
                                    text: '0 registros'
                                }
                            ],
                            listeners: {
                                afterrender: function () {
                                    this.child('#refresh').hide();
                                }
                            }
                        })
                    }]
            },
            {
//                id: 'panelCrearEditarCompania',
                name: 'panelCrearEditarCompania',
                cls: 'panelCrearEditar',
                region: 'center',
                xtype: 'form',
                layout: 'fit',
                padding: 5,
                flex: 2,
                collapsible: false,
                items: [{
                        xtype: 'form',
                        name: 'formCrearEditarCompania',
                        title: 'Compania',
                        layout: 'vbox',
                        padding: 5,
                        flex: 2,
                        cls: 'quick-graph-panel shadow panelFormulario',
                        ui: 'light',
                        defaultType: 'textfield',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [
                            {
                                xtype: 'combobox',
                                width: '100%',
                                fieldLabel: 'País',
                                tooltip: 'Seleccionar País',
                                name: 'pais',
                                value: 1,
                                emptyText: 'Seleccione',
                                reference: 'states',
                                displayField: 'pais',
                                valueField: 'idPais',
                                filterPickList: true,
                                queryParam: 'param',
                                queryMode: 'remote',
                                maxLength: '10',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                store: STORE_PAISES,
                                listeners: {
                                    select: function (record) {
                                        centrar(LIST_MAPS[ID_MAPA_COMPANIA], record.selection.data.latitud, record.selection.data.longitud, 6);
                                    },
                                }
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Compania',
                                name: 'compania',
                                emptyText: 'Compania',
                                maxLength: '150',
                                width: '100%',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                listeners: {
                                    change: function keyup(thisObj, e, eOpts) {
                                        thisObj.setValue(e.toUpperCase());
                                    }
                                }
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'RUC',
                                name: 'ruc',
                                emptyText: 'RUC',
                                maxLength: '13',
                                width: '100%',
                                minLength: '13',
                                maskRe: /[0-9.]/,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Calle Prin.',
                                name: 'callePrin',
                                emptyText: 'Calle principal',
                                maxLength: '120',
                                width: '100%',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Calle secun',
                                name: 'calleSec',
                                emptyText: 'Calle secundaria',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                maxLength: '120',
                                width: '100%',
                                minLength: '4',
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Contacto',
                                name: 'contacto',
                                maskRe: /[0-9.]/,
                                emptyText: 'Contacto',
                                maxLength: '12',
                                width: '100%',
                                minLength: '9',
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Color',
                                name: 'color',
                                width: '100%',
                                inputType: 'color',
                                emptyText: 'Color',
                                height: 25,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Latitud',
                                name: 'latitud',
                                emptyText: 'Latitud',
                                maxLength: '100',
                                minLength: '4',
                                width: '100%',
                                maskRe: /[0-9.]/,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID

                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Longitud',
                                name: 'longitud',
                                emptyText: 'Longitud',
                                maxLength: '100',
                                minLength: '4',
                                width: '100%',
                                maskRe: /[0-9.]/,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'textareafield',
                                allowBlank: true,
                                fieldLabel: 'Comentario',
                                name: 'comentario',
                                emptyText: 'Comentario',
                                width: '100%',
                                maxLength: '500',
                                minLength: '10',
                                allowOnlyWhitespace: true,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype:'textfield',
                                name:'bandera',
                                hidden:true,
                            },
                            ,
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                width: '100%',
                                defaultType: 'combobox',
                                defaults: {
                                    margin: 1
                                },
                                items: [
                                    {
                                        allowBlank: true,
                                        fieldLabel: 'Administrador',
                                        xtype: 'combobox',
                                        tooltip: 'Buscar Administradores',
                                        name: 'comboAdministrador',
                                        emptyText: 'Cédula..',
                                        displayField: 'nombres',
                                        valueField: 'id',
                                        filterPickList: true,
                                        forceSelection: true,
                                        queryParam: 'cedula',
                                        queryMode: 'remote',
                                        store: STORE_ADMINISTRADOR_COMBO,
                                        minChars: 9
                                    },
                                    {
                                        xtype: 'button',
                                        name: 'agregarAdministrador',
                                        flex: 1,
                                        iconCls: 'x-fa fa-plus',
                                        height: 23,
                                        handler: 'onAddAdministrador'}
                                ]
                            },
                            {
                                xtype: 'grid',
                                height: 50,
                                width: '100%',
//                                id: 'gridAuxEmpAdmin',
                                name: 'gridAdministradoresCompania',
                                autoScroll: true,
                                bufferedRenderer: false,
                                store: STORE_ADMINISTRADORES_COMPANIA,
                                cls: 'gridAuxAdmCompania',
                                columns: [
                                    {flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro },
                                    {flex: 2, dataIndex: 'nombre', sortable: true},
                                    {
                                        xtype: 'actioncolumn',
                                        menuDisabled: true,
                                        sortable: false,
                                        minWidth: 20,
                                        flex: 1,
                                        items: [{
                                                getClass: function (v, meta, rec) {
                                                    if (rec.data.habilitado) {
                                                        return 'gridAuxDelete x-fa fa-times';
                                                    } else {
                                                        return 'gridAuxCheck x-fa fa-check';
                                                    }
                                                },
                                                handler: function (grid, rowIndex, colIndex) {
                                                    var rec = grid.getStore().getAt(rowIndex);
                                                    MODULO_COMPANIA.down('[name=formCrearEditarCompania]').down('[name=bandera]').setValue(true);
                                                    if (rec.data.nuevo) {
                                                        grid.getStore().remove(rec);
                                                    } else {
                                                        if (rec.data.habilitado) {
                                                            rec.set('habilitado', 0);
                                                        } else {
                                                            rec.set('habilitado', 1);
                                                        }
                                                    }
                                                }
                                            }]
                                    }
                                ],
                                minHeight: 150,
                                split: true,
                                region: 'north',
                                viewConfig: {enableTextSelection: true,
                                    emptyText: '<center>Sin administradores asignados..</center>',
                                    getRowClass: function (record) {
                                        if (record.data.nuevo) {
                                            return 'newRowGrid';
                                        }
                                    }
                                },
                                listeners: {
                                    rowdblclick: function (grid, record) {
                                        if (!record.data.nuevo) {
                                            showAuditoria(grid, record, 'gridAux');
                                        }
                                    }
                                }
                            }
                        ]
                    }],
                dockedItems: [{
                        ui: 'footer',
                        xtype: 'toolbar',
                        dock: 'bottom',
                        defaults: {
                            width: '25%',
                            height: 30
                        },
                        items: [
                            {
                                text: 'Limpiar',
                                tooltip: 'Limpiar',
                                disabled: false,
                                handler: 'onLimpiar'
                            },
                            '->',
                            {
                                text: 'Editar',
                                tooltip: 'Actualizar',
//                                id: 'btnEditarCompania',
                                disabled: true,
                                name: 'btnEditar',
                                handler: 'onUpdate'
                            }, {
                                text: 'Crear',
                                tooltip: 'Crear Ciudades',
//                                id: 'btnCrearCompania',
                                disabled: true,
                                name: 'btnCrear',
                                handler: 'onCreate'
                            }]
                    }]
            },
            {
                region: 'east',
                xtype: 'panel',
                width: '35%',
                margin: 5,
                header: false,
                collapsible: true,
                collapsed: false,
                html: '<div id="' + ID_MAPA_COMPANIA + '" style="position: absolute; top: 0; left: 0; z-index: 0; width: 100%; height: 100%"></div>',
            }
        ];
        this.callParent(arguments);
    }
});

