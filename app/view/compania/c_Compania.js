var MODULO_COMPANIA;
Ext.define('Ktaxi.view.compania.c_Compania', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Compania',
    onViewCompania: function (panelLoad) {
        panelLoad.ID_MAPA = ID_MAPA_COMPANIA;//para cargar el id del nuevo mapa
        cargarMapa(panelLoad, ID_MAPA_COMPANIA);
        validarPermisosGeneral(panelLoad);
        MODULO_COMPANIA = panelLoad;
        MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().load();

    },
    onRecargar: function () {
        MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().reload();
    },
    onShowCompania: function (panelLoad) {
        PANELLOAD = panelLoad;
        PANELLOAD.ID_MAPA = ID_MAPA_COMPANIA;
    },
    onLimpiar: function (btn, e) {
        MODULO_COMPANIA.down('[name=btnCrear]').enable();
        MODULO_COMPANIA.down('[name=btnEditar]').disable();
        limpiarMapa(LIST_MAPS[ID_MAPA_COMPANIA]);
        MODULO_COMPANIA.down('[name=paramBusquedaCompania]').reset();
        var grid = MODULO_COMPANIA.down('[name=gridLeerCompanias]');
        grid.getView().deselect(grid.getSelection());
        validarPermisosGeneral(MODULO_COMPANIA);
        MODULO_COMPANIA.down('[name=gridAdministradoresCompania]').getStore().removeAll();
        MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().load();
        MODULO_COMPANIA.down('[name=formCrearEditarCompania]').getForm().reset();
    },
    onChangeSearchCompania: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var paramBusqueda = MODULO_COMPANIA.down('[name=gridLeerCompanias]').down('[name=paramBusquedaCompania]').getValue();
            var paises = MODULO_COMPANIA.down('[name=gridLeerCompanias]').down('[name=comboBuscarPais]').getValue();
            var params = {param: paramBusqueda, paises: (paises == null) ? 0 : paises};
            MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().load({
                params: params,
                callback: function (records) {
                    if (records.length <= 0) {
                        MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridCompania: function (thisObj, selected, eOpts) {
        if (selected.length > 0) {
            limpiarMapa(LIST_MAPS[ID_MAPA_COMPANIA]);
            MODULO_COMPANIA.down('[name=gridAdministradoresCompania]').getStore().removeAll();
            var comboFormPais = MODULO_COMPANIA.down('[name=formCrearEditarCompania]').down('[name=pais]');
            if (!isInStore(comboFormPais.getStore(), selected[0].data.pais, 'idPais', 'exact')) {
                comboFormPais.getStore().load({
                    params: {
                        param: selected[0].data.pais
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormPais.setValue(records);
                        }
                    }
                });
            }
            var comboAdministradores = MODULO_COMPANIA.down('[name=comboAdministrador]');
            comboAdministradores.getStore().proxy.extraParams = '';
            comboAdministradores.getStore().load();
            var formCrearEmpresa = MODULO_COMPANIA.down('[name=formCrearEditarCompania]');
            var gridAdministradores = MODULO_COMPANIA.down('[name=gridAdministradoresCompania]');
            gridAdministradores.getStore().load({
                params: {
                    idCompania: selected[0].data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        gridAdministradores.getStore().removeAll();
                    } else {
                        var asignados = [];
                        for (var i in records) {
                            asignados.push(records[i].data.id);
                        }
                        comboAdministradores.getStore().proxy.extraParams = {asignados: asignados.toString()};
                        comboAdministradores.getStore().load();
                    }
                }
            });
            formCrearEmpresa.loadRecord(selected[0]);
            graficarMarcador(LIST_MAPS[ID_MAPA_COMPANIA], selected[0].data.latitud, selected[0].data.longitud, null, 12, selected[0].data.color);
        }
    },
    onBeforeclickGridCompania: function (thisObj, record, item, index, e, eOpts) {
        MODULO_COMPANIA.down('[name=btnEditar]').enable();
        MODULO_COMPANIA.down('[name=btnCrear]').disable();
    },
    onCreate: function () {
        var params = this.getRecordsAdministrador();
        var me = this;
        var form = MODULO_COMPANIA.down('[name=formCrearEditarCompania]');
        if (form.isValid()) {
            MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().insert(0, form.getValues());
            MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().proxy.extraParams = params;
            MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiar({limpiar: true}));
                }});
        } else {
            var fields = form.form.getFields();
            mensajesValidacionForms(fields);
        }
    },

    onUpdate: function () {
       
        var params = this.getRecordsAdministrador();
        var me = this;
        var form = MODULO_COMPANIA.down('[name=formCrearEditarCompania]');
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().proxy.extraParams = params;
            MODULO_COMPANIA.down('[name=gridLeerCompanias]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiar({limpiar: true}));
                }});
        } else {
            var fields = form.getFields();
            mensajesValidacionForms(fields);
        }
    },
    onAddAdministrador: function (btn) {
        var comboAdministrador = MODULO_COMPANIA.down('[name=comboAdministrador]');
        var idAdministrador = comboAdministrador.getValue();
        var nombreAdministrador = comboAdministrador.getRawValue();
        MODULO_COMPANIA.down('[name=formCrearEditarCompania]').down('[name=bandera]').setValue(true);
        var newRecord = {
            id: idAdministrador,
            nombre: nombreAdministrador,
            habilitado: 1,
            nuevo: true
        };

        MODULO_COMPANIA.down('[name=gridAdministradoresCompania]').getStore().insert(0, newRecord);
        comboAdministrador.setValue(null);
    },
    getRecordsAdministrador: function () {
//        MODULO_COMPANIA.down('[name=comboAdministrador]').getStore().reload();
        var listAdministrador = [];
        var dataAuxAdministrador = MODULO_COMPANIA.down('[name=gridAdministradoresCompania]').getStore().data.items;
        for (var i in dataAuxAdministrador) {
            if (dataAuxAdministrador[i].data.nuevo) {
                listAdministrador[i] = {id: dataAuxAdministrador[i].data.id, habilitado: dataAuxAdministrador[i].data.habilitado};
            }
        }
        var dataAuxAdministrador = MODULO_COMPANIA.down('[name=gridAdministradoresCompania]').getStore().getUpdatedRecords();
        for (var i in dataAuxAdministrador) {
            listAdministrador[i] = {id: dataAuxAdministrador[i].data.id, habilitado: dataAuxAdministrador[i].data.habilitado};
        }
        return {administradores: JSON.stringify(listAdministrador)};
    }
});
