var MODULO_SUPER;
Ext.define('Ktaxi.view.super.c_Super', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Super',
    onViewSuper: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
        MODULO_SUPER = panelLoad;
        MODULO_SUPER.down('[name=gridLeerSuper]').getStore().load();
    },
    onChangeSearchSuper: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var panel = Ext.getCmp('panelSuper');
            var paramNombres = panel.down('[name=paramBusquedaNombres]').getValue();
            MODULO_SUPER.down('[name=gridLeerSuper]').getStore().load({
                params: {
                    param: paramNombres
                },
                callback: function (records) {
                    if (records.length <= 0) {
                        MODULO_SUPER.down('[name=gridLeerSuper]').getStore().removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridSuper: function (thisObj, selected, eOpts) {
        if (selected.length > 0) {
            var formSuper = Ext.getCmp('panelSuper').down('[name=panelCrearEditarSuper]');
            formSuper.down('[name=mostraContrasenia]').setVisible(false);
            formSuper.loadRecord(selected[0]);
            if (selected[0].data.imagen === '') {
                formSuper.down('[name=image]').setSrc(URL_IMG_SISTEMA + 'usuario.png');
            } else {
                formSuper.down('[name=image]').setSrc(URL_IMG + 'uploads/admins/' + selected[0].data.imagen);
            }
            var comboCompania = MODULO_SUPER.down('[name=comboCompania]');
            comboCompania.getStore().proxy.extraParams = '';
            comboCompania.getStore().load();
            //aplicativos asignados
            var gridCompania = MODULO_SUPER.down('[name=gridSuperCompania]');
            gridCompania.getStore().load({
                params: {
                    idAdministrador: selected[0].data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        gridCompania.getStore().removeAll();
                    } else {
                        var asignados = [];
                        for (var i in records) {
                            asignados.push(records[i].data.id);
                        }
                        gridCompania.getStore().proxy.extraParams = {asignados: asignados.toString()};
                        gridCompania.getStore().load();
                    }
                }
            });
        }
    },
    onCreateSuper: function () {
        var lista_compania = this.getRecordsCompania();
        var form = Ext.getCmp('panelSuper').down('[name=panelCrearEditarSuper]');
        var formUploadImage = Ext.getCmp('panelSuper').down('[name=formUploadImage]');
        if (form.isValid()) {
            if (Ext.getStore('s_Modulo_Super_Grid').getUpdatedRecords().length === 0) {
                Ext.MessageBox.buttonText = {
                    yes: "Sí",
                    no: "No"
                };
                Ext.MessageBox.confirm('Aviso!', '<b>Aún no ha seleccionado Accesos a este usuario.<br><center>¿desea continuar?</center></b>', function (choice) {
                    if (choice === 'yes') {
                        crearSuper(form, formUploadImage, lista_compania);
                        limpiarFormularioSuper();
                    } else {
                        showWindowsAccesosSuper();
                    }
                });
            } else {
                crearSuper(form, formUploadImage);
            }
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    },
    onUpdateSuper: function () {
        var lista_compania = this.getRecordsCompania();
        var me = this;
        var form = Ext.getCmp('panelSuper').down('[name=panelCrearEditarSuper]');
        if (changeImageSuper) {
            var formUploadImage = Ext.getCmp('panelSuper').down('[name=formUploadImage]');
            formUploadImage.submit({
                url: 'php/Uploads/upload.php?ruta=' + URL_SUBIR_IMG_ADMINS + '&nombre=' + form.down('[name=cedula]').getValue(),
                waitMsg: 'Cargando imagen...',
                success: function (fp, o) {
                    var gridLeerSuper = Ext.getCmp('panelSuper').down('[name=gridLeerSuper]');
                    var gridSelect = gridLeerSuper.getSelection();
                    gridSelect[0].set('imagen', o.result.message);
                    var listPermisos = [];
                    var dataAuxPermisosAdmin = Ext.getStore('s_Modulo_Super_Grid').getUpdatedRecords()
//                            dialog_permisos.down('[name=gridModuloAdmin]').getStore().getUpdatedRecords();
                    for (var i in dataAuxPermisosAdmin) {
                        listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
                    }
                    Ext.getStore('super.s_Super').proxy.extraParams = {permisos: JSON.stringify(listPermisos), lista_compania: lista_compania};
//                    if (Ext.getStore('s_Modulos').getUpdatedRecords().length > 0)
//                        Ext.getStore('super.s_Super').proxy.extraParams = {permisos: Ext.util.JSON.encode(getDataPermisos(Ext.getStore('s_Modulos')))};
                    form.updateRecord(form.activeRecord);
                    if (Ext.getStore('super.s_Super').getUpdatedRecords()[0].modified.contrasenia) {
                        gridSelect[0].set('contrasenia', hex_md5(gridSelect[0].data.contrasenia));
                    }
                    Ext.getStore('super.s_Super').sync();
                    limpiarFormularioSuper();
                },
                failure: function (fp, o) {
                    form.down('[name=image]').setSrc(URL_IMG_SISTEMA + 'usuario.png');
                    notificaciones(o.result.message, 2);
                }
            });
        } else {
            if (form.isValid()) {
                var gridLeerSuper = Ext.getCmp('panelSuper').down('[name=gridLeerSuper]');
                var gridSelect = gridLeerSuper.getSelection();
                var listPermisos = [];
                var dataAuxPermisosAdmin = Ext.getStore('s_Modulo_Super_Grid').getUpdatedRecords();
//                            dialog_permisos.down('[name=gridModuloAdmin]').getStore().getUpdatedRecords();
                for (var i in dataAuxPermisosAdmin) {
                    listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
                }
//                Ext.getStore('super.s_Super').proxy.extraParams = {permisos: JSON.stringify(listPermisos)};

                var listPerfiles = [];
                var dataAuxPerfil = Ext.getStore('s_Perfil_Super_Grid').data.items;
                for (var i in dataAuxPerfil) {
                    if (dataAuxPerfil[i].data.nuevo) {
                        listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo};
                    } else {
                        listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
                    }
                }
//                var listaCompania = [];
//                var dataAuxCompania = Ext.getStore('gridSuperCompania').data.items;
//                for (var i in dataAuxCompania) {
//                    if (dataAuxCompania[i].data.nuevo) {
//                        listaCompania[i] = {id: dataAuxCompania[i].data.id, habilitado: dataAuxCompania[i].data.habilitado, nuevo: dataAuxCompania[i].data.nuevo};
//                    } else {
//                        listaCompania[i] = {id: dataAuxCompania[i].data.id, habilitado: dataAuxCompania[i].data.habilitado, nuevo: false};
//                    }
//                }
                
                MODULO_SUPER.down('[name=gridLeerSuper]').getStore().proxy.extraParams = {permisos: JSON.stringify(listPermisos), perfiles: JSON.stringify(listPerfiles), lista_compania: lista_compania};
                form.updateRecord(form.activeRecord);
                MODULO_SUPER.down('[name=gridLeerSuper]').getStore().sync();
                limpiarFormularioSuper();
            } else {
                notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
            }
        }
    },
    onAddCompania: function (btn) {
        var comboCompania = MODULO_SUPER.down('[name=comboCompania]');
        var idCompania = comboCompania.getValue();
        var nombreCompania = comboCompania.getRawValue();
        var newRecord = {
            id: idCompania,
            nombre: nombreCompania,
            habilitado: 1,
            nuevo: true
        };
        MODULO_SUPER.down('[name=gridSuperCompania]').getStore().insert(0, newRecord);
        comboCompania.setValue(null);
    },
    getRecordsCompania: function () {
//        MODULO_SUCURSAL.down('[name=comboAdministrador]').getStore().reload();
        var listCompania = [];
        var dataAuxCompania = MODULO_SUPER.down('[name=gridSuperCompania]').getStore().data.items;
        for (var i in dataAuxCompania) {
            if (dataAuxCompania[i].data.nuevo) {
                listCompania[i] = {id: dataAuxCompania[i].data.id, habilitado: dataAuxCompania[i].data.habilitado, nuevo: dataAuxCompania[i].data.nuevo};
            }
        }
        var dataAuxCompania = MODULO_SUPER.down('[name=gridSuperCompania]').getStore().getUpdatedRecords();
        for (var i in dataAuxCompania) {
            console.log( dataAuxCompania[i].data.id);
            listCompania[i] = {id: dataAuxCompania[i].data.id, habilitado: dataAuxCompania[i].data.habilitado};
        }
        return {lista_compania: JSON.stringify(listCompania)};
    },
    buscarSuper: function () {
        var panel = Ext.getCmp('panelSuper');
        Ext.Ajax.request({
            async: true,
            url: 'php/Get/getAdministradorRegistrado.php',
            params: {
                cedula: MODULO_SUPER.down('[name=panelCrearEditarSuper]').down('[name=cedula]').getValue()
            },
            callback: function (callback, e, response) {
                var res = JSON.parse(response.request.result.responseText);
                if (response.request.success) {
                    if (res.administradores.length == 0) {
                        buscaPersonaRegCivil(panel.down('[name=panelCrearEditarSuper]'));
                    } else {
                        var form = MODULO_SUPER.down('[name=panelCrearEditarSuper]');
                        form.down('[name=nombres]').setValue(res.administradores[0].nombres);
                        form.down('[name=apellidos]').setValue(res.administradores[0].apellidos);
                        form.down('[name=fNacimiento]').setValue(res.administradores[0].fecha_nacimiento);
                        form.down('[name=celular]').setValue(res.administradores[0].celular);
                        form.down('[name=usuario]').setValue(res.administradores[0].usuario);
                        form.down('[name=contrasenia]').setValue(res.administradores[0].contrasenia);
                        form.down('[name=direccion]').setValue(res.administradores[0].direccion);
                        form.down('[name=correo]').setValue(res.administradores[0].correo);
                        var gridCompany = MODULO_SUPER.down('[name=gridSuperCompania]');
                        gridCompany.getStore().load({
                            params: {
                                idAdministrador: res.administradores[0].id
                            },
                            callback: function (records) {
                                if (records.length > 0) {
//                                    gridCompany.setValue(records);
                                }
                            }
                        });
                    }
                } else {
                    notificaciones('Lo sentimos hubo un error al validar el registro', 2);
                }
            }
        });
    }
});
function limpiarFormularioSuper() {
    MODULO_SUPER.down('[name=paramBusquedaNombres]').reset();
    MODULO_SUPER.down('[name=gridSuperCompania]').getStore().removeAll();
    var form_crear = MODULO_SUPER.down('[name=panelCrearEditarSuper]');
    form_crear.getForm().reset();
    var grid = MODULO_SUPER.down('[name=gridLeerSuper]');
    grid.getView().deselect(grid.getSelection());
    grid.getStore().load();
    var panel = Ext.getCmp('panelSuper');

//    panel.down('[name=panelCrearEditarSuper]').getForm().reset();
//    var gridLeerSuper = panel.down('[name=gridLeerSuper]');
//    gridLeerSuper.getView().deselect(gridLeerSuper.getSelection());
//    gridLeerSuper.getStore().reload();
    panel.down('[name=panelCrearEditarSuper]').down('[name=image]').setSrc(URL_IMG_SISTEMA + 'usuario.png');
    changeImageSuper = false;
    panel.down('[name=mostraContrasenia]').setVisible(true);
    panel.down('[name=msgB]').setVisible(false);
    panel.down('[name=msgR]').setVisible(false);
    panel.down('[name=msgB]').disable();
    panel.down('[name=msgR]').disable();
}
function showWindowsAccesosSuper() {
    windowAccesos = new Ext.create('Ext.window.Window', {
        title: 'Accesos',
        closable: true,
        layout: 'fit',
        modal: true,
        height: '75%',
        width: 400,
        items: [
            {
                xtype: 'grid',
                autoScroll: true,
                bufferedRenderer: false,
                store: 's_ModulosAdmin',
                cls: 'gridAuxModuloAdmin',
                columnLines: true,
                columns: [
                    {text: "Módulo", width: 100, dataIndex: 'modulo', sortable: true},
                    {
                        xtype: 'checkcolumn',
                        header: 'Leer',
                        dataIndex: 'leer',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    },
                    {
                        xtype: 'checkcolumn',
                        header: 'Crear',
                        dataIndex: 'crear',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    },
                    {
                        xtype: 'checkcolumn',
                        header: 'Editar',
                        dataIndex: 'editar',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    },
                    {
                        xtype: 'checkcolumn',
                        header: 'Eliminar',
                        dataIndex: 'eliminar',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    }
                ],
                forceFit: true,
                split: true,
                region: 'north'
            }
        ],
        buttons: [
            {
                iconCls: 'fa fa-times-circle',
                text: 'Cerrar ',
                tooltip: 'Cerrar y Guardar',
//                width: '100%',
                anchor: '100%',
                style: {
                    background: COLOR_SISTEMA,
                    border: '1px solid #36beb3',
                    '-webkit-border-radius': '5px 5px',
                    '-moz-border-radius': '5px 5px'
                },
                handler: function () {
                    if (Ext.getStore('s_Modulo_Super_Grid').getUpdatedRecords().length === 0) {
                        windowAccesos.close();
                        notificaciones('NO SE HAN REALIZADO CAMBIOS', 2);
                    } else {
                        windowAccesos.close();
                    }
                }
            }
        ]
    }).show();
}
function crearSuper(form, formUploadImage, lista_compania) {
    formUploadImage.submit({
        url: 'php/Uploads/upload.php?ruta=' + URL_SUBIR_IMG_ADMINS + '&nombre=' + form.down('[name=cedula]').getValue(),
        waitMsg: 'Cargando imagen...',
        success: function (fp, o) {
            var record = form.getValues();
            record.imagen = o.result.message;
            record.contrasenia = hex_md5(record.contrasenia);
//            if (Ext.getStore('s_Modulos').getUpdatedRecords().length > 0)
//                Ext.getStore('super.s_Super').proxy.extraParams = {permisos: Ext.util.JSON.encode(getDataPermisos(Ext.getStore('s_Modulos')))};
            var listPermisos = [];
            var dataAuxPermisosAdmin = Ext.getStore('s_Modulo_Super_Grid').getUpdatedRecords();
            for (var i in dataAuxPermisosAdmin) {
                listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
            }

            var listPerfiles = [];
            var dataAuxPerfil = Ext.getStore('s_Perfil_Super_Grid').data.items;
            for (var i in dataAuxPerfil) {
                if (dataAuxPerfil[i].data.nuevo) {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo};
                } else {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
                }
            }
            var storeSuper = MODULO_SUPER.down('[name=gridLeerSuper]').getStore();
            storeSuper.proxy.extraParams = {permisos: JSON.stringify(listPermisos), perfiles: JSON.stringify(listPerfiles), lista_compania: lista_compania};
            storeSuper.insert(0, record);
            storeSuper.sync();
        },
        failure: function (fp, action) {
            if (action.result.success === false) {
                notificaciones(action.result.message, 2);
            }
            var record = form.getValues();
            record.imagen = 'usuario.png';
            record.contrasenia = hex_md5(record.contrasenia);
//            if (Ext.getStore('s_Modulos').getUpdatedRecords().length > 0)
//                Ext.getStore('super.s_Super').proxy.extraParams = {permisos: Ext.util.JSON.encode(getDataPermisos(Ext.getStore('s_Modulos')))};
            var listPermisos = [];
            var dataAuxPermisosAdmin = Ext.getStore('s_Modulo_Super_Grid').getUpdatedRecords();
            for (var i in dataAuxPermisosAdmin) {
                listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
            }

            var listPerfiles = [];
            var dataAuxPerfil = Ext.getStore('s_Perfil_Super_Grid').data.items;
            for (var i in dataAuxPerfil) {
                if (dataAuxPerfil[i].data.nuevo) {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo};
                } else {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
                }
            }
            var storeSuper = MODULO_SUPER.down('[name=gridLeerSuper]').getStore();
            storeSuper.proxy.extraParams = {permisos: JSON.stringify(listPermisos), perfiles: JSON.stringify(listPerfiles), lista_compania: lista_compania};
            storeSuper.insert(0, record);
            storeSuper.sync();
        }
    });
}
function getDataPermisosSuper(store) {
    var array = [];
    var data = store.getUpdatedRecords();
    for (var i in store.getUpdatedRecords()) {
        array[i] = {id: data[i].data.id, leer: data[i].data.leer, crear: data[i].data.crear, editar: data[i].data.editar, eliminar: data[i].data.eliminar};
    }
    return array;
}

function onAddPerfilSuper(btn) {
    var comboPerfil = btn.up('container').items.items[0];
    var idPerfil = comboPerfil.getValue();
    var perfil = comboPerfil.getRawValue();
    var exist = isInStore(Ext.getStore('s_Perfil_Super_Grid'), idPerfil, 'id', 'exact');
    if (!exist) {
        if (idPerfil) {
            var newRecord = {
                id: idPerfil,
                perfil: perfil,
                habilitado: 1,
                nuevo: true
            };
            Ext.getStore('s_Perfil_Super_Grid').insert(0, newRecord);
            onCargarModulosPerfilSuper();
            comboPerfil.setValue(null);
        } else {
            notificaciones('SELECCIONE UNA EMPRESA', 2);
        }
    } else {
        notificaciones('EL PERFIL YA HA SIDO INGRESADO', 2);
    }
}
function onCargarModulosPerfilSuper() {
    var panel = Ext.getCmp('panelSuper');
//    panel.down('[name=panelCrearEditarSuper]').getForm().reset();
    var gridLeerSuper = panel.down('[name=gridLeerSuper]');
    var idAdmin = gridLeerSuper.getView().getSelection();
    if (idAdmin.length > 0) {
        idAdmin = idAdmin[0].data.id;
    } else {
        idAdmin = 0;
    }
    var records = Ext.getStore('s_Perfil_Super_Grid').getData().items;
    var perfiles = [];
    for (var i in records) {
        if (records[i].get('habilitado')) {
            perfiles.push(records[i].get('id'));
        }
    }
    if (perfiles.length > 0) {
        Ext.getStore('s_Modulo_Super_Grid').load({
            params: {
                perfiles: perfiles.toString(),
                idAdmin: idAdmin
            },
            callback: function () {
                if (records.lenth === 0) {
                    Ext.getStore('s_Modulo_Super_Grid').removeAll();
                }
            }
        });
    } else {
        Ext.getStore('s_Modulo_Super_Grid').removeAll();
    }
}

