var MODULO_ADMIN_VOUCHER;
var idAdminVoucher;
var compania;
var idCompania;
var anio;
var mes;
var fecha = new Date(), mes = fecha.getMonth() + 1, anio = fecha.getFullYear();
var arrayIdMesesAdm = [];
var arrayNombresMesesAdm = [];

var anios = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: '2019',
            "text": "2019"
        },
        {
            id: '2020',
            "text": "2020"
        },
        {
            id: '2021',
            "text": "2021"
        },
        {
            id: '2022',
            "text": "2022"
        },
        {
            id: '2023',
            "text": "2023"
        },
        {
            id: '2024',
            "text": "2024"
        },
        {
            id: '2025',
            "text": "2025"
        },
        {
            id: '2026',
            "text": "2026"
        },
        {
            id: '2027',
            "text": "2027"
        },
        {
            id: '2028',
            "text": "2028"
        }
    ]
});
var meses = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 1,
            "text": "Enero"
        },
        {
            id: 2,
            "text": "Febrero"
        },
        {
            id: 3,
            "text": "Marzo"
        },
        {
            id: 4,
            "text": "Abril"
        },
        {
            id: 5,
            "text": "Mayo"
        },
        {
            id: 6,
            "text": "Junio"
        },
        {
            id: 7,
            "text": "Julio"
        },
        {
            id: 8,
            "text": "Agosto"
        },
        {
            id: 9,
            "text": "Septiembre"
        },
        {
            id: 10,
            "text": "Octubre"
        },
        {
            id: 11,
            "text": "Noviembre"
        },
        {
            id: 12,
            "text": "Diciembre"
        },
    ]
});

var mesesHasta = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 1,
            "text": "Enero"
        },
        {
            id: 2,
            "text": "Febrero"
        },
        {
            id: 3,
            "text": "Marzo"
        },
        {
            id: 4,
            "text": "Abril"
        },
        {
            id: 5,
            "text": "Mayo"
        },
        {
            id: 6,
            "text": "Junio"
        },
        {
            id: 7,
            "text": "Julio"
        },
        {
            id: 8,
            "text": "Agosto"
        },
        {
            id: 9,
            "text": "Septiembre"
        },
        {
            id: 10,
            "text": "Octubre"
        },
        {
            id: 11,
            "text": "Noviembre"
        },
        {
            id: 12,
            "text": "Diciembre"
        },
    ]
});

Ext.define('Ktaxi.view.adminVoucher.c_AdminVoucher', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_AdminVoucher',

    onViewAdminVoucher: function (panelLoad) {
        MODULO_ADMIN_VOUCHER = panelLoad;
        validarPermisosGeneral(panelLoad);
        storeCreditoCompaniaAdminVoucher = Ext.create('Ktaxi.store.adminVoucher.s_AdminVoucher_Credito_Compania');
        storeCompania = Ext.create('Ktaxi.store.adminVoucher.s_AdminVoucher_Compania');
        var params = {
            anio: anio,
            mes: mes,
        };
        recargarStoreCompaniaAdmin(params);
    },

    onLimpiarGridCompania: function (btn, e) {
        MODULO_ADMIN_VOUCHER.down('[name=txtParam]').reset();
        //var id = getIdAdminVoucher();
        var params = {
            anio: anio,
            mes: mes,
            //idAdminVoucher: id
        };
        recargarStoreCompaniaAdmin(params);
        storeTextDefectoAdmin();
    },
    onRecargarGridCompania: function () {
        //var id = getIdAdminVoucher();
        var params = {
            anio: anio,
            mes: mes,
          //  idAdminVoucher: id
        };
        recargarStoreCompaniaAdmin(params);
        storeTextDefectoAdmin();
    },
    onRecargarGridConsumo: function () {
        MODULO_ADMIN_VOUCHER.down('[name=administracionConsumo]').getStore().reload();
    },

    onCrearCompaniaCredito: function () {
        var form = MODULO_ADMIN_VOUCHER.down('[name=panelCrearCompaniaAdminVoucher]').getForm();
        if (form.isValid()) {
            var val = form.getValues();
            if (val.nombresAC !== null && val.nombresAC !== '') {
                var numero = codigo();
                Ext.MessageBox.confirm('Atención!!!', '<h4>Está seguro que desea activar a ' + compania + ' como cliente adminVoucher<br><b>CÓDIGO</b><br>' + numero + '<h4>', function (choice) {
                    if (choice === 'yes') {
                        var params = {};
                        var form = Ext.create('Ext.form.Panel');

                        if (idAdminVoucher === undefined && idAdminVoucher > 0)
                            params = {
                                id: recordCompania.id,
                                codigo: numero
                            };
                        else
                            params = {
                                id: recordCompania.id,
                                codigo: numero,
                                idE: idAdminVoucher,
                                idAdminVoucher: idAdminVoucher
                            };

                        form.submit({
                            url: 'php/AdminVoucher/createCompania.php',
                            timeout: 6000000,
                            waitTitle: 'Procesando Información',
                            waitMsg: 'Procesando...',
                            params: params,
                            success: function (form, action) {
                                notificaciones('Compania Agregado Correctamente', 1);
                                var params = {
                                    anio: anio,
                                    mes: mes,
                                    idAdminVoucher: idAdminVoucher,
                                    idAplicativo: idAplicativo
                                };
                                recargarStoreCreditoAdmin(params);
                                recargarStoreCompaniaAdmin(params);
                                storeTextDefectoAdmin();

                            },
                            failure: function (form, action) {
                                notificaciones('No se pudo realizar la operación, intentelo nuevamente', 2);
                                MODULO_ADMIN_VOUCHER.down('[name=panelCrearCompaniaAdminVoucher]').getForm().reset();
                            }
                        });
                    }
                });
            } else {
                messageInformationEffect('Debe dar click en buscar');
            }
        } else {
            messageInformationEffect('Debe dar click en buscar');
        }
    },
    onGetClass: function (v, meta, rec) {
        if (rec.data.tipo === 1) {
            return 'x-fa fa-user';
        } else if (rec.data.tipo == 2) {
            return 'x-fa fa-check';
        }
    },
    onSelectGridCompania: function (thisObj, selected, eOpts) {
        idCompania= selected.data.idCompania,
        idAdminVoucher= selected.data.idAdminVoucher,
       // anio= selected.data.anio,
        //mes=selected.data.mes
        compania=selected.data.compania
        idCompaniaCompania=selected.data.idCompaniaCompania
        MODULO_ADMIN_VOUCHER.down('[name=asignarCredito]').enable();
        MODULO_ADMIN_VOUCHER.down('[name=administracionConsumo]').getStore().load({
            params: {
                idCompania: selected.data.idCompania,
                //idAdminVoucher: selected.data.idAdminVoucher,
                anio: selected.data.anio,
                mes: selected.data.mes
            },
            callback: function (records, operation, success) {
                MODULO_ADMIN_VOUCHER.down('[name=clienteAdmCosumo]').setText('Compania: ' + selected.data.compania);
                MODULO_ADMIN_VOUCHER.down('[name=creditoAdmCosumo]').setText('Crédito: $' + selected.data.creditoMes);
                MODULO_ADMIN_VOUCHER.down('[name=consumoAdmCosumo]').setText('Cosumido: $' + selected.data.consumoMes);
            }
        });
    },
    onShowVentanaAsignarCredito: function () {
        vnt_AsignarCredito = Ext.create('Ext.window.Window', {
            title: 'Ventana de asignar credito a un compania',
            iconCls: 'icon-services',
            layout: 'anchor',
            //            closeAction: 'hide',
            closeAction: 'destroy',
            resizable: false,
            closable: true,
            modal: true,
            plain: false,
            width: 800,
            height: 380,
            items: [{
                    layout: 'anchor',
                    width: '100%',
                    height: '100%',
                    xtype: 'panel',
                    name: 'pnl-AsigCred',
                    items: [{
                            xtype: 'label',
                            text: 'Agregar credito a ' + compania,
                            id: 'lbl-titleACr',
                            name: 'lbl-titleACr',
                            style: 'aling:center;font-size: 22px; font-weight: bold;'
                        },
                        {
                            xtype: 'label',
                            html: '<hr>',
                            id: 'lbl-lineaACr',
                            name: 'lbl-lineaACr',
                            style: 'font-size: 12px;'
                        },
                        {
                            xtype: 'label',
                            text: 'El dinero que usted asigne al compania se podra usar solo por este mes.',
                            id: 'lbl-descrACr',
                            name: 'lbl-descrACr',
                            style: 'font-size: 12px;'
                        }
                    ]
                },
                {
                    layout: 'anchor',
                    width: '100%',
                    xtype: 'form',
                    height: '100%',
                    name: 'formAsignarCredito',
                    items: [{
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                            items: [{
                                    name: 'dateAnios',
                                    xtype: 'combobox',
                                    emptyText: 'Años',
                                    fieldLabel: '<b>Año</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    value: anio,
                                    queryMode: 'remote',
                                    store: anios
                                },
                                {
                                    name: 'dateMes',
                                    xtype: 'combobox',
                                    emptyText: 'Mes',
                                    fieldLabel: '<b>Mes</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    store: meses,
                                    listeners: {
                                        select: function (combo, record, eOpts) {
                                            arrayNombresMesesAdm = [];
                                            arrayIdMesesAdm = [];
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').getStore().removeAll();                                            
                                            var rangoincial = record.data.id;
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').setValue(0);
                                            var rangoincial = record.data.id;
                                            for (let i = 0; i < 4; i++) {
                                                arrayIdMesesAdm.push(rangoincial);
                                                arrayNombresMesesAdm.push(getNombreMes(rangoincial));
                                                rangoincial++;
                                            }
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').setStore(arrayNombresMesesAdm);
                                        }
                                    }
                                },
                                {
                                    name: 'dateMesHasta',
                                    xtype: 'combobox',
                                    emptyText: 'Mes',
                                    fieldLabel: '<b>Hasta el Mes</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    //hidden:true,
                                    store: mesesHasta
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'numberfield',
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    maxValue: 500000,
                                    minValue: 0,
                                    name: 'montoACr',
                                    id: 'montoACr',
                                    fieldLabel: '<b>Monto</b>',
                                    flex: 2,
                                    emptyText: 0,
                                    enableKeyEvents: true,
                                    decimalPrecision: 2,
                                    allowDecimals: true,
                                    alwaysDisplayDecimals: true,
                                    listeners: {
                                        blur: function (thisObj, e, eOpts) {
                                            var idC = Ext.getCmp('nombresACr').getValue();
                                            if (idC === null) {
                                                Ext.getCmp('nombresACr').focus(true);
                                            } else {
                                                Ext.getCmp('razonACr').focus(true);
                                            }
                                        }
                                    }
                                },
                                {
                                    flex: 2,
                                    xtype: 'label',
                                    text: '0.00',
                                    id: 'lbl-creditoLim',
                                    name: 'lbl-creditoLim',
                                    style: 'font-size: 13px;font-weight: bold;'
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'combobox',
                                    emptyText: 'Nombre Compania',
                                    fieldLabel: '<b>Nombre Compania</b>',
                                    forceSelection: true,
                                    hidden: true,
                                    listConfig: {
                                        minWidth: 250
                                    },
                                    margin: '0 5 5 0',
                                    name: 'appACrCombo',
                                    maxLength: 90,
                                    flex: 1,
                                    //                                    store: storeCompania,
                                    trigger: true,
                                    valueField: 'id',
                                    displayField: 'text',
                                    queryMode: 'remote', //tipo de recarga del store desde afuera
                                    queryParam: 'filtro', //criterio de busqueda a enviar al php
                                    minChars: 2, //numero de caracteres limite para empezar la busqueda
                                    hideTrigger: true, //Para ocultar triángulo
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'nombresACr',
                                    id: 'nombresACr',
                                    emptyText: 'Nombre compania',
                                    fieldLabel: '<b>Nombre</b>',
                                    readOnly: true,
                                    flex: 1
                                },
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: []
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                blankText: INFOMESSAGEBLANKTEXT,
                                allowOnlyWhitespace: false,
                                xtype: 'textareafield',
                                name: 'razonACr',
                                id: 'razonACr',
                                emptyText: 'Descripción de la razón del credito',
                                fieldLabel: '<b>Razón</b>',
                                maxLength: 500,
                                maxLengthText: 'Debe ingresar hasta 500 caracter',
                                minLengthText: 'Debe ingresar mas de 20 caracteres',
                                minLength: 20,
                                flex: 1
                            }]
                        }
                    ]
                }
            ],
            buttons: ['->', {
                iconCls: 'fa fa-credit-card-alt',
                text: '<b>Asignar credito</b>',
                width: '150px',
                tooltip: 'Permite asiganar credito a un compania',
                name: 'btn_agregar_cred_cliente',
                handler: function (event, target, owner, tool) {
                    Ext.MessageBox.buttonText = {
                        yes: "Sí",
                        no: "No"
                    };
                    var form = vnt_AsignarCredito.down('[name=formAsignarCredito]').getForm();
                    if (form.isValid()) {
                        var val = form.getValues();
                        console.log(val);
                        Ext.MessageBox.confirm('Atención', '<h3>¿Está seguro de asignar $' + val.montoACr + '.00' + ' de crédito al compania ' + compania + ' para el presente mes', function (choice) {
                            if (choice === 'yes') {
                                var mesRangoFinal = vnt_AsignarCredito.down('[name=dateMesHasta]').getValue();
                                var idRangoFinal = getNumeroMes(mesRangoFinal); 
                                var data;
                                if (mesRangoFinal !== null) {
                                    for (let i = 0; i < arrayIdMesesAdm.length; i++) {
                                        if (arrayIdMesesAdm[i] !== idRangoFinal) {
                                            data = {
                                                monto: val.montoACr,
                                                anio: val.dateAnios,
                                                mes: arrayIdMesesAdm[i],
                                                idCompania: idCompania,
                                                //idCompaniaSucursal: idCompaniaSucursal,
                                                razonCredito: val.razonACr,
                                                //idCompaniaVoucher: idCompaniaVoucher,
                                            };
                                            asignarCreditoAdminVoucher(data,val);
                                        } else {
                                            data = {
                                                monto: val.montoACr,
                                                anio: val.dateAnios,
                                                mes: arrayIdMesesAdm[i],
                                                idCompania: idCompania,
                                                //idCompaniaSucursal: idCompaniaSucursal,
                                                razonCredito: val.razonACr,
                                               // idCompaniaVoucher: idCompaniaVoucher,

                                            };
                                            asignarCreditoAdminVoucher(data,val);
                                            break;
                                        }
                                    }
                                } else {
                                    data = {
                                        monto: val.montoACr,
                                        anio: val.dateAnios,
                                        mes: val.dateMes,
                                        idCompania: idCompania,
                                        //idCompaniaSucursal: idCompaniaSucursal,
                                        razonCredito: val.razonACr,
                                        //idCompaniaVoucher: idCompaniaVoucher,
                                    };
                                    asignarCreditoAdminVoucher(data,val);
                                }
                            }
                        });
                    } else {
                        Ext.MessageBox.alert('Atención', 'Complete los campos requeridos');
                    }
                }
            }, {
                iconCls: 'fa fa-window-close-o',
                text: '<b>Cancelar</b>',
                width: '100px',
                tooltip: 'Cerrar ventana',
                handler: function (event, target, owner, tool) {
                    vnt_AsignarCredito.close();
                    vnt_AsignarCredito.destroy();
                }
            }]
        });
        vnt_AsignarCredito.show();
        vnt_AsignarCredito.down('[name=nombresACr]').setValue(compania);
        vnt_AsignarCredito.down('[name=razonACr]').focus(true);
    },
    onBuscarGridCompania: function (btn, e) {
        var txtParam = MODULO_ADMIN_VOUCHER.down('[name=txtParam]').getValue();
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getStore().load({
                params: {
                    params: txtParam,
                    anio: anio,
                    mes: mes,
                    idCompaniaVoucher: idCompania,
                },
                callback: function (records, operation, success) {
                    if (!success) {
                        setMensajeGridEmptyText(MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]'), EMPTY_CARA + '<h3>' + MENSAJE_ERROR + '</h3>');
                    } else if (records.length === 0) {
                        setMensajeGridEmptyText(MODULO_ADMIN_VOUCHER.down('[name=grid]'), EMPTY_CARA + '<h3>No existen resultados.</h3>');
                    } else {
                        MODULO_ADMIN_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
                    }

                }
            });
        }
    },
});

function getIdAdminVoucher() {
    return MODULO_ADMIN_VOUCHER.down('[name=idAdminVoucher]').getValue();
}
function recargarStoreCompaniaAdmin(params) {
    MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getStore().load({
        params: params,
        callback: function (records, operation, success) {
            if (!success) {
                notificaciones(MENSAJE_ERROR, 2);
            } 
        }
    });
    MODULO_ADMIN_VOUCHER.down('[name=asignarCredito]').disable();
}

function recargarStoreCreditoAdmin(params) {
    storeCreditoCompaniaAdminVoucher.load({
        params: params,
        callback: function (records, operation, success) {
            if (!success) {
                notificaciones(MENSAJE_ERROR, 2);
            } else {
            }
        }
    });
}

function storeTextDefectoAdmin() {
    MODULO_ADMIN_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
    MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getView().deselect(MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getSelection());
    MODULO_ADMIN_VOUCHER.down('[name=clienteAdmCosumo]').setText('Compania:');
    MODULO_ADMIN_VOUCHER.down('[name=creditoAdmCosumo]').setText('Crédito: $');
    MODULO_ADMIN_VOUCHER.down('[name=consumoAdmCosumo]').setText('Cosumido: $');
}

function asignarCreditoAdminVoucher(data,val) {
    var form = Ext.create('Ext.form.Panel');
    form.getForm().submit({
        url: 'php/AdminVoucher/createCredito.php',
        timeout: 6000000,
        waitTitle: 'Procesando Información',
        waitMsg: 'Procesando...',
        params: data,
        success: function (form, action) {
            vnt_AsignarCredito.close();
            vnt_AsignarCredito.destroy();
            notificaciones('Crédito Agregado Correctamente', 1);
            var params = {
                anio:val.dateAnios,
                mes: val.dateMes,
                idCompania:idCompania
            };
            recargarStoreCreditoAdmin(params);
            recargarStoreCompaniaAdmin(params);
            storeTextDefectoAdmin();

        },
        failure: function (form, action) {
            notificaciones('No se pudo realizar la asignación,revice el crédito de la empresa o la fecha de inicio de crédito', 2);
            vnt_AsignarCredito.close();
            vnt_AsignarCredito.destroy();
            var params = {
                anio: val.dateAnios,
                mes: val.dateMes,
                idCompania: idCompania
            };
            recargarStoreCreditoAdmin(params);
            recargarStoreCompaniaAdmin(params);
            storeTextDefectoAdmin();
        }
    });
}
function getNombreMes(value) {
    var mes;
    switch (value) {
        case 1:
            mes = 'Enero'
            break;
        case 2:
            mes = 'Febrero'
            break;
        case 3:
            mes = 'Marzo'
            break;
        case 4:
            mes = 'Abril'
            break;
        case 5:
            mes = 'Mayo'
            break;
        case 6:
            mes = 'Junio'
            break;
        case 7:
            mes = 'Julio'
            break;
        case 8:
            mes = 'Agosto'
            break;
        case 9:
            mes = 'Septiembre'
            break;
        case 10:
            mes = 'Octubre'
            break;
        case 11:
            mes = 'Noviembre'
            break;
        case 12:
            mes = 'Diciembre'
            break;
        default:
            break;
    }
    return mes;
}

function getNumeroMes(value) {
    var mes;
    switch (value) {
        case 'Enero':
            mes = 1
            break;
        case 'Febrero':
            mes = 2
            break;
        case 'Marzo':
            mes = 3
            break;
        case 'Abril':
            mes = 4
            break;
        case 'Mayo':
            mes = 5
            break;
        case 'Junio':
            mes = 6
            break;
        case 'Julio':
            mes = 7
            break;
        case 'Agosto':
            mes = 8
            break;
        case 9:
            mes = 'Septiembre'
            break;
        case 'Octubre':
            mes = 10
            break;
        case 'Noviembre':
            mes = 11
            break;
        case 'Diciembre':
            mes = 12
            break;
        default:
            break;
    }
    return mes;
}