var arrayMeses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
var valueFecha = fecha.getFullYear() + '-' + arrayMeses[fecha.getMonth() + 1];
var fecha = new Date(), mes = fecha.getMonth() + 1, anio = fecha.getFullYear();
var colorBotones = '#D8D7D7';
var colorBtn_claro = '#9FD0FF';
var ID_MAPA_VOUCHER_ADMIN = 'mapAdminVoucher';
var MODULO_ADMIN_VOUCHER;
var idCompania;

Ext.define('Ktaxi.view.adminVoucher.v_AdminVoucher', {
    extend: 'Ext.panel.Panel',
    xtype: 'adminVoucher',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelAdminVoucher',
    controller: 'c_AdminVoucher',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.adminVoucher.c_AdminVoucher',
        'Ext.grid.plugin.CellEditing'
    ],
    listeners: {
        afterrender: 'onViewAdminVoucher'
    },
    initComponent: function () {
        var STORE_COMPANIA = Ext.create('Ktaxi.store.adminVoucher.s_AdminVoucher_Compania');
        var STORE_READ_CONSUMO = Ext.create('Ktaxi.store.adminVoucher.s_AdminVoucher_Read_Consumo');

        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [
                    {
                        xtype: 'form',
                        layout: 'hbox',
                        items: [
                            {
                                width: '35%',
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        flex: 1,
                                        name: 'gridLeerAdminVoucher',
                                        xtype: 'grid',
                                        columnLines: true,
                                        bufferedRenderer: false,
                                        plugins: [{ptype: 'gridfilters'}],
                                        store: STORE_COMPANIA,
//                                        defaults: {
//                                            margin: 0,
//                                            padding: 0
//                                        },
                                        tbar: [
                                            {
                                                xtype: 'textfield',
                                                flex: 2,
                                                tooltip: 'Escribir búsqueda',
                                                name: 'txtParam',
                                                emptyText: 'Nombre, Apellido..',
                                                minChars: 0,
                                                typeAhead: true,
                                                listeners: {
                                                    specialkey: 'onBuscarGridCompania'
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-search',
                                                iconAlign: 'right',
                                                tooltip: 'Buscar',
                                                handler: 'onBuscarGridCompania'
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-eraser',
                                                iconAlign: 'right',
                                                tooltip: 'Limpiar',
                                                handler: 'onLimpiarGridCompania'
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
                                                tooltip: 'Recargar',
                                                handler: 'onRecargarGridCompania',
                                            }
                                        ],
                                        columns: [
                                            {tooltip: 'Compania', text: 'Compania', dataIndex: 'compania', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {header: '<b>Credito</b>', xtype: 'numbercolumn', flex: 1, dataIndex: 'creditoMes', align: 'center', filter: true},
                                            {header: '<b>Consumo</b>', xtype: 'numbercolumn', flex: 1, dataIndex: 'consumoMes', align: 'center', filter: true},
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipContenID}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            select: 'onSelectGridCompania',
                                            rowdblclick: showAuditoria
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>'
                                        },
                                        bbar: Ext.create('Ext.PagingToolbar', {
                                            store: STORE_COMPANIA,
                                            displayInfo: true,
                                            name: 'paginacionGrid',
                                            emptyMsg: "Sin datos que mostrar.",
                                            displayMsg: 'Visualizando {0} - {1} de {2} registros',
                                            beforePageText: 'Página',
                                            afterPageText: 'de {0}',
                                            firstText: 'Primera página',
                                            prevText: 'Página anterior',
                                            nextText: 'Siguiente página',
                                            lastText: 'Última página',
                                            refreshText: 'Actualizar',
                                            inputItemWidth: 35,
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    text: 'Exportar',
                                                    iconCls: 'x-fa fa-download',
                                                    handler: function (btn) {
                                                        onExportar(btn, "Sanciones", this.up('grid'));
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                afterrender: function () {
                                                    this.child('#refresh').hide();
                                                },
                                            }
                                        })
                                    }
                                ]
                            }, {
                                flex: 1,
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        xtype: 'panel',
                                        title: 'Datos Consumo',
                                        name: 'datosConsumo',
                                        collapsible: false,
                                        collapsed: false,
                                        items: [
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Compania:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'clienteAdmCosumo'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Crédito:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'creditoAdmCosumo'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Consumo:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'consumoAdmCosumo'
                                                    },
                                                ]
                                            },
                                        ]

                                    },
                                    {
                                        flex: 1,
                                        title: 'Administración de consumo',
                                        name: 'administracionConsumo',
                                        xtype: 'grid',
                                        bufferedRenderer: false,
                                        store: STORE_READ_CONSUMO,
                                        tbar: [
                                            {
                                                text: 'Asignar Crédito',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-plus-square',
                                                iconAlign: 'right',
                                                tooltip: 'Nuevo registro',
                                                handler: 'onShowVentanaAsignarCredito',
                                                name: 'asignarCredito'
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
//                                                aling:'right',
                                                tooltip: 'Recargar',
                                                name: 'recargarGridConsumo',
                                                handler: 'onRecargarGridConsumo'
                                            },
                                            {
                                                xtype: 'container',
                                                name: 'errorFechas',
                                                html: ''
                                            },
                                        ], features: [
                                            {
                                                ftype: 'grouping',
                                                groupHeaderTpl: '{name}',
                                                hideGroupedHeader: true,
                                                enableGroupingMenu: true
                                            }],
                                        columns: [
                                            Ext.create('Ext.grid.RowNumberer', {header: 'Nº', width: 60, align: 'center'}),
                                             {header: '<b>Sucursal</b>', flex: 1, dataIndex: 'sucursal', filter: true},
                                            {header: '<b>Credito</b>', flex: 1, dataIndex: 'credito', xtype: 'numbercolumn', align: 'center', filter: true},
                                            {header: '<b>Asignado</b>', flex: 1, dataIndex: 'asignado', xtype: 'numbercolumn', align: 'center', filter: true},
                                            {header: '<b>Consumo</b>', flex: 1, dataIndex: 'consumido', xtype: 'numbercolumn', align: 'center', filter: true},

                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            rowdblclick: showAuditoria,
                                            validateedit: function (rowEditing, context, eOpts) {
                                                if (rowEditing.editor.form.isValid()) {
                                                    var panel = Ext.getCmp('panelAdminVoucher');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                }
                                            },
                                            cancelEdit: function (rowEditing, context) {
                                                if (!rowEditing.editor.form.isValid() && context.record.data.nuevo) {
                                                    var panel = Ext.getCmp('panelAdminVoucher');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                    Ext.getStore('adminVoucher.s_AdminVoucher_Promocion').remove(context.record);
                                                    onChangeDates('gridLeerAdminVoucherPromocion');
                                                }
                                            }
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>',
                                            getRowClass: function (record) {
                                                if (record.data.error) {
                                                    return 'deleteRowGrid';
                                                }
                                            }
                                        },
                                        bbar: Ext.create('Ext.PagingToolbar', {
                                            store: STORE_READ_CONSUMO,
                                            displayInfo: true,
                                            name: 'paginacionGrid',
                                            emptyMsg: "Sin datos que mostrar.",
                                            displayMsg: 'Visualizando {0} - {1} de {2} registros',
                                            beforePageText: 'Página',
                                            afterPageText: 'de {0}',
                                            firstText: 'Primera página',
                                            prevText: 'Página anterior',
                                            nextText: 'Siguiente página',
                                            lastText: 'Última página',
                                            refreshText: 'Actualizar',
                                            inputItemWidth: 35,
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    text: 'Exportar',
                                                    iconCls: 'x-fa fa-download',
                                                    handler: function (btn) {
                                                        onExportar(btn, "Sanciones", this.up('grid'));
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                afterrender: function () {
                                                    this.child('#refresh').hide();
                                                }
                                            }
                                        })
                                    }
                                ]
                            }
                        ], dockedItems: [{
                                ui: 'footer',
                                xtype: 'toolbar',
                                dock: 'bottom',
                                defaults: {
                                    width: '10%'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        height: 30,
                                        iconCls: 'fa fa-chevron-left',
                                        tooltip: 'Mes anterior',
                                        style: {
                                            background: colorBotones,
                                            borderColor: '#ffffff',
                                            borderStyle: 'solid'
                                        },
                                        handler: function () {
                                            mes = mes - 1;
                                            if (mes <= 0) {
                                                anio -= 1;
                                                mes = 12;
                                            }
                                            valueFecha = anio + '-' + arrayMeses[mes];
                                            Ext.getCmp('fechaConsultaAdmin').setValue(valueFecha);
                                            var params = {anio: anio, mes: mes, idCompania:idCompania};
                                            if (mesAterior <= 0) {
                                                mesAterior = 12;
                                                anioConf -= 1;
                                            }
                                            if (mes === mesAterior && anioConf === anio) {
                                                selModel.setLocked(false); //Permite seleccionar
                                            } else {
                                                selModel.setLocked(true); //NO PERMITE MARCAR
                                            }
                                            recargarStoreCreditoAdmin(params);
                                            recargarStoreCompaniaAdmin(params);
                                            storeTextDefectoAdmin();
                                            MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getView().deselect(MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getSelection());
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'fechaConsultaAdmin',
                                        id: 'fechaConsultaAdmin',
                                        fieldLabel: '',
                                        readOnly: true,
                                        value: valueFecha
                                    }, {
                                        xtype: 'button',
                                        height: 30,
                                        iconCls: 'fa fa-chevron-right',
                                        tooltip: 'Siguiente mes',
                                        style: {
                                            background: colorBotones,
                                            borderColor: '#ffffff',
                                            borderStyle: 'solid'
                                        },
                                        handler: function () {
                                            mes = mes + 1;
                                            if (mes > 12) {
                                                anio += 1;
                                                mes = 1;
                                            }
                                            valueFecha = anio + '-' + arrayMeses[mes];
                                            Ext.getCmp('fechaConsultaAdmin').setValue(valueFecha);
                                            var params = {anio: anio, mes: mes,idCompania:idCompania};
                                            if (mesAterior > 12) {
                                                mesAterior = 12;
                                                anioConf += 1;
                                            }
                                            if (mes === mesAterior && anioConf === anio) {
                                                selModel.setLocked(false); //Permite seleccionar
                                            } else {
                                                selModel.setLocked(true); //NO PERMITE MARCAR
                                            }
                                            recargarStoreCreditoAdmin(params);
                                            recargarStoreCompaniaAdmin(params);
                                            storeTextDefectoAdmin();
                                            MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getView().deselect(MODULO_ADMIN_VOUCHER.down('[name=gridLeerAdminVoucher]').getSelection());
                                        }
                                    }
                                ]
                            }]
                    }
                ]
            },
        ];
        this.callParent(arguments);
    }
});
var selModel = Ext.create('Ext.selection.CheckboxModel', {
    checkOnly: false,
    listeners: {
        select: function (model, record, index) {
        },
        deselect: function (model, record, index) {
        }
    }
});