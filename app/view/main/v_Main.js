Ext.define('Ktaxi.view.main.v_Main', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.v_Main',
    requires: [
        'Ext.button.Segmented',
        'Ext.list.Tree',
        'Ktaxi.view.main.c_Main',
        'Ktaxi.view.main.MainContainerWrap',
        'Ktaxi.view.main.MainModel',
        'Ktaxi.view.404.v_SinModulo',
        'Ktaxi.view.administrador.v_Adminsitrador',
        'Ktaxi.view.config_ciudad.v_Config_Ciudad',
        'Ktaxi.view.driver_ciudad.v_Driver_Ciudad',
        'Ktaxi.view.label_config.v_Label_config',
        'Ktaxi.view.paquete.v_Paquete',
        'Ktaxi.view.lugares.v_Lugares',
        'Ktaxi.view.departamentoVoucher.v_Voucher',
        'Ktaxi.view.sucursalVoucher.v_Sucursal',
        'Ktaxi.view.companiaVoucher.v_CompaniaVoucher',
        'Ktaxi.view.adminVoucher.v_AdminVoucher',
        'Ktaxi.view.compania.v_Compania',
        'Ktaxi.view.sucursal.v_Sucursal',
        'Ktaxi.view.super.v_Super',
        'Ktaxi.view.departamento.v_Departamento',
        'Ktaxi.view.alumno.v_Alumno',
        'Ktaxi.view.clase.v_Clase',
        'Ktaxi.view.reporte.v_Reporte',
        'Ktaxi.view.evaluacion.v_Evaluacion',

    ],
    controller: 'main',
    viewModel: 'main',
    cls: 'sencha-dash-viewport',
    itemId: 'v_Main',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    listeners: {
        render: 'onMainViewRender'
    },
    items: [
        {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar shadow',
            height: 64,
            itemId: 'headerBar',
            id: 'toolbarMain',
            style: {
                background: '#074975',
                color: 'white',
                border: '1px solid #36beb3'
            },
            items: [
                {
                    xtype: 'component',
                    reference: 'senchaLogo',
                    cls: 'sencha-logo',
                    html: '<div class="main-logo"><img src="' + IMG_LOGO + '" width="185"><div class="titulo">' + APP + '</div><!--<div class="subtitulo">' + TITULO_MAIN_APP + '</div>--></div>',
                    width: 150
                },
                '->',
                {
                    xtype: 'label',
                    html: '<div class = "subtitulo">' + DIAS[FECHA_ACTUAL.getDay()] + ', ' + FECHA_ACTUAL.getDate() + ' de ' + MESES[FECHA_ACTUAL.getMonth()] + '&nbsp&nbsp&nbsp&nbsp&nbsp<span class="barra">|</span>&nbsp&nbsp&nbsp&nbsp&nbsp<span id="reloj" style="font-weight: bold;">00:00:00</span>&nbsp&nbsp&nbsp&nbsp&nbsp' + '</div>'
                },
                {
                    xtype: 'button',
                    id: 'btnAlarmas',
                    iconCls: 'x-fa fa-bell',
                    ui: 'header',
                    tooltip: 'Ver Alarmas',
                    badgeText: 0,
                    cls: 'gray-badge',
                    arrowVisible: false,
                    menu: [],
                    handler: function () {
                        verAlarmas();
                    },
                    hidden: true
                },
                {
                    xtype: 'container',
                    width: '1%'
                },
                {
                    xtype: 'image',
                    name: 'FOTO_PERFIL',
                    cls: 'header-right-profile-image',
                    height: 35,
                    width: 35,
                    alt: 'Imagen de Usuario',
                    src: URL_IMG_SISTEMA + 'usuario.png',
                    listeners: {
                        render: function (me) {
                            me.el.on({
                                error: function (e, t, eOmpts) {
                                    me.setSrc(URL_IMG_SISTEMA + 'usuario.png');
                                }
                            });
                        }
                    }
                },
                {
                    xtype: 'button',
                    name: 'NOMBRE_USUARIO',
                    text: 'ADMIN',
                    style: 'color:white!important;',
                    menu: [
//                        {text: 'Mi Perfil', iconCls: 'x-fa fa-user', style: {}},
//                        {text: 'Configuracion', iconCls: 'x-fa fa-cogs', handler: '', style: {}},
//                        {text: 'Ayuda', iconCls: 'x-fa fa-question-circle', handler: '', style: {}},
                        {text: 'Salir', iconCls: 'x-fa fa-sign-out', handler: 'onSalir', style: {}}]
                }
            ]
        },
        {
            xtype: 'maincontainerwrap',
            id: 'main-view-detail-wrap',
            reference: 'mainContainerWrap',
            flex: 1,
            items: [
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    ui: 'navigation',
                    store: 'Navegacion',
                    width: 60,
                    expandedWidth: WIDTH_NAVEGACION,
                    expanderFirst: false,
                    expanderOnly: false,
                    micro: true,
                    listeners: {
                        itemClick: 'onClickMenu',
                        selectionchange: 'onNavigationTreeSelectionChange'
                    }
                },
                {
                    xtype: 'container',
                    flex: 1,
                    reference: 'mainCardPanel',
                    cls: 'sencha-dash-right-main-container',
                    itemId: 'contentPanel',
                    layout: {
                        type: 'card',
                        anchor: '100%'
                    }
                }
            ]
        }
    ]
});
