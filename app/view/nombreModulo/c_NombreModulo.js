var MODULO_NOMBRE;
Ext.define('Ktaxi.view.nombreModulo.c_NombreModulo', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_NombreModulo',
    onView: function (panelLoad) {
        MODULO_NOMBRE = panelLoad;
        MODULO_NOMBRE.down('[name=grid]').getStore().load();
        validarPermisosGeneral(MODULO_NOMBRE);
    },
    onRecargar: function () {
        MODULO_NOMBRE.down('[name=grid]').getStore().reload();
    },
    onLimpiar: function (btn, e) {
        //MODULO_NOMBRE.down('[name=cmbxTipo]').reset();
        //MODULO_NOMBRE.down('[name=cmbxTipoEstado]').reset();
        MODULO_NOMBRE.down('[name=txtParam]').reset();
        //MODULO_NOMBRE.down('[name=idUsuario]').reset();
//        MODULO_NOMBRE.down('[name=hasta]').reset();

        if (btn.limpiar) {
            MODULO_NOMBRE.down('[name=form]').getForm().reset();
//             MODULO_NOMBRE.down('[name=idUsuario]').getStore().removeAll();
            var grid = MODULO_NOMBRE.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            validarPermisosGeneral(MODULO_NOMBRE);
        }
        MODULO_NOMBRE.down('[name=grid]').getStore().load();
        MODULO_NOMBRE.down('[name=btnEditar]').setHidden(true);
        MODULO_NOMBRE.down('[name=btnCrear]').setHidden(false);
    },
    onBuscar: function (btn, e) {
        var tipo = MODULO_NOMBRE.down('[name=cmbxTipo]').getValue(), txtParam = MODULO_NOMBRE.down('[name=txtParam]').getValue(), params = {};
        var estado = MODULO_NOMBRE.down('[name=cmbxTipoEstado]').getValue();

        if (tipo)
            params['idUsuario'] = tipo;
        params['idEstado'] = estado;
        params['param'] = txtParam;
        //MODULO_NOMBRE.down('[name=paginacionGrid]').moveFirst();
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            MODULO_NOMBRE.down('[name=grid]').getStore().load({
                params: params,
                callback: function (records, operation, success) {
                    if (!success)
                        setMensajeGridEmptyText(MODULO_NOMBRE.down('[name=grid]'), EMPTY_CARA + '<h3>' + MENSAJE_ERROR + '</h3>');
                    else if (records.length === 0)
                        setMensajeGridEmptyText(MODULO_NOMBRE.down('[name=grid]'), EMPTY_CARA + '<h3>No existen resultados.</h3>');
                }
            });
        }
    },
    onSelect: function (grid, selected, eOpts) {
        //setPermisos(MODULO_NOMBRE);
        MODULO_NOMBRE.down('[name=form]').getForm().loadRecord(selected);
        MODULO_NOMBRE.down('[name=btnEditar]').setHidden(false);
        MODULO_NOMBRE.down('[name=btnCrear]').setHidden(true);
    },
    onCrear: function () {
        console.log('onCrear');
        var me = this, form = MODULO_NOMBRE.down('[name=form]').getForm();
        if (form.isValid()) {
            MODULO_NOMBRE.down('[name=grid]').getStore().insert(0, form.getValues());
            MODULO_NOMBRE.down('[name=grid]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiar({limpiar: true}));
                }});
        } else {
            mensajesValidacionForms(form.getFields());
        }
    },
    onEditar: function () {
        var me = this, form = MODULO_NOMBRE.down('[name=form]').getForm();
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_NOMBRE.down('[name=grid]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiar({limpiar: true}));
                }
            });
        } else {

            mensajesValidacionForms(form.getFields());
        }
    },
    controlarNombreModulo: function (combo, record, eOpts) {

        var fecha = moment(record.data.date).add(15, 'minutes').format('HH:mm:ss');
        MODULO_NOMBRE.down('[name=hasta]').setValue(fecha);
        MODULO_NOMBRE.down('[name=hasta]').setMinValue(fecha);
        MODULO_NOMBRE.down('[name=hasta]').setDisabled(false);
    },
    onGetClass: function (v, meta, rec) {
        if (rec.data.validada === 1 && rec.data.confimada === 1) {
//            this.items[0].tooltip = 'validar';
            return 'x-fa fa-thumbs-o-up';

        } else if (rec.data.confimada === 0) {
//            this.items[0].tooltip = 'confimar';
            return 'x-fa fa-check';
        } else {
            return 'x-fa fa-money';
        }
    },

    onVenatna: function (grid, rowIndex, colIndex, event, cell) {
        if (cell.record.data.confimada === 0) {
            this.showNombreModuloConfirmar(grid, rowIndex, colIndex, event, cell);
        } else if (cell.record.data.validada === 0) {
            this.showNombreModuloValidar(grid, rowIndex, colIndex, event, cell);
        }
        if (cell.record.data.confimada === 1 && cell.record.data.validada === 1) {
            notificaciones('El registro ya se encuentra confirmado y validado', 5);
        }
    },

    showNombreModuloConfirmar: function (grid, rowIndex, colIndex, event, cell) {
        var window = new Ext.Window({
            width: 450,
            title: 'nombreModulo',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    hidden: true,
                    name: 'mensajeNombreModulo',
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + cell.record.data.nombreR + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            html: 'Ingrese el nombreModulo mostrado en el correo para el usuario <b>' + cell.record.data.nombreR + ' ' + cell.record.data.apellidoR + '</b>.<br>Valor ingresado en transcripción es: <b>$' + cell.record.data.NombreModuloCorreo + ' USD</b>'
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: '<b>Valor</b>',
                            name: 'confCorreo',
                            minValue: 1
                        }
                    ]
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        window.close();
                    }
                },
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Continuar',
                    tooltip: 'Continuar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        var NombreModuloCorreoRegistro = window.down('[name=confCorreo]').getValue();
                        var NombreModuloIncial = cell.record.data.NombreModuloCorreo;
                        if (NombreModuloIncial === NombreModuloCorreoRegistro) {
                            window.close();
                            windowConfirmar.show();
                        } else {
                            notificaciones('El monto ingresado no corresponde con el registrado en el correo', 5);
                            //window.close();
                        }
                    }
                }
            ]
        });
        var windowConfirmar = new Ext.Window({
            width: 450,
            title: 'nombreModulo',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    hidden: true,
                    name: 'confirmarNombreModulo',
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + cell.record.data.correo + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            html: '¿Usted esta a punto de confirmar un ingreso de <b>$' + cell.record.data.NombreModuloCorreo + ' USD </b> para el usuario <b>' + cell.record.data.nombreR + ' ' + cell.record.data.apellidoR + '</b>?'
                        }
                    ]
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        windowConfirmar.close();
                    }
                },
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Confirmar',
                    tooltip: 'Confirmar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        Ext.Ajax.request({
                            async: true,
                            url: 'php/nombreModulo/update.php',
                            params: {
                                id: cell.record.data.id,
                                idUsuario: cell.record.data.idUsuario,
                                NombreModuloCorreo: cell.record.data.NombreModuloCorreo,
                                confirmada: 1,
                                validada: 0,
                            },
                            callback: function (callback, e, response) {
                                var res = JSON.parse(response.request.result.responseText);
                                if (res.success) {
                                    notificaciones('El registro ha sido confirmado', 1);
                                } else {
                                    notificaciones('Lo sentimos hubo un error al validar el registro', 2);
                                }
                                MODULO_NOMBRE.down('[name=grid]').getStore().reload();
                            }
                        });

                        windowConfirmar.close();
//                        notificaciones('El registro ya se encuentra confirmado', 5);

                    }
                }

            ]
        });
        if (cell.record.data.confimada === 1) {
            notificaciones('El registro ya se encuentra confirmado', 5);
        } else {
            window.show();
        }


    },
    showNombreModuloValidar: function (grid, rowIndex, colIndex, event, cell) {
        var windowValidar = new Ext.Window({
            width: 450,
            title: 'Validar nombreModulo',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    hidden: true,
                    name: 'validarNombreModulo',
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + cell.record.correo + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            html: 'Usted va a validar un ingreso de <b>$' + cell.record.data.NombreModuloCorreo + ' USD</b> para el usuario <b>' + cell.record.data.nombreR + ' ' + cell.record.data.apellidoR + '</b>. <br>¿Es correcta la información?'
                        }
                    ]
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'No. regresar',
                    tooltip: 'No Validar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        windowValidar.close();
                    }
                },
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Si. Validar',
                    tooltip: 'Validar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        Ext.Ajax.request({
                            async: true,
                            url: 'php/nombreModulo/update.php',
                            params: {
                                id: cell.record.data.id,
                                NombreModuloCorreo: cell.record.data.NombreModuloCorreo,
                                confirmada: 1,
                                validada: 1
                            },
                            callback: function (callback, e, response) {
//                                window.down('form').removeAll();
                                var res = JSON.parse(response.request.result.responseText);
                                if (res.success) {
                                    notificaciones('El registro se validó correctamente', 1);
                                    Ext.Ajax.request({
                                        async: true,
                                        url: 'php/nombreModulo/createNombreModulo.php',
                                        params: {
                                            id: cell.record.data.id,
                                            idUsuario: cell.record.data.idUsuario,
                                            NombreModuloCorreo: cell.record.data.NombreModuloCorreo
                                        },
                                        callback: function (callback, e, response) {
                                            var res = JSON.parse(response.request.result.responseText);
                                            if (res.success) {
//                                                notificaciones('El registro ha sido guardado', 1);
                                                  console.log('Registro Guaradado');  
                                            } 
                                            MODULO_NOMBRE.down('[name=grid]').getStore().reload();
                                        }
                                    });
                                } else {
                                    notificaciones('Lo sentimos hubo un error al validar el registro', 2);
                                }
                                MODULO_NOMBRE.down('[name=grid]').getStore().reload();
                            }
                        });

                        windowValidar.close();
                    }
                }
            ]
        });

        if (cell.record.data.validada == 1) {
            notificaciones('El nombreModulo ya se encuentra validado', 5);
        } else {
            windowValidar.show();

        }

    }
});