var editingPromociones;
var txt = 'Primero solicitar que el cliente este registrado en el APP con la cédula de identidad,' +
        'luego deberá buscar lo con el número exacto de cédula, colocando este en la caja de texto y ' +
        'presionando la boton buscar';
var fecha = new Date(), mes = fecha.getMonth() + 1, anio = fecha.getFullYear();
var cliente = '', idC, mesAterior = fecha.getMonth(), anioActual = fecha.getFullYear(), anioConf = fecha.getFullYear(), saldo_dispo_mes_act = 0;
var arrayMeses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
valueFecha = fecha.getFullYear() + '-' + arrayMeses[fecha.getMonth() + 1];
var colorBotones = '#D8D7D7';
var colorBtn_claro = '#9FD0FF';
var ID_MAPA_VOUCHER = 'mapVoucher';
var idDepartamento = 1;
var idAplicativo = 1;


Ext.define('Ktaxi.view.departamentoVoucher.v_Voucher', {
    extend: 'Ext.panel.Panel',
    xtype: 'departamentoVoucher',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelVoucher',
    controller: 'c_Voucher',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.departamentoVoucher.c_Voucher',
        'Ext.grid.plugin.CellEditing'
    ],
    listeners: {
        afterrender: 'onViewVoucher'
    },
    initComponent: function () {
        editingPromociones = Ext.create('Ext.grid.plugin.CellEditing');
        var STORE_CLIENTES = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Clientes');
        //var STORE_VALIDAR_EMPRESA = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Tipo_Empresa');
        //var STORE_CREDITO_COMPANIA = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Credito_Compania');
        var STORE_READ_CONSUMO = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Read_Consumo');
        //var STORE_BUSCAR_CLIENTE = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Buscar_Cliente');

        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [
                    {
                        xtype: 'form',
                        layout: 'hbox',
                        items: [
                            {
                                width: '35%',
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        name: 'panelCrearEditarVoucher',
                                        region: 'east',
                                        xtype: 'form',
                                        layout: 'fit',
                                        collapsible: false,
                                        items: [{
                                                cls: 'panelFormulario',
                                                xtype: 'panel',
                                                title: 'Vouchers',
                                                defaultType: 'textfield',
                                                defaults: {
                                                    width: '100%',
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    allowOnlyWhitespace: false,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    defaultType: 'textfield',
                                                    labelWidth: 85,
                                                    defaults: {
                                                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                        allowOnlyWhitespace: false,
                                                        blankText: INFOMESSAGEBLANKTEXT,
                                                        allowBlank: false,
                                                        labelWidth: 85,
                                                        width: '50%',
                                                        defaults: {
                                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                            allowOnlyWhitespace: false,
                                                            blankText: INFOMESSAGEBLANKTEXT,
                                                            allowBlank: false,
                                                            labelWidth: 85,
                                                            width: '50%'
                                                        }
                                                    }
                                                },
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                name: 'idCompania',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Compania',
                                                                displayField: 'text',
                                                                readOnly : true,
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_CompaniaVoucher'),
                                                                listeners: {
                                                                    select: function (combo, record, eOpts) {
                                                                        MODULO_VOUCHER.down('[name=idSucursal]').clearValue();
                                                                        MODULO_VOUCHER.down('[name=idSucursal]').getStore().load({
                                                                            params: {
                                                                                idCompania: record.getId()
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            },
                                                            {
                                                                margin: '0 0 0 5',
                                                                name: 'idSucursal',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Sucursal',
                                                                displayField: 'text',
                                                                readOnly : true ,
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_SucursalVoucher')
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [

                                                            {
                                                                name: 'idAplicativo',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Aplicativo',
                                                                displayField: 'text',
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_AplicativosVoucher')
                                                            },
                                                            {
                                                                margin: '0 0 0 5',
                                                                name: 'idDepartamento',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Departamento',
                                                                displayField: 'text',
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_DepartamentoVoucher')
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
//                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                xtype: 'button',
                                                                iconCls: 'x-fa fa-search',
                                                                iconAlign: 'right',
                                                                tooltip: 'Buscar',
                                                                handler: 'onBuscarClienteParams'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 0 0',
                                                        items: [
                                                            {
                                                                xtype: 'label',
                                                                text: 'Crédito',
                                                                style: 'font-size: 12px; font-weight: bold;',
                                                                name: 'creditoCompania'
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 0 0',
                                                        items: [

                                                            {
                                                                xtype: 'label',
                                                                text: 'Asignado',
                                                                style: 'font-size: 12px; font-weight: bold;',
                                                                name: 'creditoConsumido'
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 0 0',
                                                        items: [
                                                            {
                                                                xtype: 'label',
                                                                text: 'Consumido',
                                                                style: 'font-size: 12px; font-weight: bold;',
                                                                name: 'creditoDisponible'
                                                            },
                                                        ]
                                                    },
                                                    {name: 'actualizar', hidden: true, allowBlank: true, allowOnlyWhitespace: true}
                                                ]
                                            }]
                                    }, {
                                        flex: 1,
                                        name: 'gridLeerVoucher',
                                        xtype: 'grid',
                                        columnLines: true,
                                        bufferedRenderer: false,
                                        plugins: [{ptype: 'gridfilters'}],
                                        store: STORE_CLIENTES,
//                                        defaults: {
//                                            margin: 0,
//                                            padding: 0
//                                        },
                                        tbar: [
                                            {
                                                xtype: 'textfield',
                                                flex: 2,
                                                tooltip: 'Escribir búsqueda',
                                                name: 'txtParam',
                                                emptyText: 'Nombre, Apellido..',
                                                minChars: 0,
                                                typeAhead: true,
                                                listeners: {
                                                    specialkey: 'onBuscarGridCliente'
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-search',
                                                iconAlign: 'right',
                                                tooltip: 'Buscar',
                                                handler: 'onBuscarGridCliente'
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-eraser',
                                                iconAlign: 'right',
                                                tooltip: 'Limpiar',
                                                handler: 'onLimpiarGridCliente'
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
                                                tooltip: 'Recargar',
                                                handler: 'onRecargarGridCliente',
                                            }
                                        ],
                                        columns: [
                                            {tooltip: 'Cliente', text: 'Cliente', dataIndex: 'cliente', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'PIN', text: 'PIN', dataIndex: 'codigo', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {header: '<b>Aplicativo</b>', flex: 1, dataIndex: 'app', align: 'center', filter: true, renderer: showTipConten},
                                            {header: '<b>Credito</b>', xtype: 'numbercolumn', flex: 1, dataIndex: 'creditoMes', align: 'center', filter: true},
                                            {header: '<b>Consumo</b>', xtype: 'numbercolumn', flex: 1, dataIndex: 'consumoMes', align: 'center', filter: true},
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipContenID}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            select: 'onSelectGridCliente',
                                            rowdblclick: showAuditoria
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>'
                                        },
                                        bbar: Ext.create('Ext.PagingToolbar', {
                                            store: STORE_CLIENTES,
                                            displayInfo: true,
                                            name: 'paginacionGrid',
                                            emptyMsg: "Sin datos que mostrar.",
                                            displayMsg: 'Visualizando {0} - {1} de {2} registros',
                                            beforePageText: 'Página',
                                            afterPageText: 'de {0}',
                                            firstText: 'Primera página',
                                            prevText: 'Página anterior',
                                            nextText: 'Siguiente página',
                                            lastText: 'Última página',
                                            refreshText: 'Actualizar',
                                            inputItemWidth: 35,
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    text: 'Exportar',
                                                    iconCls: 'x-fa fa-download',
                                                    handler: function (btn) {
                                                        onExportar(btn, "Sanciones", this.up('grid'));
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                afterrender: function () {
                                                    this.child('#refresh').hide();
                                                }
                                            }
                                        })
                                    }
                                ]
                            }, {
                                flex: 1,
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        name: 'panelCrearClienteVoucher',
                                        region: 'east',
                                        xtype: 'form',
                                        layout: 'fit',
                                        title: 'Agregrar Cliente',
                                        collapsible: true,
                                        collapsed: true,
                                        items: [{
                                                cls: 'panelFormularioCliente',
                                                xtype: 'panel',
//                                                title: 'Agregar Cliente',
                                                defaultType: 'textfield',
                                                defaults: {
                                                    width: '100%',
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    allowOnlyWhitespace: false,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    defaultType: 'textfield',
                                                    labelWidth: 85,
                                                    defaults: {
                                                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                        allowOnlyWhitespace: false,
                                                        blankText: INFOMESSAGEBLANKTEXT,
                                                        allowBlank: false,
                                                        labelWidth: 85,
                                                        width: '50%',
                                                        defaults: {
                                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                            allowOnlyWhitespace: false,
                                                            blankText: INFOMESSAGEBLANKTEXT,
                                                            allowBlank: false,
                                                            labelWidth: 85,
                                                            width: '50%'
                                                        }
                                                    }
                                                },
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                xtype: 'label',
                                                                text: 'Buscar cliente para agregarlo',
                                                                id: 'lbl-titleACl',
                                                                name: 'lbl-titleACl',
                                                                style: 'aling:center;font-size: 18px; font-weight: bold;'
                                                            },
                                                            {
                                                                xtype: 'label',
                                                                text: 'Primero solicitar que el cliente este registrado en el APP con la cédula de identidad,luego deberá buscar lo con el número exacto de cédula, colocando este en la caja de texto y presionando la boton buscar ',
                                                                id: 'lbl-descrACl',
                                                                name: 'lbl-descrACl',
                                                                style: 'font-size: 12px;'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                xtype: 'label',
                                                                text: 'Aplicativo: ',
//                                                                id: 'lbl-titleACl',
                                                                name: 'nombreApp',
                                                                style: 'aling:center;font-size: 18px; font-weight: bold;'
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                fieldLabel: 'Cédula',
                                                                name: 'cedula',
                                                                emptyText: 'Cédula',
                                                                maxLength: '200',
                                                                minLength: '2',
                                                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                                            },
                                                            {
                                                                xtype: 'button',
                                                                iconCls: 'x-fa fa-search',
                                                                iconAlign: 'right',
                                                                tooltip: 'Buscar',
                                                                handler: 'onBuscarClienteAgregar'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                fieldLabel: 'Nombres',
                                                                name: 'nombres',
                                                                emptyText: 'Nombres',
                                                                maxLength: '200',
                                                                minLength: '2',
                                                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                                            },
                                                            {
                                                                fieldLabel: 'Apellidos',
                                                                name: 'apellidos',
                                                                emptyText: 'Apellidos',
                                                                maxLength: '200',
                                                                minLength: '2',
                                                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                fieldLabel: 'Aplicativo',
                                                                name: 'aplicativo',
                                                                emptyText: 'Aplicativo',
                                                                maxLength: '200',
                                                                minLength: '2',
                                                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                                            },
                                                            {
                                                                fieldLabel: 'Celular',
                                                                name: 'celular',
                                                                emptyText: 'Celular',
                                                                maxLength: '200',
                                                                minLength: '2',
                                                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                                            }
                                                        ],
                                                    },
                                                ], dockedItems: [{
                                                        ui: 'footer',
                                                        xtype: 'toolbar',
                                                        dock: 'bottom',
                                                        defaults: {
                                                            width: '20%'
                                                        },
                                                        items: [
                                                            {
                                                                text: 'Limpiar',
                                                                tooltip: 'Limpiar',
                                                                disabled: false,
                                                                handler: 'onLimpiarFormCliente',
                                                            },
                                                            '->',
                                                            {
                                                                text: 'Agregar Cliente',
                                                                tooltip: 'Agregar el Cliente al Sistema',
                                                                disabled: false,
                                                                name: 'btnCrearCliente',
                                                                handler: 'onCrearClienteCredito',
                                                            }]
                                                    }]
                                            }]
                                    },
                                    {
                                        xtype: 'panel',
                                        title: 'Datos Consumo',
                                        name: 'datosConsumo',
                                        collapsible: false,
                                        collapsed: false,
                                        items: [
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Cliente:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'clienteAdmCosumo'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Crédito:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'creditoAdmCosumo'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Consumo:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'consumoAdmCosumo'
                                                    },
                                                ]
                                            },
                                        ]

                                    },
                                    {
                                        flex: 1,
                                        title: 'Administración de consumo',
                                        name: 'administracionConsumo',
                                        xtype: 'grid',
                                        bufferedRenderer: false,
                                        store: STORE_READ_CONSUMO,
                                        tbar: [
                                            {
                                                text: 'Asignar Crédito',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-plus-square',
                                                iconAlign: 'right',
                                                tooltip: 'Nuevo registro',
                                                handler: 'onShowVentanaAsignarCredito',
                                                name: 'asignarCredito'
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
//                                                aling:'right',
                                                tooltip: 'Recargar',
                                                name: 'recargarGridConsumo',
                                                handler: 'onRecargarGridConsumo'
                                            },
                                            {
                                                xtype: 'container',
                                                name: 'errorFechas',
                                                html: ''
                                            },
                                        ], features: [
                                            {
                                                ftype: 'grouping',
                                                groupHeaderTpl: '{name}',
                                                hideGroupedHeader: true,
                                                enableGroupingMenu: true
                                            }],
                                        columns: [
                                            Ext.create('Ext.grid.RowNumberer', {header: 'Nº', width: 60, align: 'center'}),
                                            {
                                                menuDisabled: true,
                                                header: '<b>Rutas</b>',
                                                xtype: 'actioncolumn',
                                                width: 80,
                                                items: [{
                                                        iconCls: 'x-fa fa-map',
                                                        tooltip: 'Rutas de la solicitud',
                                                        xtype: 'button',
                                                        text: 'Rutas',
                                                        handler: function (grid, rowIndex, colIndex) {
                                                            var record = grid.getStore().getAt(rowIndex);
                                                            var data = record.data;
                                                            var params = {rzn_cre: data.rzn_cre, latCl: data.latC, lngCl: data.lngC, latCo: data.latCo, lngCo: data.lngCo};
                                                            MODULO_VOUCHER.down('[name=panelEast]').expand();
                                                            limpiarMapa(LIST_MAPS[ID_MAPA_VOUCHER]);
                                                            graficarMarcador(LIST_MAPS[ID_MAPA_VOUCHER], data.latSo, data.lngSo, null, 12);

                                                        }
                                                    }
                                                ]
                                            },
                                            {header: '<b>Cliente</b>', flex: 1, dataIndex: 'cliente', align: 'center', filter: true},
                                            {header: '<b>Desde</b>', flex: 1, dataIndex: 'fhd', xtype: 'datecolumn', format: 'Y-m-d H:i:s', align: 'center', filter: true},
                                            {header: '<b>Hasta</b>', flex: 1, dataIndex: 'fha', xtype: 'datecolumn', format: 'Y-m-d H:i:s', align: 'center', filter: true},
                                            {header: '<b>Consumo</b>', flex: 1, dataIndex: 'consumo', xtype: 'numbercolumn', align: 'center', filter: true},
                                            {header: '<b>Detalle</b>', flex: 1, dataIndex: 'detalle', align: 'left', filter: true,hidden:true},
                                            {header: '<b>Nota</b>', flex: 1, dataIndex: 'nota', align: 'left', filter: true},
                                            {text: 'Tipo', menuDisabled: true, sortable: false, xtype: 'actioncolumn', width: 40, minWidth: 40, items: [
                                                    {
                                                        xtype: 'button',
                                                        //style: 'color="red!important"',
                                                        name: 'btnFinalizarJornada',
                                                        tooltip: 'Validado Cliente o Pin',
                                                        iconCls: 'x-fa fa-ban',
                                                        isDisabled: 'onIsDisabledBtnFinalizarJornada',
//                                                        handler: 'onVenatna',
                                                        getClass: 'onGetClass'
                                                    }
                                                ]},
                                            {header: "<b>Fecha de consumo</b>", xtype: 'datecolumn', format: 'Y-m-d H:i:s', width: 139, dataIndex: 'fhc', filter: true},
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            rowdblclick: showAuditoria,
                                            validateedit: function (rowEditing, context, eOpts) {
                                                if (rowEditing.editor.form.isValid()) {
                                                    var panel = Ext.getCmp('panelVoucher');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                }
                                            },
                                            cancelEdit: function (rowEditing, context) {
                                                if (!rowEditing.editor.form.isValid() && context.record.data.nuevo) {
                                                    var panel = Ext.getCmp('panelVoucher');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                    Ext.getStore('voucher.s_Voucher_Promocion').remove(context.record);
                                                    onChangeDates('gridLeerVoucherPromocion');
                                                }
                                            }
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>',
                                            getRowClass: function (record) {
                                                if (record.data.error) {
                                                    return 'deleteRowGrid';
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        ], dockedItems: [{
                                ui: 'footer',
                                xtype: 'toolbar',
                                dock: 'bottom',
                                defaults: {
                                    width: '10%'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        height: 30,
                                        iconCls: 'fa fa-chevron-left',
                                        tooltip: 'Mes anterior',
                                        style: {
                                            background: colorBotones,
                                            borderColor: '#ffffff',
                                            borderStyle: 'solid'
                                        },
                                        handler: function () {
                                            mes = mes - 1;
                                            if (mes <= 0) {
                                                anio -= 1;
                                                mes = 12;
                                            }
                                            valueFecha = anio + '-' + arrayMeses[mes];
                                            Ext.getCmp('fechaConsultaDepartamento').setValue(valueFecha);
                                            var params = {anio: anio, mes: mes, idDepartamento: MODULO_VOUCHER.down('[name=idDepartamento]').getValue(), idAplicativo: MODULO_VOUCHER.down('[name=idAplicativo]').getValue()};
                                            if (mesAterior <= 0) {
                                                mesAterior = 12;
                                                anioConf -= 1;
                                            }
                                            if (mes === mesAterior && anioConf === anio) {
                                                selModel.setLocked(false); //Permite seleccionar
                                            } else {
                                                selModel.setLocked(true); //NO PERMITE MARCAR
                                            }
                                            recargarStore(params);
                                            MODULO_VOUCHER.down('[name=gridLeerVoucher]').getView().deselect(MODULO_VOUCHER.down('[name=gridLeerVoucher]').getSelection());
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'fechaConsultaDepartamento',
                                        id: 'fechaConsultaDepartamento',
                                        fieldLabel: '',
                                        readOnly: true,
                                        value: valueFecha
                                    }, {
                                        xtype: 'button',
                                        height: 30,
                                        iconCls: 'fa fa-chevron-right',
                                        tooltip: 'Siguiente mes',
                                        style: {
                                            background: colorBotones,
                                            borderColor: '#ffffff',
                                            borderStyle: 'solid'
                                        },
                                        handler: function () {
                                            mes = mes + 1;
                                            if (mes > 12) {
                                                anio += 1;
                                                mes = 1;
                                            }
                                            valueFecha = anio + '-' + arrayMeses[mes];
                                            Ext.getCmp('fechaConsultaDepartamento').setValue(valueFecha);
                                            var params = {anio: anio, mes: mes, idDepartamento: MODULO_VOUCHER.down('[name=idDepartamento]').getValue(), idAplicativo: MODULO_VOUCHER.down('[name=idAplicativo]').getValue()};
                                            if (mesAterior > 12) {
                                                mesAterior = 12;
                                                anioConf += 1;
                                            }
                                            if (mes === mesAterior && anioConf === anio) {
                                                selModel.setLocked(false); //Permite seleccionar
                                            } else {
                                                selModel.setLocked(true); //NO PERMITE MARCAR
                                            }
                                            recargarStore(params);
                                            MODULO_VOUCHER.down('[name=gridLeerVoucher]').getView().deselect(MODULO_VOUCHER.down('[name=gridLeerVoucher]').getSelection());
                                        }
                                    }
                                ]
                            }]
                    }
                ]
            },
            {
                region: 'east',
                name: 'panelEast',
                xtype: 'panel',
//                width: '35%',
                flex: 1,
                margin: 5,
                header: false,
                collapsible: true,
                collapsed: true,
                collapseMode: 'mini',
                html: '<div id="' + ID_MAPA_VOUCHER + '" style="position: absolute; top: 0; left: 0; z-index: 0; width: 100%; height: 100%"></div>'
            }
        ];
        this.callParent(arguments);
    }
});
var selModel = Ext.create('Ext.selection.CheckboxModel', {
    checkOnly: false,
    listeners: {
        select: function (model, record, index) {
        },
        deselect: function (model, record, index) {
        }
    }
});