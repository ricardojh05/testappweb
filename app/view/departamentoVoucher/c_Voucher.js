var MODULO_VOUCHER;
var fecha = new Date(), mes = fecha.getMonth() + 1, anio = fecha.getFullYear();
var idAplicativo;
var cliente, idApp, idAppBus, app, cedula, celular, idDepartamento, idDepartamentoCliente;
var idDepartamento = 1;
var idAplicativo = 1;
var idSucursal = 1;
var idCompania = 1;
var storeCreditoCompania;
var storeIsVoucherTipoEmpresa;
var nombreAplicativo;
var storeReadConsumo;
var storeBuscarClientes;
var storeTotalesConsumo;
var storeCliente;
var recordCliente;
Ext.define('Ktaxi.view.departamentoVoucher.c_Voucher', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Voucher',
    onViewVoucher: function (panelLoad) {
        MODULO_VOUCHER = panelLoad;
        var mapa = cargarMapa(panelLoad, ID_MAPA_VOUCHER);
        validarPermisosGeneral(panelLoad);
        storeCreditoCompania = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Credito_Compania');
        storeCliente = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Clientes'); 
        //getCompaniaSucursal();
        Ext.Ajax.request({
            async: true,
            url: 'php/DepartamentoVoucher/getAplicativoDepartamento.php',
            callback: function (callback, e, response) {
                var res = JSON.parse(response.request.result.responseText);
                if (res.success) {
                    MODULO_VOUCHER.down('[name=idDepartamento]').getStore().load({
                        params: {idDepartamento: res.idDepartamento},
                        callback: function (records, operation, success) {
                            MODULO_VOUCHER.down('[name=idDepartamento]').setValue(res.idDepartamento);
                        }
                    });
                    MODULO_VOUCHER.down('[name=idAplicativo]').getStore().load({
                        params: {idAplicativo: res.idAplicativo},
                        callback: function (records, operation, success) {
                            MODULO_VOUCHER.down('[name=idAplicativo]').setValue(res.idAplicativo);
                        }
                    });
                    MODULO_VOUCHER.down('[name=idSucursal]').getStore().load({
                        params: {idSucursal: res.idSucursal},
                        callback: function (records, operation, success) {
                            MODULO_VOUCHER.down('[name=idSucursal]').setValue(res.idSucursal);
                        }
                    });
                    MODULO_VOUCHER.down('[name=idCompania]').getStore().load({
                        params: {idCompania: res.idCompania},
                        callback: function (records, operation, success) {
                            MODULO_VOUCHER.down('[name=idCompania]').setValue(res.idCompania);
                        }
                    });
                    var params = {anio: anio, mes: mes, idDepartamento: res.idDepartamento, idAplicativo: res.idAplicativo, idSucursal: res.idSucursal, idCompania: res.idCompania};
                    recargarStore(params);

                } else {
                    notificaciones('Lo sentimos hubo un error al validar el registro', 2);
                }
            }
        });
    },
    onLimpiarGridCliente: function (btn, e) {
        MODULO_VOUCHER.down('[name=txtParam]').reset();
        var params = {anio: anio, mes: mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo};
        recargarStore(params);
    },
    onRecargarGridCliente: function () {
        idDepartamento = MODULO_VOUCHER.down('[name=idDepartamento]').getValue();
        var params = {anio: anio, mes: mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo};
        recargarStore(params);
    },
    onRecargarGridClienteConsumo: function () {
        MODULO_VOUCHER.down('[name=administracionConsumo]').getStore().reload();
    },
    onLimpiarFormCliente: function (btn, e) {
        MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').getForm().reset();
    },
    onCrearClienteCredito: function () {
        idDepartamento = MODULO_VOUCHER.down('[name=idDepartamento]').getValue();
        var form = MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').getForm();
        if (form.isValid()) {
            var val = form.getValues();
            console.log(val);
            if (val.nombresAC !== null && val.nombresAC !== '') {
                var numero = codigo();
                Ext.MessageBox.confirm('Atención!!!', '<h4>Está seguro que desea activar a ' + cliente + ' como cliente voucher<br><b>CÓDIGO</b><br>' + numero + '<h4>', function (choice) {
                    if (choice === 'yes') {
                        var params = {};
                        var form = Ext.create('Ext.form.Panel');
                        if (idDepartamento === undefined && idDepartamento > 0)
                            params = {id: recordCliente.id, codigo: numero, idApp: idAplicativo};
                        else
                            params = {id: recordCliente.id, codigo: numero, idApp: idAplicativo, idDepartamento: idDepartamento};
                        form.submit({
                            url: 'php/DepartamentoVoucher/createCliente.php',
                            timeout: 6000000,
                            waitTitle: 'Procesando Información',
                            waitMsg: 'Procesando...',
                            params: params,
                            success: function (form, action) {
                                notificaciones('Cliente Agregado Correctamente', 1);
                                var params = {anio: anio, mes: mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo};
                                recargarStore(params);
                            },
                            failure: function (form, action) {
                                notificaciones('No se pudo realizar la operación, intentelo nuevamente', 2);
                                MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').getForm().reset();
                            }
                        });
                    }
                });
            } else {
                messageInformationEffect('Debe dar click en buscar');
            }
        } else {
            messageInformationEffect('Debe dar click en buscar');
        }
    },
    onBuscarClienteAgregar: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            storeBuscarClientes = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Buscar_Cliente');
            var paramBusqueda = MODULO_VOUCHER.down('[name=cedula]').getValue();
            storeBuscarClientes.load({
                params: {
                    cedula: paramBusqueda,
                    idAplicativo: idAplicativo,
                },
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').down('[name=nombres]').setValue(records[0].data.nombres);
                        MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').down('[name=apellidos]').setValue(records[0].data.apellidos);
                        MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').down('[name=aplicativo]').setValue(records[0].data.app);
                        MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').down('[name=celular]').setValue(records[0].data.celular);
                        var data = records[0].data;
                        recordCliente = data;
                        cliente = records[0].data.cliente;
                        idAppBus = records[0].data.idApp;
                    } else {
                        notificaciones('No existen datos del cliente, por favor revise la cédula e intente nuevamente', 2);
                    }
                }
            });
        }
    },
    onGetClass: function (v, meta, rec) {
        if (rec.data.tipo === 1) {
            return 'x-fa fa-user';
        } else if (rec.data.tipo == 2) {
            return 'x-fa fa-check';
        }
    },
    onSelectGridCliente: function (thisObj, selected, eOpts) {
        console.log(selected.data);
        cliente = selected.data.cliente;
        idApp = selected.data.idAplicativo;
        app = selected.data.app;
        cedula = selected.data.cedula;
        celular = selected.data.celular;
        idDepartamento = selected.data.idDepartamento;
        idDeparCli = selected.data.idDeparCli;
        idDeparCliCre = selected.data.idDeparCliCre;
        var params = {idCliente: selected.idCliente, anio: selected.anio, mes: selected.mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo};
        storeReadConsumo = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Read_Consumo');
        storeTotalesConsumo = Ext.create('Ktaxi.store.departamentoVoucher.s_Voucher_Totales_Consumo');
        MODULO_VOUCHER.down('[name=asignarCredito]').enable();
        MODULO_VOUCHER.down('[name=administracionConsumo]').getStore().load({
            params: {
                idCliente: selected.data.idCliente,
                idDeparCliCre: selected.data.idDeparCliCre,
                idDeparCli: selected.data.idDeparCli,
                anio: anio,
                mes: mes,
                idDepartamento: idDepartamento
            },
            callback: function (records, operation, success) {
                MODULO_VOUCHER.down('[name=clienteAdmCosumo]').setText('Cliente: ' + selected.data.cliente);
                MODULO_VOUCHER.down('[name=creditoAdmCosumo]').setText('Crédito: $' + selected.data.creditoMes);
                MODULO_VOUCHER.down('[name=consumoAdmCosumo]').setText('Cosumido: $' + selected.data.consumoMes);
                MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').setCollapsed(true);
            }
        });
    },
    onShowVentanaAsignarCredito: function () {
        vnt_AsignarCredito = Ext.create('Ext.window.Window', {
            title: 'Ventana de asignar credito a un cliente',
            iconCls: 'icon-services',
            layout: 'anchor',
//            closeAction: 'hide',
            closeAction: 'destroy',
            resizable: false,
            closable: true,
            modal: true,
            plain: false,
            width: 700,
            height: 380,
            items: [
                {
                    layout: 'anchor',
                    width: '100%',
                    height: '100%',
                    xtype: 'panel',
                    name: 'pnl-AsigCred',
                    items: [{
                            xtype: 'label',
                            text: 'Agregar credito a ' + cliente,
                            id: 'lbl-titleACr',
                            name: 'lbl-titleACr',
                            style: 'aling:center;font-size: 22px; font-weight: bold;'
                        },
                        {
                            xtype: 'label',
                            html: '<hr>',
                            id: 'lbl-lineaACr',
                            name: 'lbl-lineaACr',
                            style: 'font-size: 12px;'
                        },
                        {
                            xtype: 'label',
                            text: 'El dinero que usted asigne al cliente se podra usar desde las fechas que usted seleccione acontinuación.',
                            id: 'lbl-descrACr',
                            name: 'lbl-descrACr',
                            style: 'font-size: 12px;'
                        }]
                },
                {
                    layout: 'anchor',
                    width: '100%',
                    xtype: 'form',
                    height: '100%',
                    name: 'formAsignarCredito',
                    items: [{
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    xtype: 'datefield',
                                    flex: 1,
                                    fieldLabel: '<b>Desde</b>',
                                    name: 'fechaDACr',
                                    id: 'fechaDACr',
                                    value: new Date(),
                                    minValue: new Date(),
                                    format: 'Y m d',
                                    listeners: {
                                        select: function (field, value, eOpts) {
                                            var fechaDesde=vnt_AsignarCredito.down('[name=fechaDACr]').getRawValue();       
                                            var maxFecha=moment(fechaDesde).endOf("month").add(5, 'days').format('YYYY-MM-DD');                                  
                                            vnt_AsignarCredito.down('[name=fechaHACr]').setMinValue(fechaDesde);
                                            vnt_AsignarCredito.down('[name=fechaHACr]').setValue(fechaDesde);
                                            vnt_AsignarCredito.down('[name=fechaHACr]').setMaxValue(maxFecha);
                                            Ext.getCmp('horaDACr').focus(true);
                                        }
                                    }
                                },
                                {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    xtype: 'timefield',
                                    name: 'horaDACr',
                                    id: 'horaDACr',
                                    fieldLabel: '',
                                    maxValue: '23:00',
                                    format: 'H:i',
                                    increment: 30,
                                    value: fecha.getHours(),
                                    flex: 1,
                                    listeners: {
                                        select: function (combo, records, eOpts) {
                                            Ext.getCmp('fechaHACr').focus(true);
                                        }
                                    }
                                }]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    xtype: 'datefield',
                                    flex: 1,
                                    fieldLabel: '<b>Hasta</b>',
                                    name: 'fechaHACr',
                                    id: 'fechaHACr',
                                    format: 'Y m d',
                                    minValue: new Date(),
                                    listeners: {
                                        select: function (field, value, eOpts) {
                                            Ext.getCmp('horaHACr').focus(true);
                                            var horaH = Ext.getCmp('horaDACr').getValue();
                                            Ext.getCmp('horaHACr').setMinValue(horaH);
                                        },
                                        beforerender:function ( value, eOpts ){
                                            var fechaDesde=vnt_AsignarCredito.down('[name=fechaDACr]').getRawValue();
                                            var date=moment(fechaDesde).format('YYYY-MM-DD');
                                            var dias=moment(date).get('date');
                                            var maxFecha=moment(fechaDesde).endOf("month").add(5, 'days').format('YYYY-MM-DD'); 
                                            vnt_AsignarCredito.down('[name=fechaHACr]').setMaxValue(maxFecha);
                                        } 
                                    }
                                },
                                {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    xtype: 'timefield',
                                    name: 'horaHACr',
                                    id: 'horaHACr',
                                    fieldLabel: '',
                                    minValue: '0:00',
                                    maxValue: '23:00',
                                    format: 'H:i',
                                    increment: 30,
                                    flex: 1,
                                    listeners: {
                                        select: function (combo, records, eOpts) {
                                            Ext.getCmp('montoACr').focus(true);
                                        }
                                    }
                                },
                                {
                                }]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'numberfield',
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    maxValue: 1000,
                                    minValue: 0,
                                    name: 'montoACr',
                                    id: 'montoACr',
                                    fieldLabel: '<b>Monto</b>',
                                    flex: 2,
                                    emptyText: 0,
                                    enableKeyEvents: true,
                                    decimalPrecision: 2,
                                    allowDecimals: true,
                                    alwaysDisplayDecimals: true,
                                    listeners: {
                                        blur: function (thisObj, e, eOpts) {
                                            var idCliente = Ext.getCmp('nombresACr').getValue();
                                            if (idCliente === null) {
                                                Ext.getCmp('nombresACr').focus(true);
                                            } else {
                                                Ext.getCmp('razonACr').focus(true);
                                            }
                                        }
                                    }
                                },
                                {
                                    flex: 2,
                                    xtype: 'label',
                                    text: '0.00',
                                    id: 'lbl-creditoLim',
                                    name: 'lbl-creditoLim',
                                    style: 'font-size: 13px;font-weight: bold;'
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    emptyText: 'Nombre apellido',
                                    fieldLabel: '<b>Nombres</b>',
                                    forceSelection: true,
                                    hidden: true,
                                    listConfig: {
                                        minWidth: 250
                                    },
                                    margin: '0 5 5 0',
                                    name: 'appACrCombo',
                                    maxLength: 90,
                                    flex: 1,
//                                    store: storeCliente,
                                    trigger: true,
                                    valueField: 'id',
                                    displayField: 'text',
                                    queryMode: 'remote', //tipo de recarga del store desde afuera
                                    queryParam: 'filtro', //criterio de busqueda a enviar al php
                                    minChars: 2, //numero de caracteres limite para empezar la busqueda
                                    hideTrigger: true, //Para ocultar triángulo
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'idDepartamentoACr',
//                                    id: 'nombresACr',
                                    emptyText: 'idDepartamento',
                                    fieldLabel: '<b>idDepartamento</b>',
                                    readOnly: true,
                                    hidden: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'nombresACr',
                                    id: 'nombresACr',
                                    emptyText: 'Nombre apellido',
                                    fieldLabel: '<b>Nombres</b>',
                                    readOnly: true,
                                    flex: 1
                                },
                                {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    xtype: 'textfield',
                                    name: 'cedulaACr',
                                    id: 'cedulaACr',
                                    emptyText: 'Cédula',
                                    fieldLabel: '<b>Cédula</b>',
                                    readOnly: true,
                                    flex: 1
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'textfield',
                                    name: 'aplicativoACr',
                                    id: 'aplicativoACr',
                                    emptyText: 'Aplicativo',
                                    fieldLabel: '<b>Aplicativo</b>',
                                    readOnly: true,
                                    flex: 1
                                }, {
                                    xtype: 'textfield',
                                    name: 'celularACr',
                                    id: 'celularACr',
                                    emptyText: 'Celular',
                                    fieldLabel: '<b>Celular</b>',
                                    readOnly: true,
                                    flex: 1
                                }]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    xtype: 'textareafield',
                                    name: 'razonACr',
                                    id: 'razonACr',
                                    emptyText: 'Descripción de la razón del credito',
                                    fieldLabel: '<b>Razón</b>',
                                    maxLength: 500,
                                    maxLengthText: 'Debe ingresar hasta 500 caracter',
                                    minLengthText: 'Debe ingresar mas de 20 caracteres',
                                    minLength: 20,
                                    flex: 1
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: ['->', {
                    iconCls: 'fa fa-credit-card-alt',
                    text: '<b>Asignar credito</b>',
                    width: '150px',
                    tooltip: 'Permite asiganar credito a un cliente',
                    name: 'btn_agregar_cred_cliente',
                    handler: function (event, target, owner, tool) {
                        Ext.MessageBox.buttonText = {
                            yes: "Sí",
                            no: "No"
                        };
                        var form = vnt_AsignarCredito.down('[name=formAsignarCredito]').getForm();
                        if (form.isValid()) {
                            var val = form.getValues();
                            console.log(val);
                            Ext.MessageBox.confirm('Atención', '<h3>¿Está seguro de asignar ' + val.montoACr + '.00' + ' de crédito al cliente ' + cliente + ' entre las fechas ' + val.fechaDACr + ' ' + val.horaDACr + ' y ' + val.fechaHACr + ' ' + val.horaHACr + '?</h3>', function (choice) {
                                if (choice === 'yes') {
                                    var data;
                                    var desde = new Date(val.fechaDACr);
                                    var hasta = new Date(val.fechaHACr);
                                    if (idDepartamento === undefined) {//si se realiza de voucher compania
                                        data = {idDeparCli: idDeparCli, monto: val.montoACr + '.00', desde: Ext.Date.format(desde, 'Y-m-d'),
                                            horaD: val.horaDACr, hasta: Ext.Date.format(hasta, 'Y-m-d'), horaH: val.horaHACr,
                                            razon_credito: val.razonACr, saldo: storeCreditoCompania.data.items[0].data.saldo};
                                    } else {//si se realiza de voucher empresarial admin
                                        data = {idDepartamento: idDepartamento, idDeparCli: idDeparCli, monto: val.montoACr + '.00', anio: Ext.Date.format(desde, 'Y'), mes: Ext.Date.format(desde, 'm'), desde: Ext.Date.format(desde, 'Y-m-d'),
                                            horaD: val.horaDACr, hasta: Ext.Date.format(hasta, 'Y-m-d'), horaH: val.horaHACr,
                                            razon_credito: val.razonACr, saldo: storeCreditoCompania.data.items[0].data.credito};
                                    }
                                    var form = Ext.create('Ext.form.Panel');
                                    form.getForm().submit({
                                        url: 'php/DepartamentoVoucher/createCredito.php',
                                        timeout: 6000000,
                                        waitTitle: 'Procesando Información',
                                        waitMsg: 'Procesando...',
                                        params: data,
                                        success: function (form, action) {
//                                            mensajeSuccess(action.result.message);
                                            vnt_AsignarCredito.close();
                                            vnt_AsignarCredito.destroy();
                                            notificaciones('Crédito Agregado Correctamente', 1);
                                            var params = {anio: anio, mes: mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo};
                                            recargarStore(params);

                                        },
                                        failure: function (form, action) {
                                            notificaciones('No se pudo realizar la asignación,revice el crédito de la empresa o la fecha de inicio de crédito', 2);
                                            vnt_AsignarCredito.close();
                                            vnt_AsignarCredito.destroy();
                                            var params = {anio: anio, mes: mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo};
                                            recargarStore(params);
                                        }
                                    });
                                }
                            });
                        } else {
                            Ext.MessageBox.alert('Atención', 'Complete los campos requeridos');
                        }
                    }
                }, {
                    iconCls: 'fa fa-window-close-o',
                    text: '<b>Cancelar</b>',
                    width: '100px',
                    tooltip: 'Cerrar ventana',
                    handler: function (event, target, owner, tool) {
                        vnt_AsignarCredito.close();
                        vnt_AsignarCredito.destroy();
                    }
                }
            ]
        });
        vnt_AsignarCredito.show();
        vnt_AsignarCredito.down('[name=appACrCombo]').setValue(idApp);
        vnt_AsignarCredito.down('[name=idDepartamentoACr]').setValue(idDepartamento);
        vnt_AsignarCredito.down('[name=nombresACr]').setValue(cliente);
        vnt_AsignarCredito.down('[name=aplicativoACr]').setValue(app);
        vnt_AsignarCredito.down('[name=cedulaACr]').setValue(cedula);
        vnt_AsignarCredito.down('[name=celularACr]').setValue(celular);
        vnt_AsignarCredito.down('[name=razonACr]').focus(true);
    },
    onBuscarClienteParams: function (component, check) {
        idDepartamento = MODULO_VOUCHER.down('[name=idDepartamento]').getValue();
        idAplicativo = MODULO_VOUCHER.down('[name=idAplicativo]').getValue();
        idSucursal = MODULO_VOUCHER.down('[name=idSucursal]').getValue();
        idCompania = MODULO_VOUCHER.down('[name=idCompania]').getValue();

        var params = {anio: anio, mes: mes, idDepartamento: idDepartamento, idAplicativo: idAplicativo,idSucursal: idSucursal,idCompania: idCompania};
        //getCompaniaSucursal();
        recargarStore(params);
        console.log('metodo para llenar los combos');
        getCompaniaSucursal(params);
    },
    onBuscarGridCliente: function (btn, e) {
        idDepartamento = MODULO_VOUCHER.down('[name=idDepartamento]').getValue();
        var txtParam = MODULO_VOUCHER.down('[name=txtParam]').getValue();
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            MODULO_VOUCHER.down('[name=gridLeerVoucher]').getStore().load({
                params: {
                    params: txtParam,
                    anio: anio,
                    mes: mes,
                    idDepartamento: idDepartamento,
                    idAplicativo: idAplicativo
                },
                callback: function (records, operation, success) {
                    if (!success) {
                        setMensajeGridDepartamentomptyText(MODULO_VOUCHER.down('[name=gridLeerVoucher]'), EMPTY_CARA + '<h3>' + MENSAJE_ERROR + '</h3>');
                    } else if (records.length === 0) {
                        setMensajeGridDepartamentomptyText(MODULO_VOUCHER.down('[name=grid]'), EMPTY_CARA + '<h3>No existen resultados.</h3>');
                    } else {
                        MODULO_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
                        MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').getForm().reset();

                    }

                }
            });
        }
    },
});
//FUNCIONES

function limpiarGridCliente() {
    MODULO_VOUCHER.down('[name=paramBusquedaVoucher]').reset();
    var gridLeerVoucher = MODULO_VOUCHER.down('[name=gridLeerVoucher]');
    gridLeerVoucher.getView().deselect(gridLeerVoucher.getSelection());
    MODULO_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
}

function codigo(calback) {
    var numero;
    var numero = "";
    var cifra = [];
    for (a = 0; a < 4; a++) {
        cifra[a] = parseInt(Math.random() * 10);
        if (a === 0) {	//quita esto si el número puede empezar por cero
            cifra[a] = parseInt(Math.random() * 9) + 1; //quita esto si el número puede empezar por cero
        }//quita esto si el número puede empezar por cero
        for (aa = 0; aa < a; aa++) {
            if (cifra[a] === cifra[aa]) {
                a -= 1;
                break
            }

        }
    }
    for (a = 0; a < 4; a++) {
        numero += cifra[a];
    }
    return numero;
}

function recargarStore(params) {
    storeCreditoCompania.load({
        params: {anio: params.anio, mes: params.mes, idDepartamento: params.idDepartamento, idAplicativo: params.idAplicativo},
        callback: function (records, operation, success) {
            if (!success) {
                notificaciones(MENSAJE_ERROR, 2);
            } else {
                if (records.length > 0) {
                    MODULO_VOUCHER.down('[name=creditoCompania]').setText('Crédito : $' + records[0].data.credito);
                    MODULO_VOUCHER.down('[name=creditoConsumido]').setText('Asignado : $' + records[0].data.asignado);
                    MODULO_VOUCHER.down('[name=creditoDisponible]').setText('Cosumido: $' + records[0].data.consumo);
                    idDepartamento = records[0].data.idDepartamento;
                } else {
                    MODULO_VOUCHER.down('[name=creditoCompania]').setText('Crédito: $0');
                    MODULO_VOUCHER.down('[name=creditoConsumido]').setText('Asignado: $0');
                    MODULO_VOUCHER.down('[name=creditoDisponible]').setText('Consumo: $0');
                }
            }
        }
    });
    MODULO_VOUCHER.down('[name=gridLeerVoucher]').getStore().load({
        params: {anio: params.anio, mes: params.mes, idDepartamento: params.idDepartamento, idAplicativo: params.idAplicativo},
        callback: function (records, operation, success) {
            if (!success) {
                console.log('error');
            } else {
                if (records.length > 0) {

                }
            }
        }
    });

    MODULO_VOUCHER.down('[name=asignarCredito]').disable();
    MODULO_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
    MODULO_VOUCHER.down('[name=panelCrearClienteVoucher]').getForm().reset();
    MODULO_VOUCHER.down('[name=gridLeerVoucher]').getView().deselect(MODULO_VOUCHER.down('[name=gridLeerVoucher]').getSelection());
    MODULO_VOUCHER.down('[name=clienteAdmCosumo]').setText('Cliente:');
    MODULO_VOUCHER.down('[name=creditoAdmCosumo]').setText('Crédito: $');
    MODULO_VOUCHER.down('[name=consumoAdmCosumo]').setText('Cosumido: $');
    MODULO_VOUCHER.down('[name=nombreApp]').setText('Aplicativo: ' + MODULO_VOUCHER.down('[name=idAplicativo]').getRawValue());
}

function getCompaniaSucursal(params) {
    Ext.Ajax.request({
        async: true,
        params:params,
        url: 'php/DepartamentoVoucher/getConsultaCombos.php',
        callback: function (callback, e, response) {
            var res = JSON.parse(response.request.result.responseText);
            if (res.success) {
                /* MODULO_VOUCHER.down('[name=idDepartamento]').getStore().load({
                    params: {idDepartamento: res.idDepartamento},
                    callback: function (records, operation, success) {
                        MODULO_VOUCHER.down('[name=idDepartamento]').setValue(res.idDepartamento);
                    }
                });
                MODULO_VOUCHER.down('[name=idAplicativo]').getStore().load({
                    params: {idAplicativo: res.idAplicativo},
                    callback: function (records, operation, success) {
                        MODULO_VOUCHER.down('[name=idAplicativo]').setValue(res.idAplicativo);
                    }
                }); */
                MODULO_VOUCHER.down('[name=idSucursal]').getStore().load({
                    params: {idSucursal: res.idSucursal},
                    callback: function (records, operation, success) {
                        MODULO_VOUCHER.down('[name=idSucursal]').setValue(res.idSucursal);
                    }
                });
                MODULO_VOUCHER.down('[name=idCompania]').getStore().load({
                    params: {idCompania: res.idCompania},
                    callback: function (records, operation, success) {
                        MODULO_VOUCHER.down('[name=idCompania]').setValue(res.idCompania);
                    }
                });
                

            } else {
                notificaciones('Lo sentimos hubo un error al validar el registro', 2);
            }
        }
    });
}
