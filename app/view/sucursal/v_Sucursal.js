var permisosSucursal;
Ext.define('Ktaxi.view.sucursal.v_Sucursal', {
    extend: 'Ext.panel.Panel',
    xtype: 'sucursal',
//    title: TITULO,
    store: 'sucursal.s_Sucursal',
    controller: 'c_Sucursal',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'moduloSucursal',
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.sucursal.c_Sucursal'
    ],
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onViewSucursal',
        show: 'onShowSucursal'
    },
    initComponent: function () {
        var STORE_SUCURSAL = Ext.create('Ktaxi.store.sucursal.s_Sucursal');
        var STORE_CIUDADES = Ext.create('Ktaxi.store.combos.s_Ciudades');
        var STORE_ADMINISTRADOR_COMBO = Ext.create('Ktaxi.store.combos.s_Administrador');
        var STORE_COMPANIA = Ext.create('Ktaxi.store.combos.s_Company');
        var STORE_ADMINISTRADORES_SUCURSAL = Ext.create('Ktaxi.store.sucursal.s_Administrador_Sucursal');
        this.items = [
            {
                name: 'panelLeerSucursal',
                region: 'west',
                xtype: 'panel',
                padding: 5,
                width: '38%',
                layout: 'fit',
                header: false,
                headerAsText: false,
                items: [{
                        name: 'gridLeerSucursals',
                        columnLines: true,
                        xtype: 'grid',
                        plugins: [{ptype: 'gridfilters'}],
                        bufferedRenderer: false,
                        store: STORE_SUCURSAL,
                        tbar: [
                            {
                                xtype: 'textfield',
                                width: '60%',
                                tooltip: 'Escribir búsqueda',
                                name: 'paramBusquedaSucursal',
                                emptyText: 'Sucursal, calle principal, calle secun..',
                                minChars: 0,
                                typeAhead: true,
                                listeners: {
                                    specialkey: 'onChangeSearchSucursal'
                                }
                            },
                            {
                                xtype: 'combobox',
                                width: '20%',
                                tooltip: 'Seleccionar Ciudad',
                                name: 'comboBuscarCiudad',
                                emptyText: 'Todos',
                                reference: 'states',
                                displayField: 'text',
                                valueField: 'idCiudad',
                                filterPickList: true,
                                queryParam: 'param',
                                queryMode: 'remote',
                                growMax: 20,
                                store: STORE_CIUDADES,
                                listeners: {
                                    specialkey: 'onChangeSearchSucursal'
                                }
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-search',
                                iconAlign: 'right',
                                tooltip: 'Buscar',
                                handler: 'onChangeSearchSucursal'
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-eraser',
                                iconAlign: 'right',
                                tooltip: 'Limpiar',
                                handler: 'onLimpiarSucursal'
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-refresh',
                                iconAlign: 'right',
                                tooltip: 'Recargar',
                                handler: 'onRecargar'
                            }
                        ],
                        columns: [
                            Ext.create('Ext.grid.RowNumberer', {header: '#', flex:1, align: 'center'}),
                            {filter: true, tooltip: "Sucursal", text: "Sucursal", flex:2, dataIndex: 'sucursal', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Calle Principal", text: "Calle Principal", flex:2, dataIndex: 'callePrin', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Calle Secundaria", text: "Calle Secundaria", flex:2, dataIndex: 'calleSec', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Color", text: "Color", flex:2, dataIndex: 'color', sortable: true, renderer: formatColor},
                            {filter: true, tooltip: "Contacto", text: "Contacto", flex:2, dataIndex: 'contacto', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Insertada", text: "Insertada", flex:2, dataIndex: 'insertado', sortable: true, renderer: function formatEstadoRegistroInsertado(estado) {
                                    var htmlEstado = '';
                                     if (estado) {
                                        htmlEstado = '<i style="color:green;" class="gridAuxCheck x-fa fa-check" aria-hidden="true">';
                                    } else {
                                        htmlEstado = '<i style="color:red;" class="gridAuxDelete fa x-fa fa-times" aria-hidden="true">';
                                    }
                                    return htmlEstado;
                                }},
                            {filter: true, tooltip: "Comentario", text: "Comentario", width: 100, dataIndex: 'comentario', sortable: true, renderer: showTipConten},                            
                            {filter: true, tooltip: "ID", text: "ID", width: 40, dataIndex: 'id', sortable: true, renderer: showTipContenID, hidden: true}

                        ],
                        height: 210,
                        split: true,
                        region: 'north',
                        listeners: {
                            selectionchange: 'onSelectChangeGridSucursal',
                            beforeitemclick: 'onBeforeclickGridSucursal',
                            rowdblclick: showAuditoria
                        },
                        viewConfig: {enableTextSelection: true,
                            emptyText: '<center>No existen resultados.</center>'
                        },
                        bbar: Ext.create('Ext.PagingToolbar', {
                            store: STORE_SUCURSAL,
                            displayInfo: true,
                            emptyMsg: "Sin datos que mostrar.",
                            displayMsg: ' {0} - {1} de {2} registros',
                            beforePageText: 'Página',
                            afterPageText: 'de {0}',
                            firstText: 'Primera página',
                            prevText: 'Página anterior',
                            nextText: 'Siguiente página',
                            lastText: 'Última página',
                            refreshText: 'Actualizar',
                            inputItemWidth: 35,
                            items: [
                                {
                                    xtype: 'label',
                                    name: 'numRegistrosGrid',
                                    text: '0 registros'
                                }
                            ],
                            listeners: {
                                afterrender: function () {
                                    this.child('#refresh').hide();
                                }
                            }
                        })
                    }]
            },
            {
                name: 'panelCrearEditarSucursal',
                cls: 'panelCrearEditar',
                region: 'center',
                xtype: 'form',
                layout: 'fit',
                padding: 5,
                flex: 2,
                collapsible: false,
                items: [{
                        xtype: 'form',
                        name: 'formCrearEditarSucursal',
                        title: 'Sucursal',
                        layout: 'vbox',
                        padding: 5,
                        flex: 2,
                        cls: 'quick-graph-panel shadow panelFormulario',
                        ui: 'light',
                        defaultType: 'textfield',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [
                            {
                                allowBlank: false,
                                name: 'ciudad',
                                xtype: 'combobox',
                                width: '100%',
                                emptyText: 'Seleccione',
                                fieldLabel: 'Ciudad',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                maxLength: '45',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                store: STORE_CIUDADES,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                listeners: {
                                    select: function (record) {
                                        centrar(LIST_MAPS[ID_MAPA_SUCURSAL], record.selection.data.latitud, record.selection.data.longitud);
                                    }
                                }
                            },
                            {
                                allowBlank: false,
                                name: 'compania',
                                width: '100%',
                                xtype: 'combobox',
                                emptyText: 'Seleccione',
                                fieldLabel: 'Compania',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                maxLength: '150',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                store: STORE_COMPANIA
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Sucursal',
                                name: 'sucursal',
                                emptyText: 'Sucursal',
                                maxLength: '150',
                                width: '100%',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                listeners: {
                                    change: function keyup(thisObj, e, eOpts) {
                                        thisObj.setValue(e.toUpperCase());
                                    }
                                }
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Calle Prin.',
                                name: 'callePrin',
                                emptyText: 'Calle principal',
                                maxLength: '120',
                                width: '100%',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Calle secun',
                                name: 'calleSec',
                                emptyText: 'Calle secundaria',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                maxLength: '120',
                                width: '100%',
                                minLength: '4',
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Contacto',
                                name: 'contacto',
                                maskRe: /[0-9.]/,
                                emptyText: 'Contacto',
                                maxLength: '12',
                                width: '100%',
                                minLength: '9',
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                vtype: 'email',
                                fieldLabel: 'Correo',
                                name: 'correo',
                                emptyText: 'Correo',
                                width: '100%',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                maxLength: '120'
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Color',
                                name: 'color',
                                width: '100%',
                                inputType: 'color',
                                emptyText: 'Color',
                                height: 25,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Latitud',
                                name: 'latitud',
                                emptyText: 'Latitud',
                                maxLength: '100',
                                minLength: '4',
                                width: '100%',
                                maskRe: /[0-9.]/,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID

                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Longitud',
                                name: 'longitud',
                                emptyText: 'Longitud',
                                maxLength: '100',
                                minLength: '4',
                                width: '100%',
                                maskRe: /[0-9.]/,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'textareafield',
                                allowBlank: true,
                                fieldLabel: 'Comentario',
                                name: 'comentario',
                                emptyText: 'Comentario',
                                width: '100%',
                                maxLength: '500',
                                minLength: '10',
                                allowOnlyWhitespace: true,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            }
                            ,
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                width: '100%',
                                defaultType: 'combobox',
                                defaults: {
                                    margin: 1
                                },
                                items: [
                                    {
                                        allowBlank: true,
                                        fieldLabel: 'Administrador',
                                        xtype: 'combobox',
                                        tooltip: 'Buscar Administradores',
                                        name: 'comboAdministrador',
                                        emptyText: 'Cédula..',
                                        displayField: 'nombres',
                                        valueField: 'id',
                                        filterPickList: true,
                                        forceSelection: true,
                                        queryParam: 'cedula',
                                        queryMode: 'remote',
                                        store: STORE_ADMINISTRADOR_COMBO,
                                        minChars: 9
                                    },
                                    {
                                        xtype: 'button',
                                        name: 'agregarAdministrador',
                                        flex: 1,
                                        iconCls: 'x-fa fa-plus',
                                        height: 23,
                                        handler: 'onAddAdministrador'}
                                ]
                            },
                            {
                                xtype: 'grid',
                                height: 50,
                                width: '100%',
//                                id: 'gridAuxEmpAdmin',
                                name: 'gridAdministradoresSucursal',
                                autoScroll: true,
                                bufferedRenderer: false,
                                store: STORE_ADMINISTRADORES_SUCURSAL,
                                cls: 'gridAuxAdmSucursal',
                                columns: [
                                    {flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro},
                                    {flex: 2, dataIndex: 'nombre', sortable: true},
                                    {
                                        xtype: 'actioncolumn',
                                        menuDisabled: true,
                                        sortable: false,
                                        minWidth: 20,
                                        flex: 1,
                                        items: [{
                                                getClass: function (v, meta, rec) {
                                                    if (rec.data.habilitado) {
                                                        return 'gridAuxDelete x-fa fa-times';
                                                    } else {
                                                        return 'gridAuxCheck x-fa fa-check';
                                                    }
                                                },
                                                handler: function (grid, rowIndex, colIndex) {
                                                    var rec = grid.getStore().getAt(rowIndex);
                                                    if (rec.data.nuevo) {
                                                        grid.getStore().remove(rec);
                                                    } else {
                                                        if (rec.data.habilitado) {
                                                            rec.set('habilitado', 0);
                                                        } else {
                                                            rec.set('habilitado', 1);
                                                        }
                                                    }
                                                }
                                            }]
                                    }
                                ],
                                minHeight: 150,
                                split: true,
                                region: 'north',
                                viewConfig: {enableTextSelection: true,
                                    emptyText: '<center>Sin administradores asignados..</center>',
                                    getRowClass: function (record) {
                                        if (record.data.nuevo) {
                                            return 'newRowGrid';
                                        }
                                    }
                                },
                                listeners: {
                                    rowdblclick: function (grid, record) {
                                        if (!record.data.nuevo) {
                                            showAuditoria(grid, record, 'gridAux');
                                        }
                                    }
                                }
                            }
                        ]
                    }],
                dockedItems: [{
                        ui: 'footer',
                        xtype: 'toolbar',
                        dock: 'bottom',
                        defaults: {
                            width: '25%',
                            height: 30
                        },
                        items: [
                            {
                                text: 'Limpiar',
                                tooltip: 'Limpiar',
                                disabled: false,
                                handler: 'onLimpiarSucursal'
                            },
                            '->',
                            {
                                text: 'Editar',
                                tooltip: 'Actualizar',
//                                id: 'btnEditarSucursal',
                                disabled: true,
                                name: 'btnEditar',
                                handler: 'onUpdate'
                            }, {
                                text: 'Crear',
                                tooltip: 'Crear Ciudades',
//                                id: 'btnCrearSucursal',
                                disabled: true,
                                name: 'btnCrear',
                                handler: 'onCreate'
                            }]
                    }]
            },
            {
                region: 'east',
                xtype: 'panel',
                width: '35%',
                margin: 5,
                header: false,
                collapsible: true,
                collapsed: false,
                html: '<div id="' + ID_MAPA_SUCURSAL + '" style="position: absolute; top: 0; left: 0; z-index: 0; width: 100%; height: 100%"></div>',
            }
        ];
        this.callParent(arguments);
    }
});

