var MODULO_SUCURSAL;
Ext.define('Ktaxi.view.sucursal.c_Sucursal', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Sucursal',
    onViewSucursal: function (panelLoad) {
        panelLoad.ID_MAPA = ID_MAPA_SUCURSAL;//para cargar el id del nuevo mapa
        cargarMapa(panelLoad, ID_MAPA_SUCURSAL);
        validarPermisosGeneral(panelLoad);
        MODULO_SUCURSAL = panelLoad;
        MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().load();
    },
    onRecargar: function () {
        MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().reload();
    },
    onShowSucursal: function (panelLoad) {
        PANELLOAD = panelLoad;
        PANELLOAD.ID_MAPA = ID_MAPA_SUCURSAL;
    },
    onLimpiarSucursal: function (btn, e) {
        validarPermisosGeneral(MODULO_SUCURSAL);
        limpiarMapa(LIST_MAPS[ID_MAPA_SUCURSAL]);
        MODULO_SUCURSAL.down('[name=btnCrear]').enable();
        MODULO_SUCURSAL.down('[name=btnEditar]').disable();
        MODULO_SUCURSAL.down('[name=gridAdministradoresSucursal]').getStore().removeAll();
        MODULO_SUCURSAL.down('[name=paramBusquedaSucursal]').reset();
        var form_crear = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]');
        form_crear.down('[name=compania]').enable();
        form_crear.getForm().reset();
        var grid = MODULO_SUCURSAL.down('[name=gridLeerSucursals]');
        grid.getView().deselect(grid.getSelection());
        grid.getStore().load();
    
    },
    onChangeSearchSucursal: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var paramBusqueda = MODULO_SUCURSAL.down('[name=panelLeerSucursal]').down('[name=paramBusquedaSucursal]').getValue();
            var ciudades = MODULO_SUCURSAL.down('[name=panelLeerSucursal]').down('[name=comboBuscarCiudad]').getValue();
            
            var params = {param: paramBusqueda, ciudades: (ciudades == null) ? 0 : ciudades};
            MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().load({
                params: params,
                callback: function (records) {
                    if (records.length <= 0) {
                        MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridSucursal: function (thisObj, selected, eOpts) {
        if (selected.length > 0) {
            limpiarMapa(LIST_MAPS[ID_MAPA_SUCURSAL]);
            MODULO_SUCURSAL.down('[name=gridAdministradoresSucursal]').getStore().removeAll();
            var comboFormCompania = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]').down('[name=compania]');
            comboFormCompania.disable();
            var comboFormCiudad = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]').down('[name=ciudad]');
            if (!isInStore(comboFormCiudad.getStore(), selected[0].data.ciudad, 'idCiudad', 'exact')) {
                comboFormCiudad.getStore().load({
                    params: {
                        param: selected[0].data.ciudad
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormCiudad.setValue(records);
                        }
                    }
                });
            }
            var comboFormCompany = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]').down('[name=compania]');
            if (!isInStore(comboFormCompany.getStore(), selected[0].data.idCompania, 'id', 'exact')) {
                comboFormCompany.getStore().load({
                    params: {
                        idCompania: selected[0].data.idCompania
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormCompany.setValue(records);
                        }
                    }
                });
            }
            var comboAdministradores = MODULO_SUCURSAL.down('[name=comboAdministrador]');
            comboAdministradores.getStore().proxy.extraParams = '';
            comboAdministradores.getStore().load();
            var formCrearEmpresa = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]');
            var gridAdministradores = MODULO_SUCURSAL.down('[name=gridAdministradoresSucursal]');
            gridAdministradores.getStore().load({
                params: {
                    idSucursal: selected[0].data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        gridAdministradores.getStore().removeAll();
                    } else {
                        var asignados = [];
                        for (var i in records) {
                            asignados.push(records[i].data.id);
                        }
                        comboAdministradores.getStore().proxy.extraParams = {asignados: asignados.toString()};
                        comboAdministradores.getStore().load();
                    }
                }
            });
            formCrearEmpresa.loadRecord(selected[0]);
            graficarMarcador(LIST_MAPS[ID_MAPA_SUCURSAL], selected[0].data.latitud, selected[0].data.longitud, null, 12, selected[0].data.color);
        }
    },
    onBeforeclickGridSucursal: function (thisObj, record, item, index, e, eOpts) {
        MODULO_SUCURSAL.down('[name=btnEditar]').enable();
        MODULO_SUCURSAL.down('[name=btnCrear]').disable();
    },
    onCreate: function () {
        var params = this.getRecordsAdministrador();
        var me = this;
        var form = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]');
        if (form.isValid()) {
            Ext.MessageBox.buttonText = {
                    yes: "Sí",
                    no: "No"
                };                
            Ext.MessageBox.confirm('Aviso!', 'La compania no podrá ser modificada luego de guardar.<center><b style="color:red">¿Desea continuar?</b></center></b>', function (choice) {
                if (choice === 'yes') {
                    MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().insert(0, form.getValues());
                    MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().proxy.extraParams = params;
                    MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().sync({
                        callback: function (response) {
                            onProcesarPeticion(response, me.onLimpiarSucursal({limpiar: true}));
                        }});
                }
            });

        } else {
            var fields = form.form.getFields();
            mensajesValidacionForms(fields);
        }
    },

    onUpdate: function () {
        var params = this.getRecordsAdministrador();
        var me = this;
        var form = MODULO_SUCURSAL.down('[name=formCrearEditarSucursal]');
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().proxy.extraParams = params;
            MODULO_SUCURSAL.down('[name=gridLeerSucursals]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiarSucursal({limpiar: true}));
                }});
        } else {
            var fields = form.getFields();
            mensajesValidacionForms(fields);
        }
    },
    onAddAdministrador: function (btn) {
        var comboAdministrador = MODULO_SUCURSAL.down('[name=comboAdministrador]');
        var idAdministrador = comboAdministrador.getValue();
        var nombreAdministrador = comboAdministrador.getRawValue();

        var newRecord = {
            id: idAdministrador,
            nombre: nombreAdministrador,
            habilitado: 1,
            nuevo: true
        };

        MODULO_SUCURSAL.down('[name=gridAdministradoresSucursal]').getStore().insert(0, newRecord);
        comboAdministrador.setValue(null);
    },
    getRecordsAdministrador: function () {
//        MODULO_SUCURSAL.down('[name=comboAdministrador]').getStore().reload();
        var listAdministrador = [];
        var dataAuxAdministrador = MODULO_SUCURSAL.down('[name=gridAdministradoresSucursal]').getStore().data.items;
        for (var i in dataAuxAdministrador) {
            if (dataAuxAdministrador[i].data.nuevo) {
                listAdministrador[i] = {id: dataAuxAdministrador[i].data.id, habilitado: dataAuxAdministrador[i].data.habilitado};
            }
        }
        var dataAuxAdministrador = MODULO_SUCURSAL.down('[name=gridAdministradoresSucursal]').getStore().getUpdatedRecords();
        for (var i in dataAuxAdministrador) {
            listAdministrador[i] = {id: dataAuxAdministrador[i].data.id, habilitado: dataAuxAdministrador[i].data.habilitado};
        }
        return {administradores: JSON.stringify(listAdministrador)};
    }
});

