Ext.define('Ktaxi.view.config_ciudad.v_Config_Ciudad', {
    extend: 'Ext.panel.Panel',
    xtype: 'config_ciudad',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelConfigCiudad',
    controller: 'c_Config_Ciudad',
    bodyBorder: false,
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.config_ciudad.c_Config_Ciudad'
    ],
    listeners: {
        afterrender: 'onViewAdministrador'
    },
    initComponent: function () {
        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                flex: 3,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [{
                        name: 'gridLeerConfigCiudad',
                        xtype: 'grid',
                        bufferedRenderer: false,
                        store: 'config_ciudad.s_Config_Ciudad',
                        defaults: {
                            margin: 0,
                            padding: 0
                        },
                        tbar: [
                            {
                                xtype: 'textfield',
                                flex: 2,
                                tooltip: 'Escribir búsqueda',
                                name: 'paramBusquedaConfigCiudad',
                                emptyText: 'Nombre, valor..',
                                minChars: 0,
                                typeAhead: true,
                                listeners: {
                                    specialkey: 'onChangeSearchConfigCiudad'
                                }
                            },
                            {
                                flex: 1,
                                allowBlank: false,
                                name: 'comboSearchAplicativo',
                                xtype: 'tagfield',
                                emptyText: 'Aplicativo',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                growMax: 10,
                                forceSelected: true,
                                store: Ext.create('Ktaxi.store.combos.s_Aplicativos'),
                                listeners: {
                                    specialkey: 'onChangeSearchConfigCiudad'
                                }
                            }, {
                                flex: 1,
                                allowBlank: false,
                                name: 'comboSearchCiudad',
                                xtype: 'tagfield',
                                emptyText: 'Ciudad',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                growMax: 10,
                                forceSelected: true,
                                store: Ext.create('Ktaxi.store.combos.s_Ciudades'),
                                listeners: {
                                    specialkey: 'onChangeSearchConfigCiudad'
                                }
                            }, {
                                flex: 1,
                                allowBlank: false,
                                name: 'comboSearchLabel',
                                xtype: 'tagfield',
                                emptyText: 'Label',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                growMax: 10,
                                forceSelected: true,
                                store: Ext.create('Ktaxi.store.combos.s_Labels'),
                                listeners: {
                                    specialkey: 'onChangeSearchConfigCiudad'
                                }
                            },
                            {
                                xtype: 'button',
                                iconCls: 'x-fa fa-search',
                                iconAlign: 'right',
                                tooltip: 'Buscar',
                                handler: 'onChangeSearchConfigCiudad'
                            },
                            {
                                xtype: 'button',
                                iconCls: 'x-fa fa-eraser',
                                iconAlign: 'right',
                                tooltip: 'Limpiar',
                                handler: function () {
                                    var panel = Ext.getCmp('panelConfigCiudad');
                                    panel.down('[name=paramBusquedaConfigCiudad]').reset();
                                    panel.down('[name=comboSearchAplicativo]').reset();
                                    panel.down('[name=comboSearchCiudad]').reset();
                                    panel.down('[name=comboSearchLabel]').reset();
                                    var gridLeerConfigCiudad = panel.down('[name=gridLeerConfigCiudad]');
                                    gridLeerConfigCiudad.getView().deselect(gridLeerConfigCiudad.getSelection());
                                    gridLeerConfigCiudad.getStore().clearFilter();
                                    gridLeerConfigCiudad.getStore().load();
                                }
                            },
                            {
                                width: '5%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-refresh',
                                iconAlign: 'right',
                                tooltip: 'Recargar',
                                handler: function () {
                                    Ext.getStore('config_ciudad.s_Config_Ciudad').reload();
                                }
                            }
                        ],
                        columns: [
                            {tooltip: 'Aplicativo', text: 'Aplicativo', dataIndex: 'aplicativo', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Ciudad', text: 'Ciudad', dataIndex: 'ciudad', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Label', text: 'Label', dataIndex: 'label', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Nombre', text: 'Nombre', dataIndex: 'nombre', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Valor', text: 'Valor', dataIndex: 'valor', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Habilitado', text: "Habilitado", flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstado},
                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                        ],
                        columnLines: true,
                        height: 210,
                        split: true,
                        region: 'north',
                        listeners: {
                            select: 'onSelectChangeGridConfigCiudad',
                            deselect: function () {
                                var panel = Ext.getCmp('panelConfigCiudad');
                                panel.down('[name=btnEditar]').disable();
                                panel.down('[name=btnCrear]').enable();
                            },
                            beforeitemclick: function (thisObj, record, item, index, e, eOpts) {
                                var panel = Ext.getCmp('panelConfigCiudad');
                                panel.down('[name=btnEditar]').enable();
                                panel.down('[name=btnCrear]').disable();
                            },
                            rowdblclick: showAuditoria
                        },
                        viewConfig: {
                            emptyText: '<center>No existen resultados.</center>'
                        }
                    }]
            },
            {
                name: 'panelCrearEditarConfigCiudad',
                region: 'east',
                xtype: 'form',
                layout: 'fit',
                flex: 2,
                collapsible: false,
                items: [{
                        cls: 'panelFormulario',
                        xtype: 'panel',
                        title: 'Ktaxi ciudad',
                        defaultType: 'textfield',
                        defaults: {
                            width: '100%',
                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
                            blankText: INFOMESSAGEBLANKTEXT,
                            allowBlank: false,
                            defaultType: 'textfield',
                            labelWidth: 85,
                            defaults: {
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                allowBlank: false,
                                labelWidth: 85,
                                width: '50%',
                                defaults: {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    allowOnlyWhitespace: false,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowBlank: false,
                                    labelWidth: 85,
                                    width: '50%'
                                }
                            }
                        },
                        items: [
                            {
                                allowBlank: false,
                                name: 'idAplicativo',
                                xtype: 'combobox',
                                emptyText: 'Seleccione',
                                fieldLabel: 'Aplicativo',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                store: Ext.create('Ktaxi.store.combos.s_Aplicativos')
                            }, {
                                allowBlank: false,
                                name: 'idCiudad',
                                xtype: 'combobox',
                                emptyText: 'Seleccione',
                                fieldLabel: 'Ciudad',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                store: Ext.create('Ktaxi.store.combos.s_Ciudades')
                            }, {
                                allowBlank: false,
                                name: 'idLabel',
                                xtype: 'combobox',
                                emptyText: 'Seleccione',
                                fieldLabel: 'Label',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                store: Ext.create('Ktaxi.store.combos.s_Labels')
                            },
                            {
                                fieldLabel: 'Nombre',
                                name: 'nombre',
                                emptyText: 'Nombre',
                                maxLength: '100',
                                minLength: '2',
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                            },
                            {
                                fieldLabel: 'Valor',
                                name: 'valor',
                                emptyText: 'Valor',
                                maxLength: '350',
                                minLength: '1',
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                            },
                            {
                                boxLabel: 'Habilitado',
                                xtype: 'checkbox',
                                allowBlank: true,
                                allowOnlyWhitespace: true,
                                uncheckedValue: 0,
                                inputValue: 1,
                                name: 'habilitado',
                                labelAlign: 'right'
                            }
                        ]
                    }],
                dockedItems: [{
                        ui: 'footer',
                        xtype: 'toolbar',
                        dock: 'bottom',
                        defaults: {
                            width: '25%'
                        },
                        items: [
                            {
                                text: 'Limpiar',
                                tooltip: 'Limpiar',
                                disabled: false,
                                handler: function () {
                                    limpiarFormularioConfigCiudad();
                                }
                            },
                            '->',
                            {
                                text: 'Editar',
                                tooltip: 'Actualizar',
                                disabled: true,
                                name: 'btnEditar',
                                handler: 'onUpdateConfigCiudad'
                            }, {
                                text: 'Crear',
                                tooltip: 'Crear Administradores',
                                disabled: true, name: 'btnCrear',
                                handler: 'onCreateConfigCiudad'
                            }]
                    }]
            },
        ];
        this.callParent(arguments);
    }
});
