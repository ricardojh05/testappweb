Ext.define('Ktaxi.view.config_ciudad.c_Config_Ciudad', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Config_Ciudad',
    onViewAdministrador: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
        Ext.getStore('config_ciudad.s_Config_Ciudad').load();
    },
    onChangeSearchConfigCiudad: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var panel = Ext.getCmp('panelConfigCiudad');
            var paramBusqueda = panel.down('[name=paramBusquedaConfigCiudad]').getValue();
            var tagAplicativo = panel.down('[name=comboSearchAplicativo]').getValue();
            var tagCiudad = panel.down('[name=comboSearchCiudad]').getValue();
            var tagLabel = panel.down('[name=comboSearchLabel]').getValue();
            Ext.getStore('config_ciudad.s_Config_Ciudad').load({
                params: {
                    param: paramBusqueda,
                    aplicativos: tagAplicativo.toString(),
                    ciudades: tagCiudad.toString(),
                    labels: tagLabel.toString()
                },
                callback: function (records) {
                    if (records.length <= 0) {
                        Ext.getStore('config_ciudad.s_Config_Ciudad').removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridConfigCiudad: function (thisObj, selected, eOpts) {
        if (selected) {
            var formConfigCiudad = Ext.getCmp('panelConfigCiudad').down('[name=panelCrearEditarConfigCiudad]');
            var comboFormAplicativo = formConfigCiudad.down('[name=idAplicativo]');
            if (!isInStore(comboFormAplicativo.getStore(), selected.data.idAplicativo, 'id')) {
                comboFormAplicativo.getStore().load({
                    params: {
                        param: selected.data.idAplicativo
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormAplicativo.setValue(records);
                        }
                    }
                });
            }
            var comboFormCiudad = formConfigCiudad.down('[name=idCiudad]');
            if (!isInStore(comboFormCiudad.getStore(), selected.data.idCiudad, 'id')) {
                comboFormCiudad.getStore().load({
                    params: {
                        param: selected.data.idCiudad
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormCiudad.setValue(records);
                        }
                    }
                });
            }
            var comboFormLabel = formConfigCiudad.down('[name=idLabel]');
            if (!isInStore(comboFormLabel.getStore(), selected.data.idLabel, 'id')) {
                comboFormLabel.getStore().load({
                    params: {
                        param: selected.data.idLabel
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormLabel.setValue(records);
                        }
                    }
                });
            }
            formConfigCiudad.loadRecord(selected);
        }
    },
    onCreateConfigCiudad: function () {
        var form = Ext.getCmp('panelConfigCiudad').down('[name=panelCrearEditarConfigCiudad]');
        if (form.isValid()) {
            var record = form.getValues();
            Ext.getStore('config_ciudad.s_Config_Ciudad').insert(0, record);
            Ext.getStore('config_ciudad.s_Config_Ciudad').sync();
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    },
    onUpdateConfigCiudad: function () {
        var form = Ext.getCmp('panelConfigCiudad').down('[name=panelCrearEditarConfigCiudad]');
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            Ext.getStore('config_ciudad.s_Config_Ciudad').sync();
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    }
});
function limpiarFormularioConfigCiudad() {
    var panel = Ext.getCmp('panelConfigCiudad');
    panel.down('[name=panelCrearEditarConfigCiudad]').getForm().reset();
    var gridLeerConfigCiudad = panel.down('[name=gridLeerConfigCiudad]');
    gridLeerConfigCiudad.getView().deselect(gridLeerConfigCiudad.getSelection());
    gridLeerConfigCiudad.getStore().reload();
}