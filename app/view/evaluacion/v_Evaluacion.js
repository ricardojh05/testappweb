/* global MAXIMUMMESSAGUEREQURID, INFOMESSAGEREQUERID, MINIMUMMESSAGUEREQUERID, INFOMESSAGEBLANKTEXT, showAuditoria, HEIGT_VIEWS, Ext, EMPTY_CARA, showTipConten, showTipContenID, formatEstadoRegistro */
Ext.define('Ktaxi.view.evaluacion.v_Evaluacion', {
    extend: 'Ext.panel.Panel',
    xtype: 'evaluacion',
    controller: 'c_Evaluacion',
    height: HEIGT_VIEWS,
    layout: 'border',
    store: 'evaluacion.s_Evaluacion',
    requires: ['Ktaxi.view.evaluacion.c_Evaluacion'],
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    initComponent: function () {
        var STORE_ALUMNO = Ext.create('Ktaxi.store.evaluacion.s_Evaluacion');
        this.items = [{
                xtype: 'panel',
                region: 'center',
                width: '65%',
                collapseMode: 'mini',
                margin: 5,
                header: false,
                collapsible: true,
                collapsed: false,
                layout: 'fit',
                items: [{
                    xtype: 'grid',
                    name: 'grid',
                    height: HEIGT_VIEWS - 10,
                    plugins: [{
                        ptype: 'gridfilters'
                    }],
                    bufferedRenderer: false,
                    store: STORE_ALUMNO,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        },
                        loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>' + EMPTY_CARA
                    },
                    tbar: [{
                            xtype: 'textfield',
                            name: 'txtParam',
                            width: '70%',
                            emptyText: 'Nombre...',
                            listeners: {
                                specialkey: 'onBuscar'
                            }
                        },
                        /* {
                            xtype: 'combobox',
                            name: 'cmbxTipo',
                            emptyText: 'Usuario...',
                            width: '28%',
                            valueField: 'id',
                            queryParam: 'filtro',
                            queryMode: 'remote',
                            displayField: 'texto',
                            // store: Ext.create('Ktaxi.store.stores.s_Usuario_Acceso_Api')
                        }, */
                        /* {
                            xtype: 'combobox',
                            name: 'cmbxTipoEstado',
                            emptyText: 'Estado...',
                            width: '20%',
                            queryMode: 'remote',
                            displayField: 'estado',
                            queryParam: 'filtro',
                            valueField: 'id',
                            //  store: Ext.create('Ktaxi.store.stores.s_Estado_Evaluacion')
                        }, */
                        //                            {
                        //                                xtype: 'combobox',
                        //                                label: 'Choose State',
                        //                                queryMode: 'local',
                        //                                width: '28%',
                        //                                displayField: 'name',
                        //                                valueField: 'abbr',
                        //
                        //                                store: [
                        //                                    {abbr: 'AL', name: 'Alabama'},
                        //                                    {abbr: 'AK', name: 'Alaska'},
                        //                                    {abbr: 'AZ', name: 'Arizona'}
                        //                                ]
                        //                            },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-refresh',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [{
                        ftype: 'grouping',
                        groupHeaderTpl: '{name}',
                        hideGroupedHeader: true,
                        enableGroupingMenu: true
                    }],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {
                            header: '#',
                            width: 30,
                            align: 'center'
                        }),
                        {
                            dataIndex: 'nombre',
                            text: 'Nombre',
                            tooltip: "Nombre",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'fecha',
                            text: 'Fecha',
                            tooltip: "Fecha",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: formatFecha
                        },
                        {
                            dataIndex: 'opcion',
                            text: 'Opciones',
                            tooltip: "Opcioines",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'numero',
                            text: 'Cant Preguntas',
                            tooltip: "Cantidad de Preguntas",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'respuesta',
                            text: 'Respuestas',
                            tooltip: "Respuestas",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        //{dataIndex: 'mensaje', text: 'Mensaje', tooltip: "Mensaje", filter: true, flex: 1, sortable: true, renderer: showTipConten},
                        //{dataIndex: 'EvaluacionCorreo', text: 'Evaluacion', tooltip: "Evaluacion del Correo", filter: true, flex: 1, sortable: true, renderer: showTipConten},
                        //{flex: 1, tooltip: 'Fecha Registro', xtype: 'datecolumn', header: "Fecha", dataIndex: 'fechaRegistro', sortable: true, format: 'Y/m/d'},
                        //{flex: 1, tooltip: 'Hora', xtype: 'datecolumn', header: "Hora", dataIndex: 'fechaRegistro', sortable: true, format: 'H:i:s'},
                        //                            {text: 'Estado', menuDisabled: true, sortable: false, xtype: 'actioncolumn', width: 40, minWidth: 40, items: [
                        ////                                  
                        //                                    {
                        //                                        xtype: 'button',
                        //                                        tooltip: 'Confimar',
                        //                                        iconCls: 'x-fa fa-check-square',
                        //                                        text: 'Confirmar',
                        //                                        name: 'confimar',
                        //                                        value: 'Confimar',
                        //                                        handler: 'showEvaluacionConfirmar',
                        //                                        //getClass: 'onGetClass'
                        //                                    }
                        //                                ]
                        //                            },
                        {
                            text: 'Estado',
                            menuDisabled: true,
                            sortable: false,
                            xtype: 'actioncolumn',
                            width: 40,
                            minWidth: 40,
                            items: [{
                                xtype: 'button',
                                //style: 'color="red!important"',
                                name: 'btnFinalizarJornada',
                                tooltip: 'Confirmar o Validar',
                                iconCls: 'x-fa fa-ban',
                                isDisabled: 'onIsDisabledBtnFinalizarJornada',
                                handler: 'onVenatna',
                                getClass: 'onGetClass'
                            }]
                        },
                        {
                            dataIndex: 'id',
                            text: "ID",
                            tooltip: "ID",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipContenID,
                            hidden: true
                        }
                    ],
                    listeners: {
                        select: 'onSelect',
                        //rowdblclick: showAuditoriaEvaluacion,

                    },
                    onButtonWidgetClick: function (btn) {
                        var rec = btn.getViewModel().get('record');
                        Ext.Msg.alert("Button clicked", "Hey! " + rec.get('name'));
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        //store: STORE_ALUMNO,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [{
                            xtype: 'button',
                            text: 'Exportar',
                            iconCls: 'x-fa fa-download',
                            handler: function (btn) {
                                onExportar(btn, "Evaluacion", this.up('grid'));
                            }
                        }],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }]
            },
            {
                xtype: 'panel',
                region: 'east',
                padding: 5,
                width: '35%',
                collapseMode: 'mini',
                anchor: '100% 100%',
                layout: 'anchor',
                cls: 'panelCrearEditar',
                autoScroll: true,
                collapsible: true,
                collapsed: false,
                header: false,
                headerAsText: false,
                items: [{
                        xtype: 'form',
                        title: 'Evaluacion',
                        name: 'form',
                        extend: 'Ext.Widget',
                        cls: 'quick-graph-panel shadow panelComplete',
                        ui: 'light',
                        padding: 5,
                        defaults: {
                            width: '100%',
                            margin: '0 0 5 0',
                            defaultType: 'textfield',
                            minLengthText: MINIMUMMESSAGUEREQUERID,
                            maxLengthText: MAXIMUMMESSAGUEREQURID,
                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                            blankText: INFOMESSAGEBLANKTEXT,
                            labelWidth: 90,
                            allowOnlyWhitespace: false,
                            allowBlank: false
                        },
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: '<b>Nombre</b>',
                                name: 'nombre',
                                emptyText: 'Nombre',
                                maxLength: 125,
                                minLength: 7,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            /* {
                                xtype: 'combobox',
                                name: 'idZona',
                                emptyText: 'Seleccione una zona...',
                                fieldLabel: '<b>Zona</b>',
                                displayField: 'text',
                                value: 1,
                                minChars: 2,
                                allowBlank: false,
                                valueField: 'id',
                                queryParam: 'filtro', //tomar en cuenta la variable para hacer filtros  
                                queryMode: 'remote',
                                displayField: 'texto',
                                //store: Ext.create('Simert.store.stores.s_Zona')
                            }, */
                            
                            {
                                xtype: 'numberfield',
                                fieldLabel: '<b>Preguntas</b>',
                                name: 'numero',
                                emptyText: 'Preguntas',
                                maxLength: 20,
                                minLength: 1,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'numberfield',
                                fieldLabel: '<b>Opciones</b>',
                                name: 'opcion',
                                emptyText: 'Opciones',
                                maxLength: 20,
                                minLength: 1,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'datefield',
                                name: 'fecha',
                                fieldLabel: '<b>Fecha</b>',
                                format: 'Y-m-d',
                                //maxValue: new Date(),
                                value: new Date(),
                                allowBlack: false,
                                allowOnlyWhitespace: false
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<b>Respuestas</b>',
                                name: 'respuesta',
                                emptyText: 'Respuestas',
                                maxLength: 10,
                                minLength: 7,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                listeners: {
                                    change: function keyup(thisObj, e, eOpts) {
                                        thisObj.setValue(e.toUpperCase());
                                    }
                                }
                            },
                            /* {
                                xtype: 'button',
                                tooltip: 'expandir',
                                text: 'Respuestas',
                                iconCls: 'fa fa-pencil-square-o',
                                name: 'respuestas',
                                ///fieldLabel: '<b>Respuestas</b>',
                                handler: 'onExpandirGrid'
                            }, */
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                defaultType: 'combobox',
                                width: '100%',
                                defaults: {
                                    margin: 1
                                },
                                items: [
                                    {
                                        fieldLabel: '<b>Clase<b/>',
                                        xtype: 'combobox',
                                        flex: 10,
                                        name: 'cmbxClase',
                                        emptyText: 'Clase',
                                        displayField: 'nombre',
                                        valueField: 'id',
                                        queryParam: 'param',
                                        queryMode: 'remote',
                                        store: Ext.create('Ktaxi.store.combos.s_Clase'),
                                        minChars: 3
                                    },
                                    {xtype: 'button', name: 'agregarVehiAdmin', flex: 1, iconCls: 'x-fa fa-plus', handler: 'onAddClases'}
                                ]
                            },
                            {
                                xtype: 'grid',
                                plugins: [{ptype: 'gridfilters'}],
                                store: Ext.create('Ext.data.Store', {fields: [{name: 'id'}, {name: 'nombre'}]}),
                                cls: 'gridAuxVehiAdmin',
                                height: 65,
                                name: 'gridClase',
                                split: true,
                                autoScroll: true,
                                bufferedRenderer: false,
                                viewConfig: {
                                    deferEmptyText: false,
                                    enableTextSelection: true,
                                    preserveScrollOnRefresh: true,
                                    listeners: {
                                        loadingText: 'Cargando...'
                                    },
                                    loadMask: true,
                                    emptyText: '<center><b>No existen resultados</b></center>'
                                },
                                columns: [
                                    {filter: true, width: '70%', align: 'left', dataIndex: 'nombre', sortable: true},
                                    //{filter: true, width: '40%', dataIndex: 'materia', sortable: true},
                                    //{filter: true, width: '35%', dataIndex: 'dateChange', sortable: true, renderer: formatEstadoFecha},
                                    {
                                        xtype: 'actioncolumn',
                                        width: '10%',
                                        items: [{getClass: 'onGetClassClases', handler: 'onHandlerGridClases'}]
                                    }
                                ],
                                listeners: {
                                    rowdblclick: function (grid, record) {
                                        if (!record.data.nuevo) {
                                            showAuditoria(grid, record, 'gridAux');
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    
                ],
                dockedItems: [{
                    ui: 'footer',
                    xtype: 'toolbar',
                    dock: 'bottom',
                    defaults: {
                        width: '25%',
                        height: 30
                    },
                    items: [{
                            text: 'Limpiar',
                            tooltip: 'Limpiar',
                            disabled: false,
                            limpiar: true,
                            handler: 'onLimpiar'
                        },
                        '->',
                        {
                            text: 'Crear',
                            tooltip: 'Crear',
                            name: 'btnCrear',
                            handler: 'onCrear',
                            disabled: false
                        },
                        {
                            text: 'Editar',
                            tooltip: 'Editar',
                            name: 'btnEditar',
                            handler: 'onEditar',
                            disabled: false,
                            hidden: true
                        }
                    ]
                }]
            }
        ];
        this.callParent(arguments);
    }
});