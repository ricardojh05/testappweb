var editingPromociones;
var txt = 'Primero solicitar que el cliente este registrado en el APP con la cédula de identidad,' +
        'luego deberá buscar lo con el número exacto de cédula, colocando este en la caja de texto y ' +
        'presionando la boton buscar';
var fecha = new Date(), mes = fecha.getMonth() + 1, anio = fecha.getFullYear();
var cliente = '', idC, mesAterior = fecha.getMonth(), anioActual = fecha.getFullYear(), anioConf = fecha.getFullYear(), saldo_dispo_mes_act = 0;
var arrayMeses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
valueFecha = fecha.getFullYear() + '-' + arrayMeses[fecha.getMonth() + 1];
var colorBotones = '#D8D7D7';
var colorBtn_claro = '#9FD0FF';
var ID_MAPA_VOUCHER_SUCURSAL = 'mapSucursal';
var idSucursal = 1;
var idAplicativo = 1;

var panelMeses = Ext.create('Ext.panel.Panel', {
    width: 196,
    layout: {
        type: 'hbox',
        pack: 'start',
        align: 'stretch'
    },
    items: [{
            xtype: 'button',
            height: 30,
            iconCls: 'fa fa-chevron-left',
            tooltip: 'Mes anterior',
            style: {
                background: colorBotones,
                borderColor: '#ffffff',
                borderStyle: 'solid'
            },
            handler: function () {
                mes = mes - 1;
                if (mes <= 0) {
                    anio -= 1;
                    mes = 12;
                }
                valueFecha = anio + '-' + arrayMeses[mes];
                Ext.getCmp('fechaConsultaSucursal').setValue(valueFecha);
                var params = {anio: anio, mes: mes, idSucursal: idSucursal, idAplicativo: idAplicativo};
                if (mesAterior <= 0) {
                    mesAterior = 12;
                    anioConf -= 1;
                }
                if (mes === mesAterior && anioConf === anio) {
                    selModel.setLocked(false); //Permite seleccionar
                    gridUsuariosSucursal.down('[name=btn_config_mes]').setVisible(true);
                } else {
                    selModel.setLocked(true); //NO PERMITE MARCAR
                    gridUsuariosSucursal.down('[name=btn_config_mes]').setVisible(false);
                }
                mostrarDatosDepartamento(params);
            }
        },
        {
            xtype: 'textfield',
            name: 'fechaConsultaSucursal',
            id: 'fechaConsultaSucursal',
            fieldLabel: '',
            readOnly: true,
            value: valueFecha
        },
        {
            xtype: 'button',
            height: 30,
            iconCls: 'fa fa-chevron-right',
            tooltip: 'Siguiente mes',
            style: {
                background: colorBotones,
                borderColor: '#ffffff',
                borderStyle: 'solid'
            },
            handler: function () {
                mes = mes + 1;
                if (mes > 12) {
                    anio += 1;
                    mes = 1;
                }
                valueFecha = anio + '-' + arrayMeses[mes];
                Ext.getCmp('fechaConsultaSucursal').setValue(valueFecha);
                var params = {anio: anio, mes: mes, idSucursal: idSucursal, idAplicativo: idAplicativo};
                if (mesAterior > 12) {
                    mesAterior = 12;
                    anioConf += 1;
                }
                if (mes === mesAterior && anioConf === anio) {
                    selModel.setLocked(false); //Permite seleccionar
                    gridUsuariosSucursal.down('[name=btn_config_mes]').setVisible(true);
                } else {
                    selModel.setLocked(true); //NO PERMITE MARCAR
                    gridUsuariosSucursal.down('[name=btn_config_mes]').setVisible(false);
                }
                mostrarDatosDepartamento(params);
            }
        }]
});
Ext.define('Ktaxi.view.sucursalVoucher.v_Sucursal', {
    extend: 'Ext.panel.Panel',
    xtype: 'sucursalVoucher',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelSucursal',
    controller: 'c_SucursalVoucher',
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.sucursalVoucher.c_Sucursal',
        'Ext.grid.plugin.CellEditing'
    ],
    listeners: {
        afterrender: 'onViewSucursalVoucher'
    },
    initComponent: function () {
        var STORE_DEPARTAMENTOS = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Departamento');
        var STORE_READ_CONSUMO = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Read_Consumo');

        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [
                    {
                        xtype: 'form',
                        layout: 'hbox',
                        items: [
                            {
                                width: '35%',
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        name: 'panelCrearEditarSucursal',
                                        region: 'east',
                                        xtype: 'form',
                                        layout: 'fit',
                                        collapsible: false,
                                        items: [{
                                                cls: 'panelFormulario',
                                                xtype: 'panel',
                                                title: 'Sucursals',
                                                defaultType: 'textfield',
                                                defaults: {
                                                    width: '100%',
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    allowOnlyWhitespace: false,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    defaultType: 'textfield',
                                                    labelWidth: 85,
                                                    defaults: {
                                                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                        allowOnlyWhitespace: false,
                                                        blankText: INFOMESSAGEBLANKTEXT,
                                                        allowBlank: false,
                                                        labelWidth: 85,
                                                        width: '50%',
                                                        defaults: {
                                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                            allowOnlyWhitespace: false,
                                                            blankText: INFOMESSAGEBLANKTEXT,
                                                            allowBlank: false,
                                                            labelWidth: 85,
                                                            width: '50%'
                                                        }
                                                    }
                                                },
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
//                                                                margin: '0 0 0 5',
                                                                name: 'idSucursal',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Sucursal',
                                                                displayField: 'text',
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_SucursalVoucher')
                                                            },
                                                            {
                                                                xtype: 'button',
                                                                iconCls: 'x-fa fa-search',
                                                                iconAlign: 'right',
                                                                tooltip: 'Buscar',
                                                                handler: 'onBuscarDepartamentoParams'
                                                            }
                                                        ]
                                                    },
//                                                    
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 0 0',
                                                        items: [
                                                            {
                                                                xtype: 'label',
                                                                text: 'Crédito',
                                                                style: 'font-size: 12px; font-weight: bold;',
                                                                name: 'creditoCompania'
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 0 0',
                                                        items: [

                                                            {
                                                                xtype: 'label',
                                                                text: 'Asignado',
                                                                style: 'font-size: 12px; font-weight: bold;',
                                                                name: 'creditoConsumido'
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 0 0',
                                                        items: [
                                                            {
                                                                xtype: 'label',
                                                                text: 'Consumido',
                                                                style: 'font-size: 12px; font-weight: bold;',
                                                                name: 'creditoDisponible'
                                                            },
                                                        ]
                                                    },
                                                    {name: 'actualizar', hidden: true, allowBlank: true, allowOnlyWhitespace: true}
                                                ]
                                            }]
                                    }, {
                                        flex: 1,
                                        name: 'gridLeerSucursal',
                                        xtype: 'grid',
                                        columnLines: true,
                                        bufferedRenderer: false,
                                        plugins: [{ptype: 'gridfilters'}],
                                        store: STORE_DEPARTAMENTOS,
//                                        defaults: {
//                                            margin: 0,
//                                            padding: 0
//                                        },
                                        tbar: [
                                            {
                                                xtype: 'textfield',
                                                flex: 2,
                                                tooltip: 'Escribir búsqueda',
                                                name: 'txtParam',
                                                emptyText: 'Nombre, Apellido..',
                                                minChars: 0,
                                                typeAhead: true,
                                                listeners: {
                                                    specialkey: 'onBuscarGridDepartamento'
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-search',
                                                iconAlign: 'right',
                                                tooltip: 'Buscar',
                                                handler: 'onBuscarGridDepartamento'
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-eraser',
                                                iconAlign: 'right',
                                                tooltip: 'Limpiar',
                                                handler: 'onLimpiarGridDepartamento'
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
                                                tooltip: 'Recargar',
                                                handler: 'onRecargarGridDepartamento',
                                            }
                                        ],
                                        columns: [
                                            {tooltip: 'Departamento', text: 'Departamento', dataIndex: 'departamento', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {header: '<b>Credito</b>', xtype: 'numbercolumn', flex: 1, dataIndex: 'creditoMes', align: 'center', filter: true},
                                            {header: '<b>Consumo</b>', xtype: 'numbercolumn', flex: 1, dataIndex: 'consumoMes', align: 'center', filter: true},
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipContenID}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            select: 'onSelectGridDepartamento',
                                            rowdblclick: showAuditoria
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>'
                                        },
                                        bbar: Ext.create('Ext.PagingToolbar', {
                                            store: STORE_DEPARTAMENTOS,
                                            displayInfo: true,
                                            name: 'paginacionGrid',
                                            emptyMsg: "Sin datos que mostrar.",
                                            displayMsg: 'Visualizando {0} - {1} de {2} registros',
                                            beforePageText: 'Página',
                                            afterPageText: 'de {0}',
                                            firstText: 'Primera página',
                                            prevText: 'Página anterior',
                                            nextText: 'Siguiente página',
                                            lastText: 'Última página',
                                            refreshText: 'Actualizar',
                                            inputItemWidth: 35,
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    text: 'Exportar',
                                                    iconCls: 'x-fa fa-download',
                                                    handler: function (btn) {
                                                        onExportar(btn, "Sanciones", this.up('grid'));
                                                    }
                                                }
                                            ],
                                            listeners: {
                                                afterrender: function () {
                                                    this.child('#refresh').hide();
                                                }
                                            }
                                        })
                                    }
                                ]
                            }, {
                                flex: 1,
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        xtype: 'panel',
                                        title: 'Datos Consumo',
                                        name: 'datosConsumo',
                                        collapsible: false,
                                        collapsed: false,
                                        items: [
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Departamento:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'clienteAdmCosumo'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Crédito:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'creditoAdmCosumo'
                                                    },
                                                ]
                                            },
                                            {
                                                xtype: 'container',
                                                layout: 'hbox',
                                                margin: '0 0 0 0',
                                                items: [
                                                    {
                                                        xtype: 'label',
                                                        text: 'Consumo:',
                                                        style: 'font-size: 12px; font-weight: bold;',
                                                        name: 'consumoAdmCosumo'
                                                    },
                                                ]
                                            },
                                        ]

                                    },
                                    {
                                        flex: 1,
                                        title: 'Administración de consumo',
                                        name: 'administracionConsumo',
                                        xtype: 'grid',
                                        bufferedRenderer: false,
                                        store: STORE_READ_CONSUMO,
                                        tbar: [
                                            {
                                                text: 'Asignar Crédito',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-plus-square',
                                                iconAlign: 'right',
                                                tooltip: 'Nuevo registro',
                                                handler: 'onShowVentanaAsignarCredito',
                                                name: 'asignarCredito'
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
//                                                aling:'right',
                                                tooltip: 'Recargar',
                                                name: 'recargarGridConsumo',
                                                handler: 'onRecargarGridConsumo'
                                            },
                                            {
                                                xtype: 'container',
                                                name: 'errorFechas',
                                                html: ''
                                            },
                                        ], features: [
                                            {
                                                ftype: 'grouping',
                                                groupHeaderTpl: '{name}',
                                                hideGroupedHeader: true,
                                                enableGroupingMenu: true
                                            }],
                                        columns: [
                                            Ext.create('Ext.grid.RowNumberer', {header: 'Nº', width: 60, align: 'center'}),
                                
                                            {header: '<b>Nombre</b>', flex: 1, dataIndex: 'nombres', filter: true},
                                            {header: '<b>Apellido</b>', flex: 1, dataIndex: 'apellidos', filter: true},
                                            {header: '<b>Credito</b>', flex: 1, dataIndex: 'credito', xtype: 'numbercolumn', align: 'center', filter: true},
                                            {header: '<b>Consumo</b>', flex: 1, dataIndex: 'consumo', xtype: 'numbercolumn', align: 'center', filter: true},
                                            
                                            
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            rowdblclick: showAuditoria,
                                            validateedit: function (rowEditing, context, eOpts) {
                                                if (rowEditing.editor.form.isValid()) {
                                                    var panel = Ext.getCmp('panelSucursal');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                }
                                            },
                                            cancelEdit: function (rowEditing, context) {
                                                if (!rowEditing.editor.form.isValid() && context.record.data.nuevo) {
                                                    var panel = Ext.getCmp('panelSucursal');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                    Ext.getStore('sucursal.s_Sucursal_Promocion').remove(context.record);
                                                    onChangeDates('gridLeerSucursalPromocion');
                                                }
                                            }
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>',
                                            getRowClass: function (record) {
                                                if (record.data.error) {
                                                    return 'deleteRowGrid';
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        ], dockedItems: [{
                                ui: 'footer',
                                xtype: 'toolbar',
                                dock: 'bottom',
                                defaults: {
                                    width: '10%'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        height: 30,
                                        iconCls: 'fa fa-chevron-left',
                                        tooltip: 'Mes anterior',
                                        style: {
                                            background: colorBotones,
                                            borderColor: '#ffffff',
                                            borderStyle: 'solid'
                                        },
                                        handler: function () {
                                            mes = mes - 1;
                                            if (mes <= 0) {
                                                anio -= 1;
                                                mes = 12;
                                            }
                                            valueFecha = anio + '-' + arrayMeses[mes];
                                            Ext.getCmp('fechaConsultaSucursal').setValue(valueFecha);
                                            var params = {anio: anio, mes: mes, idSucursal: MODULO_VOUCHER_SUCURSAL.down('[name=idSucursal]').getValue()};
                                            if (mesAterior <= 0) {
                                                mesAterior = 12;
                                                anioConf -= 1;
                                            }
                                            if (mes === mesAterior && anioConf === anio) {
                                                selModel.setLocked(false); //Permite seleccionar
                                            } else {
                                                selModel.setLocked(true); //NO PERMITE MARCAR
//                                                gridUsuariosSucursal.down('[name=btn_config_mes]').setVisible(false);
                                            }
                                            recargarStoreSucursal(params);
                                            MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getView().deselect(MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getSelection());
                                        }
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'fechaConsultaSucursal',
                                        id: 'fechaConsultaSucursal',
                                        fieldLabel: '',
                                        readOnly: true,
                                        value: valueFecha
                                    }, {
                                        xtype: 'button',
                                        height: 30,
                                        iconCls: 'fa fa-chevron-right',
                                        tooltip: 'Siguiente mes',
                                        style: {
                                            background: colorBotones,
                                            borderColor: '#ffffff',
                                            borderStyle: 'solid'
                                        },
                                        handler: function () {
                                            mes = mes + 1;
                                            if (mes > 12) {
                                                anio += 1;
                                                mes = 1;
                                            }
                                            valueFecha = anio + '-' + arrayMeses[mes];
                                            Ext.getCmp('fechaConsultaSucursal').setValue(valueFecha);
                                            var params = {anio: anio, mes: mes, idSucursal: MODULO_VOUCHER_SUCURSAL.down('[name=idSucursal]').getValue()};
                                            if (mesAterior > 12) {
                                                mesAterior = 12;
                                                anioConf += 1;
                                            }
                                            if (mes === mesAterior && anioConf === anio) {
                                                selModel.setLocked(false); //Permite seleccionar
                                            } else {
                                                selModel.setLocked(true); //NO PERMITE MARCAR
                                            }
                                            recargarStoreSucursal(params);
                                            MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getView().deselect(MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getSelection());
                                        }
                                    }
                                ]
                            }]
                    }
                ]
            },
        ];
        this.callParent(arguments);
    }
});
var selModel = Ext.create('Ext.selection.CheckboxModel', {
    checkOnly: false,
    listeners: {
        select: function (model, record, index) {
        },
        deselect: function (model, record, index) {
        }
    }
});