var MODULO_VOUCHER_SUCURSAL;
var fecha = new Date(),
    mes = fecha.getMonth() + 1,
    anio = fecha.getFullYear();
var idAplicativo;
var cliente, idApp, idAppBus, app, cedula, celular, idE, idCC, idDepar, idDeparClien, idSucuDepar, idSucursal, departamento;
var idSucursal = 1;
var idAplicativo = 1;
var storeCreditoCompaniaSucursal;
var storeIsSucursalTipoEmpresa;
var nombreAplicativo;
var storeReadConsumo;
var storeBuscarDepartamentos;
var storeTotalesConsumo;
var storeDepartamento;
var recordDepartamento;
var arrayIdMesesSucur = [];
var arrayNombreMesesSucur = [];
var anios = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: '2019',
            "text": "2019"
        },
        {
            id: '2020',
            "text": "2020"
        },
        {
            id: '2021',
            "text": "2021"
        },
        {
            id: '2022',
            "text": "2022"
        },
        {
            id: '2023',
            "text": "2023"
        },
        {
            id: '2024',
            "text": "2024"
        },
        {
            id: '2025',
            "text": "2025"
        },
        {
            id: '2026',
            "text": "2026"
        },
        {
            id: '2027',
            "text": "2027"
        },
        {
            id: '2028',
            "text": "2028"
        }
    ]
});
var meses = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 1,
            "text": "Enero"
        },
        {
            id: 2,
            "text": "Febrero"
        },
        {
            id: 3,
            "text": "Marzo"
        },
        {
            id: 4,
            "text": "Abril"
        },
        {
            id: 5,
            "text": "Mayo"
        },
        {
            id: 6,
            "text": "Junio"
        },
        {
            id: 7,
            "text": "Julio"
        },
        {
            id: 8,
            "text": "Agosto"
        },
        {
            id: 9,
            "text": "Septiembre"
        },
        {
            id: 10,
            "text": "Octubre"
        },
        {
            id: 11,
            "text": "Noviembre"
        },
        {
            id: 12,
            "text": "Diciembre"
        },
    ]
});
var mesesHasta = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 1,
            "text": "Enero"
        },
        {
            id: 2,
            "text": "Febrero"
        },
        {
            id: 3,
            "text": "Marzo"
        },
        {
            id: 4,
            "text": "Abril"
        },
        {
            id: 5,
            "text": "Mayo"
        },
        {
            id: 6,
            "text": "Junio"
        },
        {
            id: 7,
            "text": "Julio"
        },
        {
            id: 8,
            "text": "Agosto"
        },
        {
            id: 9,
            "text": "Septiembre"
        },
        {
            id: 10,
            "text": "Octubre"
        },
        {
            id: 11,
            "text": "Noviembre"
        },
        {
            id: 12,
            "text": "Diciembre"
        },
    ]
});
Ext.define('Ktaxi.view.sucursalVoucher.c_Sucursal', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_SucursalVoucher',
    onViewSucursalVoucher: function (panelLoad) {
        MODULO_VOUCHER_SUCURSAL = panelLoad;
        validarPermisosGeneral(panelLoad);
        storeCreditoCompaniaSucursal = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Credito_Compania');
        storeDepartamento = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Departamento');
        console.log('metodo view sucursaal voucher');
        Ext.Ajax.request({
            async: true,
            url: 'php/SucursalVoucher/getSucursal.php',
            callback: function (callback, e, response) {

                var res = JSON.parse(response.request.result.responseText);
                if (res.success) {
                    console.log(res);
                    MODULO_VOUCHER_SUCURSAL.down('[name=idSucursal]').getStore().load({
                        params: {
                            idSucursal: res.idSucursal
                        },
                        callback: function (records, operation, success) {
                            MODULO_VOUCHER_SUCURSAL.down('[name=idSucursal]').setValue(res.idSucursal);
                            idSucursal = res.idSucursal;
                        }
                    });

                    var params = {
                        anio: anio,
                        mes: mes,
                        idSucursal: res.idSucursal
                    };
                    recargarStoreSucursal(params);

                } else {
                    notificaciones('Lo sentimos hubo un error al cargar los departamentos', 2);
                }
            }
        });
    },
    onLimpiarGridDepartamento: function (btn, e) {
        MODULO_VOUCHER_SUCURSAL.down('[name=txtParam]').reset();
        var params = {
            anio: anio,
            mes: mes,
            idSucursal: idSucursal
        };
        recargarStoreSucursal(params);
    },
    onRecargarGridDepartamento: function () {
        idSucursal = MODULO_VOUCHER_SUCURSAL.down('[name=idSucursal]').getValue();
        var params = {
            anio: anio,
            mes: mes,
            idSucursal: idSucursal
        };
        recargarStoreSucursal(params);
    },
    onRecargarGridConsumo: function () {
        MODULO_VOUCHER_SUCURSAL.down('[name=administracionConsumo]').getStore().reload();
        //        var params = {anio: anio, mes: mes, idSucursal: idSucursal, idAplicativo: idAplicativo};
        //        recargarStoreSucursal(params);
    },
    onLimpiarFormDepartamento: function (btn, e) {
        MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').getForm().reset();
    },
    onCrearDepartamentoCredito: function () {
        var form = MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').getForm();
        if (form.isValid()) {
            var val = form.getValues();
            console.log(val);
            if (val.nombresAC !== null && val.nombresAC !== '') {
                var numero = codigo();
                Ext.MessageBox.confirm('Atención!!!', '<h4>Está seguro que desea activar a ' + departamento + ' como cliente sucursal<br><b>CÓDIGO</b><br>' + numero + '<h4>', function (choice) {
                    if (choice === 'yes') {
                        var params = {};
                        var form = Ext.create('Ext.form.Panel');
                        if (idSucursal === undefined && idSucursal > 0)
                            params = {
                                id: recordDepartamento.id,
                                codigo: numero
                            };
                        else
                            params = {
                                id: recordDepartamento.id,
                                codigo: numero,
                                idE: idSucursal,
                                idSucursal: idSucursal
                            };
                        form.submit({
                            url: 'php/SucursalVoucher/createDepartamento.php',
                            timeout: 6000000,
                            waitTitle: 'Procesando Información',
                            waitMsg: 'Procesando...',
                            params: params,
                            success: function (form, action) {
                                notificaciones('Departamento Agregado Correctamente', 1);
                                var params = {
                                    anio: anio,
                                    mes: mes,
                                    idSucursal: idSucursal,
                                    idAplicativo: idAplicativo
                                };
                                recargarStoreSucursal(params);
                            },
                            failure: function (form, action) {
                                notificaciones('No se pudo realizar la operación, intentelo nuevamente', 2);
                                MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').getForm().reset();
                            }
                        });
                    }
                });
            } else {
                messageInformationEffect('Debe dar click en buscar');
            }
        } else {
            messageInformationEffect('Debe dar click en buscar');
        }
    },
    onBuscarDepartamentoAgregar: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            storeBuscarDepartamentos = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Buscar_Departamento');
            var paramBusqueda = MODULO_VOUCHER_SUCURSAL.down('[name=cedula]').getValue();
            storeBuscarDepartamentos.load({
                params: {
                    cedula: paramBusqueda,
                    idAplicativo: idAplicativo,
                },
                callback: function (records) {
                    if (records.length > 0) {
                        MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').down('[name=nombres]').setValue(records[0].data.nombres);
                        MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').down('[name=apellidos]').setValue(records[0].data.apellidos);
                        MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').down('[name=aplicativo]').setValue(records[0].data.app);
                        MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').down('[name=celular]').setValue(records[0].data.celular);
                        var data = records[0].data;
                        recordDepartamento = data;
                        departamento = records[0].data.departamento;
                        idAppBus = records[0].data.idApp;
                    } else {
                        notificaciones('No existen datos del cliente, por favor revise la cédula e intente nuevamente', 2);
                    }
                }
            });
        }
    },
    onGetClass: function (v, meta, rec) {
        if (rec.data.tipo === 1) {
            return 'x-fa fa-user';
        } else if (rec.data.tipo == 2) {
            return 'x-fa fa-check';
        }
    },
    onSelectGridDepartamento: function (thisObj, selected, eOpts) {
        console.log(selected.data);
        departamento = selected.data.departamento;
        idApp = selected.data.idAplicativo;
        app = selected.data.app;
        cedula = selected.data.cedula;
        celular = selected.data.celular;
        idE = selected.data.idE;
        idCC = selected.data.idCC;
        idCCC = selected.data.idCCC;
        idDepar = selected.data.idDepar;
        idDeparClien = selected.data.idDeparClien;
        idSucuDepar = selected.data.idSucuDepar;
        idSucursal = selected.data.idSucursal;
        var params = {
            idC: selected.idC,
            //anio: selected.anio,
            //mes: selected.mes,
            idSucursal: idSucursal,
            idDepar: selected.data.idDepar
        };
        storeReadConsumo = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Read_Consumo');
        storeTotalesConsumo = Ext.create('Ktaxi.store.sucursalVoucher.s_Sucursal_Totales_Consumo');
        MODULO_VOUCHER_SUCURSAL.down('[name=asignarCredito]').enable();
        MODULO_VOUCHER_SUCURSAL.down('[name=administracionConsumo]').getStore().load({
            params: {
                idC: selected.data.idC,
                idCCC: selected.data.idCCC,
                idDepar: selected.data.idDepar,
                idDeparClien: selected.data.idDeparClien,
                anio: anio,
                mes: mes,
                idSucursal: idSucursal
            },
            callback: function (records, operation, success) {
                storeTotalesConsumo.load({
                    params: {
                        idC: selected.data.idC,
                        idCCC: selected.data.idCCC,
                        anio: anio,
                        mes: mes
                    },
                    callback: function (records, operation, success) {
                        if (records !== null) {
                            if (records.length > 0) {

                            }
                        }
                    }
                });
                MODULO_VOUCHER_SUCURSAL.down('[name=clienteAdmCosumo]').setText('Departamento: ' + selected.data.departamento);
                MODULO_VOUCHER_SUCURSAL.down('[name=creditoAdmCosumo]').setText('Crédito: $' + selected.data.creditoMes);
                MODULO_VOUCHER_SUCURSAL.down('[name=consumoAdmCosumo]').setText('Cosumido: $' + selected.data.consumoMes);
                //                MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').setCollapsed(true);
            }
        });
    },
    onShowVentanaAsignarCredito: function () {
        vnt_AsignarCredito = Ext.create('Ext.window.Window', {
            title: 'Ventana de asignar credito a un departamento',
            iconCls: 'icon-services',
            layout: 'anchor',
            //            closeAction: 'hide',
            closeAction: 'destroy',
            resizable: false,
            closable: true,
            modal: true,
            plain: false,
            width: 800,
            height: 380,
            items: [{
                    layout: 'anchor',
                    width: '100%',
                    height: '100%',
                    xtype: 'panel',
                    name: 'pnl-AsigCred',
                    items: [{
                            xtype: 'label',
                            text: 'Agregar credito a ' + departamento,
                            id: 'lbl-titleACr',
                            name: 'lbl-titleACr',
                            style: 'aling:center;font-size: 22px; font-weight: bold;'
                        },
                        {
                            xtype: 'label',
                            html: '<hr>',
                            id: 'lbl-lineaACr',
                            name: 'lbl-lineaACr',
                            style: 'font-size: 12px;'
                        },
                        {
                            xtype: 'label',
                            text: 'El dinero que usted asigne al departamento se podra usar solo por este mes.',
                            id: 'lbl-descrACr',
                            name: 'lbl-descrACr',
                            style: 'font-size: 12px;'
                        }
                    ]
                },
                {
                    layout: 'anchor',
                    width: '100%',
                    xtype: 'form',
                    height: '100%',
                    name: 'formAsignarCredito',
                    items: [{
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                            items: [{
                                    name: 'dateAnios',
                                    xtype: 'combobox',
                                    emptyText: 'Años',
                                    fieldLabel: '<b>Año</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    value: anio,
                                    store: anios
                                },
                                {
                                    name: 'dateMes',
                                    xtype: 'combobox',
                                    emptyText: 'Mes',
                                    fieldLabel: '<b>Mes</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    store: meses,
                                    listeners: {
                                        select: function (combo, record, eOpts) {
                                            arrayNombreMesesSucur = [];
                                            arrayIdMesesSucur = [];
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').getStore().removeAll();                                            var rangoincial = record.data.id;
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').setValue(0);
                                            var rangoincial = record.data.id;
                                            for (let i = 0; i < 4; i++) {
                                                arrayIdMesesSucur.push(rangoincial);
                                                arrayNombreMesesSucur.push(getNombreMes(rangoincial));
                                                rangoincial++;
                                            }
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').setStore(arrayNombreMesesSucur);
                                        }
                                    }
                                },
                                {
                                    name: 'dateMesHasta',
                                    xtype: 'combobox',
                                    emptyText: 'Mes',
                                    fieldLabel: '<b>Hasta el Mes</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    //hidden:true,
                                    store: mesesHasta
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'numberfield',
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    maxValue: 1000,
                                    minValue: 0,
                                    name: 'montoACr',
                                    id: 'montoACr',
                                    fieldLabel: '<b>Monto</b>',
                                    flex: 2,
                                    emptyText: 0,
                                    enableKeyEvents: true,
                                    decimalPrecision: 2,
                                    allowDecimals: true,
                                    alwaysDisplayDecimals: true,
                                    listeners: {
                                        blur: function (thisObj, e, eOpts) {
                                            var idC = Ext.getCmp('nombresACr').getValue();
                                            if (idC === null) {
                                                Ext.getCmp('nombresACr').focus(true);
                                            } else {
                                                Ext.getCmp('razonACr').focus(true);
                                            }
                                        }
                                    }
                                },
                                {
                                    flex: 2,
                                    xtype: 'label',
                                    text: '0.00',
                                    id: 'lbl-creditoLim',
                                    name: 'lbl-creditoLim',
                                    style: 'font-size: 13px;font-weight: bold;'
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'combobox',
                                    emptyText: 'Nombre Departamento',
                                    fieldLabel: '<b>Nombre Departamento</b>',
                                    forceSelection: true,
                                    hidden: true,
                                    listConfig: {
                                        minWidth: 250
                                    },
                                    margin: '0 5 5 0',
                                    name: 'appACrCombo',
                                    maxLength: 90,
                                    flex: 1,
                                    //                                    store: storeDepartamento,
                                    trigger: true,
                                    valueField: 'id',
                                    displayField: 'text',
                                    queryMode: 'remote', //tipo de recarga del store desde afuera
                                    queryParam: 'filtro', //criterio de busqueda a enviar al php
                                    minChars: 2, //numero de caracteres limite para empezar la busqueda
                                    hideTrigger: true, //Para ocultar triángulo
                                },
                                {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    xtype: 'textfield',
                                    name: 'idEACr',
                                    //                                    id: 'nombresACr',
                                    emptyText: 'idSucursal',
                                    fieldLabel: '<b>idSucursal</b>',
                                    readOnly: true,
                                    hidden: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'nombresACr',
                                    id: 'nombresACr',
                                    emptyText: 'Nombre departamento',
                                    fieldLabel: '<b>Nombre</b>',
                                    readOnly: true,
                                    flex: 1
                                },
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: []
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                blankText: INFOMESSAGEBLANKTEXT,
                                allowOnlyWhitespace: false,
                                xtype: 'textareafield',
                                name: 'razonACr',
                                id: 'razonACr',
                                emptyText: 'Descripción de la razón del credito',
                                fieldLabel: '<b>Razón</b>',
                                maxLength: 500,
                                maxLengthText: 'Debe ingresar hasta 500 caracter',
                                minLengthText: 'Debe ingresar mas de 20 caracteres',
                                minLength: 20,
                                flex: 1
                            }]
                        }
                    ]
                }
            ],
            buttons: ['->', {
                iconCls: 'fa fa-credit-card-alt',
                text: '<b>Asignar credito</b>',
                width: '150px',
                tooltip: 'Permite asiganar credito a un departamento',
                name: 'btn_agregar_cred_cliente',
                handler: function (event, target, owner, tool) {
                    Ext.MessageBox.buttonText = {
                        yes: "Sí",
                        no: "No"
                    };
                    var form = vnt_AsignarCredito.down('[name=formAsignarCredito]').getForm();
                    if (form.isValid()) {
                        var val = form.getValues();
                        console.log(val);
                        Ext.MessageBox.confirm('Atención', '<h3>¿Está seguro de asignar $' + val.montoACr + '.00' + ' de crédito al departamento ' + departamento + ' para el presente mes', function (choice) {
                            if (choice === 'yes') {
                                var mesRangoFinal = vnt_AsignarCredito.down('[name=dateMesHasta]').getValue();
                                var idRangoFinal = getNumeroMes(mesRangoFinal);
                                var data;
                                if (mesRangoFinal !== null) {
                                    for (let i = 0; i < arrayIdMesesSucur.length; i++) {
                                        if (arrayIdMesesSucur[i] !== idRangoFinal) {
                                            data = {
                                                monto: val.montoACr,
                                                anio: val.dateAnios,
                                                mes: arrayIdMesesSucur[i],
                                                idDepar: idDepar,
                                                idDeparClien: idDeparClien,
                                                razonCredito: val.razonACr,
                                                idSucursal: idSucursal,
                                                idSucuDepar: idSucuDepar
                                            };
                                            
                                            asignarCreditoSucursalVoucher(data, val);
                                        } else {
                                        
                                            data = {
                                                monto: val.montoACr,
                                                anio: val.dateAnios,
                                                mes: arrayIdMesesSucur[i],
                                                idDepar: idDepar,
                                                idDeparClien: idDeparClien,
                                                razonCredito: val.razonACr,
                                                idSucursal: idSucursal,
                                                idSucuDepar: idSucuDepar
                                            };
                                            asignarCreditoSucursalVoucher(data, val);
                                            break;
                                        }
                                    }
                                } else {
                                    data = {
                                        monto: val.montoACr,
                                        anio: val.dateAnios,
                                        mes: val.dateMes,
                                        idDepar: idDepar,
                                        idDeparClien: idDeparClien,
                                        razonCredito: val.razonACr,
                                        idSucursal: idSucursal,
                                        idSucuDepar: idSucuDepar
                                    };
                                    asignarCreditoSucursalVoucher(data, val);
                                }
                            }
                        });
                    } else {
                        Ext.MessageBox.alert('Atención', 'Complete los campos requeridos');
                    }
                }
            }, {
                iconCls: 'fa fa-window-close-o',
                text: '<b>Cancelar</b>',
                width: '100px',
                tooltip: 'Cerrar ventana',
                handler: function (event, target, owner, tool) {
                    vnt_AsignarCredito.close();
                    vnt_AsignarCredito.destroy();
                }
            }]
        });
        vnt_AsignarCredito.show();
        vnt_AsignarCredito.down('[name=appACrCombo]').setValue(idApp);
        vnt_AsignarCredito.down('[name=idEACr]').setValue(idE);
        vnt_AsignarCredito.down('[name=nombresACr]').setValue(departamento);
        vnt_AsignarCredito.down('[name=razonACr]').focus(true);
    },
    onBuscarDepartamentoParams: function (component, check) {
        idSucursal = MODULO_VOUCHER_SUCURSAL.down('[name=idSucursal]').getValue();
        console.log(idAplicativo);
        var params = {
            anio: anio,
            mes: mes,
            idSucursal: idSucursal,
            idAplicativo: idAplicativo
        };
        recargarStoreSucursal(params);
    },
    onBuscarGridDepartamento: function (btn, e) {
        var txtParam = MODULO_VOUCHER_SUCURSAL.down('[name=txtParam]').getValue();
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getStore().load({
                params: {
                    params: txtParam,
                    anio: anio,
                    mes: mes,
                    idSucursal: idSucursal,
                    idAplicativo: idAplicativo
                },
                callback: function (records, operation, success) {
                    if (!success) {
                        setMensajeGridEmptyText(MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]'), EMPTY_CARA + '<h3>' + MENSAJE_ERROR + '</h3>');
                    } else if (records.length === 0) {
                        setMensajeGridEmptyText(MODULO_VOUCHER_SUCURSAL.down('[name=grid]'), EMPTY_CARA + '<h3>No existen resultados.</h3>');
                    } else {
                        MODULO_VOUCHER_SUCURSAL.down('[name=administracionConsumo]').getStore().removeAll();
                        MODULO_VOUCHER_SUCURSAL.down('[name=panelCrearDepartamentoSucursal]').getForm().reset();

                    }

                }
            });
        }
    },
});


//FUNCIONES
function limpiarGridSucursal() {
    MODULO_VOUCHER_SUCURSAL.down('[name=paramBusquedaSucursal]').reset();
    var gridLeerSucursal = MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]');
    gridLeerSucursal.getView().deselect(gridLeerSucursal.getSelection());
    MODULO_VOUCHER_SUCURSAL.down('[name=administracionConsumo]').getStore().removeAll();
    //        MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getStore().load();
}

function recargarStoreSucursal(params) {
    storeCreditoCompaniaSucursal.load({
        params: {
            anio: params.anio,
            mes: params.mes,
            idSucursal: params.idSucursal
        },
        callback: function (records, operation, success) {
            if (!success) {
                notificaciones(MENSAJE_ERROR, 2);
            } else {
                if (records.length > 0) {
                    MODULO_VOUCHER_SUCURSAL.down('[name=creditoCompania]').setText('Crédito : $' + records[0].data.credito);
                    MODULO_VOUCHER_SUCURSAL.down('[name=creditoConsumido]').setText('Asignado : $' + records[0].data.asignado);
                    MODULO_VOUCHER_SUCURSAL.down('[name=creditoDisponible]').setText('Cosumido: $' + records[0].data.consumo);
                    idSucursal = records[0].data.idSucursal;
                } else {
                    MODULO_VOUCHER_SUCURSAL.down('[name=creditoCompania]').setText('Crédito: $0');
                    MODULO_VOUCHER_SUCURSAL.down('[name=creditoConsumido]').setText('Asignado: $0');
                    MODULO_VOUCHER_SUCURSAL.down('[name=creditoDisponible]').setText('Consumo: $0');
                }
            }
        }
    });
    MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getStore().load({
        params: {
            anio: params.anio,
            mes: params.mes,
            idSucursal: params.idSucursal
        },
        callback: function (records, operation, success) {
            if (!success) {
                console.log('error');
            } else {
                if (records.length > 0) {

                }
            }
        }
    });
    //recargar componentes    
    MODULO_VOUCHER_SUCURSAL.down('[name=asignarCredito]').disable();
    MODULO_VOUCHER_SUCURSAL.down('[name=administracionConsumo]').getStore().removeAll();
    MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getView().deselect(MODULO_VOUCHER_SUCURSAL.down('[name=gridLeerSucursal]').getSelection());
    MODULO_VOUCHER_SUCURSAL.down('[name=clienteAdmCosumo]').setText('Departamento:');
    MODULO_VOUCHER_SUCURSAL.down('[name=creditoAdmCosumo]').setText('Crédito: $');
    MODULO_VOUCHER_SUCURSAL.down('[name=consumoAdmCosumo]').setText('Cosumido: $');
}



function asignarCreditoSucursalVoucher(data, val) {
    var form = Ext.create('Ext.form.Panel');
    form.getForm().submit({
        url: 'php/SucursalVoucher/createCredito.php',
        timeout: 6000000,
        waitTitle: 'Procesando Información',
        waitMsg: 'Procesando...',
        params: data,
        success: function (form, action) {
            vnt_AsignarCredito.close();
            vnt_AsignarCredito.destroy();
            notificaciones('Crédito Agregado Correctamente', 1);
            var params = {
                anio: anio,
                mes: mes,
                idSucursal: idSucursal
            };
            recargarStoreSucursal(params);

        },
        failure: function (form, action) {
            notificaciones('No se pudo realizar la asignación,revice el crédito de la empresa o la fecha de inicio de crédito', 2);
            vnt_AsignarCredito.close();
            vnt_AsignarCredito.destroy();
            var params = {
                anio: anio,
                mes: mes,
                idSucursal: idSucursal
            };
            recargarStoreSucursal(params);
        }
    });
}

function getNombreMes(value) {
    var mes;
    switch (value) {
        case 1:
            mes = 'Enero'
            break;
        case 2:
            mes = 'Febrero'
            break;
        case 3:
            mes = 'Marzo'
            break;
        case 4:
            mes = 'Abril'
            break;
        case 5:
            mes = 'Mayo'
            break;
        case 6:
            mes = 'Junio'
            break;
        case 7:
            mes = 'Julio'
            break;
        case 8:
            mes = 'Agosto'
            break;
        case 9:
            mes = 'Septiembre'
            break;
        case 10:
            mes = 'Octubre'
            break;
        case 11:
            mes = 'Noviembre'
            break;
        case 12:
            mes = 'Diciembre'
            break;
        default:
            break;
    }
    return mes;
}

function getNumeroMes(value) {
    var mes;
    switch (value) {
        case 'Enero':
            mes = 1
            break;
        case 'Febrero':
            mes = 2
            break;
        case 'Marzo':
            mes = 3
            break;
        case 'Abril':
            mes = 4
            break;
        case 'Mayo':
            mes = 5
            break;
        case 'Junio':
            mes = 6
            break;
        case 'Julio':
            mes = 7
            break;
        case 'Agosto':
            mes = 8
            break;
        case 9:
            mes = 'Septiembre'
            break;
        case 'Octubre':
            mes = 10
            break;
        case 'Noviembre':
            mes = 11
            break;
        case 'Diciembre':
            mes = 12
            break;
        default:
            break;
    }
    return mes;
}