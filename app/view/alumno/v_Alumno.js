/* global MAXIMUMMESSAGUEREQURID, INFOMESSAGEREQUERID, MINIMUMMESSAGUEREQUERID, INFOMESSAGEBLANKTEXT, showAuditoria, HEIGT_VIEWS, Ext, EMPTY_CARA, showTipConten, showTipContenID, formatEstadoRegistro */
Ext.define('Ktaxi.view.alumno.v_Alumno', {
    extend: 'Ext.panel.Panel',
    xtype: 'alumno',
    controller: 'c_Alumno',
    height: HEIGT_VIEWS,
    layout: 'border',
    store: 'alumno.s_Alumno',
    requires: ['Ktaxi.view.alumno.c_Alumno'],
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    initComponent: function () {
        var STORE_ALUMNO = Ext.create('Ktaxi.store.alumno.s_Alumno');
        this.items = [{
                xtype: 'panel',
                region: 'center',
                width: '65%',
                collapseMode: 'mini',
                margin: 5,
                header: false,
                collapsible: true,
                collapsed: false,
                layout: 'fit',
                items: [{
                    xtype: 'grid',
                    name: 'grid',
                    height: HEIGT_VIEWS - 10,
                    plugins: [{
                        ptype: 'gridfilters'
                    }],
                    bufferedRenderer: false,
                    store: STORE_ALUMNO,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        },
                        loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>' + EMPTY_CARA
                    },
                    tbar: [{
                            xtype: 'textfield',
                            name: 'txtParam',
                            width: '70%',
                            emptyText: 'Nombre, apellido...',
                            listeners: {
                                specialkey: 'onBuscar'
                            }
                        },
                        /* {
                            xtype: 'combobox',
                            name: 'cmbxTipo',
                            emptyText: 'Usuario...',
                            width: '28%',
                            valueField: 'id',
                            queryParam: 'filtro',
                            queryMode: 'remote',
                            displayField: 'texto',
                            // store: Ext.create('Ktaxi.store.stores.s_Usuario_Acceso_Api')
                        }, */
                        /* {
                            xtype: 'combobox',
                            name: 'cmbxTipoEstado',
                            emptyText: 'Estado...',
                            width: '20%',
                            queryMode: 'remote',
                            displayField: 'estado',
                            queryParam: 'filtro',
                            valueField: 'id',
                            //  store: Ext.create('Ktaxi.store.stores.s_Estado_Alumno')
                        }, */
                        //                            {
                        //                                xtype: 'combobox',
                        //                                label: 'Choose State',
                        //                                queryMode: 'local',
                        //                                width: '28%',
                        //                                displayField: 'name',
                        //                                valueField: 'abbr',
                        //
                        //                                store: [
                        //                                    {abbr: 'AL', name: 'Alabama'},
                        //                                    {abbr: 'AK', name: 'Alaska'},
                        //                                    {abbr: 'AZ', name: 'Arizona'}
                        //                                ]
                        //                            },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-refresh',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [{
                        ftype: 'grouping',
                        groupHeaderTpl: '{name}',
                        hideGroupedHeader: true,
                        enableGroupingMenu: true
                    }],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {
                            header: '#',
                            width: 30,
                            align: 'center'
                        }),
                        {
                            dataIndex: 'nombre',
                            text: 'Nombre',
                            tooltip: "Nombre",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'apellido',
                            text: 'Apellido',
                            tooltip: "Apellido",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'correo',
                            text: 'Correo',
                            tooltip: "Correo",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        //{dataIndex: 'mensaje', text: 'Mensaje', tooltip: "Mensaje", filter: true, flex: 1, sortable: true, renderer: showTipConten},
                        //{dataIndex: 'AlumnoCorreo', text: 'Alumno', tooltip: "Alumno del Correo", filter: true, flex: 1, sortable: true, renderer: showTipConten},
                        //{flex: 1, tooltip: 'Fecha Registro', xtype: 'datecolumn', header: "Fecha", dataIndex: 'fechaRegistro', sortable: true, format: 'Y/m/d'},
                        //{flex: 1, tooltip: 'Hora', xtype: 'datecolumn', header: "Hora", dataIndex: 'fechaRegistro', sortable: true, format: 'H:i:s'},
                        //                            {text: 'Estado', menuDisabled: true, sortable: false, xtype: 'actioncolumn', width: 40, minWidth: 40, items: [
                        ////                                  
                        //                                    {
                        //                                        xtype: 'button',
                        //                                        tooltip: 'Confimar',
                        //                                        iconCls: 'x-fa fa-check-square',
                        //                                        text: 'Confirmar',
                        //                                        name: 'confimar',
                        //                                        value: 'Confimar',
                        //                                        handler: 'showAlumnoConfirmar',
                        //                                        //getClass: 'onGetClass'
                        //                                    }
                        //                                ]
                        //                            },
                        /* {
                            text: 'Estado',
                            menuDisabled: true,
                            sortable: false,
                            xtype: 'actioncolumn',
                            width: 40,
                            minWidth: 40,
                            items: [{
                                xtype: 'button',
                                //style: 'color="red!important"',
                                name: 'btnFinalizarJornada',
                                tooltip: 'Confirmar o Validar',
                                iconCls: 'x-fa fa-ban',
                                isDisabled: 'onIsDisabledBtnFinalizarJornada',
                                handler: 'onVenatna',
                                getClass: 'onGetClass'
                            }]
                        }, */
                        {
                            dataIndex: 'id',
                            text: "ID",
                            tooltip: "ID",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipContenID,
                            hidden: true
                        }
                    ],
                    listeners: {
                        select: 'onSelect',
                        //rowdblclick: showAuditoriaAlumno,

                    },
                    onButtonWidgetClick: function (btn) {
                        var rec = btn.getViewModel().get('record');
                        Ext.Msg.alert("Button clicked", "Hey! " + rec.get('name'));
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        //store: STORE_ALUMNO,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [{
                            xtype: 'button',
                            text: 'Exportar',
                            iconCls: 'x-fa fa-download',
                            handler: function (btn) {
                                onExportar(btn, "Alumno", this.up('grid'));
                            }
                        }],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }]
            },
            {
                xtype: 'panel',
                region: 'east',
                padding: 5,
                width: '35%',
                collapseMode: 'mini',
                anchor: '100% 100%',
                layout: 'anchor',
                cls: 'panelCrearEditar',
                autoScroll: true,
                collapsible: true,
                collapsed: false,
                header: false,
                headerAsText: false,
                items: [{
                        xtype: 'form',
                        title: 'Alumno',
                        name: 'form',
                        extend: 'Ext.Widget',
                        cls: 'quick-graph-panel shadow panelComplete',
                        ui: 'light',
                        padding: 5,
                        defaults: {
                            width: '100%',
                            margin: '0 0 5 0',
                            defaultType: 'textfield',
                            minLengthText: MINIMUMMESSAGUEREQUERID,
                            maxLengthText: MAXIMUMMESSAGUEREQURID,
                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                            blankText: INFOMESSAGEBLANKTEXT,
                            labelWidth: 90,
                            allowOnlyWhitespace: false,
                            allowBlank: false
                        },
                        items: [
                            //                            {
                            //                                xtype: 'emailfield',
                            //                                fieldLabel: '<b>Correo Remitente</b>',
                            //                                name: 'emailRemitente',
                            //                                anchor: '100%',
                            //                                minLength: 3,
                            //                                maxLength: 145
                            //                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<b>Nombre</b>',
                                name: 'nombre',
                                emptyText: 'Nombre',
                                maxLength: 125,
                                minLength: 3,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: '<b>Apellido</b>',
                                name: 'apellido',
                                emptyText: 'Apellido',
                                maxLength: 125,
                                minLength: 3,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'textfield',
                                vtype: 'email',
                                fieldLabel: '<b>Correo</b>',
                                name: 'correo',
                                emptyText: 'Correo',
                                maxLength: 125,
                                minLength: 3,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                defaultType: 'combobox',
                                width: '100%',
                                defaults: {
                                    margin: 1
                                },
                                items: [
                                    {
                                        fieldLabel: '<b>Clase<b/>',
                                        xtype: 'combobox',
                                        flex: 10,
                                        name: 'cmbxClase',
                                        emptyText: 'Clase',
                                        displayField: 'nombre',
                                        valueField: 'id',
                                        queryParam: 'param',
                                        queryMode: 'remote',
                                        store: Ext.create('Ktaxi.store.combos.s_Clase'),
                                        minChars: 3
                                    },
                                    {xtype: 'button', name: 'agregarVehiAdmin', flex: 1, iconCls: 'x-fa fa-plus', handler: 'onAddClases'}
                                ]
                            },
                            {
                                xtype: 'grid',
                                plugins: [{ptype: 'gridfilters'}],
                                store: Ext.create('Ext.data.Store', {fields: [{name: 'id'}, {name: 'nombre'}]}),
                                cls: 'gridAuxVehiAdmin',
                                height: 65,
                                name: 'gridClase',
                                split: true,
                                autoScroll: true,
                                bufferedRenderer: false,
                                viewConfig: {
                                    deferEmptyText: false,
                                    enableTextSelection: true,
                                    preserveScrollOnRefresh: true,
                                    listeners: {
                                        loadingText: 'Cargando...'
                                    },
                                    loadMask: true,
                                    emptyText: '<center><b>No existen resultados</b></center>'
                                },
                                columns: [
                                    {filter: true, width: '70%', align: 'left', dataIndex: 'nombre', sortable: true},
                                    //{filter: true, width: '40%', dataIndex: 'materia', sortable: true},
                                    //{filter: true, width: '35%', dataIndex: 'dateChange', sortable: true, renderer: formatEstadoFecha},
                                    {
                                        xtype: 'actioncolumn',
                                        width: '10%',
                                        items: [{getClass: 'onGetClassClases', handler: 'onHandlerGridClases'}]
                                    }
                                ],
                                listeners: {
                                    rowdblclick: function (grid, record) {
                                        if (!record.data.nuevo) {
                                            showAuditoria(grid, record, 'gridAux');
                                        }
                                    }
                                }
                            }
                            /*  {
                                xtype: 'list',
                                fullscreen: true,
                                itemTpl: 'lidt',
                                data: [{
                                        title: 'Item 1'
                                    },
                                    {
                                        title: 'Item 2'
                                    },
                                    {
                                        title: 'Item 3'
                                    },
                                    {
                                        title: 'Item 4'
                                    }
                                ]
                            },
 */
                            /* {
                                xtype: 'combobox',
                                name: 'idUsuario',
                                emptyText: 'Seleccione un usuario...',
                                fieldLabel: '<b>Usuario</b>',
                                displayField: 'texto',
                                allowBlank: false,
                                valueField: 'id',
                                queryParam: 'filtro', //tomar en cuenta la variable para hacer filtros  
                                queryMode: 'remote',
                               store: Ext.create('Ktaxi.store.stores.s_Usuario_Acceso_Api')
                            }, */
                            /* {
                                xtype: 'textarea',
                                fieldLabel: '<b>Mensaje</b>',
                                name: 'mensaje',
                                anchor: '100%',
                                minLength: 7,
                                maxLength: 145
                            }, */
                        ]
                    },

                ],
                dockedItems: [{
                    ui: 'footer',
                    xtype: 'toolbar',
                    dock: 'bottom',
                    defaults: {
                        width: '25%',
                        height: 30
                    },
                    items: [{
                            text: 'Limpiar',
                            tooltip: 'Limpiar',
                            disabled: false,
                            limpiar: true,
                            handler: 'onLimpiar'
                        },
                        '->',
                        {
                            text: 'Crear',
                            tooltip: 'Crear',
                            name: 'btnCrear',
                            handler: 'onCrear',
                            disabled: false
                        },
                        {
                            text: 'Editar',
                            tooltip: 'Editar',
                            name: 'btnEditar',
                            handler: 'onEditar',
                            disabled: false,
                            hidden: true
                        }
                    ]
                }]
            }
        ];
        this.callParent(arguments);
    }
});