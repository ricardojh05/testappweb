var permisosDepartamento;
var STORE_NOTA = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 0,
            "text": "Sin Nota"
        },
        {
            id: 1,
            "text": "Nota Obligatoria"
        },
        {
            id: 2,
            "text": "Nota Opcional"
        },
    ]
});

Ext.define('Ktaxi.view.departamento.v_Departamento', {
    extend: 'Ext.panel.Panel',
    xtype: 'departamento',
//    title: TITULO,
    store: 'departamento.s_Departamento',
    controller: 'c_Departamento',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'moduloDepartamento',
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.departamento.c_Departamento'
    ],
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onViewDepartamento',
        show: 'onShowDepartamento'
    },
    initComponent: function () {
        var STORE_DEPARTAMENTO = Ext.create('Ktaxi.store.departamento.s_Departamento');
        var STORE_SUCURSAL = Ext.create('Ktaxi.store.combos.s_Sucursal');
        var STORE_ADMINISTRADOR_COMBO = Ext.create('Ktaxi.store.combos.s_Administrador');
        var STORE_ADMINISTRADORES_DEPARTAMENTO = Ext.create('Ktaxi.store.departamento.s_Administrador_Departamento');
        this.items = [
            {
//                id: 'panelLeerDepartamento',
                name: 'panelLeerDepartamento',
                region: 'west',
                xtype: 'panel',
                padding: 5,
                width: '60%',
                layout: 'fit',
                header: false,
                headerAsText: false,
                items: [{
//                        id: 'gridLeerDepartamentos',
                        name: 'gridLeerDepartamentos',
                        columnLines: true,
                        xtype: 'grid',
                        plugins: [{ptype: 'gridfilters'}],
                        bufferedRenderer: false,
                        store: STORE_DEPARTAMENTO,
                        tbar: [
                            {
                                xtype: 'textfield',
                                width: '60%',
                                tooltip: 'Escribir búsqueda',
                                name: 'paramBusquedaDepartamento',
                                emptyText: 'Departamento, Contacto',
                                minChars: 0,
                                typeAhead: true,
                                listeners: {
                                    specialkey: 'onChangeSearchDepartamento'
                                }
                            },
                            {
                                xtype: 'combobox',
                                width: '20%',
                                tooltip: 'Seleccionar Sucursal',
                                name: 'comboBuscarSucursal',
                                emptyText: 'Todos',
                                reference: 'states',
                                displayField: 'text',
                                valueField: 'idSucursal',
                                filterPickList: true,
                                queryParam: 'param',
                                queryMode: 'remote',
                                growMax: 20,
                                store: STORE_SUCURSAL,
                                listeners: {
                                    specialkey: 'onChangeSearchDepartamento'
                                }
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-search',
                                iconAlign: 'right',
                                tooltip: 'Buscar',
                                handler: 'onChangeSearchDepartamento'
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-eraser',
                                iconAlign: 'right',
                                tooltip: 'Limpiar',
                                handler: 'onLimpiarDepartamento'
                            },
                            {
                                width: '6%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-refresh',
                                iconAlign: 'right',
                                tooltip: 'Recargar',
                                handler: 'onRecargar'
                            }
                        ],
                        columns: [
                            Ext.create('Ext.grid.RowNumberer', {header: '#', flex:1, align: 'center'}),
                            {filter: true, tooltip: "Sucursak", text: "Sucursal",flex:2, dataIndex: 'sucursal', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Departamento", text: "Departamento", flex:2, dataIndex: 'departamento', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Color", text: "Color", flex:2, dataIndex: 'color', sortable: true, renderer: formatColor},
                            {filter: true, tooltip: "Contacto", text: "Contacto", flex:2, dataIndex: 'contacto', sortable: true, renderer: showTipConten},
                            {filter: true, tooltip: "Comentario", text: "Comentario", flex:2, dataIndex: 'comentario', sortable: true, renderer: showTipConten},
                            //{filter: true, tooltip: "isNota", text: "Nota", flex:2, dataIndex: 'isNota', sortable: true,hidder:true, renderer: showTipConten},
                            {filter: true, tooltip: "Insertada", text: "Insertada", flex:2, dataIndex: 'insertado', sortable: true, renderer: function formatEstadoRegistroInsertado(estado) {
                                    var htmlEstado = '';
                                    if (estado) {
                                        htmlEstado = '<i style="color:green;" class="gridAuxCheck x-fa fa-check" aria-hidden="true">';
                                    } else {
                                        htmlEstado = '<i style="color:red;" class="gridAuxDelete fa x-fa fa-times" aria-hidden="true">';
                                    }
                                    return htmlEstado;
                                }},
                            {filter: true, tooltip: "ID", text: "ID", width: 40, dataIndex: 'id', sortable: true, renderer: showTipContenID, hidden: true}
                        ],
                        height: 210,
                        split: true,
                        region: 'north',
                        listeners: {
                            selectionchange: 'onSelectChangeGridDepartamento',
                            beforeitemclick: 'onBeforeclickGridDepartamento',
                            rowdblclick: showAuditoria
                        },
                        viewConfig: {enableTextSelection: true,
                            emptyText: '<center>No existen resultados.</center>'
                        },
                        bbar: Ext.create('Ext.PagingToolbar', {
                            store: STORE_DEPARTAMENTO,
                            displayInfo: true,
                            emptyMsg: "Sin datos que mostrar.",
                            displayMsg: ' {0} - {1} de {2} registros',
                            beforePageText: 'Página',
                            afterPageText: 'de {0}',
                            firstText: 'Primera página',
                            prevText: 'Página anterior',
                            nextText: 'Siguiente página',
                            lastText: 'Última página',
                            refreshText: 'Actualizar',
                            inputItemWidth: 35,
                            items: [
                                {
                                    xtype: 'label',
                                    name: 'numRegistrosGrid',
                                    text: '0 registros'
                                }
                            ],
                            listeners: {
                                afterrender: function () {
                                    this.child('#refresh').hide();
                                }
                            }
                        })
                    }]
            },
            {
//                id: 'panelCrearEditarDepartamento',
                name: 'panelCrearEditarDepartamento',
                cls: 'panelCrearEditar',
                region: 'center',
                xtype: 'form',
                layout: 'fit',
                padding: 5,
                width:'40%',
                flex: 2,
                collapsible: false,
                items: [{
                        xtype: 'form',
                        name: 'formCrearEditarDepartamento',
                        title: 'Departamento',
                        layout: 'vbox',
                        padding: 5,
                        flex: 2,
                        cls: 'quick-graph-panel shadow panelFormulario',
                        ui: 'light',
                        defaultType: 'textfield',
                        defaults: {
                            anchor: '100%'
                        },
                        items: [
                            {
                                allowBlank: false,
                                name: 'sucursal',
                                xtype: 'combobox',
                                width: '100%',
                                emptyText: 'Seleccione',
                                fieldLabel: 'Sucursal',
                                displayField: 'text',
                                minChars: 0,
                                typeAhead: true,
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                maxLength: '45',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                store: STORE_SUCURSAL,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Departamento',
                                name: 'departamento',
                                emptyText: 'Departamento',
                                maxLength: '150',
                                width: '100%',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                listeners: {
                                    change: function keyup(thisObj, e, eOpts) {
                                        thisObj.setValue(e.toUpperCase());
                                    }
                                }
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Contacto',
                                name: 'contacto',
                                maskRe: /[0-9.]/,
                                emptyText: 'Contacto',
                                maxLength: '12',
                                width: '100%',
                                minLength: '9',
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                vtype: 'email',
                                fieldLabel: 'Correo',
                                name: 'correo',
                                emptyText: 'Correo',
                                width: '100%',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                maxLength: '120'
                            },
                            {
                                allowBlank: false,
                                fieldLabel: 'Color',
                                name: 'color',
                                width: '100%',
                                inputType: 'color',
                                emptyText: 'Color',
                                height: 25,
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                            },
                            {
                                xtype: 'textareafield',
                                allowBlank: true,
                                fieldLabel: 'Comentario',
                                name: 'comentario',
                                emptyText: 'Comentario',
                                width: '100%',
                                maxLength: '500',
                                minLength: '10',
                                allowOnlyWhitespace: true,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            {
                                xtype:'textfield',
                                name:'bandera',
                                hidden:true,
                            },
                            {
                                allowBlank: false,
                                name: 'isNota',
//                                name: 'comboBoxSucursal',
                                xtype: 'combobox',
                                width: '100%',
                                emptyText: 'Nota',
                                fieldLabel: 'Nota',
                                displayField: 'text',
                                valueField: 'id',
                                queryParam: 'param',
                                queryMode: 'remote',
                                maxLength: '45',
                                minLength: '4',
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                store: STORE_NOTA,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                            }
                            ,
                            {
                                xtype: 'panel',
                                layout: 'hbox',
                                width: '100%',
                                defaultType: 'combobox',
                                defaults: {
                                    margin: 1
                                },
                                items: [
                                    {
                                        allowBlank: true,
                                        fieldLabel: 'Administrador',
                                        xtype: 'combobox',
                                        tooltip: 'Buscar Administradores',
                                        name: 'comboAdministrador',
                                        emptyText: 'Cédula..',
                                        displayField: 'nombres',
                                        valueField: 'id',
                                        filterPickList: true,
                                        forceSelection: true,
                                        queryParam: 'cedula',
                                        queryMode: 'remote',
                                        store: STORE_ADMINISTRADOR_COMBO,
                                        minChars: 9
                                    },
                                    {
                                        xtype: 'button',
                                        name: 'agregarAdministrador',
                                        flex: 1,
                                        iconCls: 'x-fa fa-plus',
                                        height: 23,
                                        handler: 'onAddAdministrador'}
                                ]
                            },
                            {
                                xtype: 'grid',
                                height: 50,
                                width: '100%',
//                                id: 'gridAuxEmpAdmin',
                                name: 'gridAdministradoresDepartamento',
                                autoScroll: true,
                                bufferedRenderer: false,
                                store: STORE_ADMINISTRADORES_DEPARTAMENTO,
                                cls: 'gridAuxAdmDepartamento',
                                columns: [
                                    {flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro },
                                    {flex: 2, dataIndex: 'nombre', sortable: true},
                                    {
                                        xtype: 'actioncolumn',
                                        menuDisabled: true,
                                        sortable: false,
                                        minWidth: 20,
                                        flex: 1,
                                        items: [{
                                                getClass: function (v, meta, rec) {
                                                    if (rec.data.habilitado) {
                                                        return 'gridAuxDelete x-fa fa-times';
                                                    } else {
                                                        return 'gridAuxCheck x-fa fa-check';
                                                    }
                                                },
                                                handler: function (grid, rowIndex, colIndex) {
                                                    var rec = grid.getStore().getAt(rowIndex);
                                                    MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]').down('[name=bandera]').setValue(true);
                                                    if (rec.data.nuevo) {
                                                        grid.getStore().remove(rec);
                                                    } else {
                                                        if (rec.data.habilitado) {
                                                            rec.set('habilitado', 0);
                                                        } else {
                                                            rec.set('habilitado', 1);
                                                        }
                                                    }
                                                }
                                            }]
                                    }
                                ],
                                minHeight: 150,
                                split: true,
                                region: 'north',
                                viewConfig: {enableTextSelection: true,
                                    emptyText: '<center>Sin administradores asignados..</center>',
                                    getRowClass: function (record) {
                                        if (record.data.nuevo) {
                                            return 'newRowGrid';
                                        }
                                    }
                                },
                                listeners: {
                                    rowdblclick: function (grid, record) {
                                        if (!record.data.nuevo) {
                                            showAuditoria(grid, record, 'gridAux');
                                        }
                                    }
                                }
                            }
                            
                        ]
                    }],
                dockedItems: [{
                        ui: 'footer',
                        xtype: 'toolbar',
                        dock: 'bottom',
                        defaults: {
                            width: '25%',
                            height: 30
                        },
                        items: [
                            {
                                text: 'Limpiar',
                                tooltip: 'Limpiar',
                                disabled: false,
                                handler: 'onLimpiarDepartamento'
                            },
                            '->',
                            {
                                text: 'Editar',
                                tooltip: 'Actualizar',
//                                id: 'btnEditarDepartamento',
                                disabled: true,
                                name: 'btnEditar',
                                handler: 'onUpdate'
                            }, {
                                text: 'Crear',
                                tooltip: 'Crear Sucursales',
//                                id: 'btnCrearDepartamento',
                                disabled: true,
                                name: 'btnCrear',
                                handler: 'onCreate'
                            }]
                    }]
            }
        ];
        this.callParent(arguments);
    }
});

