var MODULO_DEPARTAMENTO;
Ext.define('Ktaxi.view.departamento.c_Departamento', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Departamento',
    onViewDepartamento: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
        MODULO_DEPARTAMENTO = panelLoad;
        MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().load();
    },
    onRecargar: function () {
        MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().reload();
    },
    onShowDepartamento: function (panelLoad) {
        PANELLOAD = panelLoad;
    },
    onLimpiarDepartamento: function (btn, e) {
        validarPermisosGeneral(MODULO_DEPARTAMENTO);
        MODULO_DEPARTAMENTO.down('[name=btnCrear]').enable();
        MODULO_DEPARTAMENTO.down('[name=btnEditar]').disable();
        MODULO_DEPARTAMENTO.down('[name=comboAdministrador]').getStore().removeAll();
        MODULO_DEPARTAMENTO.down('[name=gridAdministradoresDepartamento]').getStore().removeAll();
        MODULO_DEPARTAMENTO.down('[name=paramBusquedaDepartamento]').reset();
        var form_crear = MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]');
        form_crear.down('[name=sucursal]').enable();
        form_crear.getForm().reset();
        var grid = MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]');
        grid.getView().deselect(grid.getSelection());
        grid.getStore().load();
    },
    onChangeSearchDepartamento: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var paramBusqueda = MODULO_DEPARTAMENTO.down('[name=panelLeerDepartamento]').down('[name=paramBusquedaDepartamento]').getValue();
            var sucursal = MODULO_DEPARTAMENTO.down('[name=panelLeerDepartamento]').down('[name=comboBuscarSucursal]').getValue();
            var params = {param: paramBusqueda, sucursal: (sucursal == null) ? 0 : sucursal};
            MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().load({
                params: params,
                callback: function (records) {
                    if (records.length <= 0) {
                        MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridDepartamento: function (thisObj, selected, eOpts) {
        if (selected.length > 0) {
            MODULO_DEPARTAMENTO.down('[name=gridAdministradoresDepartamento]').getStore().removeAll();
            var comboFormSucursal = MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]').down('[name=sucursal]');
            comboFormSucursal.disable();
            if (!isInStore(comboFormSucursal.getStore(), selected[0].data.idSucursal, 'id', 'exact')) {
                comboFormSucursal.getStore().load({
                    params: {
                        param: selected[0].data.idSucursal
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormSucursal.setValue(records);
                        }
                    }
                });
            }
            var comboAdministradores = MODULO_DEPARTAMENTO.down('[name=comboAdministrador]');
            comboAdministradores.getStore().proxy.extraParams = '';
            comboAdministradores.getStore().load();
            var formCrearEmpresa = MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]');
            var gridAdministradores = MODULO_DEPARTAMENTO.down('[name=gridAdministradoresDepartamento]');
            gridAdministradores.getStore().load({
                params: {
                    idDepartamento: selected[0].data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        gridAdministradores.getStore().removeAll();
                    } else {
                        var asignados = [];
                        for (var i in records) {
                            asignados.push(records[i].data.id);
                        }
                        comboAdministradores.getStore().proxy.extraParams = {asignados: asignados.toString()};
                        comboAdministradores.getStore().load();
                    }
                }
            });
            formCrearEmpresa.loadRecord(selected[0]);
        }
    },
    onBeforeclickGridDepartamento: function (thisObj, record, item, index, e, eOpts) {
        MODULO_DEPARTAMENTO.down('[name=btnEditar]').enable();
        MODULO_DEPARTAMENTO.down('[name=btnCrear]').disable();
    },
    onCreate: function () {
        var params = this.getRecordsAdministrador();
        var me = this;
        var form = MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]');
        if (form.isValid()) {
            Ext.MessageBox.buttonText = {
                    yes: "Sí",
                    no: "No"
                };                
            Ext.MessageBox.confirm('Aviso!', 'La sucursal no podrá ser modificada luego de guardar.<center><b style="color:red">¿Desea continuar?</b></center></b>', function (choice) {
                if (choice === 'yes') {
                    MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().insert(0, form.getValues());
                    MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().proxy.extraParams = params;
                    MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().sync({
                        callback: function (response) {
                            onProcesarPeticion(response, me.onLimpiarDepartamento({limpiar: true}));

                        }});
                }
            });

        } else {
            var fields = form.form.getFields();
            mensajesValidacionForms(fields);
        }
    },

    onUpdate: function () {

        var params = this.getRecordsAdministrador();
        var me = this;
        var form = MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]');
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().proxy.extraParams = params;
            MODULO_DEPARTAMENTO.down('[name=gridLeerDepartamentos]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiarDepartamento({limpiar: true}));
                }});
        } else {
            var fields = form.getFields();
            mensajesValidacionForms(fields);
        }
    },
    onAddAdministrador: function (btn) {
        var comboAdministrador = MODULO_DEPARTAMENTO.down('[name=comboAdministrador]');
        var idAdministrador = comboAdministrador.getValue();
        var nombreAdministrador = comboAdministrador.getRawValue();
        MODULO_DEPARTAMENTO.down('[name=formCrearEditarDepartamento]').down('[name=bandera]').setValue(true);
        var newRecord = {
            id: idAdministrador,
            nombre: nombreAdministrador,
            habilitado: 1,
            nuevo: true
        };

        MODULO_DEPARTAMENTO.down('[name=gridAdministradoresDepartamento]').getStore().insert(0, newRecord);
        comboAdministrador.setValue(null);
    },
    getRecordsAdministrador: function () {
        MODULO_DEPARTAMENTO.down('[name=comboAdministrador]').getStore().removeAll();
        var listAdministrador = [];
        var dataAuxAdministrador = MODULO_DEPARTAMENTO.down('[name=gridAdministradoresDepartamento]').getStore().data.items;
        for (var i in dataAuxAdministrador) {
            if (dataAuxAdministrador[i].data.nuevo) {
                listAdministrador[i] = {id: dataAuxAdministrador[i].data.id, habilitado: dataAuxAdministrador[i].data.habilitado};
            }
        }
        var dataAuxAdministrador = MODULO_DEPARTAMENTO.down('[name=gridAdministradoresDepartamento]').getStore().getUpdatedRecords();
        for (var i in dataAuxAdministrador) {
            listAdministrador[i] = {id: dataAuxAdministrador[i].data.id, habilitado: dataAuxAdministrador[i].data.habilitado};
        }
        return {administradores: JSON.stringify(listAdministrador)};
    },
    onMensaje() {



    }
});


