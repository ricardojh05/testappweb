var MODULO_ADMINISTRACION;
Ext.define('Ktaxi.view.administrador.c_Administrador', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Administrador',
    onViewAdministrador: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
//        Ext.getStore('administrador.s_Administrador').load();
        MODULO_ADMINISTRACION = panelLoad;
        MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().load();
    },
    onChangeSearchAdministrador: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var panel = Ext.getCmp('panelAdministrador');
            var paramNombres = panel.down('[name=paramBusquedaNombres]').getValue();
            MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().load({
                params: {
                    param: paramNombres
                },
                callback: function (records) {
                    if (records.length <= 0) {
                        MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridAdministrador: function (thisObj, selected, eOpts) {
        if (selected.length > 0) {
           // MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]').getStore().removeAll();
            var formAdministrador = Ext.getCmp('panelAdministrador').down('[name=panelCrearEditarAdministrador]');
            formAdministrador.down('[name=mostraContrasenia]').setVisible(false);
            var comboAplicativo = MODULO_ADMINISTRACION.down('[name=comboAplicativo]');
            comboAplicativo.getStore().proxy.extraParams = '';
            comboAplicativo.getStore().load();
            //aplicativos asignados
           // var gridAplicativo = MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]');
            /* gridAplicativo.getStore().load({
                params: {
                    idAdministrador: selected[0].data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        gridAplicativo.getStore().removeAll();
                    } else {
                        var asignados = [];
                        for (var i in records) {
                            asignados.push(records[i].data.id);
                        }
                        gridAplicativo.getStore().proxy.extraParams = {asignados: asignados.toString()};
                        gridAplicativo.getStore().load();
                    }
                }
            }); */
            formAdministrador.loadRecord(selected[0]);
            if (selected[0].data.imagen === '') {
                formAdministrador.down('[name=image]').setSrc(URL_IMG_SISTEMA + 'usuario.png');
            } else {
                formAdministrador.down('[name=image]').setSrc(URL_IMG + 'uploads/admins/' + selected[0].data.imagen);
            }


        }
    },
    onCreateAdministrador: function () {
        var listaAplicativo = this.getRecordsAplicativo();
        var form = Ext.getCmp('panelAdministrador').down('[name=panelCrearEditarAdministrador]');
        var formUploadImage = Ext.getCmp('panelAdministrador').down('[name=formUploadImage]');
        if (form.isValid()) {
            if (Ext.getStore('s_Modulos').getUpdatedRecords().length === 0) {
                Ext.MessageBox.buttonText = {
                    yes: "Sí",
                    no: "No"
                };
                Ext.MessageBox.confirm('Aviso!', '<b>Aún no ha seleccionado Accesos a este usuario.<br><center>¿desea continuar?</center></b>', function (choice) {
                    if (choice === 'yes') {
                        crearAdministrador(form, formUploadImage, listaAplicativo);
                    } else {
                        showWindowsAccesosAdministrador();
                    }
                });
            } else {
                crearAdministrador(form, formUploadImage);
            }
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    },
    onUpdateAdministrador: function () {
        var listaAplicativo = this.getRecordsAplicativo();
        var form = Ext.getCmp('panelAdministrador').down('[name=panelCrearEditarAdministrador]');
        if (changeImageAdministrador) {
            var formUploadImage = Ext.getCmp('panelAdministrador').down('[name=formUploadImage]');
            formUploadImage.submit({
                url: 'php/Uploads/upload.php?ruta=' + URL_SUBIR_IMG_ADMINS + '&nombre=' + form.down('[name=cedula]').getValue(),
                waitMsg: 'Cargando imagen...',
                success: function (fp, o) {
                    var gridLeerAdministrador = Ext.getCmp('panelAdministrador').down('[name=gridLeerAdministrador]');
                    var gridSelect = gridLeerAdministrador.getSelection();
                    gridSelect[0].set('imagen', o.result.message);

                    var listPermisos = [];
                    var dataAuxPermisosAdmin = Ext.getStore('s_ModuloAdmin_Grid').getUpdatedRecords()
                    for (var i in dataAuxPermisosAdmin) {
                        listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
                    }
                    MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().proxy.extraParams = {
                        permisos: JSON.stringify(listPermisos), listaAplicativo: listaAplicativo
                    };
                    form.updateRecord(form.activeRecord);
                    if (MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().getUpdatedRecords()[0].modified.contrasenia) {
                        gridSelect[0].set('contrasenia', hex_md5(gridSelect[0].data.contrasenia));
                    }
                    MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().sync();
                    notificaciones('CAMBIOS GUARDADOS CORRECTAMENTE', 1);
                    limpiarFormularioAdministrador();
                },
                failure: function (fp, o) {
                    form.down('[name=image]').setSrc(URL_IMG_SISTEMA + 'usuario.png');
                    notificaciones(o.result.message, 2);
                }
            });
        } else {
            if (form.isValid()) {
                var gridLeerAdministrador = Ext.getCmp('panelAdministrador').down('[name=gridLeerAdministrador]');
                var gridSelect = gridLeerAdministrador.getSelection();
                var listPermisos = [];
                var dataAuxPermisosAdmin = Ext.getStore('s_ModuloAdmin_Grid').getUpdatedRecords();
                for (var i in dataAuxPermisosAdmin) {
                    listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
                }

                var listPerfiles = [];
                var dataAuxPerfil = Ext.getStore('s_Perfil_Admin_Grid').data.items;
                for (var i in dataAuxPerfil) {
                    if (dataAuxPerfil[i].data.nuevo) {
                        listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo};
                    } else {
                        listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
                    }
                }
                MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().proxy.extraParams = {
                    permisos: JSON.stringify(listPermisos), perfiles: JSON.stringify(listPerfiles), listaAplicativo: listaAplicativo
                };

                form.updateRecord(form.activeRecord);
                MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore().sync();
                notificaciones('CAMBIOS GUARDADOS CORRECTAMENTE', 1);
                limpiarFormularioAdministrador();
            } else {
                notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
            }
        }
    }
    ,
    onAddAplicativo: function (btn) {
        var comboAplicativo = MODULO_ADMINISTRACION.down('[name=comboAplicativo]');
        var idAplicativo = comboAplicativo.getValue();
        var nombreAplicativo = comboAplicativo.getRawValue();
        var newRecord = {
            id: idAplicativo,
            nombre: nombreAplicativo,
            habilitado: 1,
            nuevo: true
        };

        //MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]').getStore().insert(0, newRecord);
        comboAplicativo.setValue(null);
    },
    getRecordsAplicativo: function () {
        var listAplicativo = [];
        //var dataAuxAplicativo = MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]').getStore().data.items;
        for (var i in dataAuxAplicativo) {
            if (dataAuxAplicativo[i].data.nuevo) {
                listAplicativo[i] = {id: dataAuxAplicativo[i].data.id, habilitado: dataAuxAplicativo[i].data.habilitado, nuevo: dataAuxAplicativo[i].data.nuevo};
            }
        }
        var dataAuxAplicativo = MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]').getStore().getUpdatedRecords();
        for (var i in dataAuxAplicativo) {
            listAplicativo[i] = {id: dataAuxAplicativo[i].data.id, habilitado: dataAuxAplicativo[i].data.habilitado};
        }
        return {listaAplicativo: JSON.stringify(listAplicativo)};
    }

});
function limpiarFormularioAdministrador() {
   // MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]').getStore().removeAll();
    var panel = Ext.getCmp('panelAdministrador');
    panel.down('[name=panelCrearEditarAdministrador]').getForm().reset();
    var gridLeerAdministrador = panel.down('[name=gridLeerAdministrador]');
    gridLeerAdministrador.getView().deselect(gridLeerAdministrador.getSelection());
    gridLeerAdministrador.getStore().reload();
    panel.down('[name=panelCrearEditarAdministrador]').down('[name=image]').setSrc(URL_IMG_SISTEMA + 'usuario.png');
    changeImageAdministrador = false;
    panel.down('[name=mostraContrasenia]').setVisible(true);
    panel.down('[name=msgB]').setVisible(false);
    panel.down('[name=msgR]').setVisible(false);
    panel.down('[name=msgB]').disable();
    panel.down('[name=msgR]').disable();
}
function showWindowsAccesosAdministrador() {
    windowAccesos = new Ext.create('Ext.window.Window', {
        title: 'Accesos',
        closable: true,
        layout: 'fit',
        modal: true,
        height: '75%',
        width: 400,
        items: [
            {
                xtype: 'grid',
                autoScroll: true,
                bufferedRenderer: false,
                store: 's_Modulos',
                cls: 'gridAuxVehiAdmin',
                columnLines: true,
                columns: [
                    {text: "Módulo", width: 100, dataIndex: 'modulo', sortable: true},
                    {
                        xtype: 'checkcolumn',
                        header: 'Leer',
                        dataIndex: 'leer',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    },
                    {
                        xtype: 'checkcolumn',
                        header: 'Crear',
                        dataIndex: 'crear',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    },
                    {
                        xtype: 'checkcolumn',
                        header: 'Editar',
                        dataIndex: 'editar',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    },
                    {
                        xtype: 'checkcolumn',
                        header: 'Eliminar',
                        dataIndex: 'eliminar',
                        width: 50,
                        headerCheckbox: true,
                        menuDisabled: true,
                        stopSelection: false,
                        sortable: false,
                        uncheckedValue: 0,
                        inputValue: 1
                    }
                ],
                forceFit: true,
                split: true,
                region: 'north'
            }
        ],
        buttons: [
            {
                iconCls: 'fa fa-times-circle',
                text: 'Cerrar ',
                tooltip: 'Cerrar y Guardar',
//                width: '100%',
                anchor: '100%',
                style: {
                    background: COLOR_SISTEMA,
                    border: '1px solid #36beb3',
                    '-webkit-border-radius': '5px 5px',
                    '-moz-border-radius': '5px 5px'
                },
                handler: function () {
                    if (Ext.getStore('s_Modulos').getUpdatedRecords().length === 0) {
                        windowAccesos.close();
                        notificaciones('NO SE HAN REALIZADO CAMBIOS', 2);
                    } else {
                        windowAccesos.close();
                    }
                }
            }
        ]
    }).show();
}
function crearAdministrador(form, formUploadImage, listaAplicativo) {
    formUploadImage.submit({
        url: 'php/Uploads/upload.php?ruta=' + URL_SUBIR_IMG_ADMINS + '&nombre=' + form.down('[name=cedula]').getValue(),
        waitMsg: 'Cargando imagen...',
        success: function (fp, o) {
            var record = form.getValues();
            record.imagen = o.result.message;
            record.contrasenia = hex_md5(record.contrasenia);
            var listPermisos = [];
            var dataAuxPermisosAdmin = Ext.getStore('s_ModuloAdmin_Grid').getUpdatedRecords();
            for (var i in dataAuxPermisosAdmin) {
                listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
            }

            var listPerfiles = [];
            var dataAuxPerfil = Ext.getStore('s_Perfil_Admin_Grid').data.items;
            for (var i in dataAuxPerfil) {
                if (dataAuxPerfil[i].data.nuevo) {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo};
                } else {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
                }
            }
            var storeAdministrador = MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore();
            storeAdministrador.proxy.extraParams = {permisos: JSON.stringify(listPermisos), perfiles: JSON.stringify(listPerfiles), listaAplicativo: listaAplicativo};
            storeAdministrador.insert(0, record);
            storeAdministrador.sync();
            limpiarFormularioAdministrador();
            notificaciones('CAMBIOS GUARDADOS CORRECTAMENTE', 1);
        },
        failure: function (fp, action) {
            if (action.result.success === false) {
                notificaciones(action.result.message, 2);
            }
            var record = form.getValues();
            record.imagen = 'usuario.png';
            record.contrasenia = hex_md5(record.contrasenia);
//            if (Ext.getStore('s_Modulos').getUpdatedRecords().length > 0)
//                Ext.getStore('administrador.s_Administrador').proxy.extraParams = {permisos: Ext.util.JSON.encode(getDataPermisos(Ext.getStore('s_Modulos')))};
            var listPermisos = [];
            var dataAuxPermisosAdmin = Ext.getStore('s_ModuloAdmin_Grid').getUpdatedRecords();
            for (var i in dataAuxPermisosAdmin) {
                listPermisos[i] = {id: dataAuxPermisosAdmin[i].data.id, leer: dataAuxPermisosAdmin[i].data.leer, crear: dataAuxPermisosAdmin[i].data.crear, editar: dataAuxPermisosAdmin[i].data.editar, eliminar: dataAuxPermisosAdmin[i].data.eliminar};
            }

            var listPerfiles = [];
            var dataAuxPerfil = Ext.getStore('s_Perfil_Admin_Grid').data.items;
            for (var i in dataAuxPerfil) {
                if (dataAuxPerfil[i].data.nuevo) {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: dataAuxPerfil[i].data.nuevo};
                } else {
                    listPerfiles[i] = {id: dataAuxPerfil[i].data.id, habilitado: dataAuxPerfil[i].data.habilitado, nuevo: false};
                }
            }
            var storeAdministrador = MODULO_ADMINISTRACION.down('[name=gridLeerAdministrador]').getStore();
            storeAdministrador.proxy.extraParams = {permisos: JSON.stringify(listPermisos), perfiles: JSON.stringify(listPerfiles), listaAplicativo: listaAplicativo};
            storeAdministrador.insert(0, record);
            storeAdministrador.sync();
//            limpiarFormularioAdministrador();
        }
    });
}
function getDataPermisosAdministrador(store) {
    var array = [];
    var data = store.getUpdatedRecords();
    for (var i in store.getUpdatedRecords()) {
        array[i] = {id: data[i].data.id, leer: data[i].data.leer, crear: data[i].data.crear, editar: data[i].data.editar, eliminar: data[i].data.eliminar};
    }
    return array;
}

function onAddPerfilAdministrador(btn) {
    var comboPerfil = btn.up('container').items.items[0];
    var idPerfil = comboPerfil.getValue();
    var perfil = comboPerfil.getRawValue();
    var exist = isInStore(Ext.getStore('s_Perfil_Admin_Grid'), idPerfil, 'id', 'exact');
    if (!exist) {
        if (idPerfil) {
            var newRecord = {
                id: idPerfil,
                perfil: perfil,
                habilitado: 1,
                nuevo: true
            };
            Ext.getStore('s_Perfil_Admin_Grid').insert(0, newRecord);
            onCargarModulosPerfilAdministrador();
            comboPerfil.setValue(null);
        } else {
            notificaciones('SELECCIONE UNA EMPRESA', 2);
        }
    } else {
        notificaciones('EL PERFIL YA HA SIDO INGRESADO', 2);
    }
}
function onCargarModulosPerfilAdministrador() {
    var panel = Ext.getCmp('panelAdministrador');
//    panel.down('[name=panelCrearEditarAdministrador]').getForm().reset();
    var gridLeerAdministrador = panel.down('[name=gridLeerAdministrador]');
    var idAdmin = gridLeerAdministrador.getView().getSelection();
    if (idAdmin.length > 0) {
        idAdmin = idAdmin[0].data.id;
    } else {
        idAdmin = 0;
    }
    var records = Ext.getStore('s_Perfil_Admin_Grid').getData().items;
    var perfiles = [];
    for (var i in records) {
        if (records[i].get('habilitado')) {
            perfiles.push(records[i].get('id'));
        }
    }
    if (perfiles.length > 0) {
        Ext.getStore('s_ModuloAdmin_Grid').load({
            params: {
                perfiles: perfiles.toString(),
                idAdmin: idAdmin
            },
            callback: function () {
                if (records.lenth === 0) {
                    Ext.getStore('s_ModuloAdmin_Grid').removeAll();
                }
            }
        });
    } else {
        Ext.getStore('s_ModuloAdmin_Grid').removeAll();
    }
}

