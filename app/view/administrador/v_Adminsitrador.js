var permisosVehiculo, changeImageAdministrador = false, windowAccesos;
Ext.define('Ktaxi.view.administrador.v_Adminsitrador', {
    extend: 'Ext.panel.Panel',
    xtype: 'administrador',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelAdministrador',
    name: 'panelPrincipalAdministrador',
    controller: 'c_Administrador',
    bodyBorder: false,
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.administrador.c_Administrador',
        'Ext.ux.form.MultiSelect'
    ],
    listeners: {
        afterrender: 'onViewAdministrador'
    },
    initComponent: function () {
        var STORE_ADMINISTRADOR = Ext.create('Ktaxi.store.administrador.s_Administrador');
        var STORE_PERFIL_ADMIN = Ext.create('Ktaxi.store.administrador.s_Perfil_Admin_Grid');
        var STORE_MODULO_ADMIN = Ext.create('Ktaxi.store.administrador.s_ModuloAdmin_Grid');
        var STORE_PERFIL_COMBO = Ext.create('Ktaxi.store.combos.s_Perfiles');
        var STORE_APLICATIVO = Ext.create('Ktaxi.store.combos.s_Aplicativos');
        var STORE_APLICATIVO_ADMINISTRADOR = Ext.create('Ktaxi.store.administrador.s_Aplicativo_Administrador');
        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                flex: 3,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [{
                        name: 'gridLeerAdministrador',
                        xtype: 'grid',
                        bufferedRenderer: false,
                        store: STORE_ADMINISTRADOR,
                        defaults: {
                            margin: 0,
                            padding: 0
                        },
                        tbar: [
                            {
                                xtype: 'textfield',
                                flex: 8,
                                tooltip: 'Escribir búsqueda',
                                name: 'paramBusquedaNombres',
                                emptyText: 'Nombres, apellidos..',
                                minChars: 0,
                                typeAhead: true,
                                listeners: {
                                    specialkey: 'onChangeSearchAdministrador'
                                }
                            },
                            {
                                xtype: 'button',
                                iconCls: 'x-fa fa-search',
                                iconAlign: 'right',
                                tooltip: 'Buscar',
                                handler: 'onChangeSearchAdministrador'
                            },
                            {
                                xtype: 'button',
                                iconCls: 'x-fa fa-eraser',
                                iconAlign: 'right',
                                tooltip: 'Limpiar',
                                handler: function () {
//                                    MODULO_ADMINISTRACION.down('[name=gridAdministradorAplicativo]').getStore().removeAll();
//                                    var panel = Ext.getCmp('panelAdministrador');
//                                    panel.down('[name=panelCrearEditarAdministrador]').getForm().reset();
//                                    var panel = Ext.getCmp('panelAdministrador');
//                                    panel.down('[name=paramBusquedaNombres]').reset();
//                                    var gridLeerAdministrador = panel.down('[name=gridLeerAdministrador]');
//                                    gridLeerAdministrador.getView().deselect(gridLeerAdministrador.getSelection());
//                                    gridLeerAdministrador.getStore().clearFilter();
//                                    gridLeerAdministrador.getStore().load();
                                    limpiarFormularioAdministrador();
                                }
                            },
                            {
                                width: '5%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-refresh',
                                iconAlign: 'right',
                                tooltip: 'Recargar',
                                handler: function () {
                                    Ext.getStore('administrador.s_Administrador').reload();
                                }
                            }
                        ],
                        columns: [
                            {tooltip: 'Nombres', text: 'Nombres', dataIndex: 'persona', filter: true, flex: 1, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Cédula', text: 'Cédula', dataIndex: 'cedula', filter: true, flex: 1, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Celular', text: 'Celular', dataIndex: 'celular', filter: true, flex: 1, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Usuario', text: 'Usuario', dataIndex: 'usuario', filter: true, flex: 1, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Correo', text: 'Correo', dataIndex: 'correo', filter: true, flex: 1, cellWrap: true, renderer: showTipConten},
                            {tooltip: 'Bloqueado', text: "Bloqueado", width: 50, dataIndex: 'bloqueado', sortable: true, renderer: formatEstadoInverso},
                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten},
                        ],
                        columnLines: true,
                        forceFit: true,
                        height: 210,
                        split: true,
                        region: 'north',
                        listeners: {
                            selectionchange: 'onSelectChangeGridAdministrador',
                            deselect: function () {
                                var panel = Ext.getCmp('panelAdministrador');
                                panel.down('[name=btnEditar]').disable();
                                panel.down('[name=btnCrear]').enable();
                            },
                            beforeitemclick: function (thisObj, record, item, index, e, eOpts) {
                                var panel = Ext.getCmp('panelAdministrador');
                                panel.down('[name=btnEditar]').enable();
                                panel.down('[name=btnCrear]').disable();
                            },
                            rowdblclick: showAuditoria
                        },
                        viewConfig: {
                            emptyText: '<center>No existen resultados.</center>'
                        }
                    }]
            },
            {
                name: 'panelCrearEditarAdministrador',
                region: 'east',
                xtype: 'form',
                layout: 'fit',
                flex: 2,
                collapsible: false,
                items: [{
                        cls: 'panelFormulario',
                        xtype: 'panel',
                        title: 'Administrador',
                        defaultType: 'textfield',
                        defaults: {
                            width: '100%',
                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                            allowOnlyWhitespace: false,
                            blankText: INFOMESSAGEBLANKTEXT,
                            allowBlank: false,
                            defaultType: 'textfield',
                            labelWidth: 85,
                            defaults: {
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                allowOnlyWhitespace: false,
                                blankText: INFOMESSAGEBLANKTEXT,
                                allowBlank: false,
                                labelWidth: 85,
                                width: '50%',
                                defaults: {
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    allowOnlyWhitespace: false,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowBlank: false,
                                    labelWidth: 85,
                                    width: '50%'
                                }
                            }
                        },
                        items: [
                            {
                                xtype: 'form',
                                style: 'text-align:center',
                                name: 'formUploadImage',
                                items: [
                                    {
                                        xtype: 'image', src: URL_IMG_SISTEMA + 'usuario.png', name: 'image', height: 100, width: 100, style: 'text-align:center; border-radius: 50%; border: solid; border-color: #5ecac2;background-size: auto 100%;background-position: center;', listeners: {render: function (me) {
                                                me.el.on({error: function (e, t, eOmpts) {
                                                        me.setSrc(URL_IMG_SISTEMA + 'usuario.png');
                                                    }});
                                            }}
                                    },
                                    {
                                        xtype: 'filefield',
                                        allowOnlyWhitespace: true,
                                        buttonOnly: true,
                                        iconCls: 'x-fa fa-camera',
                                        name: 'photo',
                                        msgTarget: 'side',
                                        allowBlank: true,
                                        width: '100%',
                                        anchor: '100%',
                                        buttonConfig: {
                                            text: '',
                                            iconCls: 'x-fa fa-camera'
                                        },
                                        listeners: {
                                            afterrender: function (cmp) {
                                                cmp.fileInputEl.set({
                                                    accept: '.png, .jpg, .jpeg'
                                                });
                                            },
                                            change: function (evt, a, b) {
                                                changeImageAdministrador = true;
                                                var panel = Ext.getCmp('panelAdministrador');
                                                var form = panel.down('[name=panelCrearEditarAdministrador]');
                                                var fieldImage = form.down('[name=image]');
                                                if (evt.fileInputEl.dom.files && evt.fileInputEl.dom.files) {
                                                    var reader = new FileReader();
                                                    reader.onload = function (e) {
                                                        fieldImage.setSrc(e.target.result);
                                                    };
                                                    reader.readAsDataURL(evt.fileInputEl.dom.files[0]);
                                                }
                                            }
                                        }
                                    }
                                ]
                            },
                            {xtype: 'button', text: 'Accesos', anchor: '100%', width: '100%', height: 30, cls: 'btnAccesos', handler:
                                        function () {
                                            var panel = Ext.getCmp('panelAdministrador');
                                            var gridLeerAdministrador = panel.down('[name=gridLeerAdministrador]');
                                            var gridSelect = gridLeerAdministrador.getSelection();
                                            var idAdmin = 0;
                                            if (gridSelect.length > 0) {
                                                idAdmin = gridSelect[0].data.id;
                                            }
                                            STORE_PERFIL_ADMIN.load({
                                                params: {
                                                    param: idAdmin
                                                },
                                                callback: function (records, operation, success) {
                                                    if (success) {
                                                        STORE_MODULO_ADMIN.removeAll();
                                                        if (records.length === 0) {
                                                            STORE_PERFIL_ADMIN.removeAll();
                                                        } else {
                                                            onCargarModulosPerfilAdministrador();
                                                        }
                                                        dialog_permisos = Ext.create('Ext.window.Window', {
                                                            height: '100%',
                                                            width: 700,
                                                            title: 'Accesos',
                                                            layaut: 'anchor',
                                                            autoScroll: true,
                                                            closable: true,
                                                            modal: true,
                                                            items: [
                                                                {
                                                                    tbar: [
                                                                        {
                                                                            flex: 1,
                                                                            xtype: 'container',
                                                                            layout: 'hbox',
                                                                            defaultType: 'combobox',
                                                                            items: [
                                                                                {
                                                                                    xtype: 'combobox',
                                                                                    tooltip: 'Seleccionar perfil',
                                                                                    name: 'comboBuscarPerfil',
                                                                                    emptyText: 'Seleccionar perfil',
                                                                                    displayField: 'text',
                                                                                    valueField: 'id',
                                                                                    filterPickList: true,
                                                                                    queryParam: 'param',
                                                                                    queryMode: 'remote',
                                                                                    flex: 2,
                                                                                    minChars: 1,
                                                                                    store: STORE_PERFIL_COMBO
                                                                                },
                                                                                {xtype: 'button', text: 'Nuevo perfil', name: 'agregarPerfil', flex: 1, iconCls: 'x-fa fa-plus', height: 23, handler: onAddPerfilAdministrador}
                                                                            ]
                                                                        }
                                                                    ],
                                                                    xtype: 'grid',
                                                                    autoScroll: true,
                                                                    bufferedRenderer: false,
                                                                    height: 150,
                                                                    anchor: '100% 100%',
                                                                    name: 'gridPerfilesAdmin',
//                                                            store: 's_Perfil_Admin_Grid',
                                                                    store: STORE_PERFIL_ADMIN,
                                                                    cls: 'gridAuxVehiAdmin',
                                                                    columns: [
                                                                        {header: 'Habilitado', flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro},
                                                                        {header: 'Perfil', flex: 2, dataIndex: 'perfil', sortable: true},
                                                                        {
                                                                            menuDisabled: true,
                                                                            sortable: false,
                                                                            xtype: 'actioncolumn',
                                                                            flex: 1,
                                                                            minWidth: 20,
                                                                            items: [{
                                                                                    getClass: function (v, meta, rec) {
                                                                                        if (rec.data.habilitado) {
                                                                                            return 'gridAuxDelete x-fa fa-times';
                                                                                        } else {
                                                                                            if (rec.data.nuevo) {
                                                                                                return 'gridAuxDelete x-fa fa-times';
                                                                                            } else {
                                                                                                return 'gridAuxCheck x-fa fa-check';
                                                                                            }
                                                                                        }
                                                                                    },
                                                                                    handler: function (grid, rowIndex, colIndex) {
                                                                                        var rec = grid.getStore().getAt(rowIndex);
                                                                                        if (rec.data.nuevo) {
                                                                                            STORE_PERFIL_ADMIN.remove(rec);
//                                                                                    Ext.getStore('s_Perfil_Admin_Grid').remove(rec);
                                                                                        } else {
                                                                                            if (rec.data.habilitado) {
                                                                                                rec.set('habilitado', 0);
                                                                                            } else {
                                                                                                rec.set('habilitado', 1);
                                                                                            }
                                                                                        }
                                                                                        onCargarModulosPerfilAdministrador();
                                                                                    }
                                                                                }]
                                                                        }
                                                                    ],
                                                                    split: true,
                                                                    viewConfig: {
                                                                        enableTextSelection: true,
                                                                        emptyText: '<center>Sin perfiles asignados.</center>',
                                                                        getRowClass: function (record) {
                                                                            if (record.data.nuevo) {
                                                                                return 'newRowGrid';
                                                                            }
                                                                        }
                                                                    },
                                                                    listeners: {
                                                                        rowdblclick: function (grid, record) {
                                                                            if (!record.data.nuevo) {
                                                                                showAuditoria(grid, record, 'gridAux');
                                                                            }
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'grid',
                                                                    name: 'gridModuloAdmin',
                                                                    anchor: '100% 100%',
                                                                    autoScroll: true,
                                                                    bufferedRenderer: false,
                                                                    store: STORE_MODULO_ADMIN,
//                                                            store: 's_ModuloAdmin_Grid',
                                                                    cls: 'gridAuxVehiAdmin',
                                                                    plugins: [{ptype: 'gridfilters'}],
                                                                    columnLines: true,
                                                                    minHeight: 120,
                                                                    columns: [
                                                                        {text: "Módulo", flex: 3, dataIndex: 'modulo', sortable: true, filter: true},
                                                                        {
                                                                            xtype: 'checkcolumn',
                                                                            header: 'Leer',
                                                                            dataIndex: 'leer',
                                                                            flex: 1,
                                                                            headerCheckbox: true,
                                                                            menuDisabled: true,
                                                                            stopSelection: false,
                                                                            sortable: false,
                                                                            uncheckedValue: 0,
                                                                            inputValue: 1
                                                                        },
                                                                        {
                                                                            xtype: 'checkcolumn',
                                                                            header: 'Crear',
                                                                            dataIndex: 'crear',
                                                                            flex: 1,
                                                                            headerCheckbox: true,
                                                                            menuDisabled: true,
                                                                            stopSelection: false,
                                                                            sortable: false,
                                                                            uncheckedValue: 0,
                                                                            inputValue: 1
                                                                        },
                                                                        {
                                                                            xtype: 'checkcolumn',
                                                                            header: 'Editar',
                                                                            dataIndex: 'editar',
                                                                            flex: 1,
                                                                            headerCheckbox: true,
                                                                            menuDisabled: true,
                                                                            stopSelection: false,
                                                                            sortable: false,
                                                                            uncheckedValue: 0,
                                                                            inputValue: 1
                                                                        },
                                                                        {
                                                                            xtype: 'checkcolumn',
                                                                            header: 'Eliminar',
                                                                            dataIndex: 'eliminar',
                                                                            flex: 1,
                                                                            headerCheckbox: true,
                                                                            menuDisabled: true,
                                                                            stopSelection: false,
                                                                            sortable: false,
                                                                            uncheckedValue: 0,
                                                                            inputValue: 1
                                                                        }
                                                                    ],
                                                                    split: true,
                                                                    region: 'north',
                                                                    viewConfig: {
                                                                        deferEmptyText: false,
                                                                        enableTextSelection: true,
                                                                        emptyText: '<center>Sin módulos asignados.</center>'
                                                                    }
                                                                }
                                                            ],
                                                            buttons: ['->',
                                                                {
                                                                    xtype: 'button',
                                                                    iconCls: 'fas fa-times-circle',
                                                                    iconAlign: 'right',
                                                                    text: 'Cerrar',
                                                                    tooltip: 'Cerrar',
                                                                    style: {
                                                                        background: COLOR_SISTEMA,
                                                                        border: '1px solid #36beb3',
                                                                        '-webkit-border-radius': '5px 5px',
                                                                        '-moz-border-radius': '5px 5px'
                                                                    },
                                                                    handler: function () {
                                                                        dialog_permisos.close();
                                                                    }
                                                                }
                                                            ]
                                                        }).show();
                                                    } else {
                                                        notificaciones('Su conexión a internet es demasiada lenta', 2);
                                                    }
                                                }
                                            });

                                        }},
//                            {xtype: 'button', text: 'Accesos', anchor: '100%', width: '100%', height: 30, cls: 'btnAccesos',
//                                handler: function () {
//                                    var panel = Ext.getCmp('panelAdministrador');
//                                    var gridLeerAdministrador = panel.down('[name=gridLeerAdministrador]');
//                                    var gridSelect = gridLeerAdministrador.getSelection();
//                                    var idAdmin = 0;
//                                    if (gridSelect.length > 0) {
//                                        idAdmin = gridSelect[0].data.id;
//                                    }
//                                    Ext.getStore('s_Modulos').load({
//                                        params: {
//                                            idAdmin: idAdmin
//                                        },
//                                        callback: function (records, operation, success) {
//                                            if (success === true) {
//                                                showWindowsAccesos();
//                                            } else {
//                                                notificaciones('Su conexión a internet es demasiada lenta', 2);
//                                            }
//                                        }
//                                    });
//                                }
//                            },
                            {
                                flex: 1,
                                xtype: 'container',
                                layout: 'hbox',
                                margin: '10 0 5 0',
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Cédula',
                                        name: 'cedula',
                                        emptyText: 'Cédula',
                                        maskRe: /[0-9.]/,
                                        maxLength: '10',
                                        minLength: '10',
                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                        maxLengthText: MAXIMUMMESSAGUEREQURID,
                                        flex: 8
                                    },
                                    {xtype: 'button', name: 'cedula', flex: 1, iconCls: 'x-fa fa-search', height: 23,
                                        handler: function () {
                                            var panel = Ext.getCmp('panelAdministrador');
                                            buscaPersonaRegCivil(panel.down('[name=panelCrearEditarAdministrador]'));
                                        }}
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'container',
                                layout: 'hbox',
                                margin: '10 0 5 0',
                                items: [
                                    {
                                        fieldLabel: 'Nombres',
                                        name: 'nombres',
                                        emptyText: 'Nombres',
                                        maxLength: '125',
                                        minLength: '4',
                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                        maxLengthText: MAXIMUMMESSAGUEREQURID,
                                    }, {
                                        margin: '0 0 0 5',
                                        fieldLabel: 'Apellidos',
                                        name: 'apellidos',
                                        emptyText: 'Apellidos',
                                        maxLength: '125',
                                        minLength: '4',
                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                        maxLengthText: MAXIMUMMESSAGUEREQURID,
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'container',
                                layout: 'hbox',
                                margin: '10 0 5 0',
                                items: [
                                    {
                                        fieldLabel: 'Nacimiento',
                                        maskRe: /[0-9.]/,
                                        name: 'fNacimiento',
                                        maxLength: '10',
                                        minLength: '10',
                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                        maxLengthText: MAXIMUMMESSAGUEREQURID,
                                        xtype: 'datefield',
                                        value: new Date(),
                                        format: 'd/m/Y',
                                        submitFormat: 'Y-m-d'
                                    }, {
                                        margin: '0 0 0 5',
                                        fieldLabel: 'Celular',
                                        name: 'celular',
                                        emptyText: 'Celular',
                                        minLength: '10',
                                        maxLength: '10',
                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                        maxLengthText: MAXIMUMMESSAGUEREQURID,
                                        maskRe: /[0-9.]/
                                    }
                                ]
                            },
                            {
                                flex: 1,
                                xtype: 'container',
                                layout: 'hbox',
                                margin: '10 0 5 0',
                                items: [
                                    {
                                        fieldLabel: 'Usuario',
                                        name: 'usuario',
                                        emptyText: 'Usuario',
                                        minLength: '4',
                                        maxLength: '15',
                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                        maxLengthText: MAXIMUMMESSAGUEREQURID,
                                        maskRe: new RegExp("^[a-zA-Z1-9_]+$"),
                                        listeners: {
                                            change: function (thisObj, e, eOpts) {
                                                thisObj.setValue(e.toLowerCase());
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'container',
                                        layout: 'hbox',
                                        margin: '0 0 5 5',
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                flex: 9,
                                                fieldLabel: 'Contraseña',
                                                inputType: 'password',
                                                name: 'contrasenia',
                                                emptyText: 'Contraseña',
                                                minLength: '4'
                                            },
                                            {xtype: 'button', name: 'mostraContrasenia', flex: 1, iconCls: 'x-fa fa-eye', enableToggle: true, height: 23,
                                                handler: function (btn) {
                                                    var panel = Ext.getCmp('panelAdministrador');
                                                    var form = panel.down('[name=panelCrearEditarAdministrador]');
                                                    if (btn.pressed) {
                                                        form.getForm().findField('contrasenia').inputEl.dom.type = 'text';
                                                    } else {
                                                        form.getForm().findField('contrasenia').inputEl.dom.type = 'password';
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                fieldLabel: 'Dirección',
                                name: 'direccion',
                                emptyText: 'Dirección',
                                maxLength: '300',
                                minLength: '10',
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                            },
                            {
                                vtype: 'email',
                                fieldLabel: 'Correo',
                                name: 'correo',
                                emptyText: 'Correo',
                                maxLength: '125',
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                            },
                            {
                                boxLabel: 'Bloqueado',
                                xtype: 'checkbox',
                                allowBlank: true,
                                allowOnlyWhitespace: true,
                                uncheckedValue: 0,
                                inputValue: 1,
                                name: 'bloqueado',
                                labelAlign: 'right',
                                listeners: {
                                    change: function (thisO, newValue, oldValue, eOpts) {
                                        var panel = Ext.getCmp('panelAdministrador');
                                        var gridLeerAdministrador = panel.down('[name=gridLeerAdministrador]');
                                        var gridSelect = gridLeerAdministrador.getSelection();
                                        if (gridSelect.length > 0) {
                                            if (gridSelect[0].data.bloqueado === true) {//ESTA BLOQUADO
                                                panel.down('[name=msgR]').setVisible(!newValue);
                                                panel.down('[name=msgR]').setDisabled(newValue);
//                                                panel.down('[name=msgR]').enable(!newValue);
                                            }
                                            if (gridSelect[0].data.bloqueado === false) {//NO ESTA BLOQUADO
                                                panel.down('[name=msgB]').setVisible(newValue);
                                                panel.down('[name=msgB]').setDisabled(!newValue);
//                                                panel.down('[name=msgB]').disable();
                                            }
                                        }
                                    }
                                }
                            },
                            {
                                xtype: 'textareafield',
                                name: 'msgB',
                                fieldLabel: 'Razón de bloqueo',
                                anchor: '100%',
                                maxLength: '400',
                                minLength: '10',
                                allowBlank: false,
                                disabled: true,
                                allowOnlyWhitespace: true,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                hidden: true
                            },
                            {
                                xtype: 'textareafield',
                                name: 'msgR',
                                fieldLabel: 'Razón de reactivación',
                                anchor: '100%',
                                maxLength: '400',
                                minLength: '10',
                                hidden: true,
                                disabled: true,
                                allowBlank: false,
                                allowOnlyWhitespace: true,
                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                maxLengthText: MAXIMUMMESSAGUEREQURID
                            },
                            /* {
                                xtype: 'panel',
                                layout: 'hbox',
                                width: '100%',
                                defaultType: 'combobox',
                                defaults: {
                                    margin: 1
                                },
                                items: [
                                    {
                                        name: 'comboAplicativo',
                                        tooltip: 'Buscar Aplicativo',
                                        xtype: 'combobox',
                                        emptyText: 'Seleccione',
                                        fieldLabel: 'Aplicativo',
                                        displayField: 'text',
                                        minChars: 0,
                                        typeAhead: true,
                                        valueField: 'id',
                                        queryParam: 'param',
                                        queryMode: 'remote',
                                        maxLength: '150',
                                        store: STORE_APLICATIVO
                                    },
                                    {
                                        xtype: 'button',
                                        name: 'agregarAplicativo',
                                        flex: 1,
                                        iconCls: 'x-fa fa-plus',
                                        height: 23,
                                        handler: 'onAddAplicativo'}
                                ]
                            },
                            {
                                xtype: 'grid',
                                height: 50,
                                width: '100%',
                                name: 'gridAdministradorAplicativo',
                                autoScroll: true,
                                bufferedRenderer: false,
                                store: STORE_APLICATIVO_ADMINISTRADOR,
                                cls: 'gridAuxAdmAplicativo',
                                columns: [
                                    {flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoRegistro},
                                    {flex: 2, dataIndex: 'nombre', sortable: true},
                                    {
                                        xtype: 'actioncolumn',
                                        menuDisabled: true,
                                        sortable: false,
                                        minWidth: 20,
                                        flex: 1,
                                        items: [{
                                                getClass: function (v, meta, rec) {
                                                    if (rec.data.habilitado) {
                                                        return 'gridAuxDelete x-fa fa-times';
                                                    } else {
                                                        return 'gridAuxCheck x-fa fa-check';
                                                    }
                                                },
                                                handler: function (grid, rowIndex, colIndex) {
                                                    var rec = grid.getStore().getAt(rowIndex);
                                                    if (rec.data.nuevo) {
                                                        grid.getStore().remove(rec);
                                                    } else {
                                                        if (rec.data.habilitado) {
                                                            rec.set('habilitado', 0);
                                                        } else {
                                                            rec.set('habilitado', 1);
                                                        }
                                                    }
                                                }
                                            }]
                                    }
                                ],
                                minHeight: 150,
                                split: true,
                                region: 'north',
                                viewConfig: {enableTextSelection: true,
                                    emptyText: '<center>Sin aplicativos asignados..</center>',
                                    getRowClass: function (record) {
                                        if (record.data.nuevo) {
                                            return 'newRowGrid';
                                        }
                                    }
                                },
                                listeners: {
                                    rowdblclick: function (grid, record) {
                                        if (!record.data.nuevo) {
                                            showAuditoria(grid, record, 'gridAux');
                                        }
                                    }
                                }
                            } */
                        ]
                    }],
                dockedItems: [{
                        ui: 'footer',
                        xtype: 'toolbar',
                        dock: 'bottom',
                        defaults: {
                            width: '25%'
                        },
                        items: [
                            {
                                text: 'Limpiar',
                                tooltip: 'Limpiar',
                                disabled: false,
                                handler: function () {
                                    limpiarFormularioAdministrador();
                                }
                            },
                            '->',
                            {
                                text: 'Editar',
                                tooltip: 'Actualizar',
                                disabled: true,
                                name: 'btnEditar',
                                handler: 'onUpdateAdministrador'
                            }, {
                                text: 'Crear',
                                tooltip: 'Crear Administradores',
                                disabled: true, name: 'btnCrear',
                                handler: 'onCreateAdministrador'
                            }]
                    }]
            },
        ];
        this.callParent(arguments);
    }
});
