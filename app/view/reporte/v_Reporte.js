/* global MAXIMUMMESSAGUEREQURID, INFOMESSAGEREQUERID, MINIMUMMESSAGUEREQUERID, INFOMESSAGEBLANKTEXT, showAuditoria, HEIGT_VIEWS, Ext, EMPTY_CARA, showTipConten, showTipContenID, formatEstadoRegistro */
Ext.define('Ktaxi.view.reporte.v_Reporte', {
    extend: 'Ext.panel.Panel',
    xtype: 'reporte',
    controller: 'c_Reporte',
    height: HEIGT_VIEWS,
    layout: 'border',
    store: 'reporte.s_Reporte',
    requires: ['Ktaxi.view.reporte.c_Reporte'],
    bodyBorder: false,
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true,
        bodyPadding: 0
    },
    listeners: {
        afterrender: 'onView'
    },
    initComponent: function () {
        var STORE_ALUMNO = Ext.create('Ktaxi.store.reporte.s_Reporte');
        this.items = [{
                xtype: 'panel',
                region: 'center',
                width: '65%',
                collapseMode: 'mini',
                margin: 5,
                header: false,
                collapsible: true,
                collapsed: false,
                layout: 'fit',
                items: [{
                    xtype: 'grid',
                    name: 'grid',
                    height: HEIGT_VIEWS - 10,
                    plugins: [{
                        ptype: 'gridfilters'
                    }],
                    bufferedRenderer: false,
                    store: STORE_ALUMNO,
                    viewConfig: {
                        deferEmptyText: false,
                        enableTextSelection: true,
                        preserveScrollOnRefresh: true,
                        listeners: {
                            loadingText: 'Cargando...'
                        },
                        loadMask: true,
                        emptyText: '<center><h1 style="margin:20px">No existen resultados</h1></center>' + EMPTY_CARA
                    },
                    tbar: [{
                            xtype: 'textfield',
                            name: 'txtParam',
                            width: '50%',
                            emptyText: 'Correo,mensaje,Reporte,usuario...',
                            listeners: {
                                specialkey: 'onBuscar'
                            }
                        },
                        {
                            xtype: 'combobox',
                            name: 'cmbxTipo',
                            emptyText: 'Clase...',
                            width: '28%',
                            valueField: 'id',
                            queryParam: 'filtro',
                            queryMode: 'remote',
                            displayField: 'texto',
                            // store: Ext.create('Ktaxi.store.stores.s_Usuario_Acceso_Api')
                        },
                        //                            {
                        //                                xtype: 'combobox',
                        //                                label: 'Choose State',
                        //                                queryMode: 'local',
                        //                                width: '28%',
                        //                                displayField: 'name',
                        //                                valueField: 'abbr',
                        //
                        //                                store: [
                        //                                    {abbr: 'AL', name: 'Alabama'},
                        //                                    {abbr: 'AK', name: 'Alaska'},
                        //                                    {abbr: 'AZ', name: 'Arizona'}
                        //                                ]
                        //                            },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-search',
                            tooltip: 'Buscar',
                            handler: 'onBuscar'
                        },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-eraser',
                            tooltip: 'Limpiar buscador',
                            handler: 'onLimpiar'
                        },
                        {
                            xtype: 'button',
                            width: '7%',
                            iconCls: 'x-fa fa-refresh',
                            tooltip: 'Recargar',
                            handler: 'onRecargar'
                        }
                    ],
                    features: [{
                        ftype: 'grouping',
                        groupHeaderTpl: '{name}',
                        hideGroupedHeader: true,
                        enableGroupingMenu: true
                    }],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer', {
                            header: '#',
                            width: 30,
                            align: 'center'
                        }),
                        {
                            dataIndex: 'nombre',
                            text: 'Nombre',
                            tooltip: "Nombre",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'apellido',
                            text: 'Apellido',
                            tooltip: "Apellido",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'correo',
                            text: 'Correo',
                            tooltip: "Correo",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'nombreClase',
                            text: 'Clase',
                            tooltip: "Clase",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'eva1',
                            text: 'Evaluación 1',
                            tooltip: "Evaluación 1",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'eva2',
                            text: 'Evaluación 2',
                            tooltip: "Evaluación 2",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'eva3',
                            text: 'Evaluación 3',
                            tooltip: "Evaluación 3",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        {
                            dataIndex: 'promedio',
                            text: 'Promedio',
                            tooltip: "Promedio",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipConten
                        },
                        //{dataIndex: 'mensaje', text: 'Mensaje', tooltip: "Mensaje", filter: true, flex: 1, sortable: true, renderer: showTipConten},
                        //{dataIndex: 'ReporteCorreo', text: 'Reporte', tooltip: "Reporte del Correo", filter: true, flex: 1, sortable: true, renderer: showTipConten},
                        //{flex: 1, tooltip: 'Fecha Registro', xtype: 'datecolumn', header: "Fecha", dataIndex: 'fechaRegistro', sortable: true, format: 'Y/m/d'},
                        //{flex: 1, tooltip: 'Hora', xtype: 'datecolumn', header: "Hora", dataIndex: 'fechaRegistro', sortable: true, format: 'H:i:s'},
                        //                            {text: 'Estado', menuDisabled: true, sortable: false, xtype: 'actioncolumn', width: 40, minWidth: 40, items: [
                        ////                                  
                        //                                    {
                        //                                        xtype: 'button',
                        //                                        tooltip: 'Confimar',
                        //                                        iconCls: 'x-fa fa-check-square',
                        //                                        text: 'Confirmar',
                        //                                        name: 'confimar',
                        //                                        value: 'Confimar',
                        //                                        handler: 'showReporteConfirmar',
                        //                                        //getClass: 'onGetClass'
                        //                                    }
                        //                                ]
                        //                            },
                        {
                            text: 'Estado',
                            menuDisabled: true,
                            sortable: false,
                            xtype: 'actioncolumn',
                            width: 40,
                            minWidth: 40,
                            items: [{
                                xtype: 'button',
                                //style: 'color="red!important"',
                                name: 'btnFinalizarJornada',
                                tooltip: 'Confirmar o Validar',
                                iconCls: 'x-fa fa-ban',
                                isDisabled: 'onIsDisabledBtnFinalizarJornada',
                                handler: 'onVenatna',
                                getClass: 'onGetClass'
                            }]
                        },
                        {
                            dataIndex: 'id',
                            text: "ID",
                            tooltip: "ID",
                            filter: true,
                            flex: 1,
                            sortable: true,
                            renderer: showTipContenID,
                            hidden: true
                        }
                    ],
                    listeners: {
                        //                            select: 'onSelect',
                        //rowdblclick: showAuditoriaReporte,

                    },
                    onButtonWidgetClick: function (btn) {
                        var rec = btn.getViewModel().get('record');
                        Ext.Msg.alert("Button clicked", "Hey! " + rec.get('name'));
                    },
                    bbar: Ext.create('Ext.PagingToolbar', {
                        //store: STORE_ALUMNO,
                        displayInfo: true,
                        name: 'paginacionGrid',
                        emptyMsg: "Sin datos que mostrar.",
                        displayMsg: 'Visualizando {0} - {1} de {2} registros',
                        beforePageText: 'Página',
                        afterPageText: 'de {0}',
                        firstText: 'Primera página',
                        prevText: 'Página anterior',
                        nextText: 'Siguiente página',
                        lastText: 'Última página',
                        refreshText: 'Actualizar',
                        inputItemWidth: 35,
                        items: [{
                            xtype: 'button',
                            text: 'Exportar',
                            iconCls: 'x-fa fa-download',
                            handler: function (btn) {
                                onExportar(btn, "Reporte", this.up('grid'));
                            }
                        }],
                        listeners: {
                            afterrender: function () {
                                this.child('#refresh').hide();
                            }
                        }
                    })
                }]
            },
        ];
        this.callParent(arguments);
    }
});