/* global Ext, MENSAJE_ERROR, EMPTY_CARA, moment */
var MODULO_ALUMNO;
Ext.define('Ktaxi.view.reporte.c_Reporte', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Reporte',
    onView: function (panelLoad) {
        MODULO_ALUMNO = panelLoad;
        MODULO_ALUMNO.down('[name=grid]').getStore().load();
        validarPermisosGeneral(MODULO_ALUMNO);
    },
    onRecargar: function () {
        MODULO_ALUMNO.down('[name=grid]').getStore().reload();
    },
    onLimpiar: function (btn, e) {
        MODULO_ALUMNO.down('[name=cmbxTipo]').reset();
        MODULO_ALUMNO.down('[name=cmbxTipoEstado]').reset();
        MODULO_ALUMNO.down('[name=txtParam]').reset();
        MODULO_ALUMNO.down('[name=idUsuario]').reset();
//        MODULO_ALUMNO.down('[name=hasta]').reset();

        if (btn.limpiar) {
            MODULO_ALUMNO.down('[name=form]').getForm().reset();
//             MODULO_ALUMNO.down('[name=idUsuario]').getStore().removeAll();
            var grid = MODULO_ALUMNO.down('[name=grid]');
            grid.getView().deselect(grid.getSelection());
            validarPermisosGeneral(MODULO_ALUMNO);
        }
        MODULO_ALUMNO.down('[name=grid]').getStore().load();
    },
    onBuscar: function (btn, e) {
        var tipo = MODULO_ALUMNO.down('[name=cmbxTipo]').getValue(), txtParam = MODULO_ALUMNO.down('[name=txtParam]').getValue(), params = {};
        var estado = MODULO_ALUMNO.down('[name=cmbxTipoEstado]').getValue();

        if (tipo)
            params['idUsuario'] = tipo;
        params['idEstado'] = estado;
        params['param'] = txtParam;
        //MODULO_ALUMNO.down('[name=paginacionGrid]').moveFirst();
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            MODULO_ALUMNO.down('[name=grid]').getStore().load({
                params: params,
                callback: function (records, operation, success) {
                    if (!success)
                        setMensajeGridEmptyText(MODULO_ALUMNO.down('[name=grid]'), EMPTY_CARA + '<h3>' + MENSAJE_ERROR + '</h3>');
                    else if (records.length === 0)
                        setMensajeGridEmptyText(MODULO_ALUMNO.down('[name=grid]'), EMPTY_CARA + '<h3>No existen resultados.</h3>');
                }
            });
        }
    },
    onSelect: function (grid, selected, eOpts) {
        setPermisos(MODULO_ALUMNO);
        MODULO_ALUMNO.down('[name=form]').getForm().loadRecord(selected);
    },
    onCrear: function () {
        var me = this, form = MODULO_ALUMNO.down('[name=form]').getForm();
        if (form.isValid()) {
            MODULO_ALUMNO.down('[name=grid]').getStore().insert(0, form.getValues());
            MODULO_ALUMNO.down('[name=grid]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiar({limpiar: true}));
                }});
        } else {
            mensajesValidacionForms(form.getFields());
        }
    },
    onEditar: function () {
        var me = this, form = MODULO_ALUMNO.down('[name=form]').getForm();
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            MODULO_ALUMNO.down('[name=grid]').getStore().sync({
                callback: function (response) {
                    onProcesarPeticion(response, me.onLimpiar({limpiar: true}));
                }
            });
        } else {

            mensajesValidacionForms(form.getFields());
        }
    },
    controlarReporte: function (combo, record, eOpts) {

        var fecha = moment(record.data.date).add(15, 'minutes').format('HH:mm:ss');
        MODULO_ALUMNO.down('[name=hasta]').setValue(fecha);
        MODULO_ALUMNO.down('[name=hasta]').setMinValue(fecha);
        MODULO_ALUMNO.down('[name=hasta]').setDisabled(false);
    },
    onGetClass: function (v, meta, rec) {
        if (rec.data.validada === 1 && rec.data.confimada === 1) {
//            this.items[0].tooltip = 'validar';
            return 'x-fa fa-thumbs-o-up';

        } else if (rec.data.confimada === 0) {
//            this.items[0].tooltip = 'confimar';
            return 'x-fa fa-check';
        } else {
            return 'x-fa fa-money';
        }
    },

    onVenatna: function (grid, rowIndex, colIndex, event, cell) {
        if (cell.record.data.confimada === 0) {
            this.showReporteConfirmar(grid, rowIndex, colIndex, event, cell);
        } else if (cell.record.data.validada === 0) {
            this.showReporteValidar(grid, rowIndex, colIndex, event, cell);
        }
        if (cell.record.data.confimada === 1 && cell.record.data.validada === 1) {
            notificaciones('El registro ya se encuentra confirmado y validado', 5);
        }
    },

    showReporteConfirmar: function (grid, rowIndex, colIndex, event, cell) {
        var window = new Ext.Window({
            width: 450,
            title: 'Reporte',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    hidden: true,
                    name: 'mensajeReporte',
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + cell.record.data.nombreR + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            html: 'Ingrese el Reporte mostrado en el correo para el usuario <b>' + cell.record.data.nombreR + ' ' + cell.record.data.apellidoR + '</b>.<br>Valor ingresado en transcripción es: <b>$' + cell.record.data.ReporteCorreo + ' USD</b>'
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: '<b>Valor</b>',
                            name: 'confCorreo',
                            minValue: 1
                        }
                    ]
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        window.close();
                    }
                },
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Continuar',
                    tooltip: 'Continuar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        var ReporteCorreoRegistro = window.down('[name=confCorreo]').getValue();
                        var ReporteIncial = cell.record.data.ReporteCorreo;
                        if (ReporteIncial === ReporteCorreoRegistro) {
                            window.close();
                            windowConfirmar.show();
                        } else {
                            notificaciones('El monto ingresado no corresponde con el registrado en el correo', 5);
                            //window.close();
                        }
                    }
                }
            ]
        });
        var windowConfirmar = new Ext.Window({
            width: 450,
            title: 'Reporte',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    hidden: true,
                    name: 'confirmarReporte',
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + cell.record.data.correo + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            html: '¿Usted esta a punto de confirmar un ingreso de <b>$' + cell.record.data.ReporteCorreo + ' USD </b> para el usuario <b>' + cell.record.data.nombreR + ' ' + cell.record.data.apellidoR + '</b>?'
                        }
                    ]
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Cerrar',
                    tooltip: 'Cerrar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        windowConfirmar.close();
                    }
                },
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Confirmar',
                    tooltip: 'Confirmar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        Ext.Ajax.request({
                            async: true,
                            url: 'php/Reporte/update.php',
                            params: {
                                id: cell.record.data.id,
                                idUsuario: cell.record.data.idUsuario,
                                ReporteCorreo: cell.record.data.ReporteCorreo,
                                confirmada: 1,
                                validada: 0,
                            },
                            callback: function (callback, e, response) {
                                var res = JSON.parse(response.request.result.responseText);
                                if (res.success) {
                                    notificaciones('El registro ha sido confirmado', 1);
                                } else {
                                    notificaciones('Lo sentimos hubo un error al validar el registro', 2);
                                }
                                MODULO_ALUMNO.down('[name=grid]').getStore().reload();
                            }
                        });

                        windowConfirmar.close();
//                        notificaciones('El registro ya se encuentra confirmado', 5);

                    }
                }

            ]
        });
        if (cell.record.data.confimada === 1) {
            notificaciones('El registro ya se encuentra confirmado', 5);
        } else {
            window.show();
        }


    },
    showReporteValidar: function (grid, rowIndex, colIndex, event, cell) {
        var windowValidar = new Ext.Window({
            width: 450,
            title: 'Validar Reporte',
            bodyPadding: 10,
            constrain: true,
            closable: true,
            layout: 'fit',
            modal: true,
            items: [
                {
                    hidden: true,
                    name: 'validarReporte',
                    html: '<center style="background-color:#074975; color:white;">Registro con ID:' + cell.record.correo + '</center>'
                },
                {
                    xtype: 'form',
                    width: '100%',
                    bodyPadding: 5,
                    frame: false,
                    defaults: {
                        style: 'border-color: #5ECAC2!important;',
                        defaults: {
                            border: 0,
                            defaults: {
                                labelWidth: 80
                            }
                        }
                    },
                    items: [
                        {
                            xtype: 'label',
                            html: 'Usted va a validar un ingreso de <b>$' + cell.record.data.ReporteCorreo + ' USD</b> para el usuario <b>' + cell.record.data.nombreR + ' ' + cell.record.data.apellidoR + '</b>. <br>¿Es correcta la información?'
                        }
                    ]
                }
            ],
            buttons: ['->',
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'No. regresar',
                    tooltip: 'No Validar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        windowValidar.close();
                    }
                },
                {
                    xtype: 'button',
//                    iconCls: 'fas fa-times-circle',
                    iconAlign: 'right',
                    text: 'Si. Validar',
                    tooltip: 'Validar',
                    style: {
                        background: COLOR_SISTEMA,
                        border: '1px solid #36beb3',
                        '-webkit-border-radius': '5px 5px',
                        '-moz-border-radius': '5px 5px'
                    },
                    handler: function () {
                        Ext.Ajax.request({
                            async: true,
                            url: 'php/Reporte/update.php',
                            params: {
                                id: cell.record.data.id,
                                ReporteCorreo: cell.record.data.ReporteCorreo,
                                confirmada: 1,
                                validada: 1
                            },
                            callback: function (callback, e, response) {
//                                window.down('form').removeAll();
                                var res = JSON.parse(response.request.result.responseText);
                                if (res.success) {
                                    notificaciones('El registro se validó correctamente', 1);
                                    Ext.Ajax.request({
                                        async: true,
                                        url: 'php/Reporte/createReporte.php',
                                        params: {
                                            id: cell.record.data.id,
                                            idUsuario: cell.record.data.idUsuario,
                                            ReporteCorreo: cell.record.data.ReporteCorreo
                                        },
                                        callback: function (callback, e, response) {
                                            var res = JSON.parse(response.request.result.responseText);
                                            if (res.success) {
//                                                notificaciones('El registro ha sido guardado', 1);
                                                  console.log('Registro Guaradado');  
                                            } 
                                            MODULO_ALUMNO.down('[name=grid]').getStore().reload();
                                        }
                                    });
                                } else {
                                    notificaciones('Lo sentimos hubo un error al validar el registro', 2);
                                }
                                MODULO_ALUMNO.down('[name=grid]').getStore().reload();
                            }
                        });

                        windowValidar.close();
                    }
                }
            ]
        });

        if (cell.record.data.validada == 1) {
            notificaciones('El Reporte ya se encuentra validado', 5);
        } else {
            windowValidar.show();

        }

    }
});