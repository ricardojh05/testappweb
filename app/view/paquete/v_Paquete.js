var editingPromociones;
Ext.define('Ktaxi.view.paquete.v_Paquete', {
    extend: 'Ext.panel.Panel',
    xtype: 'paquete',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelPaquete',
    controller: 'c_Paquete',
    bodyBorder: false,
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.paquete.c_Paquete',
        'Ext.grid.plugin.CellEditing'
    ],
    listeners: {
        afterrender: 'onViewPaquete'
    },
    initComponent: function () {
        editingPromociones = Ext.create('Ext.grid.plugin.CellEditing');
        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [
                    {
                        xtype: 'form',
                        layout: 'hbox',
                        items: [
                            {
                                width: '35%',
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [{
                                        name: 'panelCrearEditarPaquete',
                                        region: 'east',
                                        xtype: 'form',
                                        layout: 'fit',
                                        collapsible: false,
                                        items: [{
                                                cls: 'panelFormulario',
                                                xtype: 'panel',
                                                title: 'Paquetes',
                                                defaultType: 'textfield',
                                                defaults: {
                                                    width: '100%',
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    allowOnlyWhitespace: false,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    defaultType: 'textfield',
                                                    labelWidth: 85,
                                                    defaults: {
                                                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                        allowOnlyWhitespace: false,
                                                        blankText: INFOMESSAGEBLANKTEXT,
                                                        allowBlank: false,
                                                        labelWidth: 85,
                                                        width: '50%',
                                                        defaults: {
                                                            afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                            allowOnlyWhitespace: false,
                                                            blankText: INFOMESSAGEBLANKTEXT,
                                                            allowBlank: false,
                                                            labelWidth: 85,
                                                            width: '50%'
                                                        }
                                                    }
                                                },
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [{
                                                                name: 'idAplicativo',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Aplicativo',
                                                                displayField: 'text',
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_Aplicativos')
                                                            }, {
                                                                margin: '0 0 0 5',
                                                                name: 'idCiudad',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Ciudad',
                                                                displayField: 'text',
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_Ciudades')
                                                            }]
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        margin: '0 0 5 0',
                                                        items: [
                                                            {
                                                                fieldLabel: 'Nombre',
                                                                name: 'nombre',
                                                                emptyText: 'Nombre',
                                                                maxLength: '200',
                                                                minLength: '2',
                                                                minLengthText: MINIMUMMESSAGUEREQUERID,
                                                                maxLengthText: MAXIMUMMESSAGUEREQURID,
                                                            },
                                                            {
                                                                margin: '0 0 0 5',
                                                                name: 'idTipo',
                                                                xtype: 'combobox',
                                                                emptyText: 'Seleccione',
                                                                fieldLabel: 'Tipo',
                                                                displayField: 'text',
                                                                minChars: 0,
                                                                typeAhead: true,
                                                                valueField: 'id',
                                                                queryParam: 'param',
                                                                queryMode: 'remote',
                                                                store: Ext.create('Ktaxi.store.combos.s_Tipo_Paquete'),
                                                                listeners: {
                                                                    change: function (combo, value) {
                                                                        var panel = Ext.getCmp('panelPaquete');
                                                                        switch (value) {
                                                                            case 1:
                                                                                panel.down('[name=relacionPorcentaje]').setValue(0);
                                                                                panel.down('[name=relacionPorcentaje]').setReadOnly(true);
                                                                                panel.down('[name=relacionPorcentaje]').disable();
                                                                                break;
                                                                            case 2:
                                                                                panel.down('[name=relacionPorcentaje]').setValue(0);
                                                                                panel.down('[name=relacionPorcentaje]').setReadOnly(false);
                                                                                panel.down('[name=relacionPorcentaje]').enable();
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'textareafield',
                                                        fieldLabel: 'Descripción corta',
                                                        name: 'desCorta',
                                                        emptyText: 'Descripción corta',
                                                        maxLength: '500',
                                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                                        maxLengthText: MAXIMUMMESSAGUEREQURID
                                                    },
                                                    {
                                                        xtype: 'textareafield',
                                                        fieldLabel: 'Descripción larga',
                                                        name: 'desLarga',
                                                        emptyText: 'Descripción larga',
                                                        maxLength: '1200',
                                                        minLengthText: MINIMUMMESSAGUEREQUERID,
                                                        maxLengthText: MAXIMUMMESSAGUEREQURID
                                                    },
                                                    {
                                                        xtype: 'container',
                                                        layout: 'hbox',
                                                        items: [
                                                            {
                                                                xtype: 'numberfield',
                                                                fieldLabel: 'Costo',
                                                                name: 'costo',
                                                                emptyText: 'Costo',
                                                                decimalPrecision: 2,
                                                                step: 0.01

                                                            },
                                                            {
                                                                margin: '0 0 0 5',
                                                                xtype: 'numberfield',
                                                                fieldLabel: 'Relación',
                                                                name: 'relacion',
                                                                emptyText: 'Relación',
                                                                decimalPrecision: 2,
                                                                step: 0.01
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: 'checkboxgroup',
                                                        allowBlank: true,
                                                        allowOnlyWhitespace: true,
                                                        defaults: {
                                                            flex: 1
                                                        },
                                                        items: [
                                                            {
                                                                boxLabel: 'Relación porcentaje',
                                                                xtype: 'checkbox',
                                                                allowBlank: true,
                                                                allowOnlyWhitespace: true,
                                                                uncheckedValue: 0,
                                                                inputValue: 1,
                                                                name: 'relacionPorcentaje',
                                                                labelAlign: 'right',
                                                                listeners: {
                                                                    change: 'onChangeRelacionPorcentaje'
                                                                }
                                                            },
                                                            {
                                                                margin: '0 0 0 15',
                                                                boxLabel: 'Habilitado',
                                                                xtype: 'checkbox',
                                                                allowBlank: true,
                                                                allowOnlyWhitespace: true,
                                                                uncheckedValue: 0,
                                                                inputValue: 1,
                                                                name: 'habilitado',
                                                                labelAlign: 'right'
                                                            }
                                                        ]
                                                    },
                                                    {name: 'actualizar', hidden: true, allowBlank: true, allowOnlyWhitespace: true}
                                                ]
                                            }]
                                    }, {
                                        flex: 1,
                                        name: 'gridLeerPaquete',
                                        xtype: 'grid',
                                        bufferedRenderer: false,
                                        store: 'paquete.s_Paquete',
                                        defaults: {
                                            margin: 0,
                                            padding: 0
                                        },
                                        tbar: [
                                            {
                                                xtype: 'textfield',
                                                flex: 2,
                                                tooltip: 'Escribir búsqueda',
                                                name: 'paramBusquedaPaquete',
                                                emptyText: 'Nombre, valor..',
                                                minChars: 0,
                                                typeAhead: true,
                                                listeners: {
                                                    specialkey: 'onChangeSearchPaquete'
                                                }
                                            },
                                            {
                                                flex: 1,
                                                allowBlank: true,
                                                name: 'comboSearchAplicativo',
                                                xtype: 'combobox',
                                                emptyText: 'Aplicativo',
                                                displayField: 'text',
                                                minChars: 0,
                                                typeAhead: true,
                                                valueField: 'id',
                                                queryParam: 'param',
                                                queryMode: 'remote',
                                                growMax: 10,
                                                forceSelected: true,
                                                store: Ext.create('Ktaxi.store.combos.s_Aplicativos'),
                                                listeners: {
                                                    specialkey: 'onChangeSearchPaquete'
                                                }
                                            }, {
                                                flex: 1,
                                                allowBlank: true,
                                                name: 'comboSearchCiudad',
                                                xtype: 'combobox',
                                                emptyText: 'Ciudad',
                                                displayField: 'text',
                                                minChars: 0,
                                                typeAhead: true,
                                                valueField: 'id',
                                                queryParam: 'param',
                                                queryMode: 'remote',
                                                growMax: 10,
                                                forceSelected: true,
                                                store: Ext.create('Ktaxi.store.combos.s_Ciudades'),
                                                listeners: {
                                                    specialkey: 'onChangeSearchPaquete'
                                                }
                                            }, {
                                                flex: 1,
                                                allowBlank: true,
                                                name: 'comboSearchTipo',
                                                xtype: 'combobox',
                                                emptyText: 'Tipo',
                                                displayField: 'text',
                                                minChars: 0,
                                                typeAhead: true,
                                                valueField: 'id',
                                                queryParam: 'param',
                                                queryMode: 'remote',
                                                growMax: 10,
                                                forceSelected: true,
                                                store: Ext.create('Ktaxi.store.combos.s_Tipo_Paquete'),
                                                listeners: {
                                                    specialkey: 'onChangeSearchPaquete'
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-search',
                                                iconAlign: 'right',
                                                tooltip: 'Buscar',
                                                handler: 'onChangeSearchPaquete'
                                            },
                                            {
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-eraser',
                                                iconAlign: 'right',
                                                tooltip: 'Limpiar',
                                                handler: function () {
                                                    var panel = Ext.getCmp('panelPaquete');
                                                    panel.down('[name=paramBusquedaPaquete]').reset();
                                                    panel.down('[name=comboSearchAplicativo]').reset();
                                                    panel.down('[name=comboSearchCiudad]').reset();
                                                    panel.down('[name=comboSearchTipo]').reset();
                                                    var gridLeerPaquete = panel.down('[name=gridLeerPaquete]');
                                                    gridLeerPaquete.getView().deselect(gridLeerPaquete.getSelection());
                                                    gridLeerPaquete.getStore().clearFilter();
                                                    gridLeerPaquete.getStore().load();
                                                    Ext.getStore('paquete.s_Paquete_Promocion').removeAll();
                                                    Ext.getStore('paquete.s_Paquete_Descuento').removeAll();
                                                    onChangeDates('gridLeerPaquetePromocion');
                                                    onChangeDates('gridLeerPaqueteDescuento');
                                                }
                                            },
                                            {
                                                width: '5%',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-refresh',
                                                iconAlign: 'right',
                                                tooltip: 'Recargar',
                                                handler: function () {
                                                    var panel = Ext.getCmp('panelPaquete');
                                                    var gridLeerPaquete = panel.down('[name=gridLeerPaquete]');
                                                    var selected = gridLeerPaquete.getSelection();
                                                    gridLeerPaquete.getView().deselect(selected);
                                                    onChangeDates('gridLeerPaquetePromocion');
                                                    onChangeDates('gridLeerPaqueteDescuento');
                                                }
                                            }
                                        ],
                                        columns: [
                                            {tooltip: 'Aplicativo', text: 'Aplicativo', dataIndex: 'aplicativo', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'Ciudad', text: 'Ciudad', dataIndex: 'ciudad', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'Tipo', text: 'Tipo', dataIndex: 'tipo', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'Nombre', text: 'Nombre', dataIndex: 'nombre', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'Costo', text: 'Costo', dataIndex: 'costo', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'Relación', text: 'Relación', dataIndex: 'relacion', filter: true, flex: 2, cellWrap: true, renderer: showTipConten},
                                            {tooltip: 'Porcentaje', text: "Porcentaje", flex: 1, dataIndex: 'relacionPorcentaje', sortable: true, renderer: formatEstado},
                                            {tooltip: 'Habilitado', text: "Habilitado", flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstado},
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            select: 'onSelectChangeGridPaquete',
                                            deselect: function () {
                                                var panel = Ext.getCmp('panelPaquete');
                                                panel.down('[name=btnEditar]').disable();
                                                panel.down('[name=btnCrear]').enable();
                                                panel.down('[name=idTipo]').setReadOnly(false);
                                                panel.down('[name=idTipo]').enable();
                                            },
                                            beforeitemclick: function (thisObj, record, item, index, e, eOpts) {
                                                var panel = Ext.getCmp('panelPaquete');
                                                panel.down('[name=btnEditar]').enable();
                                                panel.down('[name=btnCrear]').disable();
                                            },
                                            rowdblclick: showAuditoria
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>'
                                        }
                                    }
                                ]
                            }, {
                                flex: 1,
                                xtype: 'panel',
                                layout: 'vbox',
                                height: '100%',
                                defaults: {
                                    width: '100%'
                                },
                                items: [
                                    {
                                        flex: 1,
                                        title: 'Promociones',
                                        name: 'gridLeerPaquetePromocion',
                                        xtype: 'grid',
                                        bufferedRenderer: false,
                                        plugins: {
                                            ptype: 'rowediting',
                                            clicksToEdit: 1
                                        },
                                        store: 'paquete.s_Paquete_Promocion',
                                        tbar: [
                                            {
                                                text: 'Nueva promoción',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-plus-square',
                                                iconAlign: 'right',
                                                tooltip: 'Nuevo registro',
                                                handler: 'onAddPromocion',
                                                name: 'onAddPromocion'
                                            },
                                            {
                                                xtype: 'container',
                                                name: 'errorFechas',
                                                html: ''
                                            }
                                        ],
                                        columns: [
                                            {tooltip: 'Fecha inicio', text: 'Fecha inicio', dataIndex: 'fInicio', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    xtype: 'datefield',
                                                    allowBlank: false,
                                                    format: 'Y-m-d',
                                                    minValue: new Date(),
                                                    value: new Date(),
                                                    minText: 'La fecha no puede ser inferior a la actual',
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaquetePromocion');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Hora inicio', text: 'Hora inicio', dataIndex: 'hInicio', filter: true, flex: 1, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'timefield',
                                                    format: 'H:i',
                                                    value: new Date('2018-02-02 00:00:00'),
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaquetePromocion');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Fecha fin', text: 'Fecha fin', dataIndex: 'fFin', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    xtype: 'datefield',
                                                    allowBlank: false,
                                                    format: 'Y-m-d',
                                                    minValue: new Date(),
                                                    value: new Date(),
                                                    minText: 'La fecha fin no puede ser inferior a la de inicio',
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaquetePromocion');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Hora fin', text: 'Hora fin', dataIndex: 'hFin', filter: true, flex: 1, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'timefield',
                                                    format: 'H:i',
                                                    value: new Date('2018-02-02 23:59:00'),
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaquetePromocion');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Caduca', text: 'Caduca', dataIndex: 'caduca', filter: true, flex: 1, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'numberfield',
                                                    decimalPrecision: 2,
                                                    value: 1,
                                                    minValue: 1,
                                                    maxValue: 90
                                                }
                                            },
                                            {tooltip: 'Promoción', text: 'Promoción', dataIndex: 'promocion', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'numberfield',
                                                    decimalPrecision: 2,
                                                    step: 0.5,
                                                    minValue: 0.01
                                                }
                                            },
                                            {tooltip: 'Porcentaje', text: 'Porcentaje', dataIndex: 'promPorcentaje', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    allowBlank: true,
                                                    xtype: 'checkbox',
                                                    uncheckedValue: 0,
                                                    inputValue: 1,
                                                    value: 0,
                                                    listeners: {
                                                        change: function (component, value) {
                                                            onChangePorcentaje('gridLeerPaquetePromocion', value);
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Descripción', text: 'Descripción', dataIndex: 'descripcion', filter: true, flex: 3, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'textareafield',
                                                    minLength: '60',
                                                    maxLength: '500',
                                                    minLengthText: MINIMUMMESSAGUEREQUERID,
                                                    maxLengthText: MAXIMUMMESSAGUEREQURID
                                                }
                                            },
                                            {tooltip: 'Habilitado', text: "Habilitado", flex: 1, dataIndex: 'habilitado', sortable: true, renderer: validateRowEdit,
                                                editor: {
                                                    allowBlank: true,
                                                    xtype: 'checkbox',
                                                    uncheckedValue: 0,
                                                    inputValue: 1,
                                                    checked: false,
                                                    value: 0,
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaquetePromocion');
                                                        }
                                                    }
                                                }
                                            },
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            rowdblclick: showAuditoria,
                                            validateedit: function (rowEditing, context, eOpts) {
                                                if (rowEditing.editor.form.isValid()) {
                                                    var panel = Ext.getCmp('panelPaquete');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                }
                                            },
                                            cancelEdit: function (rowEditing, context) {
                                                if (!rowEditing.editor.form.isValid() && context.record.data.nuevo) {
                                                    var panel = Ext.getCmp('panelPaquete');
                                                    panel.down('[name=onAddPromocion]').enable();
                                                    Ext.getStore('paquete.s_Paquete_Promocion').remove(context.record);
                                                    onChangeDates('gridLeerPaquetePromocion');
                                                }
                                            }
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>',
                                            getRowClass: function (record) {
                                                if (record.data.error) {
                                                    return 'deleteRowGrid';
                                                }
                                            }
                                        }
                                    }, {
                                        flex: 1,
                                        title: 'Descuentos',
                                        name: 'gridLeerPaqueteDescuento',
                                        xtype: 'grid',
                                        bufferedRenderer: false,
                                        plugins: {
                                            ptype: 'rowediting',
                                            clicksToEdit: 1
                                        },
                                        store: 'paquete.s_Paquete_Descuento',
                                        tbar: [
                                            {
                                                text: 'Nuevo descuento',
                                                xtype: 'button',
                                                iconCls: 'x-fa fa-plus-square',
                                                iconAlign: 'right',
                                                tooltip: 'Nuevo registro',
                                                name: 'onAddDescuento',
                                                handler: 'onAddDescuento'
                                            },
                                            {
                                                xtype: 'container',
                                                name: 'errorFechas',
                                                html: ''
                                            }
                                        ],
                                        columns: [
                                            {tooltip: 'Fecha inicio', text: 'Fecha inicio', dataIndex: 'fInicio', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    xtype: 'datefield',
                                                    allowBlank: false,
                                                    format: 'Y-m-d',
                                                    minValue: new Date(),
                                                    value: new Date(),
                                                    minText: 'La fecha no puede ser inferior a la actual',
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaqueteDescuento');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Hora inicio', text: 'Hora inicio', dataIndex: 'hInicio', filter: true, flex: 1, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'timefield',
                                                    format: 'H:i',
                                                    value: new Date('2018-02-02 00:00:00'),
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaqueteDescuento');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Fecha fin', text: 'Fecha fin', dataIndex: 'fFin', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    xtype: 'datefield',
                                                    allowBlank: false,
                                                    format: 'Y-m-d',
                                                    minValue: new Date(),
                                                    value: new Date(),
                                                    minText: 'La fecha fin no puede ser inferior a la de inicio',
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaqueteDescuento');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Hora fin', text: 'Hora fin', dataIndex: 'hFin', filter: true, flex: 1, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'timefield',
                                                    format: 'H:i',
                                                    value: new Date('2018-02-02 23:59:00'),
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaqueteDescuento');
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Relación', text: 'Relación', dataIndex: 'relacion', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'numberfield',
                                                    decimalPrecision: 2,
                                                    step: 0.5,
                                                    minValue: 0.01
                                                }
                                            },
                                            {tooltip: 'Porcentaje', text: 'Porcentaje', dataIndex: 'relacionPorcentaje', filter: true, flex: 2, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    allowBlank: true,
                                                    xtype: 'checkbox',
                                                    uncheckedValue: 0,
                                                    inputValue: 1,
                                                    listeners: {
                                                        change: function (component, value) {
                                                            onChangePorcentaje('gridLeerPaqueteDescuento', value);
                                                        }
                                                    }
                                                }
                                            },
                                            {tooltip: 'Descripción', text: 'Descripción', dataIndex: 'descripcion', filter: true, flex: 3, cellWrap: true, renderer: validateRowEdit,
                                                editor: {
                                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                    blankText: INFOMESSAGEBLANKTEXT,
                                                    allowBlank: false,
                                                    xtype: 'textareafield',
                                                    minLength: '60',
                                                    maxLength: '500',
                                                    minLengthText: MINIMUMMESSAGUEREQUERID,
                                                    maxLengthText: MAXIMUMMESSAGUEREQURID
                                                }
                                            },
                                            {tooltip: 'Habilitado', text: "Habilitado", flex: 1, dataIndex: 'habilitado', sortable: true, renderer: validateRowEdit,
                                                editor: {
                                                    allowBlank: true,
                                                    xtype: 'checkbox',
                                                    uncheckedValue: 0,
                                                    inputValue: 1,
                                                    checked: false,
                                                    value: 0,
                                                    listeners: {
                                                        change: function () {
                                                            onChangeDates('gridLeerPaqueteDescuento');
                                                        }
                                                    }
                                                }
                                            },
                                            {hidden: true, tooltip: 'Id', text: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                                        ],
                                        columnLines: true,
                                        split: true,
                                        region: 'north',
                                        listeners: {
                                            rowdblclick: showAuditoria,
                                            validateedit: function (rowEditing, context, eOpts) {
                                                if (rowEditing.editor.form.isValid()) {
                                                    var panel = Ext.getCmp('panelPaquete');
                                                    panel.down('[name=onAddDescuento]').enable();
                                                }
                                            },
                                            cancelEdit: function (rowEditing, context) {
                                                if (!rowEditing.editor.form.isValid() && context.record.data.nuevo) {
                                                    var panel = Ext.getCmp('panelPaquete');
                                                    panel.down('[name=onAddDescuento]').enable();
                                                    Ext.getStore('paquete.s_Paquete_Descuento').remove(context.record);
                                                    onChangeDates('gridLeerPaqueteDescuento');
                                                }
                                            }
                                        },
                                        viewConfig: {
                                            emptyText: '<center>No existen resultados.</center>',
                                            getRowClass: function (record) {
                                                if (record.data.error) {
                                                    return 'deleteRowGrid';
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        ], dockedItems: [{
                                ui: 'footer',
                                xtype: 'toolbar',
                                dock: 'bottom',
                                defaults: {
                                    width: '10%'
                                },
                                items: [
                                    {
                                        text: 'Limpiar',
                                        tooltip: 'Limpiar',
                                        disabled: false,
                                        handler: function () {
                                            limpiarFormularioPaquete();
                                        }
                                    },
                                    '->',
                                    {
                                        text: 'Editar',
                                        tooltip: 'Actualizar',
                                        disabled: true,
                                        name: 'btnEditar',
                                        handler: 'onUpdatePaquete'
                                    }, {
                                        text: 'Crear',
                                        tooltip: 'Crear Administradores',
                                        disabled: true,
                                        name: 'btnCrear',
                                        handler: 'onCreatePaquete'
                                    }]
                            }]
                    }
                ]
            }
        ];
        this.callParent(arguments);
    }
});
