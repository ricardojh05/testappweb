Ext.define('Ktaxi.view.paquete.c_Paquete', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Paquete',
    onViewPaquete: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
        Ext.getStore('paquete.s_Paquete').load();
    },
    onChangeSearchPaquete: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var panel = Ext.getCmp('panelPaquete');
            var paramBusqueda = panel.down('[name=paramBusquedaPaquete]').getValue();
            var comboAplicativo = panel.down('[name=comboSearchAplicativo]').getValue();
            var comboCiudad = panel.down('[name=comboSearchCiudad]').getValue();
            var comboTipo = panel.down('[name=comboSearchTipo]').getValue();
            Ext.getStore('paquete.s_Paquete').load({
                params: {
                    param: paramBusqueda,
                    aplicativos: comboAplicativo,
                    ciudades: comboCiudad,
                    tipos: comboTipo
                },
                callback: function (records) {
                    if (records.length <= 0) {
                        Ext.getStore('paquete.s_Paquete').removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridPaquete: function (thisObj, selected, eOpts) {
        if (selected) {
            var formPaquete = Ext.getCmp('panelPaquete').down('[name=panelCrearEditarPaquete]');
            formPaquete.down('[name=idTipo]').setReadOnly(true);
            formPaquete.down('[name=idTipo]').disable();
            var comboFormAplicativo = formPaquete.down('[name=idAplicativo]');
            console.log(selected.data);
            if (!isInStore(comboFormAplicativo.getStore(), selected.data.idAplicativo, 'id')) {
                comboFormAplicativo.getStore().load({
                    params: {
                        param: selected.data.idAplicativo
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormAplicativo.setValue(records);
                        }
                    }
                });
            }
            var comboFormCiudad = formPaquete.down('[name=idCiudad]');
            if (!isInStore(comboFormCiudad.getStore(), selected.data.idCiudad, 'id')) {
                comboFormCiudad.getStore().load({
                    params: {
                        param: selected.data.idCiudad
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormCiudad.setValue(records);
                        }
                    }
                });
            }
            var comboFormTipo = formPaquete.down('[name=idTipo]');
            if (!isInStore(comboFormTipo.getStore(), selected.data.idTipo, 'id')) {
                comboFormTipo.getStore().load({
                    params: {
                        param: selected.data.idTipo
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormTipo.setValue(records);
                        }
                    }
                });
            }
            Ext.getStore('paquete.s_Paquete_Promocion').load({
                params: {
                    param: selected.data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        Ext.getStore('paquete.s_Paquete_Promocion').removeAll();
                    }
                }
            });
            Ext.getStore('paquete.s_Paquete_Descuento').load({
                params: {
                    param: selected.data.id
                },
                callback: function (records) {
                    if (records.length === 0) {
                        Ext.getStore('paquete.s_Paquete_Descuento').removeAll();
                    }
                }
            });
            formPaquete.down('[name=relacionPorcentaje]').setValue(0);
            formPaquete.loadRecord(selected);
        }
    },
    onChangeRelacionPorcentaje: function (component, check) {
        var panel = Ext.getCmp('panelPaquete');
        var valor = panel.down('[name=relacion]');
        if (check) {
            valor.setMaxValue(100);
        } else {
            valor.setMaxValue(null);
        }
    },
    onAddPromocion: function () {
        var panel = Ext.getCmp('panelPaquete');
        var grid = panel.down('[name=gridLeerPaquetePromocion]');
        panel.down('[name=onAddPromocion]').disable();
        var store = grid.getStore();
        var rec = {
            idPaquete: '',
            promocion: 0,
            promPorcentaje: 0,
            fInicio: new Date(),
            fFin: new Date(),
            hInicio: new Date('2018-02-02 00:00:00'),
            hFin: new Date('2018-02-02 23:59:00'),
            descripcion: '',
            habilitado: 0,
            nuevo: true
        };
        store.insert(0, rec);
        grid.editingPlugin.startEdit(store.data.items[0]);
        onChangeDates('gridLeerPaquetePromocion');
    },
    onAddDescuento: function () {
        var panel = Ext.getCmp('panelPaquete');
        var grid = panel.down('[name=gridLeerPaqueteDescuento]');
        var idTipo = panel.down('[name=idTipo]').getValue();
        var checkRelacionPorcentaje = panel.down('[name=relacionPorcentaje]').getValue();
        panel.down('[name=onAddDescuento]').disable();
        var store = grid.getStore();
        var rec = {
            idPaquete: '',
            relacion: 0,
            relacionPorcentaje: checkRelacionPorcentaje,
            fInicio: new Date(),
            fFin: new Date(),
            hInicio: new Date('2018-02-02 00:00:00'),
            hFin: new Date('2018-02-02 23:59:00'),
            descripcion: '',
            habilitado: 0,
            nuevo: true
        };
        store.insert(0, rec);
        grid.editingPlugin.startEdit(store.data.items[0]);
        var relacion = getColumnByDataIndex(grid, 'relacion').getEditor();
        var relacionPorcentaje = getColumnByDataIndex(grid, 'relacionPorcentaje').getEditor();
        if (checkRelacionPorcentaje) {
            relacion.setMaxValue(100);
        }
        if (idTipo === 1) {
            relacionPorcentaje.setReadOnly(true);
            relacionPorcentaje.disable();
        } else {
            relacionPorcentaje.setReadOnly(false);
            relacionPorcentaje.enable();
        }
        onChangeDates('gridLeerPaqueteDescuento');
    },
    onCreatePaquete: function () {
        var panel = Ext.getCmp('panelPaquete');
        var form = panel.down('[name=panelCrearEditarPaquete]');
        var params = this.getExtraData();
        if (form.isValid()) {
            var errorPromocion = Ext.getStore('paquete.s_Paquete_Promocion').findRecord('error', true);
            var errorDescuento = Ext.getStore('paquete.s_Paquete_Descuento').findRecord('error', true);
            if (errorPromocion || errorDescuento) {
                notificaciones('EXISTEN INCONSISTENCIAS EN LAS FECHAS DE ALGUNOS REGISTROS.', 2);
            } else {
                var record = form.getValues();
                Ext.getStore('paquete.s_Paquete').insert(0, record);
                Ext.getStore('paquete.s_Paquete').proxy.extraParams = params;
                Ext.getStore('paquete.s_Paquete').sync();
                mostrarBarraProgreso('Guardando cambios...');
            }
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    },
    onUpdatePaquete: function () {
        var panel = Ext.getCmp('panelPaquete');
        var form = panel.down('[name=panelCrearEditarPaquete]');
        var params = this.getExtraData();
        if (form.isValid()) {
            if (Ext.getStore('paquete.s_Paquete_Promocion').data.items.length > 0 || Ext.getStore('paquete.s_Paquete_Descuento').data.items.length > 0) {
                var errorPromocion = Ext.getStore('paquete.s_Paquete_Promocion').findRecord('error', true);
                var errorDescuento = Ext.getStore('paquete.s_Paquete_Descuento').findRecord('error', true);
                if (errorPromocion || errorDescuento) {
                    notificaciones('EXISTEN INCONSISTENCIAS EN LAS FECHAS DE ALGUNOS REGISTROS.', 2);
                } else {
                    form.down('[name=actualizar]').setValue(true);
                    form.updateRecord(form.activeRecord);
                    Ext.getStore('paquete.s_Paquete').proxy.extraParams = params;
                    Ext.getStore('paquete.s_Paquete').sync();
                    mostrarBarraProgreso('Guardando cambios...');
                }
            } else {
                form.down('[name=actualizar]').setValue(true);
                form.updateRecord(form.activeRecord);
                Ext.getStore('paquete.s_Paquete').proxy.extraParams = params;
                Ext.getStore('paquete.s_Paquete').sync();
                mostrarBarraProgreso('Guardando cambios...');
            }
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    },
    getExtraData: function () {
        var listPromociones = [];
        var dataCreatePromociones = Ext.getStore('paquete.s_Paquete_Promocion').getData().items;
        var dataUpdatePromociones = Ext.getStore('paquete.s_Paquete_Promocion').getUpdatedRecords();
        for (var i in dataCreatePromociones) {
            if (dataCreatePromociones[i].data.nuevo) {
                var dateDesde = Ext.Date.format(new Date(dataCreatePromociones[i].data.fInicio), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataCreatePromociones[i].data.hInicio), 'H:i:s');
                var dateHasta = Ext.Date.format(new Date(dataCreatePromociones[i].data.fFin), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataCreatePromociones[i].data.hFin), 'H:i:s');
                listPromociones.push({
                    id: 0,
                    promocion: dataCreatePromociones[i].data.promocion,
                    promPorcentaje: dataCreatePromociones[i].data.promPorcentaje,
                    caduca: dataCreatePromociones[i].data.caduca,
                    desde: dateDesde,
                    hasta: dateHasta,
                    descripcion: dataCreatePromociones[i].data.descripcion,
                    habilitado: dataCreatePromociones[i].data.habilitado,
                    nuevo: true
                });
            }
        }
        for (var i in dataUpdatePromociones) {
            if (!dataUpdatePromociones[i].data.nuevo) {
                var dateDesde = Ext.Date.format(new Date(dataUpdatePromociones[i].data.fInicio), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataUpdatePromociones[i].data.hInicio), 'H:i:s');
                var dateHasta = Ext.Date.format(new Date(dataUpdatePromociones[i].data.fFin), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataUpdatePromociones[i].data.hFin), 'H:i:s');
                listPromociones.push({
                    id: dataUpdatePromociones[i].data.id,
                    promocion: dataUpdatePromociones[i].data.promocion,
                    promPorcentaje: dataUpdatePromociones[i].data.promPorcentaje,
                    caduca: dataUpdatePromociones[i].data.caduca,
                    desde: dateDesde,
                    hasta: dateHasta,
                    descripcion: dataUpdatePromociones[i].data.descripcion,
                    habilitado: dataUpdatePromociones[i].data.habilitado,
                    nuevo: false
                });
            }
        }

        var listDescuentos = [];
        var dataCreateDescuentos = Ext.getStore('paquete.s_Paquete_Descuento').getData().items;
        var dataUpdateDescuentos = Ext.getStore('paquete.s_Paquete_Descuento').getUpdatedRecords();
        for (var i in dataCreateDescuentos) {
            if (dataCreateDescuentos[i].data.nuevo) {
                var dateDesde = Ext.Date.format(new Date(dataCreateDescuentos[i].data.fInicio), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataCreateDescuentos[i].data.hInicio), 'H:i:s');
                var dateHasta = Ext.Date.format(new Date(dataCreateDescuentos[i].data.fFin), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataCreateDescuentos[i].data.hFin), 'H:i:s');
                listDescuentos.push({
                    id: 0,
                    relacion: dataCreateDescuentos[i].data.relacion,
                    relacionPorcentaje: dataCreateDescuentos[i].data.relacionPorcentaje,
                    desde: dateDesde,
                    hasta: dateHasta,
                    descripcion: dataCreateDescuentos[i].data.descripcion,
                    habilitado: dataCreateDescuentos[i].data.habilitado,
                    nuevo: true
                });
            }
        }
        for (var i in dataUpdateDescuentos) {
            if (!dataUpdateDescuentos[i].data.nuevo) {
                var dateDesde = Ext.Date.format(new Date(dataUpdateDescuentos[i].data.fInicio), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataUpdateDescuentos[i].data.hInicio), 'H:i:s');
                var dateHasta = Ext.Date.format(new Date(dataUpdateDescuentos[i].data.fFin), 'Y-m-d') + ' ' + Ext.Date.format(new Date(dataUpdateDescuentos[i].data.hFin), 'H:i:s');
                listDescuentos.push({
                    id: dataUpdateDescuentos[i].data.id,
                    relacion: dataUpdateDescuentos[i].data.relacion,
                    relacionPorcentaje: dataUpdateDescuentos[i].data.relacionPorcentaje,
                    desde: dateDesde,
                    hasta: dateHasta,
                    descripcion: dataUpdateDescuentos[i].data.descripcion,
                    habilitado: dataUpdateDescuentos[i].data.habilitado,
                    nuevo: false
                });
            }
        }

        return {promociones: JSON.stringify(listPromociones), descuentos: JSON.stringify(listDescuentos)};
    }
});

function onChangeDates(nameGrid) {
    var panel = Ext.getCmp('panelPaquete');
    var grid = panel.down('[name=' + nameGrid + ']');
    grid.down('[name=errorFechas]').update('');
    var recordActual = grid.getSelection()[0];
    if (recordActual) {
        var dateDesde = getColumnByDataIndex(grid, 'fInicio').getEditor();
        var dateHasta = getColumnByDataIndex(grid, 'fFin').getEditor();
        var hDesde = getColumnByDataIndex(grid, 'fInicio').getEditor();
        var hHasta = getColumnByDataIndex(grid, 'fFin').getEditor();
        if (dateHasta.getValue() < dateDesde.getValue()) {
            dateHasta.setValue(dateDesde.getValue());
        }
        var dateDesde = Ext.Date.format(new Date(dateDesde.getValue()), 'Y-m-d') + ' ' + Ext.Date.format(new Date(hDesde.getValue()), 'H:i');
        var dateHasta = Ext.Date.format(new Date(dateHasta.getValue()), 'Y-m-d') + ' ' + Ext.Date.format(new Date(hHasta.getValue()), 'H:i');
        onValidateDates(new Date(dateDesde), new Date(dateHasta), grid);
    }
}

function onValidateDates(dateDesde, dateHasta, grid) {
    var recordActual = grid.getSelection()[0];
    var records = grid.getStore().data.items;
    var bandera = false;
    var idsConflictos = [];
    for (var i in records) {
        var recordDesde = new Date(Ext.Date.format(new Date(records[i].data.fInicio), 'Y-m-d') + ' ' + Ext.Date.format(new Date(records[i].data.hInicio), 'H:i'));
        var recordHasta = new Date(Ext.Date.format(new Date(records[i].data.fFin), 'Y-m-d') + ' ' + Ext.Date.format(new Date(records[i].data.hFin), 'H:i'));
        if (dateDesde >= recordDesde && dateDesde <= recordHasta && records[i].data.habilitado && recordActual.data.id !== records[i].data.id) {
            if (jQuery.inArray(records[i].data.id, idsConflictos) >= 0) {
                idsConflictos.push(records[i].data.id);
                records[i].set('error', true);
            }
            bandera = true;
        }
        if (dateHasta >= recordDesde && dateHasta <= recordHasta && records[i].data.habilitado && recordActual.data.id !== records[i].data.id) {
            if (idsConflictos.indexOf(records[i].data.id)) {
                idsConflictos.push(records[i].data.id);
                records[i].set('error', true);
            }
            bandera = true;
        }
    }
    var formatDateDesde = Ext.Date.format(new Date(dateDesde), 'Y/m/d H:i');
    var formatDateHasta = Ext.Date.format(new Date(dateHasta), 'Y/m/d H:i');
    if (bandera) {
        if (recordActual) {
            recordActual.set('error', true);
        }
        grid.down('[name=errorFechas]').update('<center style="color:red;">Conflicto de fechas entre: ' + formatDateDesde + ' - ' + formatDateHasta + '.</center>');
    } else {
        if (recordActual) {
            recordActual.set('error', false);
        }
        grid.down('[name=errorFechas]').update('');
    }
}

function onChangePorcentaje(nameGrid, check) {
    var panel = Ext.getCmp('panelPaquete');
    var grid = panel.down('[name=' + nameGrid + ']');
    var valor = getColumnByDataIndex(grid, 'promocion');
    if (valor) {
        valor = valor.getEditor();
    } else {
        valor = getColumnByDataIndex(grid, 'relacion').getEditor();
    }
    if (check) {
        valor.setMaxValue(100);
    } else {
        valor.setMaxValue(null);
    }
}

function limpiarFormularioPaquete() {
    var panel = Ext.getCmp('panelPaquete');
    panel.down('[name=panelCrearEditarPaquete]').getForm().reset();
    var gridLeerPaquete = panel.down('[name=gridLeerPaquete]');
    gridLeerPaquete.getView().deselect(gridLeerPaquete.getSelection());
    gridLeerPaquete.getStore().reload();
    Ext.getStore('paquete.s_Paquete_Promocion').removeAll();
    Ext.getStore('paquete.s_Paquete_Descuento').removeAll();
    onChangeDates('gridLeerPaquetePromocion');
    onChangeDates('gridLeerPaqueteDescuento');
}