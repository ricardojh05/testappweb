Ext.define('Ktaxi.view.driver_ciudad.c_Driver_Ciudad', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Driver_Ciudad',
    onViewDriverCiudad: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
        Ext.getStore('driver_ciudad.s_Driver_Ciudad').load();
    },
    onChangeSearchDriverCiudad: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var panel = Ext.getCmp('panelDriverCiudad');
            var paramBusqueda = panel.down('[name=paramBusquedaDriverCiudad]').getValue();
            var tagAplicativo = panel.down('[name=comboSearchAplicativo]').getValue();
            var tagCiudad = panel.down('[name=comboSearchCiudad]').getValue();
            var tagLabel = panel.down('[name=comboSearchLabel]').getValue();
            Ext.getStore('driver_ciudad.s_Driver_Ciudad').load({
                params: {
                    param: paramBusqueda,
                    aplicativos: tagAplicativo.toString(),
                    ciudades: tagCiudad.toString(),
                    labels: tagLabel.toString()
                },
                callback: function (records) {
                    if (records.length <= 0) {
                        Ext.getStore('driver_ciudad.s_Driver_Ciudad').removeAll();
                    }
                }
            });
        }
    },
    onSelectChangeGridDriverCiudad: function (thisObj, selected, eOpts) {
        if (selected) {
            var formDriverCiudad = Ext.getCmp('panelDriverCiudad').down('[name=panelCrearEditarDriverCiudad]');
            var comboFormAplicativo = formDriverCiudad.down('[name=idAplicativo]');
            console.log(selected);
            if (!isInStore(comboFormAplicativo.getStore(), selected.data.idAplicativo, 'id')) {
                comboFormAplicativo.getStore().load({
                    params: {
                        param: selected.data.idAplicativo
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormAplicativo.setValue(records);
                        }
                    }
                });
            }
            var comboFormCiudad = formDriverCiudad.down('[name=idCiudad]');
            if (!isInStore(comboFormCiudad.getStore(), selected.data.idCiudad, 'id')) {
                comboFormCiudad.getStore().load({
                    params: {
                        param: selected.data.idCiudad
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormCiudad.setValue(records);
                        }
                    }
                });
            }
            var comboFormLabel = formDriverCiudad.down('[name=idLabel]');
            if (!isInStore(comboFormLabel.getStore(), selected.data.idLabel, 'id')) {
                comboFormLabel.getStore().load({
                    params: {
                        param: selected.data.idLabel
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormLabel.setValue(records);
                        }
                    }
                });
            }
            formDriverCiudad.loadRecord(selected);
        }
    },
    onCreateDriverCiudad: function () {
        var form = Ext.getCmp('panelDriverCiudad').down('[name=panelCrearEditarDriverCiudad]');
        if (form.isValid()) {
            var record = form.getValues();
            Ext.getStore('driver_ciudad.s_Driver_Ciudad').insert(0, record);
            Ext.getStore('driver_ciudad.s_Driver_Ciudad').sync();
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    },
    onUpdateDriverCiudad: function () {
        var form = Ext.getCmp('panelDriverCiudad').down('[name=panelCrearEditarDriverCiudad]');
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            Ext.getStore('driver_ciudad.s_Driver_Ciudad').sync();
        } else {
            notificaciones('LOS CAMPOS SON OBLIGATORIOS', 2);
        }
    }
});
function limpiarFormularioDriverCiudad() {
    var panel = Ext.getCmp('panelDriverCiudad');
    panel.down('[name=panelCrearEditarDriverCiudad]').getForm().reset();
    var gridLeerDriverCiudad = panel.down('[name=gridLeerDriverCiudad]');
    gridLeerDriverCiudad.getView().deselect(gridLeerDriverCiudad.getSelection());
    gridLeerDriverCiudad.getStore().reload();
}