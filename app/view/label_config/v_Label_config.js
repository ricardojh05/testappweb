var PANEL_LABELCONFIG;
Ext.define('Ktaxi.view.label_config.v_Label_config', {
    extend: 'Ext.panel.Panel',
    xtype: 'label_config',
    height: HEIGT_VIEWS,
    layout: 'border',
    id: 'panelLabel_config',
    controller: 'c_Label_config',
    bodyBorder: false,
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.label_config.c_Label_config'
    ],
    listeners: {
        afterrender: 'onViewLabel_config'
    },
    initComponent: function () {
        PANEL_LABELCONFIG = Ext.getCmp('panelLabel_config');
        this.items = [
            {
                region: 'center',
                xtype: 'panel',
                padding: 5,
                flex: 1,
                layout: 'fit',
                header: false,
                headerAsText: false,
                collapsible: true,
                collapseMode: 'mini',
                items: [{
                        name: 'gridLeerLabel_config',
                        xtype: 'grid',
                        bufferedRenderer: false,
                        store: 'label_config.s_Label_config',
                        defaults: {
                            margin: 0,
                            padding: 0
                        },
                        tbar: [
                            {
                                text: 'Nuevo registro',
                                xtype: 'button',
                                iconCls: 'x-fa fa-plus-square',
                                iconAlign: 'right',
                                tooltip: 'Nuevo registro',
                                handler: 'onAddRecord',
                                name: 'btnCrear'
                            },
                            {
                                text: 'Guardar cambios',
                                xtype: 'button',
                                iconCls: 'x-fa fa-floppy-o',
                                iconAlign: 'right',
                                tooltip: 'Guardar cambios',
                                handler: 'onSaveChanges',
                                name: 'btnEditar'
                            },
                            {
                                margin: '0 5 0 5',
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'textfield',
                                flex: 8,
                                tooltip: 'Escribir búsqueda',
                                name: 'paramBusquedaLabel_config',
                                emptyText: 'Buscar labels..',
                                minChars: 0,
                                typeAhead: true,
                                listeners: {
                                    specialkey: 'onChangeSearchLabel_config'
                                }
                            },
                            {
                                width: '5%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-search',
                                iconAlign: 'right',
                                tooltip: 'Buscar',
                                handler: 'onChangeSearchLabel_config'
                            },
                            {
                                width: '5%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-eraser',
                                iconAlign: 'right',
                                tooltip: 'Limpiar',
                                handler: function () {
                                    PANEL_LABELCONFIG.down('[name=paramBusquedaLabel_config]').reset();
                                    Ext.getStore('label_config.s_Label_config').clearFilter();
                                    limpiarFormularioLabel_config();
                                }
                            },
                            {
                                width: '5%',
                                xtype: 'button',
                                iconCls: 'x-fa fa-refresh',
                                iconAlign: 'right',
                                tooltip: 'Recargar',
                                handler: function () {
                                    Ext.getStore('label_config.s_Label_config').reload();
                                }
                            }
                        ],
                        plugins: {
                            ptype: 'rowediting',
                            clicksToEdit: 2
                        },
                        columns: [
                            {tooltip: 'Nombre label', header: 'Nombre label', dataIndex: 'nombre', filter: true, flex: 3, cellWrap: true,
                                renderer: function (value) {
                                    if (value === '') {
                                        return '<span style="color:#999999;">Ingresar nombre..</span>';
                                    } else {
                                        return value;
                                    }
                                },
                                editor: {
                                    xtype: 'textfield',
                                    allowBlank: false,
                                    listeners: {
                                        change: function (component, b, c, d) {
//                                            var grid = component.up('grid');
//                                            var record = grid.editingPlugin.context.record;
//                                            if (record.data.habilitado) {
//                                                record.set('habilitado', true);
//                                            } else {
//                                                record.set('habilitado', false);
//                                            }
                                        }
                                    }
                                }
                            },
                            {tooltip: 'Descripción del label', header: 'Descripción del label', dataIndex: 'descripcion', filter: true, flex: 3, cellWrap: true,
                                renderer: function (value) {
                                    if (value === '') {
                                        return '<span style="color:#999999;">Ingresar descripción..</span>';
                                    } else {
                                        return value;
                                    }
                                },
                                editor: {
                                    xtype: 'textareafield',
                                    allowBlank: false,
                                    listeners: {
                                        change: function (component, b, c, d) {
//                                            var grid = component.up('grid');
//                                            var record = grid.editingPlugin.context.record;
//                                            if (record.data.habilitado) {
//                                                record.set('habilitado', true);
//                                            } else {
//                                                record.set('habilitado', false);
//                                            }
                                        }
                                    }
                                }
                            },
                            {tooltip: 'Fecha registro', header: 'Fecha registro', dataIndex: 'dateCreate', xtype: 'datecolumn', format: 'Y/m/d H:i:s', filter: true, flex: 2, cellWrap: true},
                            {tooltip: 'Fecha actualizo', header: 'Fecha actualizo', dataIndex: 'dateUpdate', xtype: 'datecolumn', format: 'Y/m/d H:i:s', filter: true, flex: 2, cellWrap: true},
                            {hidden: true, tooltip: 'Id', header: 'Id', dataIndex: 'id', filter: true, flex: 1, cellWrap: true, renderer: showTipConten}
                        ],
                        columnLines: true,
                        height: 210,
                        split: true,
                        region: 'north',
                        listeners: {
                            selectionchange: 'onSelectChangeGridLabel_config',
                            beforeitemclick: function (thisObj, record, item, index, e, eOpts) {
                                PANEL_LABELCONFIG.down('[name=btnEditar]').enable();
                                PANEL_LABELCONFIG.down('[name=btnCrear]').disable();
                            },
                            deselect: function () {
                                PANEL_LABELCONFIG.down('[name=btnEditar]').disable();
                                PANEL_LABELCONFIG.down('[name=btnCrear]').enable();
                            },
                            rowdblclick: showAuditoria
                        },
                        viewConfig: {
                            emptyText: '<center>No existen resultados.</center>',
                            getRowClass: function (record) {
                                if (record.data.nuevo) {
                                    return 'newRowGrid';
                                }
                            }
                        }
                    }]
            }
        ];
        this.callParent(arguments);
    }
});
