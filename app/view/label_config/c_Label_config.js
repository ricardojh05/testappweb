Ext.define('Ktaxi.view.label_config.c_Label_config', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Label_config',
    onViewLabel_config: function (panelLoad) {
        validarPermisosGeneral(panelLoad);
        PANEL_LABELCONFIG = Ext.getCmp('panelLabel_config');
        Ext.getStore('label_config.s_Label_config').load();
    },
    onChangeSearchLabel_config: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var paramLabel_config = PANEL_LABELCONFIG.down('[name=paramBusquedaLabel_config]').getValue();
            Ext.getStore('label_config.s_Label_config').load({
                params: {
                    param: paramLabel_config
                },
                callback: function (records) {
                    if (records.length <= 0) {
                        Ext.getStore('label_config.s_Label_config').removeAll();
                    }
                }
            });
        }
    },
    onAddRecord: function () {
        var newRecord = {
            nombre: '',
            descripcion: '',
            dateCreate: new Date(),
            nuevo: true
        };
        Ext.getStore('label_config.s_Label_config').insert(0, newRecord);
    },
    onSaveChanges: function () {
        Ext.getStore('label_config.s_Label_config').sync();
    }
});
function limpiarFormularioLabel_config() {
    var gridLeerLabel_config = PANEL_LABELCONFIG.down('[name=gridLeerLabel_config]');
    gridLeerLabel_config.getView().deselect(gridLeerLabel_config.getSelection());
    gridLeerLabel_config.getStore().load();
}