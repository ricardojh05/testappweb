Ext.define('Ktaxi.view.lugares.c_Lugares', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_Lugares',
    onView: function (panelLoad) {
        panelLoad.ID_MAPA = ID_MAPA_LUGARES;
        var MAPA = cargarMapa(panelLoad, ID_MAPA_LUGARES);
        panelLoad.down('[name=gridLugares]').setLoading(true);
        MAPA.on('load', function () {
            panelLoad.down('[name=gridLugares]').setLoading(false);
        });
        validarPermisosGeneral(panelLoad);
        panelLoad.down('[name=gridLugares]').getStore().load();
        MODULO_LUGARES = panelLoad;
    },
    onChangeSearchLugar: function (btn, e) {
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            var moduloRutas = Ext.getCmp('moduloRutas');
            var paramBusqueda = moduloRutas.down('[name=paramBusquedaRuta]').getValue();
            var idCompania = moduloRutas.down('[name=company]').getValue();
            var params = {};
            if (idCompania)
                params = {param: paramBusqueda, idCompania: idCompania};
            else
                params = {param: paramBusqueda};

            Ext.getStore('lugares.s_Lugares').load({
                params: params,
                callback: function (records) {
                    if (records.length <= 0) {
                        Ext.getStore('lugares.s_Lugares').removeAll();
                    }
                }
            });
            this.onDeselectedGridChangeLugar();
        }
    },
    onExpandLugares: function () {
        MODULO_LUGARES.down('[name=panelPuntos]').collapse();
        var selected = MODULO_LUGARES.down('[name=gridLugares]').getSelection();
        if (selected.length > 0) {
            cleanLayerPoligon(LIST_MAPS[ID_MAPA_LUGARES], 'lugar');
            graficarPoligono(selected[0].get('limite'), LIST_MAPS[ID_MAPA_LUGARES], zoomMapaLugares);
        }
    },
    onExpandPuntos: function () {
        MODULO_LUGARES.down('[name=panelLugares]').collapse();
    },
    onSelectGridLugares: function (thisObj, selected, eOpts) {
        if (selected) {
            MODULO_LUGARES.down('[name=panelCrearEditarPunto]').enable();
            limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES], true);
            limpiarFormularioPunto();
            MODULO_LUGARES.down('[ident=editarLugar]').enable();
            MODULO_LUGARES.down('[ident=crearLugar]').disable();
            var form = MODULO_LUGARES.down('[name=panelCrearEditarLugar]');
            graficarPoligono(selected.get('limite'), LIST_MAPS[ID_MAPA_LUGARES], zoomMapaLugares);
            var comboFormAplicativo = form.down('[name=idAplicativo]');
            if (!isInStore(comboFormAplicativo.getStore(), selected.data.idAplicativo, 'id')) {
                comboFormAplicativo.getStore().load({
                    params: {
                        param: selected.data.idAplicativo
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormAplicativo.setValue(records);
                        }
                    }
                });
            }
            var comboFormCiudad = form.down('[name=idCiudad]');
            if (!isInStore(comboFormCiudad.getStore(), selected.data.idCiudad, 'id')) {
                comboFormCiudad.getStore().load({
                    params: {
                        param: selected.data.idCiudad
                    },
                    callback: function (records) {
                        if (records.length > 0) {
                            comboFormCiudad.setValue(records);
                        }
                    }
                });
            }
            form.loadRecord(selected);
        }
    },
    onDeselectGridLugares: function () {
        MODULO_LUGARES.down('[name=panelCrearEditarPunto]').disable();
        MODULO_LUGARES.down('[ident=editarLugar]').disable();
        MODULO_LUGARES.down('[ident=crearLugar]').enable();
        limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES], true);
        MODULO_LUGARES.down('[name=panelCrearEditarLugar]').reset();
    },
    onCargarPuntos: function (obj, rown, column, event, e, record) {
        //Cargar puntos de un lugar especifico
        MODULO_LUGARES.down('[name=gridPuntos]').getStore().proxy.extraParams.idLugar = record.get('id');
        MODULO_LUGARES.down('[name=gridPuntos]').getStore().load();
        MODULO_LUGARES.down('[name=panelPuntos]').setTitle('Puntos en: "' + record.get('detalle') + '"');
        MODULO_LUGARES.down('[name=panelPuntos]').expand();
        MODULO_LUGARES.down('[name=gridLugares]').setSelection(record);
        //Se limpiar el mapa
        limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES]);
        cleanLayerPoligon(LIST_MAPS[ID_MAPA_LUGARES], 'lugar');
        //Se dibuja geocerca del lugar
        var listCoordenadas = getCoordenadasString(record.get('limite'), 'latlng', LIST_MAPS[ID_MAPA_LUGARES], false);
        var coordCentrar = getCenterPolygon(listCoordenadas);
        centrar(LIST_MAPS[ID_MAPA_LUGARES], coordCentrar[1], coordCentrar[0], zoomMapaLugares);
        var geometryObj = {coordinates: [listCoordenadas], type: "Polygon"};
        drawLayerPoligon(geometryObj, LIST_MAPS[ID_MAPA_LUGARES], 'lugar', record.get('detalle'), record.get('color'));
    },
    onCreateLugar: function () {
        var form = MODULO_LUGARES.down('[name=panelCrearEditarLugar]');
        if (form.isValid()) {
            if (form.down('[name=limite]').getValue() !== '') {
                var store = MODULO_LUGARES.down('[name=gridLugares]').getStore();
                store.insert(0, form.getValues());
                store.sync();
            } else {
                notificaciones('POR FAVOR DIBUJE UNA ÁREA EN EL MAPA CON LA OPCIÓN <img src="img/sistema/icon_draw-poligon.png">', 2);
            }
        } else {
            notificaciones(MENSAJE_CAMPOS_OBLIGATORIOS, 2);
        }
    },
    onUpdateLugar: function () {
        var form = MODULO_LUGARES.down('[name=panelCrearEditarLugar]');
        if (form.isValid()) {
            if (form.down('[name=limite]').getValue() !== '') {
                form.updateRecord(form.activeRecord);
                var store = MODULO_LUGARES.down('[name=gridLugares]').getStore();
                store.sync();
            } else {
                notificaciones('POR FAVOR DIBUJE UNA ÁREA EN EL MAPA CON LA OPCIÓN <img src="img/sistema/icon_draw-poligon.png">', 2);
            }
        } else {
            notificaciones(MENSAJE_CAMPOS_OBLIGATORIOS, 2);
        }
    },
    onSelectGridPunto: function (thisObj, selected, eOpts) {
        if (selected) {
            MODULO_LUGARES.down('[ident=editarPunto]').enable();
            MODULO_LUGARES.down('[ident=crearPunto]').disable();
            limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES]);
            var form = MODULO_LUGARES.down('[name=panelCrearEditarPunto]');
            form.loadRecord(selected);
            if (selected.get('imagen') === '') {
                form.down('[name=image]').setSrc(URL_IMG_UPLOADS_LUGARES + 'default.png');
            } else {
                form.down('[name=image]').setSrc(URL_IMG_UPLOADS_LUGARES + selected.get('imagen'));
            }
            graficarMarcador(LIST_MAPS[ID_MAPA_LUGARES], selected.get('latitud'), selected.get('longitud'), selected.get('referencia'), zoomMapaLugares);
        }
    },
    onDeselectGridPunto: function () {
        MODULO_LUGARES.down('[ident=editarPunto]').disable();
        MODULO_LUGARES.down('[ident=crearPunto]').enable();
        limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES]);
        MODULO_LUGARES.down('[name=panelCrearEditarPunto]').reset();
    },
    onCreatePunto: function () {
        var form = MODULO_LUGARES.down('[name=panelCrearEditarPunto]');
        var formUploadImage = MODULO_LUGARES.down('[name=formUploadImage]');
        if (form.isValid()) {
            var lugar = MODULO_LUGARES.down('[name=gridLugares]').getSelection()[0];
            var store = MODULO_LUGARES.down('[name=gridPuntos]').getStore();
            var values = form.getValues();
            if (changeImagePunto) {
                values.imagen = 'default.png';
            }
            values.idLugar = lugar.getId();
            store.insert(0, values);
            store.sync({
                callback: function (response) {
                    if (changeImagePunto) {
                        var respuesta = response.operations[0]._response.responseText;
                        if (isJsonString(respuesta)) {
                            respuesta = JSON.parse(respuesta);
                            formUploadImage.submit({
                                url: 'php/Uploads/upload.php?ruta=' + URL_SUBIR_IMG_PUNTOS_LUGARES + '&nombre=' + respuesta.id,
                                waitMsg: 'Cargando imagen...',
                                success: function (fp, o) {
                                    notificaciones('IMAGÉN CARGADA CORRECTAMENTE.', 1);
                                    limpiarFormularioPunto();
                                },
                                failure: function (fp, action) {
                                    if (action.result.success === false) {
                                        notificaciones(action.result.message, 2);
                                    }
                                    limpiarFormularioPunto();
                                }
                            });
                        }
                    } else {
                        limpiarFormularioPunto();
                    }
                }
            });
        } else {
            notificaciones(MENSAJE_CAMPOS_OBLIGATORIOS, 2);
        }
    },
    onUpdatePunto: function () {
        var form = MODULO_LUGARES.down('[name=panelCrearEditarPunto]');
        var formUploadImage = MODULO_LUGARES.down('[name=formUploadImage]');
        if (form.isValid()) {
            form.updateRecord(form.activeRecord);
            var record = MODULO_LUGARES.down('[name=gridPuntos]').getSelection()[0];
            var store = MODULO_LUGARES.down('[name=gridPuntos]').getStore();
            if (changeImagePunto) {
                record.set('imagen', record.getId() + '.png');
            }
            store.sync({
                callback: function (response) {
                    if (changeImagePunto) {
                        var respuesta = response.operations[0]._response.responseText;
                        if (isJsonString(respuesta)) {
                            respuesta = JSON.parse(respuesta);
                            if (respuesta.success) {
                                formUploadImage.submit({
                                    url: 'php/Uploads/upload.php?ruta=' + URL_SUBIR_IMG_PUNTOS_LUGARES + '&nombre=' + record.getId(),
                                    waitMsg: 'Cargando imagen...',
                                    success: function (fp, o) {
                                        notificaciones('IMAGÉN CARGADA CORRECTAMENTE.', 1);
                                        limpiarFormularioPunto();
                                    },
                                    failure: function (fp, action) {
                                        if (action.result.success === false) {
                                            notificaciones(action.result.message, 2);
                                        }
                                        limpiarFormularioPunto();
                                    }
                                });
                            }
                        }
                    } else {
                        limpiarFormularioPunto();
                    }
                }
            });
        } else {
            notificaciones(MENSAJE_CAMPOS_OBLIGATORIOS, 2);
        }
    }
});

function limpiarFormularioLugar() {
    MODULO_LUGARES.down('[name=gridLugares]').getSelectionModel().deselectAll();
    MODULO_LUGARES.down('[name=panelCrearEditarLugar]').reset();
    MODULO_LUGARES.down('[name=gridLugares]').getStore().reload();
    MODULO_LUGARES.down('[name=panelPuntos]').setTitle(TITULO_PUNTOS);
    limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES], true);
    limpiarFormularioPunto(true);
}
function limpiarFormularioPunto(remover) {
    MODULO_LUGARES.down('[name=gridPuntos]').getSelectionModel().deselectAll();
    MODULO_LUGARES.down('[name=panelCrearEditarPunto]').reset();
    if (!remover) {
        MODULO_LUGARES.down('[name=gridPuntos]').getStore().reload();
    } else {
        MODULO_LUGARES.down('[name=gridPuntos]').getStore().removeAll();
    }
    MODULO_LUGARES.down('[name=panelCrearEditarPunto]').down('[name=image]').setSrc(URL_IMG_UPLOADS_LUGARES + 'default.png');
    limpiarMapa(LIST_MAPS[ID_MAPA_LUGARES]);
    changeImagePunto = false;
}