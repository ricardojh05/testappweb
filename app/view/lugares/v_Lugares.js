var TITULO_LUGAR = "Lugares";
var TITULO_PUNTOS = "Puntos";
var MODULO_LUGARES, changeImagePunto = false, zoomMapaLugares = 16;
Ext.define('Ktaxi.view.lugares.v_Lugares', {
    extend: 'Ext.panel.Panel',
    xtype: 'lugares',
    height: HEIGT_VIEWS,
    layout: 'border',
    controller: 'c_Lugares',
    bodyBorder: false,
    requires: [
        'Ext.layout.container.Border',
        'Ktaxi.view.lugares.c_Lugares',
        'Ext.slider.Single'
    ],
    defaults: {
        collapsible: true,
        collapsed: false,
        collapseMode: 'mini',
        split: true
    },
    listeners: {
        afterrender: 'onView'
    },
    initComponent: function () {
        this.items = [
            {
                title: TITULO_LUGAR,
                region: 'west',
                xtype: 'panel',
                name: 'panelLugares',
                padding: 5,
                flex: 2,
                layout: 'fit',
                collapsible: true,
                collapsed: false,
                collapseMode: 'mini',
                collapseDirection: 'left',
                listeners: {
                    expand: 'onExpandLugares'
                },
                items: [
                    {
                        name: 'panelCrearEditarLugar',
                        xtype: 'form',
                        width: '100%',
                        ui: 'light',
                        cls: 'quick-graph-panel shadow',
                        layout: 'fit',
                        items: [
                            {
                                xtype: 'panel',
                                layout: 'vbox',
                                defaultType: 'textfield',
                                width: '100%',
                                bodyPadding: 5,
                                defaults: {
                                    width: '100%',
                                    allowBlank: false,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    minLengthText: MINIMUMMESSAGUEREQUERID,
                                    maxLengthText: MAXIMUMMESSAGUEREQURID,
                                    allowOnlyWhitespace: false
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        layout: 'hbox',
                                        margin: '0 0 5 0',
                                        items: [
                                            {
                                                flex: 1.2,
                                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                allowBlank: false,
                                                name: 'idAplicativo',
                                                xtype: 'combobox',
                                                emptyText: 'Seleccionar ciudad',
                                                fieldLabel: 'Aplicativo',
                                                displayField: 'text',
                                                minChars: 0,
                                                typeAhead: true,
                                                valueField: 'id',
                                                queryParam: 'param',
                                                queryMode: 'remote',
                                                store: Ext.create('Ktaxi.store.combos.s_Aplicativos')
                                            }, {
                                                labelWidth: 50,
                                                margin: '0 0 0 5',
                                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                                allowBlank: false,
                                                name: 'idCiudad',
                                                xtype: 'combobox',
                                                emptyText: 'Seleccionar aplicativo',
                                                fieldLabel: 'Ciudad',
                                                displayField: 'text',
                                                minChars: 0,
                                                typeAhead: true,
                                                valueField: 'id',
                                                queryParam: 'param',
                                                queryMode: 'remote',
                                                store: Ext.create('Ktaxi.store.combos.s_Ciudades')
                                            }
                                        ]
                                    },
                                    {
                                        fieldLabel: 'Detalle',
                                        afterLabelTextTpl: INFOMESSAGEREQUERID,
                                        name: 'detalle',
                                        maxLength: '200',
                                        minLength: '3',
                                        emptyText: 'Origen'
                                    },
                                    {
                                        fieldLabel: 'Color',
                                        name: 'color',
                                        inputType: 'color',
                                        height: 25
                                    },
                                    {
                                        xtype: 'textareafield',
                                        fieldLabel: 'Descripción',
                                        name: 'descripcion',
                                        emptyText: 'Descripción..',
                                        maxLength: '200',
                                        minLength: '5',
                                        allowBlank: true,
                                        allowOnlyWhitespace: true
                                    },
                                    {
                                        xtype: 'checkbox',
                                        uncheckedValue: 0,
                                        name: 'habilitado',
                                        boxLabel: 'Habilitado',
                                        inputValue: 1,
                                        checked: true
                                    },
                                    {name: 'limite', hidden: true, allowBlank: true, allowOnlyWhitespace: true},
                                    {
                                        xtype: 'grid',
                                        allowDeselect: true,
                                        margin: '10 0 5 0',
                                        columnLines: true,
                                        plugins: ['gridfilters'],
                                        flex: 1,
                                        minHeight: 100,
                                        name: 'gridLugares',
                                        autoScroll: true,
                                        store: Ext.create('Ktaxi.store.lugares.s_Lugares'),
                                        header: false,
                                        headerAsText: false,
                                        collapsible: true,
                                        viewConfig: {
                                            enableTextSelection: true,
                                            emptyText: '<center>No existen resultados.</center>'
                                        },
                                        columns: [
                                            Ext.create('Ext.grid.RowNumberer', {header: '#', width: 30, align: 'center'}),
                                            {tooltip: 'Aplicativo', header: 'Aplicativo', dataIndex: 'aplicativo', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: 'Ciudad', header: 'Ciudad', dataIndex: 'ciudad', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: 'Detalle', header: 'Detalle', dataIndex: 'detalle', filter: true, flex: 2, renderer: showTipConten},
                                            {tooltip: 'Color', text: 'Color', dataIndex: 'color', flex: 1, renderer: formatColor},
                                            {tooltip: 'Descripción', header: 'Descripción', dataIndex: 'descripcion', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: "Estado", header: "Estado", align: 'center', flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoPunto},
                                            {tooltip: "ID", text: "ID", width: 40, dataIndex: 'id', sortable: true, renderer: showTipConten, hidden: true},
                                            {
                                                header: 'Ver puntos',
                                                align: 'center',
                                                tooltip: 'Ver puntos',
                                                menuDisabled: true,
                                                sortable: false,
                                                width: 50,
                                                xtype: 'actioncolumn',
                                                items: [{
                                                        getClass: function (v, meta, rec) {
                                                            return 'gridAuxDelete x-fa fa-map-marker';
                                                        },
                                                        handler: 'onCargarPuntos'
                                                    }]
                                            }
                                        ],
                                        listeners: {
                                            select: 'onSelectGridLugares',
                                            deselect: 'onDeselectGridLugares',
                                            rowdblclick: showAuditoria
                                        },
                                        bbar: [
                                            {
                                                xtype: 'label',
                                                name: 'numRegistrosGridL',
                                                text: '0 registros'
                                            },
//                                            {
//                                                xtype: 'button',
//                                                text: 'Exportar',
//                                                iconCls: 'x-fa fa-download',
//                                                handler: function (btn) {
//                                                    onExportar(btn, "Puntos_Lugares ", this.up('grid'));
//                                                }
//                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        dockedItems: [{
                                ui: 'footer',
                                xtype: 'toolbar', dock: 'bottom',
                                defaults: {
                                    width: '25%',
                                    height: 30
                                },
                                items: [
                                    {
                                        text: 'Limpiar',
                                        tooltip: 'Limpiar',
                                        disabled: false,
                                        handler: function () {
                                            limpiarFormularioLugar();
                                        }
                                    },
                                    '->',
                                    {
                                        text: 'Editar',
                                        ident: 'editarLugar',
                                        tooltip: 'Editar',
                                        name: 'btnEditar',
                                        disabled: true,
                                        handler: 'onUpdateLugar'
                                    }, {
                                        text: 'Crear',
                                        ident: 'crearLugar',
                                        tooltip: 'Crear Equipo',
                                        name: 'btnCrear',
                                        disabled: false,
                                        handler: 'onCreateLugar'
                                    }]
                            }]
                    }
                ]
            },
            {
//                disabled: true,
                region: 'center',
                xtype: 'panel',
                name: 'pnlRutasMapa',
                flex: 3,
                margin: 5,
                header: false,
                collapseMode: 'mini',
                html: '<div id="' + ID_MAPA_LUGARES + '" style="position: absolute; top: 0; left: 0; z-index: 0; width: 100%; height: 100%"></div><button id="btnShowFormPunto" title="Mostrar Formulario" onclick="showPanelCrearEditarPunto();" class="btnMap" style="top: 150px; display:none;"><span class="fas fa-pencil-alt"></span></button>'
            },
            {
                region: 'east',
                name: 'panelPuntos',
                title: TITULO_PUNTOS,
                cls: 'panelCrearEditar',
                xtype: 'form',
                layout: 'fit',
                padding: 5,
                flex: 2,
                collapsible: true,
                collapsed: true,
                listeners: {
                    expand: 'onExpandPuntos'
                },
                items: [
                    {
                        name: 'panelCrearEditarPunto',
                        disabled: true,
                        xtype: 'form',
                        width: '100%',
                        ui: 'light',
                        cls: 'quick-graph-panel shadow',
                        layout: 'fit',
                        items: [
                            {
                                xtype: 'panel',
                                layout: 'vbox',
                                defaultType: 'textfield',
                                width: '100%',
                                bodyPadding: 5,
                                defaults: {
                                    width: '100%',
                                    allowBlank: false,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    minLengthText: MINIMUMMESSAGUEREQUERID,
                                    maxLengthText: MAXIMUMMESSAGUEREQURID,
                                    //afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    allowOnlyWhitespace: false
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype: 'container',
                                                defaultType: 'textfield',
                                                flex: 3,
                                                defaults: {
                                                    width: '100%',
                                                    anchor: '100%'
                                                },
                                                items: [
                                                    {
                                                        fieldLabel: '',
                                                        name: 'barrio',
                                                        maxLength: '45',
                                                        minLength: '5',
                                                        emptyText: 'Barrio'
                                                    },
                                                    {
                                                        name: 'principal',
                                                        maxLength: '120',
                                                        minLength: '5',
                                                        emptyText: 'Calle principal'
                                                    },
                                                    {
                                                        name: 'secundaria',
                                                        maxLength: '120',
                                                        minLength: '5',
                                                        emptyText: 'Calle secundaria'
                                                    },
                                                    {
                                                        name: 'referencia',
                                                        maxLength: '120',
                                                        minLength: '5',
                                                        emptyText: 'Referencia'
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: 'form',
                                                style: 'text-align:center',
                                                name: 'formUploadImage',
                                                margin: '0 0 0 5',
                                                width: 100,
                                                bodyStyle: 'border: 0px;',
                                                items: [
                                                    {
                                                        xtype: 'image', src: URL_IMG_UPLOADS_LUGARES + 'default.png', name: 'image', height: 100, width: 100, style: 'text-align:center; border-radius: 50%; border: solid; border-color: #5ecac2;background-size: auto 100%;background-position: center;',
                                                        listeners: {
                                                            render: function (me) {
                                                                me.el.on({error: function (e, t, eOmpts) {
                                                                        me.setSrc(URL_IMG_UPLOADS_LUGARES + 'default.png');
                                                                    }});
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: 'filefield',
                                                        allowOnlyWhitespace: true,
                                                        buttonOnly: true,
                                                        iconCls: 'x-fa fa-camera',
                                                        name: 'photo',
                                                        style: 'position: fixed; bottom: -5px;',
                                                        msgTarget: 'side',
                                                        allowBlank: true,
                                                        width: '100%',
                                                        anchor: '100%',
                                                        buttonConfig: {
                                                            text: '',
                                                            iconCls: 'x-fa fa-camera'
                                                        },
                                                        listeners: {
                                                            afterrender: function (cmp) {
                                                                cmp.fileInputEl.set({
                                                                    accept: '.png, .jpg, .jpeg'
                                                                });
                                                            },
                                                            change: function (evt, a, b) {
                                                                changeImagePunto = true;
                                                                var fieldImage = MODULO_LUGARES.down('[name=image]');
                                                                if (evt.fileInputEl.dom.files && evt.fileInputEl.dom.files) {
                                                                    var reader = new FileReader();
                                                                    reader.onload = function (e) {
                                                                        fieldImage.setSrc(e.target.result);
                                                                    };
                                                                    reader.readAsDataURL(evt.fileInputEl.dom.files[0]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        layout: 'hbox',
                                        items: [
                                            {width: 50, height: 50, xtype: 'button', iconCls: 'x-fa fa-map-marker', tooltip: 'Obtener coordenadas', handler: function () {
                                                    addClickPoint(LIST_MAPS[ID_MAPA_LUGARES], MODULO_LUGARES, true);
                                                }},
                                            {
                                                xtype: 'container',
                                                margin: '0 0 0 5',
                                                defaultType: 'textfield',
                                                flex: 3,
                                                defaults: {
                                                    width: '100%',
                                                    anchor: '100%'
                                                },
                                                items: [
                                                    {name: 'latitud', emptyText: 'Latitud', maxLength: '100', minLength: '4', maskRe: /[0-9.]/},
                                                    {name: 'longitud', emptyText: 'Longitud', maxLength: '100', minLength: '4', maskRe: /[0-9.]/}
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'checkbox',
                                        uncheckedValue: 0,
                                        name: 'habilitado',
                                        boxLabel: 'Habilitado',
                                        inputValue: 1,
                                        checked: true
                                    },
                                    {
                                        xtype: 'grid',
                                        margin: '10 0 5 0',
                                        columnLines: true,
                                        plugins: ['gridfilters'],
                                        flex: 1,
                                        minHeight: 100,
                                        name: 'gridPuntos',
                                        autoScroll: true,
                                        store: Ext.create('Ktaxi.store.lugares.s_Punto'),
                                        header: false,
                                        headerAsText: false,
                                        collapsible: true,
                                        viewConfig: {
                                            enableTextSelection: true,
                                            emptyText: '<center>No existen resultados.</center>'
                                        },
                                        columns: [
                                            Ext.create('Ext.grid.RowNumberer', {header: '#', width: 30, align: 'center'}),
                                            {tooltip: 'Barrio', header: 'Barrio', dataIndex: 'barrio', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: 'Calle Principal', header: 'C. Principal', dataIndex: 'principal', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: 'Calle Secundaria', header: 'C. Secundaria', dataIndex: 'secundaria', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: 'Referencia', header: 'Referencia', dataIndex: 'referencia', filter: true, flex: 1, renderer: showTipConten},
                                            {tooltip: "Estado", header: "Estado", align: 'center', flex: 1, dataIndex: 'habilitado', sortable: true, renderer: formatEstadoPunto},
                                            {tooltip: "ID", text: "ID", width: 40, dataIndex: 'id', sortable: true, renderer: showTipConten, hidden: true}
                                        ],
                                        listeners: {
                                            select: 'onSelectGridPunto',
                                            deselect: 'onDeselectGridPunto',
                                            rowdblclick: showAuditoria
                                        },
                                        bbar: [
                                            {
                                                xtype: 'label',
                                                name: 'numRegistrosGridP',
                                                text: '0 registros'
                                            },
//                                            {
//                                                xtype: 'button',
//                                                text: 'Exportar',
//                                                iconCls: 'x-fa fa-download',
//                                                handler: function (btn) {
//                                                    onExportar(btn, "Puntos_Lugares ", this.up('grid'));
//                                                }
//                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        dockedItems: [{
                                ui: 'footer',
                                xtype: 'toolbar', dock: 'bottom',
                                defaults: {
                                    width: '25%',
                                    height: 30
                                },
                                items: [
                                    {
                                        text: 'Editar',
                                        ident: 'editarPunto',
                                        tooltip: 'Editar',
                                        name: 'btnEditar',
                                        disabled: true,
                                        handler: 'onUpdatePunto'
                                    }, {
                                        text: 'Crear',
                                        ident: 'crearPunto',
                                        tooltip: 'Crear Equipo',
                                        name: 'btnCrear',
                                        disabled: false,
                                        handler: 'onCreatePunto'
                                    },
                                    '->',
                                    {
                                        text: 'Limpiar',
                                        tooltip: 'Limpiar',
                                        disabled: false,
                                        handler: function () {
                                            limpiarFormularioPunto();
                                        }
                                    }
                                ]
                            }]
                    }
                ]
            }
        ];
        this.callParent(arguments);
    }
});