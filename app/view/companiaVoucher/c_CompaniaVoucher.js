var MODULO_COMPANIA_VOUCHER;
var idCompaniaVoucher;
var sucursal;
var idSucursal;
var anio;
var mes;
var arrayIdMesesComp = [];
var arrayNombresMesesComp = [];


var anios = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: '2019',
            "text": "2019"
        },
        {
            id: '2020',
            "text": "2020"
        },
        {
            id: '2021',
            "text": "2021"
        },
        {
            id: '2022',
            "text": "2022"
        },
        {
            id: '2023',
            "text": "2023"
        },
        {
            id: '2024',
            "text": "2024"
        },
        {
            id: '2025',
            "text": "2025"
        },
        {
            id: '2026',
            "text": "2026"
        },
        {
            id: '2027',
            "text": "2027"
        },
        {
            id: '2028',
            "text": "2028"
        }
    ]
});
var meses = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 1,
            "text": "Enero"
        },
        {
            id: 2,
            "text": "Febrero"
        },
        {
            id: 3,
            "text": "Marzo"
        },
        {
            id: 4,
            "text": "Abril"
        },
        {
            id: 5,
            "text": "Mayo"
        },
        {
            id: 6,
            "text": "Junio"
        },
        {
            id: 7,
            "text": "Julio"
        },
        {
            id: 8,
            "text": "Agosto"
        },
        {
            id: 9,
            "text": "Septiembre"
        },
        {
            id: 10,
            "text": "Octubre"
        },
        {
            id: 11,
            "text": "Noviembre"
        },
        {
            id: 12,
            "text": "Diciembre"
        },
    ]
});
var mesesHasta = Ext.create('Ext.data.Store', {
    fields: ['id', 'text'],
    data: [{
            id: 1,
            "text": "Enero"
        },
        {
            id: 2,
            "text": "Febrero"
        },
        {
            id: 3,
            "text": "Marzo"
        },
        {
            id: 4,
            "text": "Abril"
        },
        {
            id: 5,
            "text": "Mayo"
        },
        {
            id: 6,
            "text": "Junio"
        },
        {
            id: 7,
            "text": "Julio"
        },
        {
            id: 8,
            "text": "Agosto"
        },
        {
            id: 9,
            "text": "Septiembre"
        },
        {
            id: 10,
            "text": "Octubre"
        },
        {
            id: 11,
            "text": "Noviembre"
        },
        {
            id: 12,
            "text": "Diciembre"
        },
    ]
});

Ext.define('Ktaxi.view.companiaVoucher.c_CompaniaVoucher', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.c_CompaniaVoucher',

    onViewCompaniaVoucher: function (panelLoad) {
        MODULO_COMPANIA_VOUCHER = panelLoad;
        validarPermisosGeneral(panelLoad);
        storeCreditoCompaniaCompaniaVoucher = Ext.create('Ktaxi.store.companiaVoucher.s_CompaniaVoucher_Credito_Compania');
        storeSucursal = Ext.create('Ktaxi.store.companiaVoucher.s_CompaniaVoucher_Sucursal');
        Ext.Ajax.request({
            async: true,
            url: 'php/CompaniaVoucher/getCompaniaVoucher.php',
            callback: function (callback, e, response) {
                var res = JSON.parse(response.request.result.responseText);
                if (res.success) {
                    MODULO_COMPANIA_VOUCHER.down('[name=idCompaniaVoucher]').getStore().load({
                        params: {
                            idCompaniaVoucher: res.idCompaniaVoucher
                        },
                        callback: function (records, operation, success) {
                            MODULO_COMPANIA_VOUCHER.down('[name=idCompaniaVoucher]').setValue(res.idCompaniaVoucher);
                            idCompaniaVoucher = res.idCompaniaVoucher;
                        }
                    });
                    var params = {
                        anio: anio,
                        mes: mes,
                        idCompaniaVoucher: res.idCompaniaVoucher
                    };
                    recargarStoreCreditoCompania(params);
                    reacragarStoreSucrsalCompania(params);
                    setTextDefectoCompania();
                    //recargarStoreCompaniaVoucher(params);

                } else {
                    notificaciones('No se pudo cargar una compania por defecto', 2);
                }
            }
        });
    },

    onLimpiarGridSucursal: function (btn, e) {
        MODULO_COMPANIA_VOUCHER.down('[name=txtParam]').reset();
        var id = getIdCompaniaVoucher();
        var params = {
            anio: anio,
            mes: mes,
            idCompaniaVoucher: id
        };
        recargarStoreCreditoCompania(params);
        reacragarStoreSucrsalCompania(params);
        setTextDefectoCompania();
    },
    onRecargarGridSucursal: function () {
        var id = getIdCompaniaVoucher();
        var params = {
            anio: anio,
            mes: mes,
            idCompaniaVoucher: id
        };
        recargarStoreCreditoCompania(params);
        reacragarStoreSucrsalCompania(params);
        setTextDefectoCompania();
    },
    onRecargarGridConsumo: function () {
        MODULO_COMPANIA_VOUCHER.down('[name=administracionConsumo]').getStore().reload();
    },

    onCrearSucursalCredito: function () {
        var form = MODULO_COMPANIA_VOUCHER.down('[name=panelCrearSucursalCompaniaVoucher]').getForm();
        if (form.isValid()) {
            var val = form.getValues();
            if (val.nombresAC !== null && val.nombresAC !== '') {
                var numero = codigo();
                Ext.MessageBox.confirm('Atención!!!', '<h4>Está seguro que desea activar a ' + sucursal + ' como cliente companiaVoucher<br><b>CÓDIGO</b><br>' + numero + '<h4>', function (choice) {
                    if (choice === 'yes') {
                        var params = {};
                        var form = Ext.create('Ext.form.Panel');

                        if (idCompaniaVoucher === undefined && idCompaniaVoucher > 0)
                            params = {
                                id: recordSucursal.id,
                                codigo: numero
                            };
                        else
                            params = {
                                id: recordSucursal.id,
                                codigo: numero,
                                idE: idCompaniaVoucher,
                                idCompaniaVoucher: idCompaniaVoucher
                            };

                        form.submit({
                            url: 'php/CompaniaVoucher/createSucursal.php',
                            timeout: 6000000,
                            waitTitle: 'Procesando Información',
                            waitMsg: 'Procesando...',
                            params: params,
                            success: function (form, action) {
                                notificaciones('Sucursal Agregado Correctamente', 1);
                                var params = {
                                    anio: anio,
                                    mes: mes,
                                    idCompaniaVoucher: idCompaniaVoucher,
                                    idAplicativo: idAplicativo
                                };
                                recargarStoreCreditoCompania(params);
                                reacragarStoreSucrsalCompania(params);
                                setTextDefectoCompania();

                            },
                            failure: function (form, action) {
                                notificaciones('No se pudo realizar la operación, intentelo nuevamente', 2);
                                MODULO_COMPANIA_VOUCHER.down('[name=panelCrearSucursalCompaniaVoucher]').getForm().reset();
                            }
                        });
                    }
                });
            } else {
                messageInformationEffect('Debe dar click en buscar');
            }
        } else {
            messageInformationEffect('Debe dar click en buscar');
        }
    },
    onGetClass: function (v, meta, rec) {
        if (rec.data.tipo === 1) {
            return 'x-fa fa-user';
        } else if (rec.data.tipo == 2) {
            return 'x-fa fa-check';
        }
    },
    onSelectGridSucursal: function (thisObj, selected, eOpts) {
        idSucursal = selected.data.idSucursal,
            idCompaniaVoucher = selected.data.idCompaniaVoucher,
            //anio = selected.data.anio,
            //mes = selected.data.mes
        sucursal = selected.data.sucursal;
        idCompaniaSucursal = selected.data.idCompaniaSucursal;
        MODULO_COMPANIA_VOUCHER.down('[name=asignarCredito]').enable();
        MODULO_COMPANIA_VOUCHER.down('[name=administracionConsumo]').getStore().load({
            params: {
                idSucursal: selected.data.idSucursal,
                idCompaniaVoucher: selected.data.idCompaniaVoucher,
                anio: selected.data.anio,
                mes: selected.data.mes
            },
            callback: function (records, operation, success) {
                MODULO_COMPANIA_VOUCHER.down('[name=clienteAdmCosumo]').setText('Sucursal: ' + selected.data.sucursal);
                MODULO_COMPANIA_VOUCHER.down('[name=creditoAdmCosumo]').setText('Crédito: $' + selected.data.creditoMes);
                MODULO_COMPANIA_VOUCHER.down('[name=consumoAdmCosumo]').setText('Cosumido: $' + selected.data.consumoMes);
            }
        });
    },
    onShowVentanaAsignarCredito: function () {
        vnt_AsignarCredito = Ext.create('Ext.window.Window', {
            title: 'Ventana de asignar credito a un sucursal',
            iconCls: 'icon-services',
            layout: 'anchor',
            //            closeAction: 'hide',
            closeAction: 'destroy',
            resizable: false,
            closable: true,
            modal: true,
            plain: false,
            width: 800,
            height: 380,
            items: [{
                    layout: 'anchor',
                    width: '100%',
                    height: '100%',
                    xtype: 'panel',
                    name: 'pnl-AsigCred',
                    items: [{
                            xtype: 'label',
                            text: 'Agregar credito a ' + sucursal,
                            id: 'lbl-titleACr',
                            name: 'lbl-titleACr',
                            style: 'aling:center;font-size: 22px; font-weight: bold;'
                        },
                        {
                            xtype: 'label',
                            html: '<hr>',
                            id: 'lbl-lineaACr',
                            name: 'lbl-lineaACr',
                            style: 'font-size: 12px;'
                        },
                        {
                            xtype: 'label',
                            text: 'El dinero que usted asigne al sucursal se podra usar solo por este mes.',
                            id: 'lbl-descrACr',
                            name: 'lbl-descrACr',
                            style: 'font-size: 12px;'
                        }
                    ]
                },
                {
                    layout: 'anchor',
                    width: '100%',
                    xtype: 'form',
                    height: '100%',
                    name: 'formAsignarCredito',
                    items: [{
                            layout: {
                                type: 'hbox',
                                pack: 'start',
                                align: 'stretch'
                            },
                            items: [{
                                    name: 'dateAnios',
                                    xtype: 'combobox',
                                    emptyText: 'Años',
                                    fieldLabel: '<b>Año</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    value: anio,
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    store: anios
                                },
                                {
                                    name: 'dateMes',
                                    xtype: 'combobox',
                                    emptyText: 'Mes',
                                    fieldLabel: '<b>Mes</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    store: meses,
                                    listeners: {
                                        select: function (combo, record, eOpts) {
                                            arrayNombresMesesComp = [];
                                            arrayIdMesesComp = [];
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').getStore().removeAll();                                            var rangoincial = record.data.id;
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').setValue(0);                                            var rangoincial = record.data.id;
                                            for (let i = 0; i < 4; i++) {
                                                arrayIdMesesComp.push(rangoincial);
                                                arrayNombresMesesComp.push(getNombreMes(rangoincial));
                                                rangoincial++;
                                            }
                                            vnt_AsignarCredito.down('[name=dateMesHasta]').setStore(arrayNombresMesesComp);
                                        }
                                    }
                                },
                                {
                                    name: 'dateMesHasta',
                                    xtype: 'combobox',
                                    emptyText: 'Mes',
                                    fieldLabel: '<b>Hasta el Mes</b>',
                                    displayField: 'text',
                                    minChars: 0,
                                    typeAhead: true,
                                    valueField: 'id',
                                    queryParam: 'param',
                                    queryMode: 'remote',
                                    //hidden:true,
                                    store: mesesHasta
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'numberfield',
                                    afterLabelTextTpl: INFOMESSAGEREQUERID,
                                    blankText: INFOMESSAGEBLANKTEXT,
                                    allowOnlyWhitespace: false,
                                    maxValue: 1000,
                                    minValue: 0,
                                    name: 'montoACr',
                                    id: 'montoACr',
                                    fieldLabel: '<b>Monto</b>',
                                    flex: 2,
                                    emptyText: 0,
                                    enableKeyEvents: true,
                                    decimalPrecision: 2,
                                    allowDecimals: true,
                                    alwaysDisplayDecimals: true,
                                    listeners: {
                                        blur: function (thisObj, e, eOpts) {
                                            var idC = Ext.getCmp('nombresACr').getValue();
                                            if (idC === null) {
                                                Ext.getCmp('nombresACr').focus(true);
                                            } else {
                                                Ext.getCmp('razonACr').focus(true);
                                            }
                                        }
                                    }
                                },
                                {
                                    flex: 2,
                                    xtype: 'label',
                                    text: '0.00',
                                    id: 'lbl-creditoLim',
                                    name: 'lbl-creditoLim',
                                    style: 'font-size: 13px;font-weight: bold;'
                                }
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                    xtype: 'combobox',
                                    emptyText: 'Nombre Sucursal',
                                    fieldLabel: '<b>Nombre Sucursal</b>',
                                    forceSelection: true,
                                    hidden: true,
                                    listConfig: {
                                        minWidth: 250
                                    },
                                    margin: '0 5 5 0',
                                    name: 'appACrCombo',
                                    maxLength: 90,
                                    flex: 1,
                                    //                                    store: storeSucursal,
                                    trigger: true,
                                    valueField: 'id',
                                    displayField: 'text',
                                    queryMode: 'remote', //tipo de recarga del store desde afuera
                                    queryParam: 'filtro', //criterio de busqueda a enviar al php
                                    minChars: 2, //numero de caracteres limite para empezar la busqueda
                                    hideTrigger: true, //Para ocultar triángulo
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'nombresACr',
                                    id: 'nombresACr',
                                    emptyText: 'Nombre sucursal',
                                    fieldLabel: '<b>Nombre</b>',
                                    readOnly: true,
                                    flex: 1
                                },
                            ]
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: []
                        },
                        {
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                pack: 'start'
                            },
                            items: [{
                                afterLabelTextTpl: INFOMESSAGEREQUERID,
                                blankText: INFOMESSAGEBLANKTEXT,
                                allowOnlyWhitespace: false,
                                xtype: 'textareafield',
                                name: 'razonACr',
                                id: 'razonACr',
                                emptyText: 'Descripción de la razón del credito',
                                fieldLabel: '<b>Razón</b>',
                                maxLength: 500,
                                maxLengthText: 'Debe ingresar hasta 500 caracter',
                                minLengthText: 'Debe ingresar mas de 20 caracteres',
                                minLength: 20,
                                flex: 1
                            }]
                        }
                    ]
                }
            ],
            buttons: ['->', {
                iconCls: 'fa fa-credit-card-alt',
                text: '<b>Asignar credito</b>',
                width: '150px',
                tooltip: 'Permite asiganar credito a un sucursal',
                name: 'btn_agregar_cred_cliente',
                handler: function (event, target, owner, tool) {
                    Ext.MessageBox.buttonText = {
                        yes: "Sí",
                        no: "No"
                    };
                    var form = vnt_AsignarCredito.down('[name=formAsignarCredito]').getForm();
                    if (form.isValid()) {
                        var val = form.getValues();
                        //console.log(val);
                        Ext.MessageBox.confirm('Atención', '<h3>¿Está seguro de asignar $' + val.montoACr + '.00' + ' de crédito al sucursal ' + sucursal + ' para el presente mes', function (choice) {
                            if (choice === 'yes') {
                                var mesRangoFinal = vnt_AsignarCredito.down('[name=dateMesHasta]').getValue();
                                var idRangoFinal = getNumeroMes(mesRangoFinal); 
                                var data;
                                if (mesRangoFinal !== null) {
                                    for (let i = 0; i < arrayIdMesesComp.length; i++) {
                                        if (arrayIdMesesComp[i] !== idRangoFinal) {
                                            data = {
                                                monto: val.montoACr,
                                                anio: val.dateAnios,
                                                mes: arrayIdMesesComp[i],
                                                idSucursal: idSucursal,
                                                idCompaniaSucursal: idCompaniaSucursal,
                                                razonCredito: val.razonACr,
                                                idCompaniaVoucher: idCompaniaVoucher,
                                            };
                                            asignarCreditoCompaniaVoucher(data,val);
                                        } else {
                                            data = {
                                                monto: val.montoACr,
                                                anio: val.dateAnios,
                                                mes: arrayIdMesesComp[i],
                                                idSucursal: idSucursal,
                                                idCompaniaSucursal: idCompaniaSucursal,
                                                razonCredito: val.razonACr,
                                                idCompaniaVoucher: idCompaniaVoucher,

                                            };
                                            asignarCreditoCompaniaVoucher(data,val);
                                            break;
                                        }
                                    }
                                } else {
                                    data = {
                                        monto: val.montoACr,
                                        anio: val.dateAnios,
                                        mes: val.dateMes,
                                        idSucursal: idSucursal,
                                        idCompaniaSucursal: idCompaniaSucursal,
                                        razonCredito: val.razonACr,
                                        idCompaniaVoucher: idCompaniaVoucher,
                                    };
                                    asignarCreditoCompaniaVoucher(data,val);
                                }
                            }
                        });
                    } else {
                        Ext.MessageBox.alert('Atención', 'Complete los campos requeridos');
                    }
                }
            }, {
                iconCls: 'fa fa-window-close-o',
                text: '<b>Cancelar</b>',
                width: '100px',
                tooltip: 'Cerrar ventana',
                handler: function (event, target, owner, tool) {
                    vnt_AsignarCredito.close();
                    vnt_AsignarCredito.destroy();
                }
            }]
        });
        vnt_AsignarCredito.show();
        vnt_AsignarCredito.down('[name=nombresACr]').setValue(sucursal);
        vnt_AsignarCredito.down('[name=razonACr]').focus(true);
    },
    onBuscarSucursalParams: function (component, check) {
        idCompaniaVoucher = MODULO_COMPANIA_VOUCHER.down('[name=idCompaniaVoucher]').getValue();
        var params = {
            anio: anio,
            mes: mes,
            idCompaniaVoucher: idCompaniaVoucher,
            idAplicativo: idAplicativo
        };
        recargarStoreCreditoCompania(params);
        reacragarStoreSucrsalCompania(params);
        setTextDefectoCompania();
    },
    onBuscarGridSucursal: function (btn, e) {
        var txtParam = MODULO_COMPANIA_VOUCHER.down('[name=txtParam]').getValue();
        if (btn.xtype === 'button' || e.event.keyCode === 13) {
            MODULO_COMPANIA_VOUCHER.down('[name=gridLeerCompaniaVoucher]').getStore().load({
                params: {
                    params: txtParam,
                    anio: anio,
                    mes: mes,
                    idCompaniaVoucher: idCompaniaVoucher,
                    idAplicativo: idAplicativo
                },
                callback: function (records, operation, success) {
                    if (!success) {
                        setMensajeGridEmptyText(MODULO_COMPANIA_VOUCHER.down('[name=gridLeerCompaniaVoucher]'), EMPTY_CARA + '<h3>' + MENSAJE_ERROR + '</h3>');
                    } else if (records.length === 0) {
                        setMensajeGridEmptyText(MODULO_COMPANIA_VOUCHER.down('[name=grid]'), EMPTY_CARA + '<h3>No existen resultados.</h3>');
                    } else {
                        MODULO_COMPANIA_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
                    }

                }
            });
        }
    },
});

function getIdCompaniaVoucher() {
    return MODULO_COMPANIA_VOUCHER.down('[name=idCompaniaVoucher]').getValue();
}

function reacragarStoreSucrsalCompania(params) {
    MODULO_COMPANIA_VOUCHER.down('[name=gridLeerCompaniaVoucher]').getStore().load({
        params: params,
        callback: function (records, operation, success) {
            if (!success) {
                notificaciones(MENSAJE_ERROR, 2);
            }
        }
    });
    MODULO_COMPANIA_VOUCHER.down('[name=asignarCredito]').disable();
}

function recargarStoreCreditoCompania(params) {
    storeCreditoCompaniaCompaniaVoucher.load({
        params: params,
        callback: function (records, operation, success) {
            if (!success) {
                notificaciones(MENSAJE_ERROR, 2);
            } else {
                if (records.length > 0) {
                    MODULO_COMPANIA_VOUCHER.down('[name=creditoCompania]').setText('Crédito : $' + records[0].data.credito);
                    MODULO_COMPANIA_VOUCHER.down('[name=creditoConsumido]').setText('Asignado : $' + records[0].data.asignado);
                    MODULO_COMPANIA_VOUCHER.down('[name=creditoDisponible]').setText('Cosumido: $' + records[0].data.consumo);
                } else {
                    MODULO_COMPANIA_VOUCHER.down('[name=creditoCompania]').setText('Crédito: $0');
                    MODULO_COMPANIA_VOUCHER.down('[name=creditoConsumido]').setText('Asignado: $0');
                    MODULO_COMPANIA_VOUCHER.down('[name=creditoDisponible]').setText('Consumo: $0');
                }
            }
        }
    });
}

function setTextDefectoCompania() {
    
    MODULO_COMPANIA_VOUCHER.down('[name=administracionConsumo]').getStore().removeAll();
    MODULO_COMPANIA_VOUCHER.down('[name=gridLeerCompaniaVoucher]').getView().deselect(MODULO_COMPANIA_VOUCHER.down('[name=gridLeerCompaniaVoucher]').getSelection());
    MODULO_COMPANIA_VOUCHER.down('[name=clienteAdmCosumo]').setText('Sucursal:');
    MODULO_COMPANIA_VOUCHER.down('[name=creditoAdmCosumo]').setText('Crédito: $');
    MODULO_COMPANIA_VOUCHER.down('[name=consumoAdmCosumo]').setText('Cosumido: $');
}

function asignarCreditoCompaniaVoucher(data,val) {
    var form = Ext.create('Ext.form.Panel');
    form.getForm().submit({
        url: 'php/CompaniaVoucher/createCredito.php',
        timeout: 6000000,
        waitTitle: 'Procesando Información',
        waitMsg: 'Procesando...',
        params: data,
        success: function (form, action) {
            vnt_AsignarCredito.close();
            vnt_AsignarCredito.destroy();
            notificaciones('Crédito Agregado Correctamente', 1);
            var params = {
                anio: val.dateAnios,
                mes: val.dateMes,
                idCompaniaVoucher: idCompaniaVoucher
            };
            recargarStoreCreditoCompania(params);
            reacragarStoreSucrsalCompania(params);
            setTextDefectoCompania();

        },
        failure: function (form, action) {
            notificaciones('No se pudo realizar la asignación,revice el crédito de la empresa o la fecha de inicio de crédito', 2);
            vnt_AsignarCredito.close();
            vnt_AsignarCredito.destroy();
            var params = {
                anio: val.dateAnios,
                mes: val.dateMes,
                idCompaniaVoucher: idCompaniaVoucher
            };
            recargarStoreCreditoCompania(params);
            reacragarStoreSucrsalCompania(params);
            setTextDefectoCompania();
        }
    });
}

function getNombreMes(value) {
    var mes;
    switch (value) {
        case 1:
            mes = 'Enero'
            break;
        case 2:
            mes = 'Febrero'
            break;
        case 3:
            mes = 'Marzo'
            break;
        case 4:
            mes = 'Abril'
            break;
        case 5:
            mes = 'Mayo'
            break;
        case 6:
            mes = 'Junio'
            break;
        case 7:
            mes = 'Julio'
            break;
        case 8:
            mes = 'Agosto'
            break;
        case 9:
            mes = 'Septiembre'
            break;
        case 10:
            mes = 'Octubre'
            break;
        case 11:
            mes = 'Noviembre'
            break;
        case 12:
            mes = 'Diciembre'
            break;
        default:
            break;
    }
    return mes;
}

function getNumeroMes(value) {
    var mes;
    switch (value) {
        case 'Enero':
            mes = 1
            break;
        case 'Febrero':
            mes = 2
            break;
        case 'Marzo':
            mes = 3
            break;
        case 'Abril':
            mes = 4
            break;
        case 'Mayo':
            mes = 5
            break;
        case 'Junio':
            mes = 6
            break;
        case 'Julio':
            mes = 7
            break;
        case 'Agosto':
            mes = 8
            break;
        case 9:
            mes = 'Septiembre'
            break;
        case 'Octubre':
            mes = 10
            break;
        case 'Noviembre':
            mes = 11
            break;
        case 'Diciembre':
            mes = 12
            break;
        default:
            break;
    }
    return mes;
}