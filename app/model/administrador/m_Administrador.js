/* global Ext */
Ext.define('Ktaxi.model.administrador.m_Administrador', {
    extend: 'Ext.data.Model',
    fields: ['id', 'usuario', 'contrasenia','persona','nombres', 'apellidos', 'celular', 'cedula', 'correo', 'cedula', 'fNacimiento', 'direccion', 'tipo_sangre', 'imagen', 'tipo', 'idAR', 'idAE', 'bloqueado', 'msgB', 'fhr', 'fhe', 'fhre', 'msgR']
});
