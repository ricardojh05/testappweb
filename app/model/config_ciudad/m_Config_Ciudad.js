/* global Ext */
Ext.define('Ktaxi.model.config_ciudad.m_Config_Ciudad', {
    extend: 'Ext.data.Model',
    fields: ['id', 'idAplicativo', 'aplicativo', 'idCiudad', 'ciudad', 'idLabel', 'label', 'nombre', 'valor', 'habilitado']
});
