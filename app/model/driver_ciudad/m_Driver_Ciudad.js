/* global Ext */
Ext.define('Ktaxi.model.driver_ciudad.m_Driver_Ciudad', {
    extend: 'Ext.data.Model',
    fields: ['id', 'idAplicativo', 'aplicativo', 'idCiudad', 'ciudad', 'idLabel', 'label', 'nombre', 'valor', 'habilitado']
});
