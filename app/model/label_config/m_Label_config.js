/* global Ext */
Ext.define('Ktaxi.model.label_config.m_Label_config', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nombre'}, {name: 'descripcion'},
        {name: 'idUserCreate', type: 'int'},
        {name: 'dateCreate', type: 'date'},
        {name: 'idUserChange', type: 'int'},
        {name: 'dateChange', type: 'date'}
    ]
});
