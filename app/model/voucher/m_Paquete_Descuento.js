Ext.define('Ktaxi.model.paquete.m_Paquete_Descuento', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        'idPaquete',
        {name: 'relacion', type: 'float'},
        'relacionPorcentaje',
        {name: 'fInicio', type: 'date', formatDate: 'Y-m-d'},
        {name: 'hInicio', type: 'date', formatDate: 'H:i'},
        {name: 'fFin', type: 'date', formatDate: 'Y-m-d'},
        {name: 'hFin', type: 'date', formatDate: 'H:i'},
        'descripcion',
        'habilitado'
    ]
});
