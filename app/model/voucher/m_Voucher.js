/* global Ext */
Ext.define('Ktaxi.model.voucher.m_Voucher', {
    extend: 'Ext.data.Model',
    fields: ['id', 'idAplicativo', 'aplicativo', 'idCiudad', 'ciudad', 'idTipo', 'tipo', 'nombre', 'desCorta', 'desLarga', 'costo', 'relacion', 'relacionPorcentaje', 'habilitado', {name: 'actualizar', type: 'bool', defaultValue: false}]
});
