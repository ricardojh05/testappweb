Ext.define('Ktaxi.model.paquete.m_Paquete_Promocion', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'idPaquete',
        {name: 'promocion', type: 'float'},
        {name: 'caduca', type: 'int'},
        'promPorcentaje',
        {name: 'fInicio', type: 'date', formatDate: 'Y-m-d'},
        {name: 'hInicio', type: 'date', formatDate: 'H:i'},
        {name: 'fFin', type: 'date', formatDate: 'Y-m-d'},
        {name: 'hFin', type: 'date', formatDate: 'H:i'},
        'descripcion',
        'habilitado'
    ]
});
