/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('dataClientesV', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idC', mapping: 'id', type: 'int'}, //id cliente
        {name: 'idCC', type: 'int'}, //id_cliente_compania
        {name: 'idCCC', type: 'int'}, // id credito cliente compania
        {name: 'idE', type: 'int'}, //id empresa
        {name: 'cliente'}, //cliente
        {name: 'codigo'}, //codigo cliente
        {name: 'credito', type: 'float'}, //credito
        {name: 'consumo', type: 'float'}, //consumio
        {name: 'montoMaximo', type: 'float'} //monto maximo
    ]
});
