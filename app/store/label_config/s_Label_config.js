/* global Ext */
Ext.define('Ktaxi.store.label_config.s_Label_config', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.label_config.m_Label_config',
    alias: 'label_config.s_Label_config',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Label_config/read.php',
            create: 'php/Label_config/create.php',
            update: 'php/Label_config/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'labels',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('label_config.s_Label_config').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('label_config.s_Label_config').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioLabel_config();
            }
        }
    }
});

