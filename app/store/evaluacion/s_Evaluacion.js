/* global Ext, MENSAJE_ERROR_CRUD */
Ext.define('Ktaxi.store.evaluacion.s_Evaluacion', {
    extend: 'Ext.data.Store',
    fields: [

        {name: 'id', type: 'int'},
        {name: 'idEvaluacion', type: 'int'},
        {name: 'numero', type: 'int'},
        {name: 'opcion', type: 'int'},
        {name: 'nombre'},
        {name: 'correo'},
        {name: 'respuesta'},
        {name: 'clases' ,type:'auto'},
        {name: 'fechaRegistro', type: 'date'},
       {name: 'fecha'}


    ],
    remoteSort: false,
    alias: 'evaluacion.s_Evaluacion',
    pageSize: 25,
//    groupField: 'zona',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Evaluacion/read.php',
            create: 'php/Evaluacion/create.php',
            update: 'php/Evaluacion/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    listeners: {
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            notificaciones(obj.error, 2);
                        else if (obj.message)
                            notificaciones(obj.message, 2);
                        else
                            notificaciones(MENSAJE_ERROR, 2);
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    notificaciones(obj.message, 2);
                    console.warn(operation._response.responseText);
                }
            }
        }
    }
});

