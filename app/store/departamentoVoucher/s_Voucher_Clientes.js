/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('Ktaxi.store.departamentoVoucher.s_Voucher_Clientes', {
    extend: 'Ext.data.Store',
//    autoload:false,
//    autoSync: true,
//    model: 'dataClientesV',
    pageSize: 50,
    fields: [
        {name: 'idCliente', type: 'int'}, //id cliente
        {name: 'idDeparCli', type: 'int'}, //id_cliente_compania
        {name: 'idDeparCliCre', type: 'int'}, // id credito cliente compania
        {name: 'idDepartamento', type: 'int'}, //id empresa
        {name: 'cliente'}, //cliente
        {name: 'codigo'}, //codigo cliente
        {name: 'credito', type: 'float'}, //credito
        {name: 'consumo', type: 'float'}, //consumio
        {name: 'creditoMes', type: 'float'}, //consumio
        {name: 'consumoMes', type: 'float'}, //consumio
        {name: 'montoMaximo', type: 'float'} //monto maximo
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/DepartamentoVoucher/getClientes.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_VOUCHER) {
                var params = {
                    anio: anio,
                    mes: mes
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

