/* global Ext */
Ext.define('Ktaxi.store.departamentoVoucher.s_Voucher', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.departamentoVoucher.m_Voucher',
    alias: 'voucher.s_Voucher',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/DepartamentoVoucher/read.php',
            create: 'php/DepartamentoVoucher/create.php',
            update: 'php/DepartamentoVoucher/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'vouchers',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('voucher.s_Voucher').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('voucher.s_Voucher').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioVoucher();
                ocultarBarraProgreso();
            }
        },
        load: function (thisObj, records, successful, eOpts) {
            if (successful) {
            }
        }
    }
});

