/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('Ktaxi.store.departamentoVoucher.s_Voucher_Buscar_Cliente', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/DepartamentoVoucher/search_clientes.php',
        reader: {
            type: 'json',
            root: 'data'
        }
    },
    fields: [
        {name: 'id', mapping: 'id', type: 'int'}, //id cliente
        {name: 'idApp', type: 'int'}, //id app
        {name: 'idC', type: 'int'}, // id ciudad
        {name: 'app'}, //aplicativo
        {name: 'cliente'}, //cliente
        {name: 'correo'}, //credito
        {name: 'cedula'}, //consumio
        {name: 'celular'} // celular
    ]
});

