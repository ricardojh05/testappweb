/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('Ktaxi.store.departamentoVoucher.s_Voucher_Tipo_Empresa', {
    extend: 'Ext.data.Store',
    autoSync: true,
    fields: [
        {name: 'empresa'},
        {name: 'idAplicativo'},
        {name: 'isValid', type: 'int'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/DepartamentoVoucher/getValidarEmpresa.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },

});