/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('Ktaxi.store.departamentoVoucher.s_Voucher_Totales_Consumo', {
    extend: 'Ext.data.Store',
    autoSync: true,
//    model: 'dataClientesV',
    fields: [
       {name: 'totalCo', type: 'float'},
        {name: 'totalCr', type: 'float'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/DepartamentoVoucher/getTotalConsumo.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    }
});

