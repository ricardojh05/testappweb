if (MODULOS.length <= 0)
    run(function (response) {
        if (response === 1)
            llenar_Store_Navegacion();
        else {
            notificaciones(MENSAJE_ERROR, 2);
        }
    });
else
    llenar_Store_Navegacion();
function llenar_Store_Navegacion() {
    var itemMenu = {
        'id': 'menu',
        'iconCls': 'x-fa fa-bars',
        'viewType': '',
        'inicio': '',
        'text': 'Menú',
        'name': '',
        'PATH': '',
        'leaf': true,
        'selectable': false,
        'rowCls': 'menuPrincipal',
        'permisos': ''};
    MODULOS.unshift(itemMenu);
    Ext.define('Ktaxi.store.Navegacion', {
        extend: 'Ext.data.TreeStore',
        storeId: 'Navegacion',
        data: MODULOS,
        fields: [{name: 'text'}],
        root: {
            expanded: true,
        }
    });
    setTimeout('runReloj()', 2000);//EJECUTA RELOJ
    setTimeout('asignarDatosMain()', 2000);//ASIGNA DATOS
}
