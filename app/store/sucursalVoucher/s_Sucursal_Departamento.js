/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('Ktaxi.store.sucursalVoucher.s_Sucursal_Departamento', {
    extend: 'Ext.data.Store',
//    autoload:false,
//    autoSync: true,
//    model: 'dataDepartamentosV',
    pageSize: 50,
    fields: [
        {name: 'idC', mapping: 'id', type: 'int'}, //id cliente
        {name: 'idCC', type: 'int'}, //id_cliente_compania
        {name: 'idCCC', type: 'int'}, // id credito cliente compania
        {name: 'idE', type: 'int'}, //id empresa
        {name: 'cliente'}, //cliente
        {name: 'codigo'}, //codigo cliente
        {name: 'credito', type: 'float'}, //credito
        {name: 'consumo', type: 'float'}, //consumio
        {name: 'creditoPr', type: 'float'}, //consumio
        {name: 'consumoPr', type: 'float'}, //consumio
        {name: 'montoMaximo', type: 'float'} //monto maximo
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/SucursalVoucher/getDepartamentos.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_VOUCHER_SUCURSAL) {
                var params = {
                    anio: anio,
                    mes: mes
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

