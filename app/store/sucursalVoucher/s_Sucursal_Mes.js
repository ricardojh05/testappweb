/* global Ext */
Ext.define('Ktaxi.store.sucursalVoucher.s_Sucursal', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.sucursalVoucher.m_Sucursal',
    alias: 'sucursal.s_Sucursal',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/SucursalVoucher/read.php',
            create: 'php/SucursalVoucher/create.php',
            update: 'php/SucursalVoucher/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'sucursals',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('sucursal.s_Sucursal').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('sucursal.s_Sucursal').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioSucursal();
                ocultarBarraProgreso();
            }
        },
        load: function (thisObj, records, successful, eOpts) {
            if (successful) {
            }
        }
    }
});

