/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('Ktaxi.store.sucursalVoucher.s_Sucursal_Read_Consumo', {
    extend: 'Ext.data.Store',
    autoSync: true,
//    model: 'dataDepartamentosV',
//groupField: 'Detalle',
pageSize: 50,
    fields: [
       {name: 'id', type: 'int'}, //
        {name: 'idC', type: 'int'}, // id cliente
        {name: 'idCCC', type: 'int'}, //id cliente credito compania
        {name: 'idE', type: 'int'}, // id empresa
        {name: 'idSol', type: 'int'}, // id solicitud
        {name: 'cliente', type: 'string'}, ///cliente
        {name: 'codigo', type: 'string'}, //codigo
        {name: 'consumo', type: 'number'}, // consumo
        {name: 'latC', type: 'number'}, //lat cliente
        {name: 'lngC', type: 'number'}, // longitud cliente
        {name: 'latCo', type: 'number'}, //lat conductor
        {name: 'lngCo', type: 'number'}, //lng conductor
        {name: 'tipo', type: 'int'}, // tipo
        {name: 'credito', type: 'number'}, //credito
        {name: 'rzn_cre', type: 'string'}, //codigo
        {name: 'Detalle', type: 'string'}, //codigo
        {name: 'fha', type: 'date', dateFormat: 'Y-m-d H:i:s'}, //fecha hora asignada
        {name: 'fhd', type: 'date', dateFormat: 'Y-m-d H:i:s'}, //fecha hora hasta
        {name: 'fhh', type: 'date', dateFormat: 'Y-m-d H:i:s'}, //fecha hora desde 
        {name: 'fhc', type: 'date', dateFormat: 'Y-m-d H:i:s'}// fto maximo
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/SucursalVoucher/readConsumo.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_VOUCHER_SUCURSAL) {
                var params = {
                    anio: anio,
                    mes: mes,
                    idDepar:idDepar
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

