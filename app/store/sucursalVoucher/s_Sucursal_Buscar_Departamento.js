/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('Ktaxi.store.sucursalVoucher.s_Sucursal_Buscar_Departamento', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/SucursalVoucher/search_clientes.php',
        reader: {
            type: 'json',
            root: 'data'
        }
    },
    fields: [
        {name: 'id', mapping: 'id', type: 'int'}, //id cliente
        {name: 'idApp', type: 'int'}, //id app
        {name: 'idC', type: 'int'}, // id ciudad
        {name: 'app'}, //aplicativo
        {name: 'cliente'}, //cliente
        {name: 'correo'}, //credito
        {name: 'cedula'}, //consumio
        {name: 'celular'} // celular
    ]
});

