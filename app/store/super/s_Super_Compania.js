Ext.define('Ktaxi.store.super.s_Super_Compania', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getSuperCompania.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nombre'},
        {name: 'habilitado', type: 'bool'}
    ]
});


