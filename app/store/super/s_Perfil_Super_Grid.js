Ext.define('Ktaxi.store.super.s_Perfil_Super_Grid', {
    extend: 'Ext.data.Store',
    storeId: 's_Perfil_Super_Grid',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Get/getPerfilesSuper.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'perfiles',
            successProperty: 'success'
        }
    },
    fields: ['id', 'idAdministrador', 'modulo']
});