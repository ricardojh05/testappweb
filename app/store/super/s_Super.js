/* global Ext */
Ext.define('Ktaxi.store.super.s_Super', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.super.m_Super',
    alias: 'super.s_Super',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Super/read.php',
            create: 'php/Super/create.php',
            update: 'php/Super/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'super',
            successProperty: 'success'
        }
    },
//    onCreateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('super.s_Super').remove(records);
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    onUpdateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('super.s_Super').rejectChanges();
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    listeners: {
//        write: function (store, operation, eOpts) {
//            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
//                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
//                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
//                limpiarFormularioSuper();
//            }
//        },
//        load: function (thisObj, records, successful, eOpts) {
//            if (successful) {
//            }
//        }
//    }
});

