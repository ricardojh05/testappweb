/* global Ext */
Ext.define('Ktaxi.store.super.s_Modulo_Super_Grid', {
    extend: 'Ext.data.Store',
    storeId: 's_Modulo_Super_Grid',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Get/getModulosSuper.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'modulos',
            successProperty: 'success'
        }
    },
    fields: ['id', 'idAdministrador', 'modulo',
        {name: 'leer', type: 'boolean', defaultValue: 0, convert: null},
        {name: 'crear', type: 'boolean', defaultValue: 0, convert: null},
        {name: 'editar', type: 'boolean', defaultValue: 0, convert: null},
        {name: 'eliminar', type: 'boolean', defaultValue: 0, convert: null}
    ]
});

