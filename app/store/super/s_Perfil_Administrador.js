/* global Ext */
Ext.define('Ktaxi.store.super.s_Perfil_Administrador', {
    extend: 'Ext.data.Store',
    storeId: 's_Perfil_Admin_Grid',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Get/getPerfilAdministrador.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    fields: ['id', 'idAdministrador', 'modulo']
});


