Ext.define('Ktaxi.store.administrador.s_Compania_Administrador', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getCompaniaAdministrador.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nombre'},
        {name: 'habilitado', type: 'bool'}
    ]
});


