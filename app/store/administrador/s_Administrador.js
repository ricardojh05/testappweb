/* global Ext */
Ext.define('Ktaxi.store.administrador.s_Administrador', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.administrador.m_Administrador',
    alias: 'administrador.s_Administrador',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Administrador/read.php',
            create: 'php/Administrador/create.php',
            update: 'php/Administrador/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'administrador',
            successProperty: 'success'
        }
    },
//    onCreateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('administrador.s_Administrador').remove(records);
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    onUpdateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('administrador.s_Administrador').rejectChanges();
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    listeners: {
//        write: function (store, operation, eOpts) {
//            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
//                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
//                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
//                limpiarFormularioAdministrador();
//            }
//        },
//        load: function (thisObj, records, successful, eOpts) {
//            if (successful) {
//            }
//        }
//    }
});

