/* global Ext */
Ext.define('Ktaxi.store.administrador.s_ModuloAdmin_Grid', {
    extend: 'Ext.data.Store',
    storeId: 's_ModuloAdmin_Grid',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Get/getModulosAdministrador.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'modulos',
            successProperty: 'success'
        }
    },
    fields: ['id', 'idAdministrador', 'modulo',
        {name: 'leer', type: 'boolean', defaultValue: 0, convert: null},
        {name: 'crear', type: 'boolean', defaultValue: 0, convert: null},
        {name: 'editar', type: 'boolean', defaultValue: 0, convert: null},
        {name: 'eliminar', type: 'boolean', defaultValue: 0, convert: null}
    ]
});

