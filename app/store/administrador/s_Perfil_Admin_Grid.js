/* global Ext */
Ext.define('Ktaxi.store.administrador.s_Perfil_Admin_Grid', {
    extend: 'Ext.data.Store',
    storeId: 's_Perfil_Admin_Grid',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Get/getPerfilesAdmin.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'perfiles',
            successProperty: 'success'
        }
    },
    fields: ['id', 'idAdministrador', 'modulo']
});

