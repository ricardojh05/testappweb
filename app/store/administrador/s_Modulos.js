Ext.define('Ktaxi.store.administrador.s_Modulos', {
    extend: 'Ext.data.Store',
    storeId: 's_Modulos',
    proxy: {
        type: 'ajax',
        url: 'php/Administrador/getModulos.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'modulos'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'crear', type: 'bool'},
        {name: 'editar', type: 'bool'},
        {name: 'eliminar', type: 'bool'},
        {name: 'leer', type: 'bool'},
        {name: 'modulo'}
    ]
});