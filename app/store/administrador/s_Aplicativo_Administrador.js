Ext.define('Ktaxi.store.administrador.s_Aplicativo_Administrador', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getAplicativoAdministrador.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idAdministrador', type: 'int'},
        {name: 'nombre'},
        {name: 'habilitado', type: 'bool'}
    ]
});


