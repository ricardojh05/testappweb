Ext.define('Ktaxi.store.adminVoucher.s_AdminVoucher_Read_Consumo', {
    extend: 'Ext.data.Store',
    autoSync: true,
//    model: 'dataCompaniasV', 
pageSize: 50,
    fields: [

        {name: 'departamento', type: 'string'}, ///departamento
        {name: 'codigo', type: 'string'}, //codigo
        {name: 'consumido', type: 'number'}, // consumido
        {name: 'asignado', type: 'number'}, // asignado
        {name: 'credito', type: 'number'}, //credito
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/AdminVoucher/readConsumo.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_ADMIN_VOUCHER) {
                var params = {
                    anio: anio,
                    mes: mes,
                    idCompania:idCompania
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

