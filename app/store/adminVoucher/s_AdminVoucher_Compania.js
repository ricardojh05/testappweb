//var fecha = new Date(), mes = fecha.getMonth() + 1, anio = fecha.getFullYear();
Ext.define('Ktaxi.store.adminVoucher.s_AdminVoucher_Compania', {
    extend: 'Ext.data.Store',
    autoload:true,
    autoSync: true,
//    model: 'dataCompaniasV',
    pageSize: 50,
    fields: [
        
        {name: 'id', type: 'int'}, //consumio
        {name: 'idCompaniaCompania', type: 'int'}, //consumio
        {name: 'idAdminVoucher', type: 'int'}, //consumio
        {name: 'idCompania', type: 'int'}, //consumio
        {name: 'idUserCreate', type: 'int'}, //consumio
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/AdminVoucher/getCompania.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        },
        
        
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_ADMIN_VOUCHER) {
                var params = {
                    anio: anio,
                    mes: mes
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

