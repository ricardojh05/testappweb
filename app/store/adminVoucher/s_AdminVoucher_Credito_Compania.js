/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('Ktaxi.store.adminVoucher.s_AdminVoucher_Credito_Compania', {
    extend: 'Ext.data.Store',
    autoSync: true,
    fields: [
        {name: 'idCompania', type: 'int'},
        {name: 'mes', type: 'int'},
        {name: 'anio', type: 'int'},
        {name: 'compania'},
        {name: 'credito', type: 'float'},
        {name: 'consumo', type: 'float'},
        {name: 'saldo', type: 'float'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/AdminVoucher/getCreditoCompania.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    }
});