/* global Ext, MENSAJE_ERROR_CRUD */
Ext.define('Ktaxi.store.reporte.s_Reporte', {
    extend: 'Ext.data.Store',
    fields: [

        {name: 'id', type: 'int'},
        {name: 'idUsuario', type: 'int'},
        {name: 'mensaje'},
        {name: 'correo'},
        {name: 'ReporteCorreo', type: 'number'},
        {name: 'confimacionReporte'},
        {name: 'usuario'},
        {name: 'nombreR'},
        {name: 'apellidoR'},
        {name: 'idUsuarioRegistro', type: 'int'},
        {name: 'fechaRegistro', type: 'date'},
        {name: 'idUsuarioConfirmo', type: 'int'},
        {name: 'fechaConfirmo', type: 'date'},
        {name: 'idUsuarioValido', type: 'int'},
        {name: 'fechaValido', type: 'date'}



    ],
    remoteSort: false,
    alias: 'reporte.s_Reporte',
    pageSize: 25,
//    groupField: 'zona',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Reporte/read.php',
            create: 'php/Reporte/create.php',
            update: 'php/Reporte/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    listeners: {
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            notificaciones(obj.error, 2);
                        else if (obj.message)
                            notificaciones(obj.message, 2);
                        else
                            notificaciones(MENSAJE_ERROR, 2);
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    notificaciones(obj.message, 2);
                    console.warn(operation._response.responseText);
                }
            }
        }
    }
});

