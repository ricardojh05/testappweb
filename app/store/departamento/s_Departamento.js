/* global Ext */
Ext.define('Ktaxi.store.departamento.s_Departamento', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idSucursal'},
        {name: 'departamento'}, 
        {name: 'sucursal'},
        {name: 'contacto'},
        {name: 'correo'},
        {name: 'color'},
        {name: 'comentario'},
        {name: 'insertado'},
        {name: 'isNota'},
        {name: 'bandera', type:'boolean',defaultValue: false},
        
    ],
    alias: 'departamento.s_Departamento',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Departamento/read.php',
            create: 'php/Departamento/create.php',
            update: 'php/Departamento/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'departamentoes',
            successProperty: 'success'
        }
    },
//    onCreateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('departamento.s_Departamento').remove(records);
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    onUpdateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('departamento.s_Departamento').rejectChanges()
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    listeners: {
//        write: function (store, operation, eOpts) {
//            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
//                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
//                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
//                limpiarFormularioDepartamento();
//            }
//        },
//        load: function (thisObj, records, successful, eOpts) {
//            if (successful) {
//                var modulo = Ext.getCmp('moduloDepartamento');
//                if (modulo) {
//                    modulo.down('[name=numRegistrosGrid]').setText(records.length + ' Registros');
//                }
//            }
//        }
//    }
});

