/* global Ext */
Ext.define('Ktaxi.store.paquete.s_Paquete_Descuento', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.paquete.m_Paquete_Descuento',
    alias: 'paquete.s_Paquete_Descuento',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Paquete/readDescuentos.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'descuentos',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('paquete.s_Paquete_Descuento').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('paquete.s_Paquete_Descuento').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioPaqueteDescuento();
            }
        },
        load: function (thisObj, records, successful, eOpts) {
            if (successful) {
            }
        }
    }
});

