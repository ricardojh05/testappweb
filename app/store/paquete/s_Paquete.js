/* global Ext */
Ext.define('Ktaxi.store.paquete.s_Paquete', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.paquete.m_Paquete',
    alias: 'paquete.s_Paquete',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Paquete/read.php',
            create: 'php/Paquete/create.php',
            update: 'php/Paquete/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'paquetes',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('paquete.s_Paquete').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('paquete.s_Paquete').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioPaquete();
                ocultarBarraProgreso();
            }
        },
        load: function (thisObj, records, successful, eOpts) {
            if (successful) {
            }
        }
    }
});

