Ext.define('Ktaxi.store.compania.s_Administrador_Compania', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getAdministradorCompania.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nombre'},
        {name: 'habilitado', type: 'bool'}
    ]
});

