/* global Ext */
Ext.define('Ktaxi.store.compania.s_Compania', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idPais'},
        {name: 'compania'},
        {name: 'ruc'},
        {name: 'callePrin'},
        {name: 'calleSec'},
        {name: 'contacto'},
        {name: 'color'},
        {name: 'comentario'},
        {name: 'latitud'},
        {name: 'longitud'},
        {name: 'bandera', type:'boolean',defaultValue: false},
    ],
    alias: 'compania.s_Compania',
    pageSize: 5,
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Compania/read.php',
            create: 'php/Compania/create.php',
            update: 'php/Compania/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'compania',
            successProperty: 'success'
        }
    },
//    onCreateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('compania.s_Compania').remove(records);
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    onUpdateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('compania.s_Compania').rejectChanges()
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    listeners: {
//        write: function (store, operation, eOpts) {
//            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
//                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
//                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
//                limpiarFormularioCompania();
//            }
//        },
//        load: function (thisObj, records, successful, eOpts) {
//            if (successful) {
//                var modulo = Ext.getCmp('moduloCompania');
//                if (modulo) {
//                    modulo.down('[name=numRegistrosGrid]').setText(records.length + ' Registros');
//                }
//            }
//        }
//    }
});

