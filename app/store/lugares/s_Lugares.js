Ext.define('Ktaxi.store.lugares.s_Lugares', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idAplicativo'},
        {name: 'idCiudad'},
        {name: 'detalle'},
        {name: 'color'},
        {name: 'descripcion'},
        {name: 'habilitado'},
        {name: 'limite'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Lugares/readLugar.php',
            create: 'php/Lugares/createLugar.php',
            update: 'php/Lugares/updateLugar.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            if (MODULO_LUGARES)
                MODULO_LUGARES.down('[name=gridLugares]').getStore().rejectChanges();
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.error, 2);
            }
        } else {
            limpiarFormularioLugar();
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.message, 1);
            }
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            if (MODULO_LUGARES)
                MODULO_LUGARES.down('[name=gridLugares]').getStore().rejectChanges();
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.error, 2);
            }
        } else {
            limpiarFormularioLugar();
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.message, 1);
            }
        }
    },
    listeners: {
        load: function (thisObj, records, successful, operation, eOpts) {
            if (successful) {
                if (MODULO_LUGARES)
                    MODULO_LUGARES.down('[name=numRegistrosGridL]').setText(records.length + ' Registros');
            } else {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            notificaciones(obj.error, 2);
                        else if (obj.message)
                            notificaciones(obj.message, 2);
                        else
                            notificaciones(MENSAJE_ERROR_CRUD, 2);
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    notificaciones(obj.message, 2);
                    console.warn(operation._response.responseText);
                }
            }
        }
    }
});