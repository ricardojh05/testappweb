Ext.define('Ktaxi.store.lugares.s_Punto', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idLugar'},
        {name: 'barrio'},
        {name: 'principal'},
        {name: 'secundaria'},
        {name: 'referencia'},
        {name: 'latitud'},
        {name: 'longitud'},
        {name: 'imagen'},
        {name: 'habilitado'}
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Lugares/readPunto.php',
            create: 'php/Lugares/createPunto.php',
            update: 'php/Lugares/updatePunto.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            if (MODULO_LUGARES)
                MODULO_LUGARES.down('[name=gridPuntos]').getStore().rejectChanges();
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.error, 2);
            }
        } else {
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.message, 1);
            }
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            if (MODULO_LUGARES)
                MODULO_LUGARES.down('[name=gridPuntos]').getStore().rejectChanges();
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.error, 2);
            }
        } else {
            if (isJsonString(operation._response.responseText)) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.message, 1);
            }
        }
    },
    listeners: {
        load: function (thisObj, records, successful, operation, eOpts) {
            if (successful) {
                if (MODULO_LUGARES)
                    MODULO_LUGARES.down('[name=numRegistrosGridP]').setText(records.length + ' Registros');
            } else {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            notificaciones(obj.error, 2);
                        else if (obj.message)
                            notificaciones(obj.message, 2);
                        else
                            notificaciones(MENSAJE_ERROR_CRUD, 2);
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    notificaciones(obj.message, 2);
                    console.warn(operation._response.responseText);
                }
            }
        }
    }
});