/* global Ext */
Ext.define('Ktaxi.store.driver_ciudad.s_Driver_Ciudad', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.driver_ciudad.m_Driver_Ciudad',
    alias: 'driver_ciudad.s_Driver_Ciudad',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/DriverCiudad/read.php',
            create: 'php/DriverCiudad/create.php',
            update: 'php/DriverCiudad/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'configuraciones',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('driver_ciudad.s_Driver_Ciudad').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('driver_ciudad.s_Driver_Ciudad').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioDriverCiudad();
            }
        },
        load: function (thisObj, records, successful, eOpts) {
            if (successful) {
            }
        }
    }
});

