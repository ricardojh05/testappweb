Ext.define('Ktaxi.store.sucursal.s_Administrador_Sucursal', {
    extend: 'Ext.data.Store',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getAdministradorSucursal.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nombre'},
        {name: 'habilitado', type: 'bool'}
    ]
});

