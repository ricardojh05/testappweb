/* global Ext */
Ext.define('Ktaxi.store.sucursal.s_Sucursal', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'idCompania'},
        {name: 'ciudad'},
        {name: 'sucursal'},
        {name: 'callePrin'},
        {name: 'calleSec'},
        {name: 'contacto'},
        {name: 'correo'},
        {name: 'color'},
        {name: 'comentario'},
        {name: 'latitud'},
        {name: 'insertado'},
        {name: 'longitud'}
    ],
    alias: 'sucursal.s_Sucursal',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Sucursal/read.php',
            create: 'php/Sucursal/create.php',
            update: 'php/Sucursal/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'sucursales',
            successProperty: 'success'
        }
    },
//    onCreateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('sucursal.s_Sucursal').remove(records);
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    onUpdateRecords: function (records, operation, success) {
//        if (!success) {
//            Ext.getStore('sucursal.s_Sucursal').rejectChanges()
//            var res = JSON.parse(operation._response.responseText);
//            notificaciones(res.error, 2);
//        }
//    },
//    listeners: {
//        write: function (store, operation, eOpts) {
//            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
//                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
//                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
//                limpiarFormularioSucursal();
//            }
//        },
//        load: function (thisObj, records, successful, eOpts) {
//            if (successful) {
//                var modulo = Ext.getCmp('moduloSucursal');
//                if (modulo) {
//                    modulo.down('[name=numRegistrosGrid]').setText(records.length + ' Registros');
//                }
//            }
//        }
//    }
});

