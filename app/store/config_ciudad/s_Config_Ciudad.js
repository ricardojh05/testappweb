/* global Ext */
Ext.define('Ktaxi.store.config_ciudad.s_Config_Ciudad', {
    extend: 'Ext.data.Store',
    model: 'Ktaxi.model.config_ciudad.m_Config_Ciudad',
    alias: 'config_ciudad.s_Config_Ciudad',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/ConfigCiudad/read.php',
            create: 'php/ConfigCiudad/create.php',
            update: 'php/ConfigCiudad/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'configuraciones',
            successProperty: 'success'
        }
    },
    onCreateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('config_ciudad.s_Config_Ciudad').remove(records);
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    onUpdateRecords: function (records, operation, success) {
        if (!success) {
            Ext.getStore('config_ciudad.s_Config_Ciudad').rejectChanges();
            var res = JSON.parse(operation._response.responseText);
            notificaciones(res.error, 2);
        }
    },
    listeners: {
        write: function (store, operation, eOpts) {
            if ((operation.getRequest().getInitialConfig(['action']) === 'create') ||
                    (operation.getRequest().getInitialConfig(['action']) === 'update')) {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioConfigCiudad();
            }
        },
        load: function (thisObj, records, successful, eOpts) {
            if (successful) {
            }
        }
    }
});

