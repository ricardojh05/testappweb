/* global Ext, MENSAJE_ERROR_CRUD */
Ext.define('Ktaxi.store.alumno.s_Alumno', {
    extend: 'Ext.data.Store',
    fields: [

        {name: 'id', type: 'int'},
        {name: 'idAlumno', type: 'int'},
        {name: 'nombre'},
        {name: 'apellido'},
        {name: 'correo'},
        {name: 'clases' ,type:'auto'},
        {name: 'bandera', defaultValue: false},
       /*  {name: 'idUsuarioRegistro', type: 'int'},
        {name: 'fechaRegistro', type: 'date'},
        {name: 'idUsuarioConfirmo', type: 'int'},
        {name: 'fechaConfirmo', type: 'date'},
        {name: 'idUsuarioValido', type: 'int'},
        {name: 'fechaValido', type: 'date'} */



    ],
    remoteSort: false,
    alias: 'alumno.s_Alumno',
    pageSize: 25,
    //groupField: 'clase',
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/Alumno/read.php',
            create: 'php/Alumno/create.php',
            update: 'php/Alumno/update.php'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        },
        simpleSortMode: true
    },
    listeners: {
        load: function (thisObj, records, successful, operation, eOpts) {
            if (!successful) {
                var obj = {};
                if (isJsonString(operation._response.responseText)) {
                    obj = JSON.parse(operation._response.responseText);
                    if (!obj.success)
                        if (obj.error)
                            notificaciones(obj.error, 2);
                        else if (obj.message)
                            notificaciones(obj.message, 2);
                        else
                            notificaciones(MENSAJE_ERROR, 2);
                } else {
                    obj.resAjax = -2;
                    obj['error'] = "Exite un error al crear el JSON";
                    obj['message'] = "Existe un error con la solicitud realizada, inténtelo mas tarde por favor.";
                    notificaciones(obj.message, 2);
                    console.warn(operation._response.responseText);
                }
            }
        },
        onCreateRecords: function (records, operation, success) {
            if (!success) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.error, 2);
            } else {
                var res = JSON.parse(operation._response.responseText);
    //            registrarSensor(res.id, records[0].data.sensor, records[0].data.imei);
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioSensor();
            }
        },
        onUpdateRecords: function (records, operation, success) {
            if (!success) {
                var res = JSON.parse(operation._response.responseText);
                notificaciones(res.error, 2);
            } else {
                notificaciones(MENSAJE_SUCCESS_CREAR, 1);
                limpiarFormularioSensor();
            }
        },
    }
});

