Ext.define('Ktaxi.store.companiaVoucher.s_CompaniaVoucher_Sucursal', {
    extend: 'Ext.data.Store',
//    autoload:false,
//    autoSync: true,
//    model: 'dataSucursalsV',
    pageSize: 10,
    fields: [
        
        {name: 'id', type: 'int'}, //consumio
        {name: 'idCompaniaSucursal', type: 'int'}, //consumio
        {name: 'idCompaniaVoucher', type: 'int'}, //consumio
        {name: 'idSucursal', type: 'int'}, //consumio
        {name: 'idUserCreate', type: 'int'}, //consumio
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/CompaniaVoucher/getSucursal.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_COMPANIA_VOUCHER) {
                var params = {
                    anio: anio,
                    mes: mes
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

