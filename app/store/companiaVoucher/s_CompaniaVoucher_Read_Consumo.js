Ext.define('Ktaxi.store.companiaVoucher.s_CompaniaVoucher_Read_Consumo', {
    extend: 'Ext.data.Store',
    autoSync: true,
//    model: 'dataSucursalsV', 
pageSize: 50,
    fields: [

        {name: 'departamento', type: 'string'}, ///departamento
        {name: 'codigo', type: 'string'}, //codigo
        {name: 'consumido', type: 'number'}, // consumido
        {name: 'asignado', type: 'number'}, // asignado
        {name: 'credito', type: 'number'}, //credito
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: 'php/CompaniaVoucher/readConsumo.php'
        },
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'data',
            messageProperty: 'message'
        },
        writer: {
            type: 'json',
            writeAllFields: false
        }
    },
    listeners: {
        beforeload: function (store, operation, eOpts) {
            if (MODULO_COMPANIA_VOUCHER) {
                var params = {
                    anio: anio,
                    mes: mes,
                    idSucursal:idSucursal
                };
                store.proxy.extraParams = params;
            }
        }
    }
});

