Ext.define('Ktaxi.store.combos.s_Clase', {
    extend: 'Ext.data.Store',
    storeId: 's_Clase',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getClase.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idClase', type: 'int'},
        {name: 'nombre'},
        {name: 'materia'},
        {name: 'text'},
    ]
    
});