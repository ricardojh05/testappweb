Ext.define('Ktaxi.store.combos.s_Paises', {
    extend: 'Ext.data.Store',
    storeId: 's_Paises',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getPaises.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'idPais', type: 'int'},
        {name: 'pais'},
        {name: 'latitud'},
        {name: 'longitud'}
    ]
});