Ext.define('Ktaxi.store.combos.s_Materia', {
    extend: 'Ext.data.Store',
    storeId: 's_Materia',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getMateria.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'idMateria', type: 'int'},
        {name: 'materia'},
        {name: 'text'},
    ]
});