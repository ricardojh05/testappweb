/* global Ext */
Ext.define('Ktaxi.store.combos.s_Administrador', {
    extend: 'Ext.data.Store',
    fields: [
        {name: 'id'},
        {name: 'nombre'}       
    ],
    alias: 'combos.s_administrador',
    pageSize: 15,
    proxy: {
        type: 'ajax',
        url: 'php/Get/getAdministrador.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'administradores'
        }
    },
});

