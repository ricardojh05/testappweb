/* global Ext */
//NOMENCLATURA: S-> STORE | Get-> METODO | N->Nombre
Ext.define('Ktaxi.store.combos.s_Usuario', {
    extend: 'Ext.data.Store',
    storeId: 's_Get_Usuario',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getUsuario.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'nombres'},
        {name: 'cedula'},
        {name: 'celular'},
        {name: 'texto'}
    ]
});