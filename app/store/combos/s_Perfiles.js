Ext.define('Ktaxi.store.combos.s_Perfiles', {
    extend: 'Ext.data.Store',
    storeId: 's_Pefiles_Combo',
    proxy: {
        type: 'ajax',
        url: 'php/Get/getPerfiles.php',
        method: 'GET',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    },
    fields: [
        {name: 'id', type: 'int'},
        {name: 'text'}
    ]
});