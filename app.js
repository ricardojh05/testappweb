Ext.application({
    name: 'Ktaxi',
    appFolder: 'app',
    requires: [
        'Ktaxi.view.main.v_Main',
        'Ktaxi.view.main.c_Main',
        'Ktaxi.view.main.MainContainerWrap',
        'Ktaxi.view.main.MainModel'
    ],
    stores: [
        'Navegacion',
        //STORES PREDETERMINADOS
        'combos.s_Usuario',
        //STORES ADMINISTRADOR
        'administrador.s_Administrador',
        'administrador.s_Modulos',
        //STORES CONFIGURACIÓN CIUDAD
        'config_ciudad.s_Config_Ciudad',
        //STORES CONFIGURACIÓN DRIVER CIUDAD
        'driver_ciudad.s_Driver_Ciudad',
        //STORES CONFIGURACIÓN DE LABELS DE KTAXI
        'label_config.s_Label_config',
        //STORES DE MODULO PAQUETE
        'paquete.s_Paquete',
        'paquete.s_Paquete_Promocion',
        'paquete.s_Paquete_Descuento'
    ],
    mainView: 'Ktaxi.view.main.v_Main',
    launch: function () {
//        console.log('launch');
    }
});