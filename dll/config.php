<?php
$AMBIENTE = 1; // 0 DESARROLLO | 1 PRODUCCION
$VERSION = "0.4.49";
$APP = 1; //SI ES: 1 KTAXI Y SI ES: 2 OTRA
$URL_IMG = "img/";
$LIMITE_REGISTROS = 50;
$LIMITE_COMBOS = 10;
//Ponemos la BD historica
$DB_NAME = "testapp";
$DB_NAME_HISTORICO = "ktaxihistoricodb";
$TOKEN_LOGIN = 'd2be0658f23e36fdf58c408302faabbb';
//VARIABLES DE LOGIN
switch ($APP) {
    case 1:
        $BANNER = $URL_IMG . "login/banner_login.png";
        $APP_ICONO = $URL_IMG . "sistema/sistema_logo.png";
        $NOMBRE_APP = "TESTAPP";
        break;
    case 2:
        $BANNER = $URL_IMG . "";
        $APP_ICONO = $URL_IMG . "";
        $NOMBRE_APP = "";
        break;
}
//NAVS
$NAV_1 = "";
$NAV_2 = "";
$NAV_3 = "";
/* PERFILES */
$PERFIL_CLIENTE = 1;
$PERFIL_EMPRESARIAL = 2;
$PERFIL_CEO = 3;
//$PERFIL_DISTRIBUCION = 4;
// VARIABLES DE ERROR
$MSJ_NO_EXIST_RESULT = "NO EXISTEN RESULTADOS";
$MSJ_ERROR_CONEXION = "PROBLEMAS DE CONEXIÓN CON EL SERVIDOR";
    if (!isset($_SESSION)) {
    session_start();
}
if (isset($_SESSION["AMBIENTE"])) {
    $_SESSION["AMBIENTE"] = $AMBIENTE;
} else {
    $_SESSION["AMBIENTE"] = $AMBIENTE;
    $_SESSION["IS_SESSION"] = 0;
    $_SESSION["URL_SISTEMA"] = "";
}

function getConectionDb() {
    /* Datos de servidor de base de datos */
    $DB_NAME = "testapp";
    if ($_SESSION["AMBIENTE"] === 0) {
        $db_host = "127.0.0.1"; //DESARROLLO
        $db_user = "testappweb";
        //$db_password = "cuervo201210";
        $db_password = "";
        return conectBase($db_host, $db_user, $db_password, $DB_NAME, 3306);
    } else if ($_SESSION["AMBIENTE"] === 1) {
       // $db_host = "169.62.217.189"; //PRODUCCION
        $db_host = "10.73.211.235"; //PRODUCCION
        $db_user = "Test";
        $db_password = "RomehHg562435%&bv_jhahTYty676";
        return conectBase($db_host, $db_user, $db_password, $DB_NAME, 3306);
    } else {
        echo json_encode(array('success' => false, 'message' => 'EL AMBIENTE DE FUNCIONAMIENTO NO ESTÁ CONFIGURADO CORRECTAMENTE.', 'url' => '../../index.php'));
        return;
    }
}

function conectBase($db_host, $db_user, $db_password, $DB_NAME, $puerto) {
    $mysqli = new mysqli($db_host, $db_user, $db_password, $DB_NAME, $puerto);
    $mysqli->set_charset("utf8mb4");
    if ($mysqli->connect_errno) {
        echo json_encode(array('success' => false, 'message' => "ERROR EN LA CONEXION A BASE DE DATOS"));
        return false;
    }
    return $mysqli;
}
