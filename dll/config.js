//VARIABLES DEL SISTEMA
var AMBIENTE = 1;////SI ES: 0 DESARROLLO Y 1 PRODUCCION 
var SISTEMA = 1;//SI ES: 1 KTAXI Y SI ES: 2 OTRA
var APP = "";
var TITULO_LOGIN = "";
var TITULO_MAIN_APP = "";
switch (SISTEMA) {
    case 1:
        break;
        APP = "";
        TITULO_LOGIN = "Bienvenido a " + APP;
        //MAIN
        TITULO_MAIN_APP = APP + " de estacionamiento rotativo tarifario";
    case 2:
        APP = "";
        TITULO_LOGIN = "Bienvenido a " + APP;
        //MAIN
        TITULO_MAIN_APP = APP + "";
        break;
}
var USUARIO = {};
var MODULOS = [];
var FECHA_ACTUAL = new Date();
var WIDTH_NAVEGACION = 220;
var HEIGT_VIEWS = 0;
var WIDTH_VIEWS = 0;
if (document.body) {
    WIDTH_VIEWS = (document.body.clientWidth);
    HEIGT_VIEWS = (document.body.clientHeight);
} else {
    WIDTH_VIEWS = (window.innerWidth);
    HEIGT_VIEWS = (window.innerHeight);
}
HEIGT_VIEWS = HEIGT_VIEWS - 65;
var COLOR_SISTEMA = '#031b82';
var TIPO = 2;//1 APP MOVIL | 2 APP WEB
//====VARIABLES IMPORTANTES====
var TOKEN_MAPBOX = "pk.eyJ1Ijoia2F0aWVuY2lhcCIsImEiOiJjamN1a3VkbTUxMHllMnduemQ3OTh1ajB5In0.DDiBB1jMawcG_4IRpHNjiQ";
//URL DE LOS SERVICIOS 
var PUERTO = 0;
var URL_SERVICIO = '';
if (AMBIENTE === 1) {//PRODUCCION
//    PUERTO = 8080;
//    URL_SERVICIO = 'http://169.60.159.22:' + PUERTO + '/';
} else {//DESARROLLO
    APP = "DESARROLLO";
//    PUERTO = 9090;
//    URL_SERVICIO = 'http://169.60.159.19:' + PUERTO + '/';
}

var SOCKET;
//EMITS
var EMIT_AUTENTICAR = "";
var EMIT_A_REG_UNICO_RASTREO = "";//a_registrar_rastreo
//RECURSOS REST
var REST_RASTREO = '';
var RECURSO_DIRECCION = 'http://investigacion.kradac.com:3000/intersection-street';
//IMAGENES LOGIN
var URL_IMG = "img/";
var URL_IMG_SISTEMA = URL_IMG + "sistema/";
var URL_IMG_UPLOADS = URL_IMG + "uploads/";
var URL_IMG_UPLOADS_ADMINS = URL_IMG_UPLOADS + "admins/";
var URL_IMG_UPLOADS_LUGARES = URL_IMG_UPLOADS + "puntosLugares/";
//SUBIR IMAGENES
var URL_SUBIR_IMG_ADMINS = "../../" + URL_IMG_UPLOADS + "admins/";
var URL_SUBIR_IMG_PUNTOS_LUGARES = "../../" + URL_IMG_UPLOADS + "puntosLugares/";
var URL_SUBIR_IMG_CONTROLADOR = "../../" + URL_IMG_UPLOADS + "controladores/";
var URL_SUBIR_IMG_TABLETS = "../../" + URL_IMG_UPLOADS + "tablet/";
//IMAGENES POR DEFECTO
var ICO = URL_IMG + "launcher.png";
var IMG_BANNER = URL_IMG + "login/encabezado.png";
var IMG_LOGO = URL_IMG_SISTEMA + "logo2.png";
var IMG_USUARIO_DEFECTO = URL_IMG + "defecto/usuario.png";
var IMG_USUARIO_DEFECTO_INCOGNITO = URL_IMG + "defecto/usuario_incognito.png";
var IMG_VEHICULO_INCOGNITO = URL_IMG + "defecto/vehiculo_incognito.png";
//MENSAJES PREDETERMINADOS
var E_R_R = 'POR EL MOMENTO NO CUENTA CON CONEXIÓN A INTERNET.';
var CAMPO_INFO_OBLIGATORIO = 'ESTE CAMPO ES OBLIGATORIO';
var MENSAJE_ERROR = 'PROBLEMA EN LA COMUNICACIÓN CON EL SERVIDOR.';
var MENSAJE_ERROR_CREAR = 'EL REGISTRO NO SE HA GUARDADO';
var MENSAJE_SUCCESS_CREAR = 'EL REGISTRO SE HA GUARDADO CORRECTAMENTE';
var MENSAJE_NO_INICIO = 'NO TIENE ASIGNADO UN MODULO, POR FAVOR SOLICITAR UNO AL ADMINISTRADOR DEL SISTEMA';
var MENSAJE_NO_TIENE_ASIGNADO_URL = 'EL MODULO DE INICIO NO TIENE ASIGNADO UNA RUTA DE ARRANQUE';
var MENSAJE_NO_EXISTEN_VEHICULOS_REPORTANDO = 'NO EXISTEN VEHICULOS REPORTANDO';
var MENSAJE_NO_EXISTEN_TRAMAS = 'NO EXISTEN TRAMAS REPORTADAS';
var MENSAJE_CAMPOS_OBLIGATORIOS = 'LOS CAMPOS SON OBLIGATORIOS';

var INFOMESSAGEREQUERID = '<span style="color:red;font-weight:bold" data-qtip="Obligatorio">*</span>';
var INFOMESSAGEBLANKTEXT = 'DEBE INGRESAR UN VALOR';
var MINIMUMMESSAGUEREQUERID = "Este campo requiere un minimo de {0} caracteres";
var MAXIMUMMESSAGUEREQURID = "Este campo acepta un maximo de {0} caracteres";

var URL_REST_SERVICIOS = "http://www.karview.kradac.com:8080";//Consulta datos de registro civil

var EMPTY_CARA = '<center><div class="grid-data-empty"><h3><span style="font-size: 100px;" class="x-fa fa-frown-o" aria-hidden="true"></span><br></h3></div></center>';
//ID MAPAS
var ID_MAPA_LUGARES = 'map_lugares';
var ID_MAPA_VOUCHER = 'map_voucher';
var ID_MAPA_VOUCHER_SUCURSAL = 'map_sucusalvoucher';
var ID_MAPA_VOUCHER_ADMIN = 'map_adminvoucher';
var ID_MAPA_VOUCHER_COMPANIA = 'map_adminvoucher';
var ID_MAPA_COMPANIA = 'map_compania';
var ID_MAPA_SUCURSAL = 'map_sucursal';